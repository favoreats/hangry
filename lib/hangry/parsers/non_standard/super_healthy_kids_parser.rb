module Hangry
  module Parsers
    module NonStandard
      class SuperHealthyKidsParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('superhealthykids.com')
        end

        def parse_author
          author = super
          if !author.present?
            author = nokogiri_doc.css('.single-author').first&.content
          end

          author
        end
      end
    end
  end
end