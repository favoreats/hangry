module Hangry
  module Parsers
    module NonStandard
      class TheGardenGrazerParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('thegardengrazer.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('[itemprop="name"]').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.separator img').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/../..')
            ingredients.children.each{|elem| elem.remove if (elem.content.include?("Vegan") || elem.content.include?("Ingredients"))}
            ingredients = ingredients.children.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n").split("Directions")[0].split("\n")
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/../..')
            instructions = instructions.children.map{ |i| i.content.strip }.uniq.join("\n").split("Directions")[1]
          end

          instructions
        end
      end
    end
  end
end

