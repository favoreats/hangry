module Hangry
  module Parsers
    module NonStandard
      class SaltNTurmericBlogspotParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_includes?('saltnturmeric.blogspot.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.fullpost:nth-of-type(3) span').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.post-body img').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.fullpost:nth-of-type(5)').first&.content&.split("\n")
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.fullpost:nth-of-type(6)').first&.content&.gsub(/^[0-9]./, '')
          end

          instructions
        end
      end
    end
  end
end
