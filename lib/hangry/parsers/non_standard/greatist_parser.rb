module Hangry
  module Parsers
    module NonStandard
      class GreatistParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('greatist.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.title').first&.content
          end

          name
        end

        def parse_ingredients
          ingredients = super
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.article-body-content ul li').map { |i| i.content.split("\n") }.flatten
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/../following-sibling::p[1]').children.map{|i| i.text}
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(text(),"What You\'ll Need:")]/../following-sibling::p[./following::*[contains(text(),"What to Do:")]]').children.map{|i| i.text}
                  if !ingredients.present?
                    ingredients = nokogiri_doc.xpath('//*[(text()="You’ll Need:") or (text()="What You\'ll Need:")]/../following-sibling::p[1]').children.map { |i| i.content.split("\n") }.flatten
                  end
                end
              end

              ingredients = ingredients.reject(&:blank?)
            end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions_elem = nokogiri_doc.css('.recipe-directions li')
            instructions = instructions_elem.map{|instr| instr.text}.reject(&:blank?).uniq.join("\n") if instructions_elem
            if !instructions.present?
              instructions_elem = nokogiri_doc.css('.article-body-content ol li')
              instructions = instructions_elem.map{|instr| instr.text}.reject(&:blank?).uniq.join("\n") if instructions_elem
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[(text()="Directions:") or (contains(text(),"What to Do:"))]/../following-sibling::ol').text
              end
            end
          end

          instructions
        end
      end
    end
  end
end
