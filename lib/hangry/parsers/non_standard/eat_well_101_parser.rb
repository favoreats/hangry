module Hangry
  module Parsers
    module NonStandard
      class EatWell101Parser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('eatwell101.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            if !image_elem.present?
              image_elem = nokogiri_doc.css('img.bzr').first
            end
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients") or contains(text(),"Ingredient")]/following::ul[1]/li[./following::*[(text()="Directions") or (contains(text(),"Instructions"))]] | //*[contains(text(),"marinade:") or contains(text(),"Marinade")]/following::ul[1]/li[./following::*[(text()="Directions") or (contains(text(),"Instructions"))]]')
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[not(em)][./following::*[contains(text(),"You’ll") or contains(text(),"Notes")]]')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[./following::*[@class="brandon black bold"]]')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(text(),"Cooking instructions") or contains(text(),"Instruction")  or contains(text(),"Method")]/following::p[1]')
              end
            end
            instructions = instructions.map { |i| i.content.gsub(/^[0-9]./, '').strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
