module Hangry
  module Parsers
    module NonStandard
      class JustFoodRecipesParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('justfoodrecipes.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredient')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('[itemprop = "ingredients"]')
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/following::p[1]')
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(@class, "entry-content")]/p[3] | //*[contains(@class, "entry-content")]/p[5]')
                end
              end
            end
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.instructions')
            if !instructions.present?
              instructions = nokogiri_doc.css('[itemprop = "recipeInstructions"]')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[./following::*[contains(text(),"Tips")]]')
                instructions.children.each{|elem| elem.remove if elem.content.include?("Directions")}
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[contains(text(),"Related:")]/../following::p[./following::*[contains(text(),"Cook’s Note")]]')
                end
              end
            end
            instructions = instructions.map { |i| i.content.tr("0-9.", "").strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end