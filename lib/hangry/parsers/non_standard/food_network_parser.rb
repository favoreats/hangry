module Hangry
  module Parsers
    module NonStandard
      class FoodNetworkParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('foodnetwork.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('span.o-AssetTitle__a-HeadlineText').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.o-ImageEmbed img').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath("//*[contains(text(),'Ingredients:')]/../following::p[./following::*[(text()='Directions:')]]")
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath("//*[contains(text(),'Directions:')]/../following::p[not(i) and not(a)]")
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
