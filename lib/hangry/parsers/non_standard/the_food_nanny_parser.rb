module Hangry
  module Parsers
    module NonStandard
      class TheFoodNannyParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('thefoodnanny.com')
        end

        def parse_author
          author = nokogiri_doc.css('.author').first&.content

          # It seems each recipe is having author set as "The Food Nanny"
          author = 'The Food Nanny' unless author.present?

          author
        end

        def parse_name
          name = nokogiri_doc.css('.BlogItem-title').first&.content

          name
        end

        def parse_image_url
          image_elem = nokogiri_doc.css('.sqs-block-content p img').first
          image_url = image_elem["src"] if image_elem.present?
          if !image_url.present?
            image_elem = nokogiri_doc.css('.intrinsic img.thumb-image').first

            if image_elem.present?
              image_url = image_elem["src"]
              image_url = image_elem["data-src"] unless image_url.present?
            end
          end

          image_url
        end

        def parse_ingredients
          ingredients = nokogiri_doc.css('ul li').map{|i| i.content.strip }

          ingredients
        end

        def parse_instructions
          instructions = nokogiri_doc.css('ol li').map{|i| i.content.strip }.uniq.join("\n")

          instructions
        end

        def parse_published_date
          date = nokogiri_doc.css('.Blog-meta-item--date').first&.content
          date = date.present? ? Date.parse(date) : nil

          date
        rescue ArgumentError
          nil
        end
      end
    end
  end
end

