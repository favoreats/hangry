module Hangry
  module Parsers
    module NonStandard
      class MoneySavingMomParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('moneysavingmom.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entryTitle').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[@class="print-this-content"]/ul/li')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[@class="pf-content"]/ul[1]/li')
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"adapted")]/following::p[./following::*[contains(text(),"Preheat")]]')
              end
            end
            ingredients = ingredients.children.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"DIRECTIONS:")]/following::p[./following::*[contains(text(),"NOTE")]]')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Method")]/following::p[1]')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[@class="print-this-content"]/ul/li[last()]/following::p[./following::*[contains(text(),"NOTE") or contains(text(),"Note") or contains(text(),"Makes")]]')
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[@class="print-this-content"]/ol/li')
                  if !instructions.present?
                    instructions = nokogiri_doc.xpath('//*[contains(text(),"DIRECTIONS")]/following::ul[1]/li')
                  end
                end
              end
            end
            instructions = instructions.map { |i| i.content.gsub(/^[0-9]./, '').strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
