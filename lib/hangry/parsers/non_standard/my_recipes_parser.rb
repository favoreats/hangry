module Hangry
  module Parsers
    module NonStandard
      class MyRecipesParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('myrecipes.com')
        end

        def parse_name
          name = super
          if !name.present?
            name =  nokogiri_doc.css("h1.headline").first&.content
          end

          name
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredients ul li').map{ |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions_elem  =  nokogiri_doc.css(".recipe-instructions:first-child .step p")
            instructions = instructions_elem.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
