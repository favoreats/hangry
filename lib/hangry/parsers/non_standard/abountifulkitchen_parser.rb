module Hangry
  module Parsers
    module NonStandard
      class ABountifulKitchenParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('abountifulkitchen.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h2.wprm-recipe-name').first&.content
            if !name.present?
              name = nokogiri_doc.css('h1.entry-title').first&.content
            end
          end

          name
        end

        def parse_author
          # => all from her?
          'Si Foster'
        end

        def parse_prep_time
          prep_time = super
          if !prep_time.present?
            prep_time = nokogiri_doc.css('.wprm-recipe-prep_time').first&.content
          end

          prep_time
        end

        def parse_yield
          serving_size = super
          if !serving_size.present?
            serving_size = nokogiri_doc.css('.wprm-recipe-servings').first&.content
          end

          serving_size
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            if !image_elem.present?
              image_elem = nokogiri_doc.css('img.alignleft').first
            end

            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.wprm-recipe-ingredients li').map { |i| i.content.strip }.flatten.reject(&:blank?)
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.cookbook-ingredient-item').map { |i| i.content.strip }.reject(&:blank?)
              if !ingredients.present?
                ingredients = nokogiri_doc.css('div[class="cookbook-summary"] ul li').map { |i| i.content.strip }.reject(&:blank?)
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(text(),"print recipe") or (contains(text(),"Print recipe"))]/../following::p[1]').children.map { |i| i.content.strip }.reject(&:blank?)
                  if !ingredients.present?
                    ingredients = nokogiri_doc.xpath('//i[contains(text(),"A Bountiful Kitchen")]/../following::p[1]').children.map { |i| i.content.strip }.reject(&:blank?)
                    if !ingredients.present?
                      ingredients = nokogiri_doc.xpath('//*[contains(text(),"printable recipe")]/../following::p[1]').children.map { |i| i.content.strip }.reject(&:blank?)
                    end
                  end
                end
              end
            end
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions_elem = nokogiri_doc.css('.wprm-recipe-instructions li')
            instructions = instructions_elem.map { |i| i.content.strip }.uniq.join("\n")
            if !instructions.present?
              instructions_elem = nokogiri_doc.css('.cookbook-instruction-item')
              instructions = instructions_elem.map { |i| i.content.strip }.uniq.join("\n")
              if !instructions.present?
                instructions_elem = nokogiri_doc.css('div[class="cookbook-summary"] ol li')
                instructions = instructions_elem.map { |i| i.content.strip }.uniq.join("\n")
                if !instructions.present?
                  instructions_elem = nokogiri_doc.xpath('//*[contains(text(),"print recipe") or (contains(text(),"Print recipe"))]/../following::p[2] | //*[contains(text(),"print recipe")]/../following::p[2]/following-sibling::div')
                  instructions = instructions_elem.map { |i| i.content.strip }.uniq.join("\n").split('Tips')[0]
                  if !instructions.present?
                    instructions_elem = nokogiri_doc.xpath('//i[contains(text(),"A Bountiful Kitchen")]/../following::p[2]')
                    instructions = instructions_elem.map { |i| i.content.strip }.uniq.join("\n")
                    if !instructions.present?
                      instructions_elem = nokogiri_doc.css('.cookbook-notes')
                      instructions = instructions_elem.map { |i| i.content.strip }.uniq.join("\n")
                      if !instructions.present?
                        instructions_elem = nokogiri_doc.xpath('//*[contains(text(),"printable recipe")]/../following::p[2]')
                        instructions = instructions_elem.map { |i| i.content.strip }.uniq.join("\n")
                      end
                    end
                  end
                end
              end
            end
          end

          instructions
        end
      end
    end
  end
end
