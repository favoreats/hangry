module Hangry
  module Parsers
    module NonStandard
      class AThriftyMomParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('athriftymom.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if image_url.nil?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients") or contains(text(),"Yield") or contains(text(),"need")]/following::ul[1]/li')
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::span[./following::*[contains(text(),"Instructions")]]')
              ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]')
                ingredients.children.each { |elem| elem.remove if elem.content.include?('Ingredients') }
                ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
              end
            end
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]')
            instructions.children.each { |elem| elem.remove if elem.content.include?('Directions') }
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Instructions")]/following::span[./following::*[contains(text(),"cupcakes")]]')
            end
            instructions = instructions.map { |i| i.content.gsub(/^[0-9]./, '').strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
