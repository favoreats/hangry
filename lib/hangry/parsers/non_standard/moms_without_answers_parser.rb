module Hangry
  module Parsers
    module NonStandard
      class MomsWithoutAnswersParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('momswithoutanswers.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            if !image_elem.present?
              image_elem = nokogiri_doc.css('.teaser-image img').first
            end
            image_url = image_elem['src'] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredient').map { |i| i.content.strip }.flatten.reject(&:blank?)
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.component-text').map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
              if !ingredients.present?
                ingredients = nokogiri_doc.css('.component-name p').map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
                if !ingredients.present?
                  ingredients = nokogiri_doc.css('.component-name').map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
                  if !ingredients.present?
                    ingredients = nokogiri_doc.xpath('//*[contains(text(),"Serves")]/following::p[1]').map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
                    if !ingredients.present?
                      ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::p[./following::*[contains(text(),"Directions") or contains(text(),"Instructions")]]').children.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
                      if !ingredients.present?
                        ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::div[./following::*[contains(text(),"Directions") or contains(text(),"Instructions")]]').map { |i| i.content.strip }.flatten.reject(&:blank?)
                        if !ingredients.present?
                          ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::p[./following-sibling::*[contains(text(),"Preheat")]]').map { |i| i.content.strip }.flatten.reject(&:blank?)
                          if !ingredients.present?
                            ingredients = nokogiri_doc.xpath('//*[contains(text(),"You will need")]/following::p[./following::*[contains(text(),"greased")]]').map { |i| i.content.strip }.flatten.reject(&:blank?)
                            if !ingredients.present?
                              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Here")]/following::p[1]').children.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.instruction')
            if !instructions.present?
              instructions = nokogiri_doc.css('.directions-item-text p')
              if !instructions.present?
                instructions = nokogiri_doc.css('.directions-item-text')
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[contains(text(),"Serves")]/following::p[1]/following::p[./following-sibling::*[contains(text(),"Little")]]')
                  if !instructions.present?
                    instructions = nokogiri_doc.xpath('//*[contains(text(),"Instructions") or contains(text(),"Directions")]/following::p[1]')
                    if !instructions.present?
                      instructions = nokogiri_doc.xpath('//*[@class="article-content"]/ol/li')
                      if !instructions.present?
                        instructions = nokogiri_doc.xpath('//*[contains(text(),"Here")]/following::p[3]')
                      end
                    end
                  end
                end
              end
            end
            instructions = instructions.map { |i| i.content.strip }.reject(&:blank?).uniq.join("\n").gsub(/^[0-9]./, '')
          end

          instructions
        end
      end
    end
  end
end
