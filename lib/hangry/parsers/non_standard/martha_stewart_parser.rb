module Hangry
  module Parsers
    module NonStandard
      class MarthaStewartParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('marthastewart.com')
        end

        def parse_name
          name = super
          if !name.present?
            name =  nokogiri_doc.css(".page-title").first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.teaser-image img').first
            if !image_elem.present?
              image_elem = nokogiri_doc.css('.instruction-image-wrap-l img').first
              if !image_elem.present?
                image_elem = nokogiri_doc.css('.top-intro-image-play img').first
              end
            end
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.component-text').map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.component-name p').map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
              if !ingredients.present?
                ingredients = nokogiri_doc.css('.component-name').map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(text(),"Serves")]/following::p[1]').map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
                  if !ingredients.present?
                    ingredients = nokogiri_doc.xpath('//*[@class="article-content"]/h3/following::p[1]').children.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
                  end
                end
              end  
            end
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions  =  nokogiri_doc.css('.directions-item-text p')
            if !instructions.present?
              instructions  =  nokogiri_doc.css('.directions-item-text')
              if !instructions.present?
                instructions =  nokogiri_doc.xpath('//*[contains(text(),"Serves")]/following::p[1]/following::p[./following-sibling::*[contains(text(),"Little")]]')
                if !instructions.present?
                  instructions =  nokogiri_doc.xpath('//*[@class="article-content"]/ol/li')
                end
              end
            end
            instructions = instructions.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n").gsub(/^[0-9]./, '')
          end

          instructions
        end
      end
    end
  end
end
