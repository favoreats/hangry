module Hangry
  module Parsers
    module NonStandard
      class CafedelitesParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('cafedelites.com')
        end

        def parse_instructions
          instructions = nokogiri_doc.xpath('//*[@class="wprm-recipe-instruction-text"]')
          instructions = instructions.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
          if !instructions.present?
            instructions = super
          end

          instructions
        end
      end
    end
  end
end
