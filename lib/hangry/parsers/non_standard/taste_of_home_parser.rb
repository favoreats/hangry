module Hangry
  module Parsers
    module NonStandard
      class TasteOfHomeParser < SchemaOrgRecipeParser
        def self.root_selector
          '[itemtype*="schema.org/recipe"]'
        end

        def can_parse?
          canonical_url_matches_domain?('tasteofhome.com')
        end

        def parse_author
          author = super
          if !author.present?
            author_elem = nokogiri_doc.css('[name="parsely-author"]').first
            author = author_elem['content'] if author_elem
          end

          author
        end

        def parse_name
          name = nokogiri_doc.css('h1.recipe-title').first&.content
          if !name.present?
            name_elem = nokogiri_doc.css('meta[itemprop="name"]').first
            name = name_elem['content'] if name_elem
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.recipe-image-and-meta-sidebar img').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.recipe-ingredients li')
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.recipe-directions__list li')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end

        def parse_yield
          value(node_with_itemprop(:recipeyield).content)
        end
      end
    end
  end
end
