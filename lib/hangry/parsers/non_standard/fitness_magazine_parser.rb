module Hangry
  module Parsers
    module NonStandard
      class FitnessMagazineParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('fitnessmagazine.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.photo-wrapper img').first
            if !image_elem.present?
              image_elem = nokogiri_doc.css('.wg_img_disp_left img').first
            end
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredient')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('//*[@property="content:encoded"]/ul/li')
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[@property="content:encoded"]/p[2]')
                ingredients.children.each{|elem| elem.remove if elem.content.include?("Ingredients")}
              end
            end
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.instructions')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[@property="content:encoded"]/ul/following::p[./following::*[contains(text(),"Make")]]')
              instructions.children.each{|elem| elem.remove if elem.content.include?("Directions")}
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[@property="content:encoded"]/ol/li')
              end
            end
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n").gsub(/^[0-9]./, '')
          end

          instructions
        end
      end
    end
  end
end
