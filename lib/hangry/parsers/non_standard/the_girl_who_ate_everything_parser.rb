module Hangry
  module Parsers
    module NonStandard
      class TheGirlWhoAteEverythingParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('the-girl-who-ate-everything.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css("h1.entry-title").first&.content
          end

          name
        end
      end
    end
  end
end
