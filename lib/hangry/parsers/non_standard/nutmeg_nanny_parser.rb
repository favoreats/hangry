module Hangry
  module Parsers
    module NonStandard
      class NutmegNannyParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('nutmegnanny.com')
        end

        def parse_ingredients
          ingredients = nokogiri_doc.css('.ingredients ul li').map{ |i| i.content.strip }.reject(&:blank?)
          if !ingredients.present?
            ingredients = super
          end

          ingredients
        end
      end
    end
  end
end
