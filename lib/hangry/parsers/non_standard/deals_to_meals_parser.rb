module Hangry
  module Parsers
    module NonStandard
      class DealsToMealsParser < SchemaOrgRecipeParser
        def can_parse?
          node = nokogiri_doc.css('meta[name="shareaholic:url"]').first

          host = URI.parse(node['content']).host if !node.nil?

          return host == "www.dealstomealsblog.com"
        end

        def parse_author
          author = nokogiri_doc.css('.author-link').first.content

          author = super if author.nil?

          author
        rescue NoMethodError
          nil
        end
      end
    end
  end
end
