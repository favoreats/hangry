module Hangry
  module Parsers
    module NonStandard
      class BakersRoyaleParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('bakersroyale.com')
        end

        def parse_name
          name = super
          unless name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if image_url.nil?
            image_elem = nokogiri_doc.css('.size-full').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath("//*[contains(text(), 'Ingredients')]/following-sibling::ul")
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath("//*[contains(text(), 'Instructions')]/../following-sibling::ol")
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
