module Hangry
  module Parsers
    module NonStandard
      class SavyNaturalistaParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_includes?('savynaturalista.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[text()="Molasses Pear Butter Bread Recipe:"]/following::p[./following::*[contains(text(),"Directions")]]')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.entry-content p:nth-of-type(11)')
            end
            ingredients = ingredients.uniq.join("\n").split("Recipe Directions")[0].split("\n").flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n").split("Directions:")[1]
          end

          instructions
        end
      end
    end
  end
end
