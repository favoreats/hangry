module Hangry
  module Parsers
    module NonStandard
      class KayoticParser < HRecipeParser
        def can_parse?
          canonical_url_matches_domain?('kayotic.nl')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h2.title a').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.article img').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/../following-sibling::p[1]')
          ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          if !ingredients.present?
            ingredients = super
          end

          ingredients
        end

        def parse_instructions
          instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/../following-sibling::p').text
          if !instructions.present?
            instructions = super
          end

          instructions
        end
      end
    end
  end
end