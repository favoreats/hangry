module Hangry
  module Parsers
    module NonStandard
      class PopCultureParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('popculture.com')
        end

        def parse_name
          name = nokogiri_doc.css('#title0').first&.content
          name = name.sub(/Recipe:/, '').strip if name.present?
          if !name.present?
            name = super
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            if !image_elem.present?
              image_elem = nokogiri_doc.css('.aligncenter img').first
            end
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.zlrecipe ul li')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.layoutArea ul li')
              if !ingredients.present?
                ingredients =  nokogiri_doc.css('[itemprop="articleBody"] ul li')
              end
            end
            ingredients = ingredients.map { |i| i.content.strip }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.zlrecipe ol li')
            if !instructions.present?
              instructions = nokogiri_doc.css('.layoutArea ol li')
              if !instructions.present?
                instructions = nokogiri_doc.css('[itemprop="articleBody"] ol li')
              end
            end
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
