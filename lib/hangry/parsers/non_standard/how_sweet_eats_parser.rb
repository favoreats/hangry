module Hangry
  module Parsers
    module NonStandard
      class HowSweetEatsParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('howsweeteats.com')
        end

        def parse_author
          meta_info = nokogiri_doc.css(".post-meta").first&.content
          author = meta_info.split(' ')[1] if meta_info

          author = super if author.nil?

          author
        end

        def parse_name
          name = super
          if !name.present?
            name =  nokogiri_doc.xpath("//title").first&.content
          end

          name
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('div[class="ingredients"]').map{|i| i.text.strip}
            ingredients = ingredients.map{|ingredient| ingredient.split("\n")}.first
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"serves")]/following::p[./following::*[contains(text(),"Preheat")]]')
              ingredients = ingredients.map{|i| i.content.strip }.reject(&:blank?)
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"makes")]/following::p[./following::*[contains(text(),"saucepan") or contains(text(),"Preheat")]]')
                ingredients = ingredients.map{|i| i.content.strip }.reject(&:blank?)
              end
            end
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"crackers")]/following::p[./following::*[contains(text(),"The end")]]')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"makes")]/following::p[./following::*[contains(text(),"Preheat")]][last()]/following::p[./following::*[contains(text(),"And")]]')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(text(),"makes")]/following::p[./following::*[contains(text(),"saucepan") or contains(text(),"Preheat")]][last()]/following::p[./following::*[contains(text(),"Note")]]')
              end
            end
            instructions = instructions.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end

        def parse_image_url
          image_url = super
          unless image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end
      end
    end
  end
end