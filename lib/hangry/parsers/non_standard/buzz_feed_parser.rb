module Hangry
  module Parsers
    module NonStandard
      class BuzzFeedParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('buzzfeed.com')
        end

        def parse_name
          name = super
          unless name.present?
            name = nokogiri_doc.css('.js-subbuzz__title-text').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if image_url.nil?
            image_elem = nokogiri_doc.css('img.subbuzz__media-image').first
            image_url = image_elem['data-src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/../following::p[./following::*[(text()="PREPARATION")]]')
            ingredients.children.each{|elem| elem.remove if elem.content.include?("Serves")}
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"PREPARATION")]/../following::p[./following::*[(@data-module="action-bar-subbuzz")]]')
            instructions = instructions.map { |i| i.content.gsub(/^[0-9]./, '').strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
