module Hangry
  module Parsers
    module NonStandard
      class TwoPeasAndTheirPodParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('twopeasandtheirpod.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.post-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = nokogiri_doc.css('.ingredient').map { |i| i.content }
          if !ingredients.present?
            ingredients = nokogiri_doc.css('div[class="ingredients"]').map { |i| i.text }
            ingredients = ingredients.map { |ingredient| ingredient.split("\n") }.first.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.instructions')
            instructions = instructions.children.map { |instr| instr.content.strip }.reject(&:blank?).uniq.join("\n").gsub(/^[0-9]./, '')
          end

          instructions
        end
      end
    end
  end
end
