module Hangry
  module Parsers
    module NonStandard
      class DamnDeliciousParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('damndelicious.net')
        end

        def parse_image_url
          image_elem = nokogiri_doc.css('[class*="wp-image"]')[1]
          image_url = image_elem['src'] if image_elem
          if !image_elem.present?
            image_url = super
          end

          image_url
        end
      end
    end
  end
end
