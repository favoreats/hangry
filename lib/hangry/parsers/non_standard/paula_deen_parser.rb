module Hangry
  module Parsers
    module NonStandard
      class PaulaDeenParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('pauladeen.com')
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('img.img-fluid').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.directions__content p')
            instructions = instructions.map { |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
