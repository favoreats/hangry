module Hangry
  module Parsers
    module NonStandard
      class WeeliciousParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('weelicious.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.row h1').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
            if !image_url.present?
              image_elem = nokogiri_doc.css('.image img').first
              image_url = 'https://weelicious.com' + image_elem['src'] if image_elem
            end
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::ul[1]/li')
            ingredients = ingredients.map { |i| i.content.strip }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Preparation")]/following::p[./following::*[contains(@class,"instafollow")]]')
            instructions = instructions.children.map { |i| i.content.gsub(/^[0-9]./, '').strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
