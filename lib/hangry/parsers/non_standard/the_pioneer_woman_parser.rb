module Hangry
  module Parsers
    module NonStandard
      class ThePioneerWomanParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('thepioneerwoman.com')
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.recipe-summary-thumbnail img').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end
      end
    end
  end
end