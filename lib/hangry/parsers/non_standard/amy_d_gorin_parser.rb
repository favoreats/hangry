module Hangry
  module Parsers
    module NonStandard
      class AmyDGorinParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('amydgorin.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.j-blog-header').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if image_url.nil?
            image_elem = nokogiri_doc.css('.cc-imagewrapper.cc-m-image-align-1 img').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::li[./following::*[contains(text(),"Directions")]]')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::p[./following::*[contains(text(),"Directions")]]')
            end
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[./following::*[contains(text(),"Nutritional")]]')
            instructions = instructions.map { |i| i.content.gsub(/^[0-9]./, '').strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
