module Hangry
  module Parsers
    module NonStandard
      class CouponsAndFreebiesMomParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('couponsandfreebiesmom.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.post_title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/following::ul/li[./following::*[contains(text(),"DIRECTIONS") or contains(text(),"Preparation")]]')
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients") or contains(text(),"INGREDIENTS")]/following::p[./following::*[contains(text(),"Directions") or contains(text(),"DIRECTIONS")]]')
              ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/../..')
                ingredients.children.each { |elem| elem.remove if elem.content.include?('INGREDIENTS') }
                ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
              end
            end
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"DIRECTIONS")]/following::ul[1]/li')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
