module Hangry
  module Parsers
    module NonStandard
      class PenniesIntoPearlsParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('penniesintopearls.com')
        end

        def parse_name
          name = nokogiri_doc.css(".post-header h1").first&.content

          name
        end

        def parse_author
          author = nokogiri_doc.css("a[rel='author']").first&.content

          author
        end

        def parse_image_url
          image_elem = nokogiri_doc.css('[class*="wp-image"]').first
          image_url = image_elem["src"] if image_elem

          image_url
        end

        def parse_ingredients
          ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/..').first&.content
          ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients") or contains(text(),"All you need:")]').first&.content unless ingredients

          ["Ingredients\n", "All you need:\n"].each {|replacement| ingredients.gsub!(replacement, '')} if ingredients
          ingredients = ingredients.split("\n")

          ingredients
        end

        def parse_instructions
          instructions = nokogiri_doc.xpath('//*[contains(text(),"All you need:") or contains(text(),"Ingredients")]//../following-sibling::p[not(img)]')
          instructions.search('//*[text()="Instructions"]').remove
          instructions = instructions.to_a.delete_if{ |instr| instr.children.count == 1 && instr.search('a').present? }
          instructions = instructions.map{|instr| instr.content.strip }.reject(&:blank?).join("\n")

          instructions
        end
      end
    end
  end
end
