module Hangry
  module Parsers
    module NonStandard
      class FoxAndBriarParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('foxandbriar.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.cookbook-ingredient-item')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('[itemprop="ingredients"]')
            end
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.cookbook-instruction-item')
            if !instructions.present?
              instructions = nokogiri_doc.css('[itemprop="recipeInstructions"]')
            end
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
