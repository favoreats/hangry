module Hangry
  module Parsers
    module NonStandard
      class TheFrugalGirlsParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('thefrugalgirls.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if image_url.nil?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[@class="entry-content"]/ul[2]/li')
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[@class="entry-content"]/ul[3]/li')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[@class="entry-content"]/ol/li')
            end
            instructions = instructions.map { |i| i.content.gsub(/^[0-9]./, '').strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
