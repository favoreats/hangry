module Hangry
  module Parsers
    module NonStandard
      class EatLiveRunParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('eatliverun.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.post-header h1').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.wprm-recipe-ingredients li')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients:")]/following::p[./following::*[contains(text(),"Directions")]]')
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"Print this recipe!")]/following::p[./following::*[contains(text(),"Rub")]]')
              end
            end
            ingredients = ingredients.map { |i| i.content.strip }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.wprm-recipe-instructions li')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[./following::*[(text()="Time:")]]')
            end
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
