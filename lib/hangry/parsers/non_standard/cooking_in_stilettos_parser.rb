module Hangry
  module Parsers
    module NonStandard
      class CookingInStilettosParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('cookinginstilettos.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.aligncenter').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients_elem = nokogiri_doc.css('.tasty-recipe-ingredients ul ul li')
            ingredients = ingredients_elem.map{ |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.tasty-recipe-instructions ol ol li')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end