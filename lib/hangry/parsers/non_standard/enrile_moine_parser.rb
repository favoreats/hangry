module Hangry
  module Parsers
    module NonStandard
      class EnrileMoineParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('enrilemoine.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_url = nokogiri_doc.css('[property="og:image:secure_url"]').first.values[1]
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Makes")]/following::div[1]')
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Preparation")]/following::div[1]')
            instructions = instructions.map { |i| i.content.gsub(/^[0-9]./, '').strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
