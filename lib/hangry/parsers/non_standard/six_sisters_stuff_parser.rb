module Hangry
  module Parsers
    module NonStandard
      class SixSistersStuffParser < JsonLDParser
        def can_parse?
          logo_node = node_with_class('logo-wrap')

          host = URI.parse(logo_node['href']).host

          return host == 'www.sixsistersstuff.com'
        end

        def parse_yield
          serving_size = super
          if !serving_size.present?
            serving_size = nokogiri_doc.css('[itemprop="servingSize"]').first&.content || NullObject.new
          end

          serving_size
        end

        def parse_name
          name = super
          name = nokogiri_doc.css('h1.entry-title').first&.content if !name.present?

          name
        end

        def parse_image_url
          image_elem = nokogiri_doc.css('.wp-post-image').first
          image_url = image_elem['src'] if image_elem
          if !image_url.present?
            image_url = super
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if invalid_ingredients(ingredients)
            ingredients = nokogiri_doc.xpath("//*[text()='Ingredients:']/ancestor::p[1]")
            if invalid_ingredients(ingredients)
              ingredients = nokogiri_doc.xpath("//*[contains(text(),'Ingredients:')]/../following::*[./following::*[text()='Directions:']]")
              ingredients = ingredients.map { |i| i.text.strip } if ingredients.present?
              if invalid_ingredients(ingredients)
                ingredients = nokogiri_doc.xpath("//*[contains(text(),'Ingredients:')]/..")
                if invalid_ingredients(ingredients)
                  ingredients = nokogiri_doc.xpath("//*[contains(text(),'Recipe adapted')]/following::p[./following::*[contains(text(),'Preheat')]]")
                  if invalid_ingredients(ingredients)
                    ingredients = nokogiri_doc.css("//*[contains(@id,'mpprecipe-ingredient-')]")
                  end
                end
              end
            end

            if !ingredients.is_a?(Array) && ingredients.text.present?
              ingredients.search('br').each { |el| el.replace("\n") }
              ingredients = ingredients.text.split('Ingredients:').last
              ingredients = ingredients.split('Directions:').first

              ingredients = ingredients.split("\n")
            end
          end

          ingredients.reject(&:blank?)
        end

        def invalid_ingredients(ingredients)
          !ingredients.present? || ingredients == 'Ingredients:'
        end

        def parse_instructions
          instructions = super
          if invalid_instructions(instructions)
            instructions = nokogiri_doc.xpath("//*[text()='Directions:']/ancestor::p[1]")
            if invalid_instructions(instructions)
              instructions = nokogiri_doc.css('.instructions')
              if invalid_instructions(instructions)
                instructions = nokogiri_doc.xpath("//*[text()='Directions:']/ancestor::p/following-sibling::p[not(descendant::strong) and not(em)]")
                if invalid_instructions(instructions)
                  instructions = nokogiri_doc.xpath("//*[contains(text(),'Directions')]/../following-sibling::div[not(contains(@style,'text-align: center'))]")
                  if invalid_instructions(instructions)
                    instructions = nokogiri_doc.xpath("//*[contains(text(),'Recipe adapted')]/following::p[./following::*[contains(text(),'Preheat')]][last()]/following::p[./following::*[contains(text(),'***')]]")
                  end
                end
              end
            end
            instructions.children.each { |elem| elem.remove if elem.content.include?('Directions:') }
            instructions = instructions.map { |instr| instr.text }.reject(&:blank?).uniq.join("\n").delete('-')
          end

          instructions
        end

        def invalid_instructions(instructions)
          !instructions.present?
        end

        def parse_published_date
          date = super
          if !date.present?
            date = nokogiri_doc.css('.post-meta .updated').first&.content
          end

          date
        end
      end
    end
  end
end
