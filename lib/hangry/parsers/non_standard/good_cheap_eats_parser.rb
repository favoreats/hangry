module Hangry
  module Parsers
    module NonStandard
      class GoodCheapEatsParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('goodcheapeats.com')
        end

        def parse_author
          author = super
          if !author.present?
            author = nokogiri_doc.css('.author span').first&.content
          end

          author
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.print-this-content ol li').map{|i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end

        def parse_published_date
          date = super
          if !date.present?
            date = nokogiri_doc.css('.date.published.time').first&.content
          end

          date
        end
      end
    end
  end
end
