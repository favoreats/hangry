module Hangry
  module Parsers
    module NonStandard
      class SweetLittleBluebirdParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('sweetlittlebluebird.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css("h1.entry-title").first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.separator img').first
            if !image_elem.present?
              image_elem = nokogiri_doc.css('.aligncenter').first
            end
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/../following::ul[./following::*[contains(text(),"Directions")]]')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/../following::div[./following::*[contains(text(),"Directions")]]')
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/../following::ul[./following::*[contains(text(),"DIRECTIONS")]]')
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/../following::ul[./following::*[contains(text(),"Directions")]]')
                  if !ingredients.present?
                    ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/../following::ul[./following::*[contains(text(),"DIRECTIONS")]]')
                    if !ingredients.present?
                      ingredients = nokogiri_doc.xpath('//*[contains(text(), "Ingredients")]/..')
                      ingredients.children.each{|elem| elem.remove if elem.content.include?("Ingredients")}
                    end
                  end
                end
              end
            end
            ingredients = ingredients.children.map{ |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions_elems = nokogiri_doc.xpath('//*[contains(text(), "Directions")]/../following::p[./following::p[strong]]')
            elem = instructions_elems.search('strong').first&.content
            instructions = instructions_elems.map{ |i| i.content.strip }&.reject(&:blank?)&.uniq&.join("\n")
            instructions = instructions.split(/#{elem}/)[0] if instructions
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(), "Directions")]/../following-sibling::div[1]')
              instructions = instructions.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(text(), "Directions")]/../../following::p[1]/span')
                instructions = instructions.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[contains(text(), "Directions")]/../following::span[1]').first&.content
                  if !instructions.present?
                    instructions = nokogiri_doc.xpath('//*[contains(text(), "DIRECTIONS")]/../following::span[1]').first&.content
                  end
                end
              end
            end
          end

          instructions
        end
      end
    end
  end
end

