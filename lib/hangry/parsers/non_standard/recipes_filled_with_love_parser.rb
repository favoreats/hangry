module Hangry
  module Parsers
    module NonStandard
      class RecipesFilledWithLoveParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_includes?('recipesfilledwithlove.blogspot.com')
        end
        
        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('b:nth-child(2) span').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.separator img').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients:")]/following::span[./following::*[(text()="Instructions:")]]')
            ingredients.children.each{|elem| elem.remove if (elem.content.include?("Base:") || elem.content.include?("Topping:")) }
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions_elem = nokogiri_doc.xpath('//*[contains(text(),"Instructions:")]/following::span[./following::*[contains(text(),"Posted")]]')
            instructions = instructions_elem.children.map{|instr| instr.text}.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end