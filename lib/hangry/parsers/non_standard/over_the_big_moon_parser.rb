module Hangry
  module Parsers
    module NonStandard
      class OverTheBigMoonParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('overthebigmoon.com')
        end

        def parse_name
          name = super
          if !name.present?
            name =  nokogiri_doc.css(".uk-article h1").first&.content
          end

          name
        end

        def parse_published_date
          date = super
          if !date.present?
            date = nokogiri_doc.css("time").first&.content
          end

          date
        end

        def parse_author
          author = super
          if !author.present?
            author_elem = nokogiri_doc.css('meta[property="author"]').first
            author = author_elem['content'] if author_elem
          end

          author
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.aligncenter').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"You need")]').first&.content
            ingredients = ingredients&.delete("You need:")
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::*[./following::*[(text()="Directions:") or (contains(text(),"Instructions"))]]').first&.content
            end
            ingredients = ingredients&.split("\n")&.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"You need")]/following-sibling::p')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[(text()="Directions:")]/../../following-sibling::p')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[(text()="Directions:") or (contains(text(),"Instructions"))]/following-sibling::ol')
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[(text()="Directions:") or (contains(text(),"Instructions"))]/following-sibling::p')
                end
              end
            end
            instructions.children.each{|elem| elem.remove if (elem.content.include?("Directions:") || elem.content.include?("Ingredients:")) }
            instructions = instructions.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end