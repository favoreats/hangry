module Hangry
  module Parsers
    module NonStandard
      class MyMommyStyleParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('mymommystyle.com')
        end

        def parse_name
          name = super
          if !name.present?
            name =  nokogiri_doc.css("h1.entry-title").first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.aligncenter').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(@class,"recipePartIngredient")]')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(@class,"article-content")]//ul/li')
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"serves")]/following::p[1]')
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(@class,"ingredient")]')
                  if !ingredients.present?
                    ingredients = nokogiri_doc.xpath('//*[contains(text(),"You will need")]/following::p[./following::*[contains(text(),"Directions")]]')
                    if !ingredients.present?
                      ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients:")]/following::p[./following::*[contains(text(),"Directions")]]')
                      if !ingredients.present?
                        ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients:")]/following::p[./following-sibling::*[contains(text(),"1.")]]')
                        if !ingredients.present?
                          ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::div[./following::*[contains(text(),"Directions")]]')
                          if !ingredients.present?
                            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/../following::p[1]')
                            if !ingredients.present?
                              ingredients = nokogiri_doc.xpath('//*[contains(text(),"What you need")]/following::p[./following::*[contains(text(),"How to make")]]')
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end
            ingredients = ingredients.children.map{ |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.Directions ul li')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(@class,"article-content")]//ol/li')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(@class,"instruction")]')
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[contains(@class,"rd_ingredient")]')
                  if !instructions.present?
                    instructions = nokogiri_doc.xpath('//*[contains(@class,"liststyle")]/li')
                    if !instructions.present?
                      instructions = nokogiri_doc.xpath('//*[contains(text(),"DIRECTIONS")]/following::p[./following::*[contains(text(),"Enjoy")]]')
                      if !instructions.present?
                        instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[not(a)][./following::*[contains(@class,"adjacent")]]')
                        if !instructions.present?
                          instructions = nokogiri_doc.xpath('//*[contains(text(),"How to make")]/following::p[./following::*[contains(@class,"adjacent-posts-links")]]')
                          if !instructions.present?
                            instructions = nokogiri_doc.xpath('//*[contains(@class,"article-content")]/ul[last()]/following::p[./following::*[contains(text(),"If")]]')
                            if !instructions.present?
                              instructions = nokogiri_doc.xpath('//*[contains(text(),"Ingredients:")]/following::p[./following-sibling::*[contains(text(),"1.")]][last()]/following::p[./following-sibling::*[contains(text(),"Note")]]')
                              if !instructions.present?
                                instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[1]')
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end
            instructions = instructions.map{ |i| i.content.gsub(/^[0-9]./, '').strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
