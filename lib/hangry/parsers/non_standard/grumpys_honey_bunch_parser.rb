module Hangry
  module Parsers
    module NonStandard
      class GrumpysHoneyBunchParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('grumpyshoneybunch.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.aligncenter').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredients li').map{ |i| i.content.strip }
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.instructions').first&.content
          end

          instructions
        end
      end
    end
  end
end