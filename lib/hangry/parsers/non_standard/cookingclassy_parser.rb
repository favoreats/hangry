module Hangry
  module Parsers
    module NonStandard
      class CookingClassyParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('cookingclassy.com')
        end

        def parse_yield
          yields = super
          if yields.is_a? NullObject
            yields = nokogiri_doc.css('p:contains("Yield:")').first.content.strip.sub(/Yield: /, '')
          end

          yields
        rescue NoMethodError
          nil
        end
      end
    end
  end
end
