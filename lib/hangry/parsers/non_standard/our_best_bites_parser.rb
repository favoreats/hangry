module Hangry
  module Parsers
    module NonStandard
      class OurBestBitesParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('ourbestbites.com')
        end

        def parse_name
          nokogiri_doc.css(".entry-title").first&.content
        end

        def parse_author
          author = nokogiri_doc.css("a[rel='author']").first&.content
        end

        def parse_image_url
          image = nokogiri_doc.css('[class*="wp-image"]').first
          image_url = image['src'] if image

          image_url
        end

        def parse_ingredients
          ingredients = nokogiri_doc.xpath("//*[contains(text(), 'Ingredients')]/../following-sibling::p[1]")
          if invalid_ingredients(ingredients)
            ingredients = nokogiri_doc.xpath("//*[contains(text(), 'Ingredients')]/following-sibling::p[1]")
            if invalid_ingredients(ingredients)
              ingredients = nokogiri_doc.xpath("//*[contains(text(), 'Ingredients')]")
              if invalid_ingredients(ingredients)
                ingredients = nokogiri_doc.xpath("//*[contains(text(), 'Ingredients')]/..")
                if invalid_ingredients(ingredients)
                  ingredients = nokogiri_doc.css(".print-this-content > p:nth-child(2)")
                  if invalid_ingredients(ingredients)
                    ingredients = nokogiri_doc.xpath("//*[text()='Ingredients:']/../following-sibling::div")
                  end
                end
              end
            end
          end

          ingredients.children.each{|elem| elem.remove if elem.content.include?("Ingredients")}
          ingredients.search('br').each { |el| el.replace("\n") }
          ingredients = ingredients.text.split("\n")

          ingredients.reject(&:blank?)
        end

        def invalid_ingredients(ingredients)
          (ingredients.present? && !ingredients.text.present?) ||
              ingredients.text.include?("Instructions") ||
              ingredients.text == "Ingredients" || ingredients.text == "Ingredients:"
          !ingredients.present?
        end

        def parse_instructions
          instructions = nokogiri_doc.css(".recipe-instructions ol li")
          if !instructions.present?
            instructions = nokogiri_doc.xpath("//*[text()='Instructions:']/following-sibling::p")
            if !instructions.present?
              instructions = nokogiri_doc.xpath("//*[text()='Instructions:']/../following-sibling::p")
              if !instructions.present?
                instructions = nokogiri_doc.xpath("//*[text()='Instructions']/../following-sibling::p")
                if !instructions.present?
                  instructions = nokogiri_doc.xpath("//*[text()='Instructions']")
                  instructions.children.each{|elem| elem.remove if (elem.content.include?("Instructions"))}
                  if !instructions.present?
                    instructions = nokogiri_doc.xpath("//*[text()='Instructions:']/../following-sibling::div")
                  end
                end
              end
            end
          end

          instructions.map{|instr| instr.text}.reject(&:blank?).uniq.join("\n")
        end
      end
    end
  end
end