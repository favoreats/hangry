module Hangry
  module Parsers
    module NonStandard
      class OhSweetBasilParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('ohsweetbasil.com')
        end

        def parse_author
          author = nokogiri_doc.css('.author-text h3').first&.content

          author = super unless author.present?

          author
        end

        def parse_ingredients
          ingredients = super

          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredients').map{|i| i.text.strip}
            ingredients = ingredients.map{|ingredient| ingredient.split("\n")}.first.reject(&:blank?)
          end

          ingredients
        end

        def parse_cook_time
          cook_time = super
          if !cook_time.present? || cook_time.zero?
            cook_time = nokogiri_doc.xpath('//*[@itemprop="cookTime"]/..').first&.content
            cook_time = cook_time&.split(':')&.last&.strip
          end

          cook_time
        end

        def parse_prep_time
          prep_time = super
          if !prep_time.present? || prep_time.zero?
            prep_time = nokogiri_doc.xpath('//*[@itemprop="prepTime"]/..').first&.content
            prep_time = prep_time.split(':')&.last&.strip
          end

          prep_time
        end

        def parse_total_time
          total_time = super
          if !total_time.present? || total_time.zero?
            total_time = nokogiri_doc.xpath('//*[@itemprop="totalTime"]/..').first&.content
            total_time = total_time.split(':')&.last&.strip
          end

          total_time
        end
      end
    end
  end
end

