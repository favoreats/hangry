module Hangry
  module Parsers
    module NonStandard
      class MyTableForEightParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_includes?('mytableforeight.blogspot.com')
        end

        def parse_name
          name = super
          if !name.present?
            name =  nokogiri_doc.css(".entry-title").first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.separator img').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients:")]/../following::div[./following::*[(text()="Preparation:")]]')
            ingredients = ingredients.map{ |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions_elem  =  nokogiri_doc.xpath('//*[contains(text(),"Preparation:")]/../following::div[2]')
            instructions = instructions_elem.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
