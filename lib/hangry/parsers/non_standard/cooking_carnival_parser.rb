module Hangry
  module Parsers
    module NonStandard
      class CookingCarnivalParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('cookingcarnival.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1[itemprop = "headline"]').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath("//*[contains(text(),'Ingredients') or contains(text(),'Ingredient')]/following::p[./following::*[contains(text(),'Direction')]]")
            ingredients = ingredients.map { |i| i.content.delete('–').strip }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath("//*[contains(text(),'Direction')]/following::p[not(img)][./following::*[@class='entry-meta']] ")
            instructions = instructions.map { |i| i.content.gsub(/^[0-9]./, '').strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
