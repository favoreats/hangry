module Hangry
  module Parsers
    module NonStandard
      class SkinnyTasteParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('skinnytaste.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.post-title h1').first&.content
          end

          name
        end

        def parse_author
          author = super
          if !author.present?
            author = nokogiri_doc.css('.post-meta').first&.content&.split('by')[1].strip
          end

          author
        rescue NoMethodError
          nil
        end

        def parse_image_url
          image_url = super
          if image_url.is_a? NullObject
            image_elem = nokogiri_doc.xpath('//*[@class="post-title"]/following-sibling::*/a/img').first
            image_url = image_elem.present? ? image_elem['src'] : nil
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.post .ingredients ul li')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.post ul li')
            end
            ingredients = ingredients.map{ |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions:")]/../following-sibling::p')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions:")]/following-sibling::p')
            end
            instructions = instructions.map{|i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end

        def parse_published_date
          date = super
          if !date.present?
            date = nokogiri_doc.css('.post-meta strong:nth-child(2)').first&.content

            date = date.present? ? Date.parse(date) : nil
          end

          date
        rescue ArgumentError
          nil
        end
      end
    end
  end
end
