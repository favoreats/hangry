module Hangry
  module Parsers
    module NonStandard
      class SmartSchoolHouseParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('smartschoolhouse.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredients li')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('div.entry-content ul li')
            end
            ingredients = ingredients.children.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.directions li')
            if !instructions.present?
              instructions = nokogiri_doc.css('div.entry-content ol li')
            end
            instructions = instructions.children.map { |instr| instr.content.strip }.reject(&:blank?).uniq.join("\n").gsub(/^[0-9]./, '')
          end

          instructions
        end
      end
    end
  end
end
