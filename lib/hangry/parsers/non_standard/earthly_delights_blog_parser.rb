module Hangry
  module Parsers
    module NonStandard
      class EarthlyDelightsBlogParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('earthlydelightsblog.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.singlepost h2').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.aligncenter').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.singlepost p:nth-of-type(6)').first&.content
            ingredients = ingredients&.split("\n")&.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.singlepost p:nth-of-type(7)').first&.content
          end

          instructions
        end
      end
    end
  end
end