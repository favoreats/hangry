module Hangry
  module Parsers
    module NonStandard
      class TheSweetChickParser < HRecipeParser
        def can_parse?
          canonical_url_matches_domain?('thesweetchick.com')
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.instruction')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
