module Hangry
  module Parsers
    module NonStandard
      class MenuMUsingsBlogspotParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_includes?('menumusings.blogspot.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.post-body img').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('[itemprop="articleBody"] ul li')
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end
      end
    end
  end
end
