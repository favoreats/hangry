module Hangry
  module Parsers
    module NonStandard
      class ATastyLoveStoryParser < SchemaOrgRecipeParser

        def can_parse?
          canonical_url_matches_domain?('atastylovestory.com')
        end

        def parse_name
          name = nokogiri_doc.css('h2.entry-title').first&.content

          name
        end

        def parse_image_url
          image_elem = nokogiri_doc.css('.post-img img')[1]
          image_url = image_elem["src"] if image_elem.present?

          image_url
        end

        def parse_ingredients
          doc = nokogiri_doc.dup
          ingredients = doc.xpath('//*[@class="pf-content"]')
          ingredients.search('p').each { |el| el.remove }
          ingredients = ingredients.first&.content&.strip&.split("\n")

          ingredients
        end

        def parse_instructions
          instructions_elems = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/../..')
          instructions_elems.children.each{|elem| elem.remove if (elem.content.include?("Directions")) }
          instructions = instructions_elems.map{ |i| i.content.tr("0-9.", "").strip }.reject(&:blank?).uniq.join("\n")

          instructions
        end
      end
    end
  end
end
