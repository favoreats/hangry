module Hangry
  module Parsers
    module NonStandard
      class FoodAndWineParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('foodandwine.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.headline').first&.content
          end

          name
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredients li')
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('[itemprop="recipeInstructions"] p')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end

        def parse_prep_time
          prep_time = super
          if !prep_time.present?
            prep_time = nokogiri_doc.xpath('//*[@class="recipe-meta-item"][1]/div[2]').first&.content
          end

          prep_time
        end

        def parse_total_time
          total_time = super
          if !total_time.present?
            total_time = nokogiri_doc.xpath('//*[@class="recipe-meta-item"][2]/div[2]').first&.content
          end

          total_time
        end
      end
    end
  end
end