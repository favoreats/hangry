module Hangry
  module Parsers
    module NonStandard
      class ASliceOfStyleParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('asliceofstyle.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.separator img').first
            if !image_elem.present?
              image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            end
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredient')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.ERSIngredients .ERSIngredientsHeader')
              ingredients.children.each{|elem| elem.remove if elem.content.include?("Ingredients")}
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::span//center[./following::*[contains(text(),"Directions")]]')
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::span[./following::*[(text()="Directions") or (contains(text(),"Instructions"))]]')
                  if !ingredients.present?
                    ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/../following::div[./following::*[contains(text(),"Directions")]]')
                    if !ingredients.present?
                      ingredients = nokogiri_doc.css('.entry-content ul:nth-of-type(1) li')
                    end
                  end
                end
              end
            end
            ingredients = ingredients.map{|i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.instruction')
            if !instructions.present?
              instructions = nokogiri_doc.css('.ERSInstructions .ERSInstructionsHeader')
              instructions.children.each{|elem| elem.remove if elem.content.include?("Directions")}
              if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::span//span[./following::*[contains(text(),"Note")]]')
                  if !instructions.present?
                    instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::span[./following::*[contains(text(),"Note")]]')
                    if !instructions.present?
                      instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/../following::div[./following::*[contains(text(),"Posted")]]')
                      if !instructions.present?
                        instructions = nokogiri_doc.xpath('//*[(text()="Directions") or (contains(text(),"Instructions"))]/following::span[./following::*[contains(text(),"Posted")]]')
                        if !instructions.present?
                          instructions = nokogiri_doc.css('.entry-content ul:nth-of-type(2) li')
                        end
                      end
                    end
                  end
                end
              end
            instructions = instructions.map{ |i| i.content.gsub(/^[0-9]./, '').strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
