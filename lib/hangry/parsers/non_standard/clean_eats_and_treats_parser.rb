module Hangry
  module Parsers
    module NonStandard
      class CleanEatsAndTreats < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('cleaneatsandtreats.com')
        end

        def parse_name
          name = nokogiri_doc.css('title').first.content.strip.sub(/\s\|\sClean\sEats\s&\sTreats/, '')
          name = super if name.blank?

          name
        rescue NoMethodError
          nil
        end

        def parse_image_url
          image_url = super
          if image_url.is_a? NullObject
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end
      end
    end
  end
end
