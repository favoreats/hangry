module Hangry
  module Parsers
    module NonStandard
      class TablerPartyOfTwoParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('tablerpartyoftwo.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.aligncenter').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/../following::p[./following::*[contains(text(),"INSTRUCTIONS")]]')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/../following::p[1]')
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(@class,"entry-content")]/ul[1]')
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/..')
                  ingredients.children.each{|elem| elem.remove if elem.content.include?("INGREDIENTS")}
                end
              end
            end
            ingredients = ingredients.map { |i| i.content.gsub("-","").split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"INSTRUCTIONS")]/following::p[./following::*[contains(text(),"xoxo")]]')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"INSTRUCTIONS")]/..')
              instructions.children.each{|elem| elem.remove if elem.content.include?("INSTRUCTIONS")}
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(@class,"entry-content")]/ul[2]')
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[contains(text(),"Instructions")]/../following::p[./following::*[contains(text(),"xoxo")]]')
                  if !instructions.present?
                    instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/../following::p[./following::*[contains(text(),"xoxo")]]')
                    if !instructions.present?
                      instructions = nokogiri_doc.xpath('//*[contains(text(),"Assembly")]/../following::p[./following::*[contains(text(),"Coupon")]]')
                      if !instructions.present?
                        instructions = nokogiri_doc.xpath('//*[contains(text(),"DIRECTIONS")]/..')
                        instructions.children.each{|elem| elem.remove if elem.content.include?("DIRECTIONS")}
                      end
                    end
                  end
                end
              end
            end
            instructions = instructions.map { |i| i.content.tr("0-9.", "").strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
