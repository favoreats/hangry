module Hangry
  module Parsers
    module NonStandard
      class WholeSistersParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('whole-sisters.com')
        end

        def parse_instructions
          instruction = super
          if !instruction.present?
            instruction = nodes_with_class("cookbook-instruction-item").map { |i| i.content.strip }.uniq.join("\n")
          end
          instruction
        end
      end
    end
  end
end
