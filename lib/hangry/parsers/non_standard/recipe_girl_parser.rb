module Hangry
  module Parsers
    module NonStandard
      class RecipeGirlParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('recipegirl.com')
        end

        def parse_name
          name = nokogiri_doc.css(".posttitle").first&.content
          name = super unless name.present?

          name
        end

        def parse_author
          meta_info = nokogiri_doc.css(".postmeta").first&.content
          author = meta_info.split("posted by")[1].strip if meta_info.present?

          author = super if author.nil?

          author
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css(".photo nopin").first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super

          if !ingredients.present?
            ingredients = nokogiri_doc.css('div[class="ingredients"]').first&.content
            ingredients = ingredients.split("\n").reject(&:blank?)
            ingredients = ingredients.map{|i| i.strip}
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css(".instructions").map { |i| i.content.strip}.uniq.join("\n")
          end

          instructions
        end

        def parse_yield
          yields = super
          if yields.is_a? NullObject
            yields = nokogiri_doc.css('p:contains("Yield:")').first.content.strip.sub(/Yield: /, '')
          end

          yields
        rescue NoMethodError
          nil
        end
      end
    end
  end
end