module Hangry
  module Parsers
    module NonStandard
      class KellystilWellParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('kellystilwell.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.mv-create-ingredients li')
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::div[./following::*[contains(text(),"Instructions")]]')
              ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::p[./following::*[contains(text(),"Directions")]]')
                ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::ul[1]/li')
                  ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
                end
              end
            end
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Instructions")]/following::ol[1]/li')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions:")]/following::p[./following::*[@class="entry-meta"]]')
              instructions = instructions.map { |i| i.content.strip }.uniq.join("\n").delete('-')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[1]')
                instructions = instructions.children.map { |i| i.content.strip }.uniq.join("\n")
              end
            end
          end

          instructions
        end
      end
    end
  end
end
