module Hangry
  module Parsers
    module NonStandard
      class ThePinJunkieParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('thepinjunkie.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.separator img').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredient')
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::div[./following::*[contains(text(),"Find")]]')
            instructions = instructions.map { |i| i.content.tr("0-9.", "").strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
