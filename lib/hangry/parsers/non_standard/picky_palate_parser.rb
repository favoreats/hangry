module Hangry
  module Parsers
    module NonStandard
      class PickyPalateParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('picky-palate.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.title h1').children.first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.xpath('//*[@id="content"]/div/p/a/img').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredient')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[@class="paper-text"]/p[./following-sibling::*[contains(text(),"1.")]]')
              ingredients.children.each{|elem| elem.remove if (elem.content.include?("Update"))}
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[@id="content"]//p/strong/following::p[./following-sibling::*[contains(text(),"1.")]]')
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[@class="storycontent"]/h2/following::p[./following-sibling::*[contains(text(),"1.")]]')
                  if !ingredients.present?
                    ingredients = nokogiri_doc.xpath('//*[@class="MsoNormal"][./following-sibling::*[contains(text(),"1.")]]')
                    if !ingredients.present?
                      ingredients = nokogiri_doc.xpath('//*[@class="paper-text"]/p[1]')
                      if !ingredients.present?
                        ingredients = nokogiri_doc.xpath('//*[@id="content"]/div/p[5] | //*[@id="content"]/div/p[6]')
                      end
                    end
                  end
                end
              end
            end
            ingredients = ingredients.children.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.instruction')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[@class="paper-text"]/p[./following-sibling::*[contains(text(),"1.")]][last()]/following::p[./following-sibling::*[contains(text(),"Makes") or contains(text(),"servings") or contains(text(),"full") or contains(text(),"bars") or contains(text(),"about")]]')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[@class="paper-text"]/p[./following-sibling::*[contains(text(),"1.")]][last()]/following::p[./following::*[(@class="instagram")]]')
                instructions.children.each{|elem| elem.remove if (elem.content.include?("_"))}
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[@id="content"]//p/strong/following::p[./following-sibling::*[contains(text(),"1.")]][last()]/following::p[./following-sibling::*[contains(text(),"squares") or contains(text(),"mini")]]')
                  if !instructions.present?
                    instructions = nokogiri_doc.xpath('//*[@class="MsoNormal"][./following-sibling::*[contains(text(),"1.")]][last()]/following::p[./following::*[(@class="instagram")]]')
                    if !instructions.present?
                      instructions = nokogiri_doc.xpath('//*[@class="storycontent"]/h2/following::p[./following-sibling::*[contains(text(),"1.")]][last()]/following::p[./following-sibling::*[contains(text(),"brownies")]]')
                      if !instructions.present?
                        instructions = nokogiri_doc.xpath('//*[@class="paper-text"]/p[2]')
                        if !instructions.present?
                          instructions = nokogiri_doc.xpath('//*[@id="content"]/div/p[6]/following::p[./following::*[contains(text(),"doubles")]]')
                        end
                      end
                    end
                  end
                end
              end
            end
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n").gsub(/^[0-9]./, '')
          end

          instructions
        end
      end
    end
  end
end