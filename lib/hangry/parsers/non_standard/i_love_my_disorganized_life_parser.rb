module Hangry
  module Parsers
    module NonStandard
      class ILoveMyDisorganizedLifeParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('ilovemydisorganizedlife.com')
        end

        def parse_name
          name = super
          if !name.present?
            name =  nokogiri_doc.css("h1.entry-title").first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.content img')[3]
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Heres")]/../following::ol[1]//li')
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Here’s")]/../following::ol[1]//li')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end