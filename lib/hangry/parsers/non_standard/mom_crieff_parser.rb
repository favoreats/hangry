module Hangry
  module Parsers
    module NonStandard
      class MomCrieffParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('momcrieff.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]')[1]
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients:")]/following::ul[1]/li[./following::*[contains(text(),"Directions")]]')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients:")]/following::p[./following::*[contains(text(),"Intructions:") or contains(text(),"Instructions:") or contains(text(),"Directions")]][not(@class="wp-caption-text")]')
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"measure")]/following::p[./following::*[contains(text(),"maybe")]]')
              end
            end
            ingredients = ingredients.children.map{ |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::ul[1]/li[./following::*[contains(text(),"How")]]')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[./following::*[contains(text(),"Hey") or contains(text(),"Here") or contains(text(),"Seriously")]]')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[./following::*[@class="entry-meta"]]')
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[contains(text(),"Here’s the recipe")]/following::p[not(@class="wp-caption-text")][./following::*[contains(text(),"Hope")]]')
                end
              end
            end
            instructions = instructions.map{ |instr| instr.content.gsub(/^[0-9]./, '').strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
