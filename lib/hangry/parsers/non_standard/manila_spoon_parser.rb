module Hangry
  module Parsers
    module NonStandard
      class ManilaSpoonParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('manilaspoon.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1[itemprop="name"]').first&.content
            if !name.present?
              name = nokogiri_doc.css('h1[itemprop="headline"]').first&.content
            end
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.pf-content img').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::li[./following::*[contains(text(),"Procedure")]]')
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::p[./following::*[contains(text(),"Procedure")]]')
              ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::ul[1]/li')
                ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
              end
            end
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Procedure")]/following::p[./following::*[contains(text(),"you like what")]]')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Instructions")]/following::ol[1]/li')
            end
            instructions = instructions.map { |i| i.content.gsub(/^[0-9]./, '').strip }.reject(&:blank?).uniq.join("\n")
          end
          instructions
        end
      end
    end
  end
end
