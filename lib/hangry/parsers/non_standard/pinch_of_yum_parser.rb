module Hangry
  module Parsers
    module NonStandard
      class PinchOfYumParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('pinchofyum.com')
        end

        def parse_image_url
          image_elem = nokogiri_doc.css('[class*="wp-image"]').first
          image_url = image_elem['src'] if image_elem
          if !image_elem.present?
            image_url = super
          end

          image_url
        end
      end
    end
  end
end
