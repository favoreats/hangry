module Hangry
  module Parsers
    module NonStandard
      class TheSeasonedMomParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('theseasonedmom.com')
        end

        def parse_instructions
          instructions_elem = nokogiri_doc.css('.wprm-recipe-instructions div')
          instructions = instructions_elem.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
          if !instructions.present?
            instructions = super
          end

          instructions
        end
      end
    end
  end
end
