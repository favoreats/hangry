module Hangry
  module Parsers
    module NonStandard
      class BrittanySpantryParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('brittanyspantry.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.headline').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[@class="article"]//strong[1]/following::p[1]')
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[@class="article"]//strong[1]/following::p[1]/following::p[1]')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
