module Hangry
  module Parsers
    module NonStandard
      class RealMomKitchenParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('realmomkitchen.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[@class="row"]//strong/following::ul/li[./following::*[contains(text(),"Slice") or contains(text(),"Preheat") or contains(text(),"Serve")]]')
            ingredients = ingredients.children.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[@class="row"]//strong/following::ol[1]/li')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[@class="row"]//strong/following::p[1]')
            end
            instructions = instructions.children.map { |instr| instr.content.strip }.reject(&:blank?).uniq.join("\n").gsub(/^[0-9]./, '')
          end

          instructions
        end
      end
    end
  end
end
