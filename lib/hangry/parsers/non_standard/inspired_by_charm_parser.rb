module Hangry
  module Parsers
    module NonStandard
      class InspiredByCharmParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('inspiredbycharm.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('[itemprop="headline"]').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('section.entry-content img').first
            image_url = image_elem['src'] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.wprm-recipe-ingredients li')
            ingredients = ingredients.map { |i| i.content.gsub(/[\n\s]+/, ' ').strip }.flatten.reject(&:blank?)
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(@class,"recipe-title")]/following::p[2]')
              ingredients.children.each { |elem| elem.remove if elem.content.include?('what you') || elem.content.include?('You will') }
              ingredients = ingredients.children.map { |i| i.content.strip }.flatten.reject(&:blank?)
            end
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.wprm-recipe-instructions li')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(@class,"recipe-title")]/following::p[2]/following::p[./following::*[contains(@class,"print-link")]]')
            end
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
