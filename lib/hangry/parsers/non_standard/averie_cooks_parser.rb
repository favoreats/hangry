module Hangry
  module Parsers
    module NonStandard
      class AverieCooksParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('averiecooks.com')
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredient_elems = nokogiri_doc.xpath('//*[@class="ingredients"]/*')
            ingredient_elems.each do |elem|
              elem.search('br').each { |el| el.replace("\n") }
              ingredients << elem.content.split("\n")
            end
          end

          ingredients.flatten.reject(&:blank?)
        end

        def parse_published_date
          date = super
          date = nokogiri_doc.css("[itemprop='dateModified']").first&.content if date.nil?

          date = nokogiri_doc.css(".post-meta span:first-child").first&.content&.sub('posted on ', '') if date.nil?

          date
        end
      end
    end
  end
end
