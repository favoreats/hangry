module Hangry
  module Parsers
    module NonStandard
      class TheBestBlogRecipesParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_includes?('thebestblogrecipes.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.alignnone').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/..')
            ingredients.children.each{|elem| elem.remove if elem.content.include?("Ingredients")}
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Instructions")]/..')
            instructions.children.each{|elem| elem.remove if elem.content.include?("Instructions")}
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
