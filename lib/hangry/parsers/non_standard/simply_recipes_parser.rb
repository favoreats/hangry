module Hangry
  module Parsers
    module NonStandard
      class SimplyRecipesParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('simplyrecipes.com')
        end

        def parse_name
          name = nokogiri_doc.css('h1.entry-title').first&.content
          if !name.present?
            name = super
          end

          name
        end
      end
    end
  end
end
