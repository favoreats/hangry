module Hangry
  module Parsers
    module NonStandard
      class SomethingSwankyParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('somethingswanky.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.post-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.aligncenter').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredients')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.ingredient')
              if !ingredients.present?
                ingredients = nokogiri_doc.css('.ingredients .p1 .s1')
              end
            end
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.instructions')
            if !instructions.present?
              instructions = nokogiri_doc.css('.instructions .p1 .s1')
            end
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
