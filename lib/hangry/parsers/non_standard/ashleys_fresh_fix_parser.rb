module Hangry
  module Parsers
    module NonStandard
      class AshleysFreshFixParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('ashleysfreshfix.com')
        end

        def parse_image_url
          image_elem = nokogiri_doc.css('.entry-thumbnail img').first
          image_url = image_elem["src"] if image_elem
          if !image_url.present?
            image_url = super
          end

          image_url
        end
      end
    end
  end
end
