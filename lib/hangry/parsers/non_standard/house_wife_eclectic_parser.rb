module Hangry
  module Parsers
    module NonStandard
      class HouseWifeEclecticParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('housewifeeclectic.com')
        end

        def parse_name
          name = super
          if !name.present?
            name =  nokogiri_doc.css(".entry-title").first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.separator img').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.entry-content ul li')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/following::p[./following::*[contains(text(),"PREPARATION")]]')
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/../following::div[./following::*[contains(text(),"PREPARATION") or contains(text(),"DIRECTIONS")]]')
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(text(),"INGREDIENTS")]/following::p[./following::*[contains(text(),"DIRECTIONS") or contains(text(),"INSTRUCTIONS")]] | //*[contains(text(),"INGREDIENTS")]')
                  ingredients.children.each{|elem| elem.remove if (elem.content.include?("INGREDIENTS") || elem.content.include?("DIRECTIONS"))}
                  if !ingredients.present?
                    ingredients = nokogiri_doc.xpath('//*[contains(text(),"You need") or contains(text(),"You will need")]')
                    ingredients.children.each{|elem| elem.remove if (elem.content.include?("You need") || elem.content.include?("You will need"))}
                    if !ingredients.present?
                      ingredients = nokogiri_doc.xpath('//*[@class="entry-content"]/p[2]')
                    end
                  end
                end
              end
            end
            ingredients = ingredients.children.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.entry-content ol li')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"DIRECTIONS")]/following::p[./following::*[@class="centered"]]')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(text(),"DIRECTIONS")]')
                instructions.children.each{|elem| elem.remove if elem.content.include?("DIRECTIONS")}
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[contains(text(),"PREPARATION")]/following::p[./following::*[@class="centered"]]')
                  if !instructions.present?
                    instructions = nokogiri_doc.xpath('//*[@class="entry-content"]/ul/following::p[./following::*[@class="centered"]] | //*[@class="entry-content"]/ul/following::div[./following::*[@class="centered"]]')
                    if !instructions.present?
                      instructions = nokogiri_doc.xpath('//*[contains(text(),"Steps")]/following::div[./following::*[contains(text(),"Feel")]]')
                      if !instructions.present?
                        instructions = nokogiri_doc.xpath('//*[contains(text(),"You need") or contains(text(),"You will need")]/following::p[./following::*[contains(text(),"Filed")]]')
                        if !instructions.present?
                          instructions = nokogiri_doc.xpath('//*[@class="entry-content"]/p[2]/following::p[./following::*[contains(text(),"Filed")]]')
                        end
                      end
                    end
                  end
                end
              end
            end
            instructions = instructions.map { |i| i.content.tr("0-9).","").strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end