module Hangry
  module Parsers
    module NonStandard
      class RealSimpleParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('realsimple.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.headline').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.lazy-image').first
            image_url = image_elem['data-src'] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredients li')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"What You Need")]/following::ul[1]/li')
            end
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.step p')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Steps")]/following::ol/li')
            end
            instructions = instructions.map { |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
