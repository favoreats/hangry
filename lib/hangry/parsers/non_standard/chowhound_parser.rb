module Hangry
  module Parsers
    module NonStandard
      class ChowhoundParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('chowhound.com')
        end

        def parse_name
          content = nokogiri_doc.css('title').first&.content
          name = content.strip.sub(/\sRecipe\s-\sChowhound/, '') if content.present?
          name = super unless name.present?
          name
        end

        def parse_instructions
          instructions = nokogiri_doc.css('div[itemprop="recipeInstructions"] ol li')
          instructions.search("span[class='number_divider']").remove
          instructions.map { |i| i.content.strip}.uniq.join("\n")
        end
      end
    end
  end
end
