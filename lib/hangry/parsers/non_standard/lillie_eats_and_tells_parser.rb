module Hangry
  module Parsers
    module NonStandard
      class LillieEatsAndTellsParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('lillieeatsandtells.com')
        end

        def parse_image_url
          image_elem = nokogiri_doc.css('[class*="wp-image"]').first
          image_url = image_elem["src"] if image_elem
          if !image_url.present?
            image_url = super
          end

          image_url
        end

        def parse_cook_time
          cook_time = super
          if cook_time.nil?
            cook_time = nokogiri_doc.css(".tasty-recipes-prep-time").first&.content
          end

          cook_time
        end

        def parse_published_date
          published_date = super
          if published_date.nil?
            date = nokogiri_doc.css("[itemprop='datePublished']").first&.content
            published_date = date.present? ? Date.parse(date) : nil
          end

          published_date
        rescue ArgumentError
          nil
        end
      end
    end
  end
end
