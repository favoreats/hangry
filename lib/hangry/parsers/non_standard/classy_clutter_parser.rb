module Hangry
  module Parsers
    module NonStandard
      class ClassyClutterParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('classyclutter.net')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.post-header h1').first&.content
          end

          name
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.post-entry ul:nth-of-type(1) li')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients") or contains(text(),"INGREDIENTS")]/following::p[./following::*[contains(text(),"Instructions") or contains(text(),"Directions") or contains(text(),"DIRECTIONS")]]')
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/../following::div[./following::*[contains(text(),"Directions")]]')
              end
            end
            ingredients = ingredients.map { |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Instructions")]/following-sibling::ul')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Instructions:")]/following-sibling::p')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(text(), "Directions")]/../following::div[./following::*[div//a//img]]')
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::ol/li')
                  if !instructions.present?
                    instructions = nokogiri_doc.xpath('//*[contains(text(),"DIRECTIONS")]/following::p[./following::*[contains(text(),"that!")]]')
                    if !instructions.present?
                      instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[1]')
                    end
                  end
                end
              end
            end
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.xpath('//*[@class="post-entry"]//img[contains(@class, "aligncenter") or contains(@class,"wp-image")]').first
            if !image_elem.present?
              image_elem = nokogiri_doc.xpath('//*[@class="post-entry"]//img').first
            end
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_published_date
          date = super
          if !date.present?
            date = nokogiri_doc.css('.meta-date').first&.content
          end

          date
        end
      end
    end
  end
end
