module Hangry
  module Parsers
    module NonStandard
      class TheKitchnParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('thekitchn.com')
        end

        def recipe_data
          unless @recipe_data
            elem = nokogiri_doc.css("[data-render-react-id='recipes/Recipe']").first
            @recipe_data = Oj.load(elem['data-props'])['recipe'] if elem
          end

          @recipe_data
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            elem = nokogiri_doc.css("[data-render-react-id='images/LazyPicture']").first
            image_data = Oj.load(elem['data-props'])['sources'].first if elem
            image_url = image_data["src"] if image_data.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = recipe_data['ingredient_groups_attributes'].first['ingredients_attributes'] if recipe_data
            ingredients = ingredients.map{ |i| "#{i['quantity']} #{i['unit']} #{i['pretty_ingredient']}" }.reject(&:blank?)
          end

          ingredients
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.PostHeader__headline').first&.content&.strip
            name = name.sub('Recipe: ', '')
          end

          name
        end

        def parse_author
          author = super
          if !author.present?
            author = nokogiri_doc.css('.PostByline__author .PostByline__name').first&.content
          end

          author
        end

        def parse_published_date
          date = super
          if !date.present?
            date = nokogiri_doc.css('.PostByline__timestamp').first&.content&.strip
          end

          date
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = recipe_data["recipe_instruction_groups_attributes"].first['recipe_steps_attributes'] if recipe_data
            instructions = instructions.map{ |i| "#{i['step']}" }.reject(&:blank?).join if instructions.present?
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[@itemprop="ingredients"]/../following-sibling::p')
              instructions = instructions.map{ |instr| instr.content.strip }.reject(&:blank?).uniq.join("\n") if instructions.present?
            end
          end

          instructions
        end

        def parse_description
          description = super
          if !description.present?
            description = nokogiri_doc.css('meta[name="description"]').first["content"]
          end

          description
        rescue NoMethodError
          nil
        end
      end
    end
  end
end

