module Hangry
  module Parsers
    module NonStandard
      class RachaelRaymagParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('rachaelraymag.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('[itemprop = "headline"]').first&.content
          end

          name
        end
      end
    end
  end
end