module Hangry
  module Parsers
    module NonStandard
      class TarathuesonParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('tarathueson.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css(".entry-title").first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.printable-header img').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredients ul li')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.ingredients p')
            end

            ingredients = ingredients.map{ |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end


        def parse_instructions
          instructions = super
            if !instructions.present?
              instructions = nokogiri_doc.css('.directions p')
              instructions.children.each{|elem| elem.remove if elem.content.include?("Instructions:")}
              instructions = instructions.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
            end

          instructions
        end
      end
    end
  end
end

