module Hangry
  module Parsers
    module NonStandard
      class LaurensLatestParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('laurenslatest.com')
        end

        def parser_author
          author = super
          if !author.present?
            author = nokogiri_doc.css('.cookbook-author').first&.content
          end

          author
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.cookbook-instruction-item ol li')
            instructions = instructions.map{ |instr| instr.content.strip }.reject(&:blank?).uniq.join("\n") if instructions.present?
          end

          instructions
        end

        def parse_published_date
          date = super
          if !date.present?
            date = nokogiri_doc.css('[itemprop="datePublished"]').first&.content
          end

          date
        end
      end
    end
  end
end

