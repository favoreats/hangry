module Hangry
  module Parsers
    module NonStandard
      class YourHomeBasedMomParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('yourhomebasedmom.com')
        end

        def parse_image_url
          image_elem = nokogiri_doc.css('[class*="wp-image"]').first
          image_url = image_elem['src'] if image_elem
          if !image_elem.present?
            image_url = super
          end

          image_url
        end
      end
    end
  end
end
