module Hangry
  module Parsers
    module NonStandard
      class OhSoDelicioso < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('ohsodelicioso.com')
        end

        def parse_image_url
          image_url = super
          if image_url.is_a? NullObject
            image_elem = nokogiri_doc.css('[class*="alignnone wp-image"]').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end
      end
    end
  end
end
