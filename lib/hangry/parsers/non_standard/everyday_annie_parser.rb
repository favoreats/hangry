module Hangry
  module Parsers
    module NonStandard
      class EverydayAnnieParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('everydayannie.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.post-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.aligncenter').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredient-text ul li')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('.ingredient-text p')
              ingredients.children.each{|elem| elem.remove if elem.content.include?("Ingredients")}
              if !ingredients.present?
                ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::span[not(contains(@style,"font-size:12pt"))][./following::*[contains(text(),"Directions")]]')
                if !ingredients.present?
                  ingredients = nokogiri_doc.xpath('//*[contains(text(),"Printer")]/following::p[./following::*[contains(text(),"Directions")]]')
                  ingredients.children.each{|elem| elem.remove if (elem.content.include?("Ingredients") || elem.content.include?("Yield"))}
                end
              end
            end
            ingredients = ingredients.children.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.step-text')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following::p[./following::*[contains(text(),"Note")]] | //*[contains(text(),"Directions")]/..')
              instructions.children.each{|elem| elem.remove if elem.content.include?("Directions")}
            end
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
