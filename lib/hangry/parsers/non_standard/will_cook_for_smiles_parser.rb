module Hangry
  module Parsers
    module NonStandard
      class WillCookForSmilesParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('willcookforsmiles.com')
        end

        def parse_instructions
          instructions = nokogiri_doc.css('.wprm-recipe-instructions li')
          instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          if !instructions.present?
            instructions = super
          end

          instructions
        end
      end
    end
  end
end
