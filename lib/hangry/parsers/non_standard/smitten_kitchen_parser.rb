module Hangry
  module Parsers
    module NonStandard
      class SmittenKitchenParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('smittenkitchen.com')
        end

        def parse_author
          author = super
          if author.is_a? NullObject
            author = nokogiri_doc.css('.author').first&.content
          end

          author
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.post-thumbnail-container img').first
            image_url = image_elem['src'] if image_elem
          end

          image_url
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css(".jetpack-recipe-directions").map { |i|
              i.content.strip
            }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
