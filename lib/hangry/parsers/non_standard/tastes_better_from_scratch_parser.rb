module Hangry
  module Parsers
    module NonStandard
      class TastesBetterFromScratchParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('tastesbetterfromscratch.com')
        end

        def parse_instructions
          instructions_elem = nokogiri_doc.css(".wprm-recipe-instruction-text")
          instructions = instructions_elem.children.map{|i| i.content}.uniq.join("\n")
          if !instructions.present?
            instructions = super
          end

          instructions
        end
      end
    end
  end
end
