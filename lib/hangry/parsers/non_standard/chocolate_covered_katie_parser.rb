module Hangry
  module Parsers
    module NonStandard
      class ChocolateCoveredKatieParser < SchemaOrgRecipeParser

        def can_parse?
          canonical_url_matches_domain?('chocolatecoveredkatie.com')
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[@class="entry-content"]/ul/li')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[@class="entry-content"]/ul/ul/li')
            end
            ingredients = ingredients.map{ |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.aligncenter').first
            if !image_url.present?
              image_elem = nokogiri_doc.css('.entry-content img').first
            end
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_instructions
          instructions_elem = nokogiri_doc.css('.entry-content ol li')
          instructions = instructions_elem.map{|instr| instr.text}.reject(&:blank?).uniq.join("\n")
          if !instructions.present?
            instructions_elem = nokogiri_doc.xpath('//*[contains(text(),"Instructions")]/following-sibling::div/p[not(a)]')
            instructions = instructions_elem.map{|instr| instr.text}.reject(&:blank?).uniq.join("\n")
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[@class="entry-content"]/ul/following-sibling::p[1]').first&.content
              if !instructions.present?
                instructions = super
              end
            end
          end

          instructions
        end
      end
    end
  end
end
