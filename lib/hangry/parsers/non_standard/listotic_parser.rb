module Hangry
  module Parsers
    module NonStandard
      class ListoticParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('listotic.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.xpath('//*[@class="entry-content"]/h2').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[@class="entry-content"]/ul/li[./following::*[contains(text(),"INSTRUCTIONS")]]')
            ingredients = ingredients.map { |i| i.content.strip }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[@class="entry-content"]/ol/li')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
