module Hangry
  module Parsers
    module NonStandard
      class HelloFreshParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('hellofresh.com')
        end

        def parse_ingredients
          ingredients = nokogiri_doc.css('div.fela-1nnptk7')
          ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          if !ingredients.present?
            ingredients = super
          end

          ingredients
        end

        def parse_instructions
          instructions = nokogiri_doc.css('div.fela-1qsq4x8 p')
          instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          if !instructions.present?
            instructions = super
          end

          instructions
        end
      end
    end
  end
end
