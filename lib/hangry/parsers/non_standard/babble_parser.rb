module Hangry
  module Parsers
    module NonStandard
      class BabbleParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('babble.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.tm-single-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.tm-single-content img').first
            image_url = image_elem["data-src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.tm-single-content ul li')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/../following-sibling::p[1] | //*[contains(text(),"Dressing")]/../following-sibling::p[1]')
              ingredients.children.each{|elem| elem.remove if elem.content.include?("Ingredients")}
            end
            ingredients = ingredients.children.map{|i| i.text}.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.css('.tm-single-content ol li')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Method:")]/following-sibling::p[not(em)]')
              if !instructions.present?
                instructions = nokogiri_doc.xpath('//*[contains(text(),"Method")]/../following-sibling::p | //*[contains(text(),"Method")]/..')
                instructions.children.each{|elem| elem.remove if elem.content.include?("Method")}
                if !instructions.present?
                  instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/following-sibling::p[not(strong)]')
                end
              end
            end
            instructions = instructions.map{|instr| instr.text}.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
