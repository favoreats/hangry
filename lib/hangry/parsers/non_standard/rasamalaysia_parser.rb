module Hangry
  module Parsers
    module NonStandard
      class RasamalaysiaParser < SchemaOrgRecipeParser
        def can_parse?
          link = nokogiri_doc.css("[rel='wlwmanifest']").first
          link[:href].include?('rasamalaysia.com') if link.present?
        end

        def parse_name
          name = nokogiri_doc.css('h1.entry-title').first&.content
          if !name.present?
            name = super
          end

          name
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients:")]/following::p[./following::*[contains(text(),"Method")]]')
            if !ingredients.present?
              ingredients = nokogiri_doc.css('#Recipe p:nth-of-type(7)')
            end
            ingredients = ingredients.children.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Method")]/following-sibling::p[./following::*[contains(text(),"Cook’s Note")]]')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[contains(text(),"Method")]/following-sibling::p[./following::*[contains(text(),"miss")]]')
            end
            instructions = instructions.children.map { |i| i.content.strip }.uniq.join("\n").gsub(/^[0-9]./, '')
          end

          instructions
        end
      end
    end
  end
end