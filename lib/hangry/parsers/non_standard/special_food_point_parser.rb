module Hangry
  module Parsers
    module NonStandard
      class SpecialFoodPointParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_includes?('specialfoodpoint.blogspot.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.separator img').first
            image_url = image_elem["src"] if image_elem
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.entry-content ul li')
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/../following::span[./following::*[contains(text(),"Posted ")]]')
            instructions = instructions.map { |i| i.content.strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
