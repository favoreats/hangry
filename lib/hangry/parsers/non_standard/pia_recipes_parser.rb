module Hangry
  module Parsers
    module NonStandard
      class PiaRecipesParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('piarecipes.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.css('h1.entry-title').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('.separator img').first
            image_url = image_elem['src'] if image_elem
            image_url = 'http:' + image_url
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[@class="post-body entry-content"]//ul/li')
            if !ingredients.present?
              ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/following::div[./following::*[contains(text(),"Instructions")]]')
            end
            ingredients = ingredients.map { |i| i.content.delete('•').strip }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Instructions:")]/following::div[./following::*[(@class="googlepublisherads")]]')
            if !instructions.present?
              instructions = nokogiri_doc.xpath('//*[@class="post-body entry-content"]//ol[1]/li')
            end
            instructions = instructions.map { |i| i.content.gsub(/^[0-9]./, '').strip }.uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end
