module Hangry
  module Parsers
    module NonStandard
      class GimmeSomeOvenParser < JsonLDParser
        def can_parse?
          canonical_url_matches_domain?('gimmesomeoven.com')
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.css('.ingredients')
            ingredients = ingredients.map { |i| i.content.split("\n") }.flatten.reject(&:blank?)
          end

          ingredients
        end

        def parse_image_url
          image_elem = nokogiri_doc.css('[class*="wp-image"]').first
          image_url = image_elem['src'] if image_elem
          if !image_elem.present?
            image_url = super
          end

          image_url
        end
      end
    end
  end
end
