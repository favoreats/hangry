module Hangry
  module Parsers
    module NonStandard
      class TheFirstYearBlogParser < SchemaOrgRecipeParser
        def can_parse?
          canonical_url_matches_domain?('thefirstyearblog.com')
        end

        def parse_name
          name = super
          if !name.present?
            name = nokogiri_doc.xpath('//*[@class="post-title"]//h1').first&.content
          end

          name
        end

        def parse_image_url
          image_url = super
          if !image_url.present?
            image_elem = nokogiri_doc.css('[class*="wp-image"]').first
            image_url = image_elem["src"] if image_elem.present?
          end

          image_url
        end

        def parse_ingredients
          ingredients = super
          if !ingredients.present?
            ingredients = nokogiri_doc.xpath('//*[contains(text(),"Ingredients")]/..')
            ingredients.children.each{|elem| elem.remove if elem.content.include?("Ingredients")}
            ingredients = ingredients.children.map{ |i| i.content.strip }.reject(&:blank?)
          end

          ingredients
        end


        def parse_instructions
          instructions = super
          if !instructions.present?
            instructions = nokogiri_doc.xpath('//*[contains(text(),"Directions")]/.. | //*[contains(text(),"Directions")]/../following::p[./following::*[contains(text(),"Adapted")]]')
            instructions.children.each{|elem| elem.remove if elem.content.include?("Directions")}
            instructions = instructions.map{ |i| i.content.strip }.reject(&:blank?).uniq.join("\n")
          end

          instructions
        end
      end
    end
  end
end

