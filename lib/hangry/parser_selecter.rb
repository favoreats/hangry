require 'hangry/recipe_parser'
require 'hangry/null_recipe_parser'
require 'hangry/hrecipe_parser'
require 'hangry/schema_org_recipe_parser'
require 'hangry/data_vocabulary_recipe_parser'
require 'hangry/json_ld_parser'
require 'hangry/parsers/non_standard/copykat_parser'
require 'hangry/parsers/non_standard/eating_well_parser'
require 'hangry/parsers/non_standard/epicurious_parser'
require 'hangry/parsers/non_standard/jamie_oliver_parser'
require 'hangry/parsers/non_standard/taste_of_home_parser'
require 'hangry/parsers/non_standard/rachaelray_parser'
require 'hangry/parsers/non_standard/all_recipes_parser'
require 'hangry/parsers/non_standard/whole_sisters_parser'
require 'hangry/parsers/non_standard/six_sisters_stuff_parser'
require 'hangry/parsers/non_standard/cookingclassy_parser'
require 'hangry/parsers/non_standard/oh_so_delicioso_parser'
require 'hangry/parsers/non_standard/smitten_kitchen_parser'
require 'hangry/parsers/non_standard/super_healthy_kids_parser'
require 'hangry/parsers/non_standard/clean_eats_and_treats_parser'
require 'hangry/parsers/non_standard/chowhound_parser'
require 'hangry/parsers/non_standard/deals_to_meals_parser'
require 'hangry/parsers/non_standard/abountifulkitchen_parser'
require 'hangry/parsers/non_standard/twopeasandtheirpod_parser'
require 'hangry/parsers/non_standard/our_best_bites_parser'
require 'hangry/parsers/non_standard/how_sweet_eats_parser'
require 'hangry/parsers/non_standard/lillie_eats_and_tells_parser'
require 'hangry/parsers/non_standard/oh_sweet_basil_parser'
require 'hangry/parsers/non_standard/recipe_girl_parser'
require 'hangry/parsers/non_standard/skinny_taste_parser'
require 'hangry/parsers/non_standard/the_food_nanny_parser'
require 'hangry/parsers/non_standard/good_cheap_eats_parser'
require 'hangry/parsers/non_standard/the_kitchn_parser'
require 'hangry/parsers/non_standard/pennies_into_pearls_parser'
require 'hangry/parsers/non_standard/laurens_latest_parser'
require 'hangry/parsers/non_standard/averie_cooks_parser'
require 'hangry/parsers/non_standard/classy_clutter_parser'
require 'hangry/parsers/non_standard/over_the_big_moon_parser'
require 'hangry/parsers/non_standard/my_recipes_parser'
require 'hangry/parsers/non_standard/tarathueson_parser'
require 'hangry/parsers/non_standard/atastylovestory_parser'
require 'hangry/parsers/non_standard/the_girl_who_ate_everything_parser'
require 'hangry/parsers/non_standard/the_pioneer_woman_parser'
require 'hangry/parsers/non_standard/buzz_feed_parser'
require 'hangry/parsers/non_standard/make_it_do_parser'
require 'hangry/parsers/non_standard/bakers_royale_parser'
require 'hangry/parsers/non_standard/rachaelraymag_parser'
require 'hangry/parsers/non_standard/ezra_pound_cake_parser'
require 'hangry/parsers/non_standard/kayotic_parser'
require 'hangry/parsers/non_standard/grumpys_honey_bunch_parser'
require 'hangry/parsers/non_standard/earthly_delights_blog_parser'
require 'hangry/parsers/non_standard/chocolate_and_carrots_parser'
require 'hangry/parsers/non_standard/pipandebby_parser'
require 'hangry/parsers/non_standard/saltnturmeric_blogspot_parser'
require 'hangry/parsers/non_standard/something_swanky_parser'
require 'hangry/parsers/non_standard/simply_recipes_parser'
require 'hangry/parsers/non_standard/greatist_parser'
require 'hangry/parsers/non_standard/babble_parser'
require 'hangry/parsers/non_standard/chocolate_covered_katie_parser'
require 'hangry/parsers/non_standard/the_best_blog_recipes_parser'
require 'hangry/parsers/non_standard/love_bakes_good_cakes_parser'
require 'hangry/parsers/non_standard/nutmeg_nanny_parser'
require 'hangry/parsers/non_standard/cooking_in_stilettos_parser'
require 'hangry/parsers/non_standard/lmld_parser'
require 'hangry/parsers/non_standard/the_seasoned_mom_parser'
require 'hangry/parsers/non_standard/diethood_parser'
require 'hangry/parsers/non_standard/sweet_little_bluebird_parser'
require 'hangry/parsers/non_standard/food_network_parser'
require 'hangry/parsers/non_standard/recipes_filled_with_love_parser'
require 'hangry/parsers/non_standard/tastes_better_from_scratch_parser'
require 'hangry/parsers/non_standard/jamhands_parser'
require 'hangry/parsers/non_standard/the_first_year_blog_parser'
require 'hangry/parsers/non_standard/just_food_recipes_parser'
require 'hangry/parsers/non_standard/an_everyday_blessing_parser'
require 'hangry/parsers/non_standard/my_table_for_eight_parser'
require 'hangry/parsers/non_standard/the_pin_junkie_parser'
require 'hangry/parsers/non_standard/wine_and_glue_parser'
require 'hangry/parsers/non_standard/dessert_now_dinner_later_parser'
require 'hangry/parsers/non_standard/the_sweet_chick_parser'
require 'hangry/parsers/non_standard/creations_by_kara_parser'
require 'hangry/parsers/non_standard/sweet_twist_of_blogging_parser'
require 'hangry/parsers/non_standard/embellish_mints_parser'
require 'hangry/parsers/non_standard/special_food_point_parser'
require 'hangry/parsers/non_standard/raining_hot_coupons_parser'
require 'hangry/parsers/non_standard/i_love_my_disorganized_life_parser'
require 'hangry/parsers/non_standard/high_heels_and_grills_parser'
require 'hangry/parsers/non_standard/will_cook_for_smiles_parser'
require 'hangry/parsers/non_standard/a_slice_of_style_parser'
require 'hangry/parsers/non_standard/martha_stewart_parser'
require 'hangry/parsers/non_standard/fitness_magazine_parser'
require 'hangry/parsers/non_standard/rasamalaysia_parser'
require 'hangry/parsers/non_standard/cafedelites_parser'
require 'hangry/parsers/non_standard/everyday_annie_parser'
require 'hangry/parsers/non_standard/picky_palate_parser'
require 'hangry/parsers/non_standard/hello_fresh_parser'
require 'hangry/parsers/non_standard/food_and_wine_parser'
require 'hangry/parsers/non_standard/tabler_party_of_two_parser'
require 'hangry/parsers/non_standard/my_mommy_style_parser'
require 'hangry/parsers/non_standard/house_wife_eclectic_parser'
require 'hangry/parsers/non_standard/mom_crieff_parser'
require 'hangry/parsers/non_standard/savy_naturalista_parser'
require 'hangry/parsers/non_standard/listotic_parser'
require 'hangry/parsers/non_standard/money_saving_mom_parser'
require 'hangry/parsers/non_standard/gimme_some_oven_parser'
require 'hangry/parsers/non_standard/the_garden_grazer_parser'
require 'hangry/parsers/non_standard/ashleys_fresh_fix_parser'
require 'hangry/parsers/non_standard/my_kitchen_craze_parser'
require 'hangry/parsers/non_standard/moms_without_answers_parser'
require 'hangry/parsers/non_standard/gluten_free_on_a_shoe_string_parser'
require 'hangry/parsers/non_standard/eat_live_run_parser'
require 'hangry/parsers/non_standard/enrile_moine_parser'
require 'hangry/parsers/non_standard/amy_d_gorin_parser'
require 'hangry/parsers/non_standard/brittany_spantry_parser'
require 'hangry/parsers/non_standard/real_mom_kitchen_parser'
require 'hangry/parsers/non_standard/cooking_carnival_parser'
require 'hangry/parsers/non_standard/pinch_of_yum_parser'
require 'hangry/parsers/non_standard/damn_delicious_parser'
require 'hangry/parsers/non_standard/your_home_based_mom_parser'
require 'hangry/parsers/non_standard/my_baking_addiction_parser'
require 'hangry/parsers/non_standard/pia_recipes_parser'
require 'hangry/parsers/non_standard/inspired_by_charm_parser'
require 'hangry/parsers/non_standard/pop_culture_parser'
require 'hangry/parsers/non_standard/menumusings_blogspot_parser'
require 'hangry/parsers/non_standard/weelicious_parser'
require 'hangry/parsers/non_standard/coupons_and_freebies_mom_parser'
require 'hangry/parsers/non_standard/real_simple_parser'
require 'hangry/parsers/non_standard/smart_school_house_parser'
require 'hangry/parsers/non_standard/eat_well_101_parser'
require 'hangry/parsers/non_standard/athrifty_mom_parser'
require 'hangry/parsers/non_standard/fox_and_briar_parser'
require 'hangry/parsers/non_standard/paula_deen_parser'
require 'hangry/parsers/non_standard/manila_spoon_parser'
require 'hangry/parsers/non_standard/the_frugal_girls_parser'
require 'hangry/parsers/non_standard/kellystil_well_parser'
require 'hangry/parsers/non_standard/cooking_light_parser'

module Hangry
  class ParserSelector
    def initialize(nokogiri_doc)
      nokogiri_doc = Nokogiri::HTML(nokogiri_doc) if nokogiri_doc.is_a?(String)
      @nokogiri_doc = nokogiri_doc
    end

    def parser
      # Prefer the more specific parsers
      parser_classes = [
        Parsers::NonStandard::CopykatParser,
        Parsers::NonStandard::EatingWellParser,
        Parsers::NonStandard::EpicuriousParser,
        Parsers::NonStandard::JamieOliverParser,
        Parsers::NonStandard::TasteOfHomeParser,
        Parsers::NonStandard::RachaelRayParser,
        Parsers::NonStandard::AllRecipesParser,
        Parsers::NonStandard::WholeSistersParser,
        Parsers::NonStandard::SixSistersStuffParser,
        Parsers::NonStandard::CookingClassyParser,
        Parsers::NonStandard::OhSoDelicioso,
        Parsers::NonStandard::SmittenKitchenParser,
        Parsers::NonStandard::SuperHealthyKidsParser,
        Parsers::NonStandard::CleanEatsAndTreats,
        Parsers::NonStandard::ChowhoundParser,
        Parsers::NonStandard::DealsToMealsParser,
        Parsers::NonStandard::ABountifulKitchenParser,
        Parsers::NonStandard::TwoPeasAndTheirPodParser,
        Parsers::NonStandard::OurBestBitesParser,
        Parsers::NonStandard::HowSweetEatsParser,
        Parsers::NonStandard::LillieEatsAndTellsParser,
        Parsers::NonStandard::OhSweetBasilParser,
        Parsers::NonStandard::RecipeGirlParser,
        Parsers::NonStandard::SkinnyTasteParser,
        Parsers::NonStandard::TheFoodNannyParser,
        Parsers::NonStandard::GoodCheapEatsParser,
        Parsers::NonStandard::TheKitchnParser,
        Parsers::NonStandard::PenniesIntoPearlsParser,
        Parsers::NonStandard::LaurensLatestParser,
        Parsers::NonStandard::AverieCooksParser,
        Parsers::NonStandard::ClassyClutterParser,
        Parsers::NonStandard::OverTheBigMoonParser,
        Parsers::NonStandard::MyRecipesParser,
        Parsers::NonStandard::TarathuesonParser,
        Parsers::NonStandard::ATastyLoveStoryParser,
        Parsers::NonStandard::TheGirlWhoAteEverythingParser,
        Parsers::NonStandard::ThePioneerWomanParser,
        Parsers::NonStandard::BuzzFeedParser,
        Parsers::NonStandard::MakeItDoParser,
        Parsers::NonStandard::BakersRoyaleParser,
        Parsers::NonStandard::RachaelRaymagParser,
        Parsers::NonStandard::EzraPoundCakeParser,
        Parsers::NonStandard::KayoticParser,
        Parsers::NonStandard::GrumpysHoneyBunchParser,
        Parsers::NonStandard::EarthlyDelightsBlogParser,
        Parsers::NonStandard::ChocolateAndCarrotsParser,
        Parsers::NonStandard::PipandebbyParser,
        Parsers::NonStandard::SaltNTurmericBlogspotParser,
        Parsers::NonStandard::SomethingSwankyParser,
        Parsers::NonStandard::SimplyRecipesParser,
        Parsers::NonStandard::GreatistParser,
        Parsers::NonStandard::BabbleParser,
        Parsers::NonStandard::ChocolateCoveredKatieParser,
        Parsers::NonStandard::TheBestBlogRecipesParser,
        Parsers::NonStandard::LoveBakesGoodCakesParser,
        Parsers::NonStandard::NutmegNannyParser,
        Parsers::NonStandard::CookingInStilettosParser,
        Parsers::NonStandard::LMLDParser,
        Parsers::NonStandard::TheSeasonedMomParser,
        Parsers::NonStandard::DietHoodParser,
        Parsers::NonStandard::SweetLittleBluebirdParser,
        Parsers::NonStandard::FoodNetworkParser,
        Parsers::NonStandard::RecipesFilledWithLoveParser,
        Parsers::NonStandard::TastesBetterFromScratchParser,
        Parsers::NonStandard::JamhandsParser,
        Parsers::NonStandard::TheFirstYearBlogParser,
        Parsers::NonStandard::JustFoodRecipesParser,
        Parsers::NonStandard::AnEverydayBlessingParser,
        Parsers::NonStandard::MyTableForEightParser,
        Parsers::NonStandard::ThePinJunkieParser,
        Parsers::NonStandard::WineAndGlueParser,
        Parsers::NonStandard::DessertNowDinnerLaterParser,
        Parsers::NonStandard::TheSweetChickParser,
        Parsers::NonStandard::CreationsByKaraParser,
        Parsers::NonStandard::SweetTwistOfBloggingParser,
        Parsers::NonStandard::EmbellishMintsParser,
        Parsers::NonStandard::SpecialFoodPointParser,
        Parsers::NonStandard::RainingHotCouponsParser,
        Parsers::NonStandard::ILoveMyDisorganizedLifeParser,
        Parsers::NonStandard::HighHeelsAndGrillsParser,
        Parsers::NonStandard::WillCookForSmilesParser,
        Parsers::NonStandard::ASliceOfStyleParser,
        Parsers::NonStandard::MarthaStewartParser,
        Parsers::NonStandard::FitnessMagazineParser,
        Parsers::NonStandard::RasamalaysiaParser,
        Parsers::NonStandard::CafedelitesParser,
        Parsers::NonStandard::EverydayAnnieParser,
        Parsers::NonStandard::PickyPalateParser,
        Parsers::NonStandard::HelloFreshParser,
        Parsers::NonStandard::FoodAndWineParser,
        Parsers::NonStandard::TablerPartyOfTwoParser,
        Parsers::NonStandard::MyMommyStyleParser,
        Parsers::NonStandard::HouseWifeEclecticParser,
        Parsers::NonStandard::MomCrieffParser,
        Parsers::NonStandard::SavyNaturalistaParser,
        Parsers::NonStandard::ListoticParser,
        Parsers::NonStandard::MoneySavingMomParser,
        Parsers::NonStandard::GimmeSomeOvenParser,
        Parsers::NonStandard::TheGardenGrazerParser,
        Parsers::NonStandard::AshleysFreshFixParser,
        Parsers::NonStandard::MyKitchenCrazeParser,
        Parsers::NonStandard::MomsWithoutAnswersParser,
        Parsers::NonStandard::GlutenFreeOnAShoeStringParser,
        Parsers::NonStandard::EatLiveRunParser,
        Parsers::NonStandard::EnrileMoineParser,
        Parsers::NonStandard::AmyDGorinParser,
        Parsers::NonStandard::BrittanySpantryParser,
        Parsers::NonStandard::RealMomKitchenParser,
        Parsers::NonStandard::CookingCarnivalParser,
        Parsers::NonStandard::PinchOfYumParser,
        Parsers::NonStandard::DamnDeliciousParser,
        Parsers::NonStandard::YourHomeBasedMomParser,
        Parsers::NonStandard::MyBakingAddictionParser,
        Parsers::NonStandard::PiaRecipesParser,
        Parsers::NonStandard::InspiredByCharmParser,
        Parsers::NonStandard::PopCultureParser,
        Parsers::NonStandard::MenuMUsingsBlogspotParser,
        Parsers::NonStandard::WeeliciousParser,
        Parsers::NonStandard::CouponsAndFreebiesMomParser,
        Parsers::NonStandard::RealSimpleParser,
        Parsers::NonStandard::SmartSchoolHouseParser,
        Parsers::NonStandard::EatWell101Parser,
        Parsers::NonStandard::AThriftyMomParser,
        Parsers::NonStandard::FoxAndBriarParser,
        Parsers::NonStandard::PaulaDeenParser,
        Parsers::NonStandard::ManilaSpoonParser,
        Parsers::NonStandard::TheFrugalGirlsParser,
        Parsers::NonStandard::KellystilWellParser,
        Parsers::NonStandard::CookingLightParser
      ]
      parser_classes += [JsonLDParser, SchemaOrgRecipeParser, HRecipeParser, DataVocabularyRecipeParser]
      parser_classes.each do |parser_class|
        parser = parser_class.new(@nokogiri_doc)
        return parser if parser.can_parse?
      end
      NullRecipeParser.new(@nokogiri_doc)
    end
  end
end
