module Hangry
  class RecipeAttributeCleaner
    MULTILINE_ATTRIBUTES = [:instructions]

    attr_reader :recipe

    def initialize(recipe)
      @recipe = recipe
    end

    def clean
      RECIPE_ATTRIBUTES.each do |attribute|
        recipe.public_send("#{attribute}=", clean_attribute(recipe, attribute))
      end
      recipe
    end

    private

    def clean_attribute(recipe, attribute)
      value = recipe.public_send(attribute)
      if respond_to? "clean_#{attribute}", true
        value = send("clean_#{attribute}", value)
      end
      clean_value(value, preserve_newlines: MULTILINE_ATTRIBUTES.include?(attribute))
    end

    def clean_value(value, options = {})
      case value
      when String
        clean_string(value, options)
      when Array
        value.map { |value| clean_value(value, options) }
      when Hash
        value.each do |key, value|
          next unless value
          recipe.nutrition[key] = clean_value(value, options)
        end
      when Hangry::RecipeParser::NullObject
        nil
      else
        value
      end
    end

    def clean_string(string, options = {})
      preserve_newlines = options.fetch(:preserve_newlines, false)

      string.strip!                     # remove leading and trailing spaces
      if preserve_newlines
        string.gsub!(/\s*\n\s*/, "\n")  # replace any whitespace group with a newline with a single newline
        string.squeeze!(' ')            # consolidate duplicate spaces into a single space
      else
        string.gsub!(/\s+/, ' ')        # replace all consecutive whitespace with a single space
      end
      string
    end

    def clean_instructions(text)
      return if text.nil?
      instructions = []
      split_index = text.index /[.|,|!](\s{4}|([^(\s|(&nbsp;)|(\u00A0;)|)|.|_|0-9|”|"|!)]|[0-9][.]))/
      return text unless split_index
      instructions << text.slice(0...split_index+1)
      text = text.slice(split_index+1..-1)
      instructions << clean_instructions(text)

      instructions.flatten.uniq.reject(&:blank?).join("\n")
    end
  end
end
