#encoding: UTF-8

describe Hangry do
  context "thebestblogrecipes.com doritos-cheesy-chicken-pasta-casserole" do
    before(:all) do
      @html = File.read("spec/fixtures/the_best_blog_recipes.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://thebestblogrecipes.com/doritos-cheesy-chicken-pasta-casserole/" }
    its(:image_url) { should == "http://thebestblogrecipes.com/wp-content/uploads/2017/02/easy-dorito-casserole-recipe.jpg" }
    its(:ingredients) { should == ["3 cups cooked chicken, *rotisserie has a great flavor and is easy to use in this recipe!", "1/2 of a 12oz bag of egg noodles", "1 tablespoon chives", "1 teaspoon garlic salt", "1 can cream of mushroom soup", "1 can cream of chicken soup", "1 can Rotel Mild Tomatoes", "1 can of corn, drained", "2 – 2.5 cups Mexican shredded cheese, divided", "1 bag of nacho cheese Doritos, crushed"] }
    its(:instructions) { should == "Cook your egg noodles per package directions, drain\nIn a large mixing bowl combine your egg noodles, cooked chicken, chives, garlic salt, cream of mushroom soup, cream of chicken soup, Rotel Tomatoes, and corn.\nStir until everything is well mixed.\nPlace 1/2 of the chicken mixture in a 9×13 casserole dish.\nSprinkle 1/2 of your cheese over the chicken mixture.\nPlace the rest of the chicken mixture on top of the cheese and spread out evenly.\nSprinkle the remaining cheese and cover with tin foil.\nBake at 400° for 20 minutes.\nRemove the tin foil and continue to bake for an additional 5 minutes.\nRemove from oven and sprinkle the crushed Doritos over the top of the casserole.\nServe and enjoy." }
    its(:name) { should == "Doritos Cheesy Chicken Pasta Casserole" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
