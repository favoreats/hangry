# encoding: UTF-8

describe Hangry do
  context "whole-sisters.com bbq sweet sriracha shrimp recipe" do
    before(:all) do
      @html = File.read("spec/fixtures/whole_sisters_sriracha_shrimp.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:image_url) { should == "https://www.whole-sisters.com/wp-content/uploads/2017/03/Sweet-Sriracha-Shrimp-275x275.jpg" }
    its(:canonical_url) { should == 'https://www.whole-sisters.com/bbq-sweet-sriracha-shrimp/' }
    its(:ingredients) { should == ["1 pound deveined large shrimp",
                                   "3 tablespoons pure maple syrup",
                                   "2 teaspoons Sriracha sauce",
                                   "1 lime, juiced",
                                   "dash of salt"] }
    its(:instructions) { should == "1. Soak skewers in cold water for 10-15 minutes. Then skewer 5 shrimp per skewer.\n2. Heat grill to medium high, lightly oil grill.\n3. Mix ingredients and with a basting brush apply sauce liberally to skewered shrimp.\n4. Barbecue for approximately 4 minutes per side. May baste more sauce between flipping if desired." }
    its(:author) { should == "Nan"}
    its(:name) { should == "BBQ Sweet Sriracha Shrimp" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
