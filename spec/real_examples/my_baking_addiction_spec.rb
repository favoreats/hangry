# encoding: UTF-8

describe Hangry do
  context 'mybakingaddiction.com holiday-recipe-exchange-pumpkin-molasses-cookies' do
    before(:all) do
      @html = File.read('spec/fixtures/my_baking_addiction_holiday_recipe_exchange_pumpkin_molasses_cookies.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.mybakingaddiction.com/holiday-recipe-exchange-pumpkin-molasses-cookies/' }
    its(:image_url) { should == 'https://www.mybakingaddiction.com/wp-content/uploads/images/Pumpkin-Molasses-Cookies-11.jpg' }
    its(:ingredients) { should == ['2 1/3 cups flour', '2 tsp baking soda', '1/2 tsp salt', '1 tablespoon pumpkin pie spice', '1/4 tsp black pepper', '8 Tbs butter, room temperature', '1 cup brown sugar, packed', '1/4 cup molasses', '2/3 cup pumpkin puree', '1 large egg', '1/2 cup sugar, for rolling'] }
    its(:instructions) { should == "1. Whisk together the flour, baking soda, salt, pumpkin pie spice, and pepper.\n2. Working with a stand mixer, preferably fitted with a paddle attachment, or a hand mixer in a large bowl, beat the butter on medium speed until smooth and creamy. Add the brown sugar, molasses, and pumpkin puree and beat for 2 minutes, scraping down the sides of the bowl as needed. Add the egg and beat for 1 minute more. Reduce the mixer speed to low and add the dry ingredients, mixing until the flour and spices disappear. If flour remains in the bottom of the bowl, mix the last of the dry ingredients by hand to avoid over beating. You will have a very soft dough.\n3. Divide the dough in half and wrap each piece in plastic wrap. Freeze for at least 30 minutes, or refrigerate for at least 1 hour. The dough is sticky, so the longer time it can chill the easier it is to work with.\n4. Preheat oven to 350 degrees F. Line 2 baking sheets with parchment paper.\n5. Put the sugar in a small bowl. Working with one packet of dough at a time, divide it into 12 pieces, and roll each piece into a ball. Roll the balls in the sugar and use a the bottom of a glass to press down on the cookies until they are between 1/4 and 1/2 inch thick. Transfer to cookie sheets. Do not over crowd.\n6. Bake the cookies one sheet at a time for 12-14 minutes, or until the top feels set to the touch. Remove baking sheets from the oven. Let cookies cool 5 minutes on the sheets before transferring them to a cooling rack.\n7. Repeat with second batch of dough." }
    its(:name) { should == 'Holiday Recipe Exchange | Pumpkin Molasses Cookies' }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
