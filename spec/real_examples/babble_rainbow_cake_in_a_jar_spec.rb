# encoding: UTF-8

describe Hangry do
  context "babble.com rainbow-cake-in-a-jar" do
    before(:all) do
      @html = File.read("spec/fixtures/babble_rainbow_cake_in_a_jar.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.babble.com/best-recipes/rainbow-cake-in-a-jar/" }
    its(:image_url) { should == "https://www.babble.com/wp-content/uploads/2011/04/BabbleJarRainbow3.jpg" }
    its(:ingredients) { should == ["1 box white cake mix made according to package instructions", "Neon food coloring in pink, yellow, green, turquoise, and purple", "3 one-pint canning jars", "1 can vanilla frosting", "Rainbow sprinkles"] }
    its(:instructions) { should == "Preheat oven to 350 degrees. Thoroughly wash and dry the inside of each canning jar. Spray the inside of each jar thoroughly with nonstick cooking spray. Set aside.\nScoop about 1/2 cups of cake batter into five small bowls. It doesn’t have to be perfect, don’t panic if you get a little more of less of one color than another. Tint each bowl of cake batter with the food coloring until very vibrant.\nSpoon about 3 tablespoons of the purple batter into the bottom of each jar. Spoon equal amounts of turquoise batter, then green, yellow, and pink. Place the jars in a shallow baking dish, add about 1/4″ in water in the baking dish. Place the baking dish in the oven and bake for 40-45 minutes.\nRemove jars and allow to cool completely before scooping a small portion from the top of your cake and adding a hefty dollop of vanilla buttercream to the top. Sprinkle & serve, or cover with a lid and store in the fridge or pop into the mail & surprise someone you love!"}
    its(:name) { should == "Rainbow Cake in a Jar" }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when jamaican_rice_salad recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/babble_jamaican_rice_salad.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients')]/../following-sibling::p[1] | //*[contains(text(),'Dressing')]/../following-sibling::p[1]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["3 cups cooked rice", "1 can black beans, rinsed and drained", "2 cups chopped pineapple, fresh or canned", "1/2 green bell pepper, chopped", "1/2 red bell pepper, chopped", "1/2 yellow bell pepper, chopped", "1/2 red onion, chopped", "the juice of 2 limes", "1/4 cup olive oil", "1/2 teaspoon cinnamon", "1/2 teaspoon nutmeg", "1/2 teaspoon garlic powder", "1/4 teaspoon allspice", "1 teaspoon cumin", "1 teaspoon coriander", "1 teaspoon dried thyme", "1 teaspoon black pepper"])
        end
      end

      context "and the selector '//*[contains(text(),'Method')]/../following-sibling::p | //*[contains(text(),'Method')]/..' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("1. Cook rice according to package directions. For more flavor, I cook it with chicken stock.\n2. Combine all ingredients for salad in a large bowl. In a separate bowl, combine all ingredients for dressing but oil. Whisk well. While whisking, stream in olive oil.\n3. Pour dressing over salad and toss well.")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Method:')]/following-sibling::p[not(em)]' is used in parse_instruction" do
      before(:each) do
        @html = File.read("spec/fixtures/babble_mini_cornbread_muffins_recipe.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Preheat oven to 425 degrees. Butter a mini muffin tin. In a large bowl, combine corn meal, flour, baking powder, baking soda, salt, and sugar.\nIn a small bowl, combine buttermilk, milk, and butter. Pour into dry ingredients and whisk to combine. Spoon into muffin tins. Bake for 10-12 minutes, or until lightly golden brown on the edges. Let cool in the muffin tin for 10 minutes before removing.")
      end
    end

    context "when the selector '//*[contains(text(),'Directions')]/following-sibling::p[not(strong)]' is used in parse_instruction" do
      before(:each) do
        @html = File.read("spec/fixtures/babble_pie_cinnamon_roll_cookies.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Combine flour, baking soda and salt in a small bowl, whisk until combined.\nIn your mixing bowl, combine butter, sugars, egg white and vanilla, mix until smooth.\nAdd in your flour mixture and mix until incorporated and smooth.\nChill the dough for 30 minutes, then roll it out between sheets of floured parchment paper until its about 8 x 12 or so. Combine the brown sugar and pumpkin pie spice for the filling together in a small bowl and sprinkle it liberally across the surface of the dough. Carefully roll the dough away from you into a log and refrigerate for 30 minutes.\nWhen ready to bake, gently slice log into 1/4 inch slices, place on a parchment paper lined baking sheet at least 1 inch apart and bake at 350 degrees.\nOnce cookies are cooled, make glaze by whisking powdered sugar, milk and pumpkin pie spice together. You want the glaze to be on the thicker side. Drizzle the glaze over-top the cookies and enjoy! Glaze will begin to harden.")
      end
    end
  end
end

