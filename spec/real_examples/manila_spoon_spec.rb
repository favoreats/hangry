# encoding: UTF-8

describe Hangry do
  context 'manilaspoon.com slow-cooker-chicken-adobo' do
    before(:all) do
      @html = File.read('spec/fixtures/manila_spoon_slow_cooker_chicken_adobo.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.manilaspoon.com/2016/04/slow-cooker-chicken-adobo.html' }
    its(:image_url) { should == 'https://3.bp.blogspot.com/-2Tqw1Ez0jmI/Vw1GXF2AAoI/AAAAAAADbl0/4_K1zMcIUugq7SvjpTB60M7BM8vDsWQvQCLcB/s1600/IMG_6745.jpg-1.jpg' }
    its(:ingredients) { should == ['3 pounds Chicken pieces (like thighs and drumsticks)', '2 Tablespoons Oil', '1/2 cup white Vinegar (preferably the Cane Vinegar from the Philippines)', '1/3 cup Soy Sauce', '1/4 cup Water (optional – omit, if you want a more intense flavor or don’t want it diluted)', '1 Whole head Garlic, cloves separated, crushed, left unpeeled or peeled', '2 teaspoons whole Black Peppercorns (or freshly ground pepper – to taste)', '3 Bay leaves', '1 Tablespoon Sugar or to taste (for those who prefer a slightly sweeter Adobo but leave out if doing low-carb)'] }
    its(:instructions) { should == "Heat the oil in a large frying pan over medium heat. Brown all the chicken pieces in batches. This step is highly suggested but if you are in a hurry, this may be omitted. Alternatively, you can use skinless chicken pieces to avoid the browning process. Mix together in a bowl the vinegar, soy sauce and water.\nPlace the browned chicken pieces in the slow cooker. Pour the vinegar mixture all over the chicken. Evenly distribute the garlic, peppercorns and bay leaves. If you don’t fancy picking out the peppercorns on your plate, feel free to replace with freshly ground pepper but adjust the amount to your taste preference (a teaspoon to begin with). Cook on low for 4-6 hours or until chicken is tender.\nNOTE: If you want a slightly sweeter adobo, add the sugar to the vinegar mixture or towards the end you can also sprinkle the sugar all over and gently stir it in. Serve with freshly cooked rice. Can be made a day or 2 ahead." }
    its(:name) { should == 'Slow Cooker Chicken Adobo' }

    context 'when cranberry-choco-chip-cookies recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/manila_spoon_cranberry_choco_chip_cookies.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients')]/following::p[./following::*[contains(text(),'Procedure')]]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['1 1/2 cups flour', '1 tsp baking soda', '1 tsp cinnamon', '1/4 tsp nutmeg (optional)', '1 tsp salt', '3 cups rolled oats (I used quick cooking oats) 3/4 cup unsalted butter, softened or room temp (1 1/2 sticks) – (if using salted butter lessen the salt to 1/2 tsp)', '1 cup dark brown sugar, firmly packed', '1/4 cup white sugar', '2 eggs', '1 tsp vanilla extract 1 cup fresh cranberries, coarsely chopped (Yes, the fresh ones are better.)', '1 cup semi-sweet chocolate chips'])
        end
      end

      context "and the selector '//*[contains(text(),'Procedure')]/following::p[./following::*[contains(text(),'you like what')]]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Preheat the oven to 350 F. Prepare two cookie sheets. Grease or line them with parchment paper.\nSift into a big bowl or whisk together the flour, baking soda, cinnamon, nutmeg and salt. Add the oats to the flour mixture and mix well.\nIn a mixing bowl, beat together the butter, sugar, eggs and vanilla extract just until combined. My butter was already melted when I combined it with the other ingredients that’s why it looks like this – more liquidy (I was in a hurry and had to melt them instead). However, if you have the luxury of time – room temp butter (softened) is so much better because the cookies would be more chewy and moist. If the butter is just softened it will only melt while at the oven hence the cookies retain more moisture. The taste will be the same either way though.\nAdd the egg mixture to the dry ingredients. Mix well until all the ingredients are moist.\nFold well into the batter the chopped cranberries and chocolate chips. Drop the mixture by rounded tablespoons onto the greased cookie sheets. An ice cream scooper is so helpful for this. I used a large one. Do not flatten the cookies. Bake for 10-12 minutes (depending on how big your cookies are, mine were big so I cooked for about 12 minutes but if you use a tablespoon as measurement then 10 minutes is perfect!) or until lightly browned. Do not overcook.\nLet stand for 5 minutes before you transfer to a wire rack to cool completely. They will be quite soft so you need to let them sit a bit. Makes between 50-55 cookies if you are using a rounded tablespoon. I used a large ice cream scoop the first time so I only managed to make about 2 dozen but the cookies were huge.\nPerfect Christmas cookies in my opinion. Chewy, moist and delicious!")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients')]/following::li[./following::*[contains(text(),'Procedure')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/manila_spoon_italian_sausage_and_white_bean_cassoulet.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(['2 Tablespoons Olive oil, plus more if needed', '2 lbs Italian Sausage links', '1 large Onion, chopped', '4-6 Garlic cloves, crushed', '1 lb Cherry Tomatoes', '2 Tablespoons Balsamic Vinegar', '2 teaspoons dried Thyme', '3 Bay leaves', '3 (16 oz) cans White Beans, undrained', 'Salt and Pepper, to taste'])
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
