#encoding: UTF-8

describe Hangry do
  context "www.tablerpartyoftwo.com smores-bites-betcha-cant-eat-just-one" do
    before(:all) do
      @html = File.read("spec/fixtures/smores_bites_betcha_cant_eat_just_one.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:image_url) { should == "https://i2.wp.com/www.tablerpartyoftwo.com/wp-content/uploads/2015/06/SmoresBItes.jpg?resize=640%2C960" }
    its(:ingredients) { should == ["7 whole graham crackers, finely crushed (equals one cup)", "1/4 cup powdered sugar", "6 tablespoons butter, melted", "2 dark or milk chocolate candy bars (1.55 oz each), divided into rectangles", "12 large marshmallows, cut in half", "mini muffin pan (NOTE: I used a regular muffin pan. I think they would have been better in minis)"] }
    its(:instructions) { should == "Preheat oven to °F\nGrease pan cooking spray\nIn a medium bowl, mix graham cracker crumbs, powdered sugar and butter\nEvenly distribute the mixture into the muffin cups\nPress along the bottom and sides to make shallow cups\nBake for five minutes\nWhile cups are baking, cut marshmallows in half using scissors and divide chocolate candy bars into rectangles\nRemove pan from the oven and place one candy bar rectangle in each cup\nPlace half a marshmallow, cut side facing down, on each candy bar rectangle\nTurn oven on broil\nBroil for about two minutes Watch them carefully! I ended up rotating my pan around to make sure they all browned\nLet them cool for - minutes, then carefully pop them out with a spoon\nS’more Bites are a great dessert to serve at a party They were definitely a crowd-pleaser when I served them at our seafood boil a few weeks ago!\nRowdie used his puppy powers to try to convince me to give him one But I had to say “No, they are for the adults, only”  🙂" }
    its(:name) { should == "S’mores Bites – Betcha Can’t Eat Just One!" }

    context "when kale-salad-chick-peas-sun-dried-tomatoes recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/kale_salad_chick_peas_sun_dried_tomatoes.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients')]/../following::p[1]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1 small bunch curly kale, trimmed and thinly sliced", "1 (28 ounce) can chickpeas, rinsed and drained", "1/2 cup pine nuts", "1/2 cup sundried tomatoes, thinly sliced", "3 tablespoons scallions, thinly sliced", "4 tablespoons extravirgin olive oil", "2 tablespoons fresh lemon juice", "3 or 4 drops of Tabasco Sauce", "2 teaspoons red wine vinegar", "4 tablespoons fresh mint leaves, roughly chopped", "1 teaspoon garlic, grated", "sea salt and freshly ground black pepper"])
        end
      end

      context "and the selector '//*[contains(text(),'Instructions')]/../following::p[./following::*[contains(text(),'xoxo')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Place the sliced kale into a bowl Add tablespoon olive oil, then season with salt and pepper Massage kale until it is well coated in oil Set aside at room temperature\nNext, place pine nuts in a skillet and toast over low heat, stirring frequently, until they turn golden brown Set them aside to cool\nAdd the sun-dried tomatoes, chickpeas and pine nuts to the kale In a bowl, combine the garlic, lemon juice, Tabasco, remaining olive oil, vinegar and mint Season with salt and pepper and stir well I picked one of my first lemons from our little, dwarf Myer lemon tree for this recipe Talk about fresh!\nToss the dressing with the salad mixture and serve\nIt’s so good, I had seconds It’s my new favorite salad!\nDo you have a favorite kale recipe? Please share!")
        end
      end
    end

    context "when grilled-flank-steak-salad recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/tabler_party_of_two_grilled_flank_steak_salad.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'INGREDIENTS')]/..' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["Grilled Flank Steak with Chimichurri Sauce (here is my recipe)", "Baby greens", "Grape tomatoes, sliced", "Red Onion, sliced", "Green olives, sliced", "Pine nuts"])
        end
      end

      context "and the selector '//*[contains(text(),'INSTRUCTIONS')]/..' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Blend one tablespoon (or more, to your taste) into left over Chimichurri Sauce\nToss all ingredients together and serve chilled or at room temperature\n ")
        end
      end
    end

    context "when balsamic-brown-sugar-rosemary-pork-chops recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/tabler_party_of_two_balsamic_brown_sugar_rosemary_pork_chops.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(@class,'entry-content')]/ul[1]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["4 chops", "1/2 cup brown sugar", "2 Tablespoons balsamic vinegar", "2 sprigs of rosemary with the leaves pulled from the stem", "1 teaspoon minced garlic", "a dash of salt and pepper"])
        end
      end

      context "and the selector '//*[contains(@class,'entry-content')]/ul[2]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("heat a cast iron or non-stick skillet over medium heat with tablespoon of olive oil\nplace the chops in the skillet, adding all of the brown sugar sauce to the pan\ncook the chops on each side until dark brown and caramelized")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Directions')]/../following::p[./following::*[contains(text(),'xoxo')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/skinny_cake_dip.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Place the Cool Whip and Yoplait Greek Whips! in a medium mixing bowl and stir until mixed thoroughly Add cake mix a little at a time Stir until mixture is smooth and then add more cake mix Continue until all cake mix has been added and there are no lumps Chill for - hours Garnish with rainbow sprinkles before serving\nYoplait Greek Whips! come in eight delicious flavors They have only calories plus  fat and grams of protein! Right now you can download a coupon to save $ when you buy five cups of any variety Yoplait Greek, Yoplait Greek or Yoplait Greek Whips! yogurt What dessert would you whip up with greek yogurt?\nThanks to Yoplait Greek Whips! for sponsoring today’s discussion")
      end
    end

    context "when the selector '//*[contains(text(),'Assembly')]/../following::p[./following::*[contains(text(),'Coupon')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/tabler_party_of_two_edible_christmas_tree.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Set your cone on the serving tray As an option, you can use toothpicks to attach lettuce over the entire tree so that the background is green I did not do that this week (I’m waging battle against perfectionism this holiday season!) Starting at the base of the tree, attach broccoli florets, starting with the larger ones Continue around and up the tree, adding tomatoes, carrots, olives and Giardiniera to fill in gaps between florets\nFor the top of the tree, save the largest round carrot piece in the jar of Giardiniera Cut it into the shape of a star before adhering to the top of the tree with a final toothpick\nSee that little gap showing the white styrofoam cone? It’s proof I’m working on combatting perfectionism this Christmas!\nIt’s really fun to make this creative appetizer!\nServe with dip (I used jalapeno ranch) and the Mezzetta Grilled Artichokes with remaining Giardiniera veggies, along with the cheese, crackers and salami antipasta board So pretty! And so FLAVORFUL!\n \nThe grilled artichoke heart is the perfect topper for my cracker with cheese and salami!")
      end
    end

    context "when the selector '//*[contains(text(),'DIRECTIONS')]/..' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/tabler_party_of_two_july_4th_cocktail_berry_coconut_spritz.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Muddle the blueberries and raspberries in a pitcher\nAdd the lemon juice and coconut rum\nStir until well combined \nAdd the ginger ale and gently stir until combined\nGarnish rims of glasses with coconut flakes (optional)\nFill glasses with ice and add Berry Coconut Spritz")
      end
    end
  end
end
