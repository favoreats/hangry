# encoding: UTF-8

describe Hangry do
  context 'couponsandfreebiesmom.com homemade-nutella-pop-tarts.html' do
    before(:all) do
      @html = File.read('spec/fixtures/coupons_and_freebies_mom_homemade_nutella_pop_tarts.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.couponsandfreebiesmom.com/2014/04/homemade-nutella-pop-tarts.html' }
    its(:image_url) { should == 'https://www.couponsandfreebiesmom.com/wp-content/uploads/2014/04/Homemade-Nutella-Pop-Tarts.jpg' }
    its(:ingredients) { should == ['2 1/2 cup Flour', '1 tbsp Sugar', '1/2 tsp Salt', '1 cup Butter', '2 Eggs', '2 tbsp Milk', '9 tbsp Nutella (or use our Homemade Nutella recipe)'] }
    its(:instructions) { should == "Cut butter into small cubes.\nCombine flour, sugar and salt in a large bowl.\nWork butter into the flour mixture with a pastry cutter.\nWhisk together one egg and milk and add to the flour mixture.\nKnead until ball forms.\nTurn dough out on to floured surface and knead for 1 minute.\nRefrigerate dough for 30 minutes.\nPreheat oven to 350.\nDivide in half.\nOn floured surface, roll out dough into a 9″ × 12” rectangle.\nCut into 9 rectangles.\nRepeat with other piece of dough.\nMake an eggwash by beating the remaining egg and brush over entire surface of first 9 rectangles.\nPlace a tablespoon of Nutella on top of each egg washed rectangle and spread. Be sure to leave 1/2″ bare around edges.\nPlace a rectangle on top of filling and press with fingertips to seal around edges.\nUse the tines of a fork to seal all around.\nPoke top with fork to allow steam to vent.\nPlace on parchment lined baking sheet and bake for 25 minutes or until lightly browned.\nCool on wire rack." }
    its(:name) { should == 'Homemade Nutella Pop Tarts' }

    context "when the selector '//*[contains(text(),'Ingredients') or contains(text(),'INGREDIENTS')]/following::p[./following::*[contains(text(),'Directions') or contains(text(),'DIRECTIONS')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/coupons_and_freebies_mom_curried_roasted_purple_cabbage.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['1 Large Purple Cabbage', '2 Tablespoons Extra Virgin Olvie Oil', '1 Teaspoon Curry Powder', '1 Teaspoon Garlic Powder', '1/4 Teaspoon Cumin', '1/2 Teaspoon Salt', '1/2 Teaspoon Cracked Black Pepper'])
      end
    end

    context "when the selector '//*[contains(text(),'INGREDIENTS')]/following::ul/li[./following::*[contains(text(),'DIRECTIONS') or contains(text(),'Preparation')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/coupons_and_freebies_mom_chocolate_drizzled_cream_cheese_crescent_rolls.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['1 can Crescent Dinner rolls', '1/4 cup Powdered Sugar', '4 oz Cream Cheese softened', '1 1/2 tsp cool strong Coffee', '1 tsp Vanilla', '1/4 cup chopped Macadamia Nuts', '1/4 cup Chocolate Chips', '1/2 tsp Shortening'])
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
