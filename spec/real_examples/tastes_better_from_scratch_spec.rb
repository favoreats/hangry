#encoding: UTF-8

describe Hangry do
  context "tastesbetterfromscratch.com mint-chocolate-chip-cookies" do
    before(:all) do
      @html = File.read("spec/fixtures/tastes_better_from_scratch.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://tastesbetterfromscratch.com/mint-chocolate-chip-cookies/" }
    its(:image_url) { should == "https://tastesbetterfromscratch.com/wp-content/uploads/2013/04/Mintcookiewatermark.jpg" }
    its(:ingredients) { should == ["1 cup butter (, softened)", "1 1/2 cup granulated sugar", "2 large egg", "1 teaspoon vanilla extract", "2 cup all-purpose flour", "2/3 cup unsweetened cocoa powder", "1/2 teaspoon baking soda", "1/4 teaspoon salt", "1 package mint chocolate chips (I use the Andes mint pieces, found in the baking aisle in the bright green Andes mints 12 oz. bag, at the grocery store.)"] }
    its(:instructions) { should == "Preheat oven to 350 degrees.\nIn a large bowl combine butter, sugar, eggs, and vanilla and beat until light and fluffy.\nStir together flour, cocoa, baking soda, salt; add to butter mixture. Stir in chocolate chips.\nDrop by rounded teaspoons onto an ungreased cookie sheet. Bake 8-10 minutes or just until set. (Cookies may appear slightly doughy but will set up after removing them from the oven to cool for a few minutes.)" }
    its(:name) { should == "Mint Chocolate Chip Cookies" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
