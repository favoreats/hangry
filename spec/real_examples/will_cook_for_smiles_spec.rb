# encoding: UTF-8

describe Hangry do
  context "willcookforsmiles.com chocolate-pumpkin-cheesecake" do
    before(:all) do
      @html = File.read("spec/fixtures/will_cook_for_smiles_chocolate_pumpkin_cheesecake.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == "Chocolate Pumpkin Cheesecake" }
    its(:canonical_url) { should == "https://www.willcookforsmiles.com/chocolate-pumpkin-cheesecake/" }
    its(:image_url) { should == "https://www.willcookforsmiles.com/wp-content/uploads/2013/09/Pumpkin-Chocolate-Cheesecake-4-c-willcookforsmiles.com-pumpkin-cheesecake-chocolate-1.jpg" }
    its(:ingredients) { should == ["1 1/2 cups graham cracker crumbs", "1/4 cup dark brown sugar", "5 tbs butter (melted)", "1 tsp vanilla extract", "1/4 tsp ground cinnamon", "Pinch ground nutmeg", "4.4 oz chocolate bar (broken in pieces)", "24 oz cream cheese (softened)", "1 cup pumpkin puree", "2 tbsp sour cream", "2 eggs (room temperature)", "1 tsp vanilla extract", "1 1/2 tbsp corn starch", "1 cup brown sugar", "1 tsp ground cinnamon", "1/2 tsp ground all spice", "1/4 tsp ground nutmeg", "1 cup pecans (chopped)", "Chocolate syrup or hot fudge topping"] }
    its(:instructions) { should == "Preheat the oven to 325 degrees and grease a 9 inch springform pan.\nMix the ingredients for the crust, very well, until all evenly incorporated. (Not the chocolate pieces.) Press graham cracker mixture into the pan, evenly, all over the bottom and about half way up the side.\nBreak up the chocolate bar and lay the chocolate pieces all over the crust, evenly.\nStart beating the cream cheese in a mixer, on medium-high speed, for about 2 minutes.\nAdd the vanilla extract, sour cream, and pumpkin puree. Beat until completely combines. Scrape sides and bottom of the bowl. \nAdd the eggs, one at the time, beating after each addition. Scrape sides and bottom of the bowl.\nAdd sugar, spice and corn starch. Mix on low speed. Make sure that all ingredients are well combined.\nPour the cheesecake batter into the pan with crust. Sprinkle chopped pecans over the top.\nBake for 55-60 minutes. \nTake it out of the oven and let it rest for 10 minutes on the counter and then gently run a greased butter knife between sides of the cheesecakes and the springform to carefully separate it. (You don't actually have to take the cheesecake out of the pan until ready to frost.)\nCool cheesecake for an hour and then place it in the refrigerator. Refrigerate for at least 4 hours before servings. \nDrizzle chocolate syrup of hot fudge topping over the cheesecake when ready to serve. (If you put it on hot cheesecake, most of it will run off fast.)" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end