# encoding: UTF-8

describe Hangry do
  context 'moneysavingmom.com easy-individual-mini-meat-lasagnas-freezer-friendly' do
    before(:all) do
      @html = File.read('spec/fixtures/money_saving_mom_easy_individual_mini_meat_lasagnas_freezer_friendly.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://moneysavingmom.com/2017/08/easy-individual-mini-meat-lasagnas-freezer-friendly.html' }
    its(:image_url) { should == 'https://img.moneysavingmom.com/wp-content/uploads/2017/08/Mini-Freezer-Friendly-Lasagnas-564x1500.png' }
    its(:ingredients) { should == ['3 pounds ground beef', '2 Tablespoons minced dried onion (optional)', '1 3/4 teaspoon salt', '1/2 teaspoons black pepper', '2 Tablespoon fresh basil, chopped', '3 teaspoons fresh parsley, chopped', '1 1/2 teaspoon Italian seasoning', '72 ounces spaghetti sauce (3 24-ounce jars)', '24 ounces cottage cheese (about 2 3/4 cups)', '30 ounces ricotta cheese (about 3 1/4 cups)', '1/4 cup parmesan cheese', '2 eggs', '18 ounces oven-ready lasagna noodles, broken in half', '2 pounds mozzarella cheese (1 pound thinly sliced, the other pound shredded)', 'Mini Foil Pans'] }
    its(:instructions) { should == "Brown ground beef with minced onions in a large pan. When meat is cooked, drain off fat. Stir in spaghetti sauce, 1 1/2 teaspoons salt, 1/8 teaspoon black pepper, basil, 2 teaspoons chopped parsley, and Italian seasoning.\nIn another bowl, combine cottage cheese, ricotta cheese, parmesan cheese, 1/8 teaspoon black pepper, 1/4 teaspoon salt, 1 teaspoon parsley, and eggs.\nSpread a layer of meat sauce in the bottom of the mini foil pans. Layer uncooked noodles, sliced cheese, cottage cheese mixture, and meat mixture as desired, ending with meat mixture. Sprinkle with shredded mozzarella cheese.\nBake uncovered lasagnas at 350 degrees for 30-35 minutes, or until bubbly and cheese is somewhat crispy. Let cool, cover pans securely with foil, and freeze.\nTo prepare, bake uncovered, frozen lasagnas at 350 degrees for 1 hour. You can also prepare the lasagnas in the microwave. Pop them out of the foil pans, place on a microwave-safe dish, and microwave on high for 8-10 minutes. (You may need to thaw them slightly in order to be able to remove them from the pans.)" }
    its(:name) { should == 'Easy Individual Mini Meat Lasagnas {Freezer-Friendly!}' }

    context 'when do-it-yourself-homemade-rice-milk recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/money_saving_mom_do_it_yourself_homemade_rice_milk.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@class='pf-content']/ul[1]/li' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['1 cup cooked rice (I prefer to use brown organic for a more nutritious option. We buy it in bulk to save money.)', '4 cups water (filtered, if possible)', 'Dash salt (I recommend', 'RealSalt.', ')', '1 Tablespoon sweetener (or to taste; optional)', 'Additional flavoring (optional; vanilla for vanilla-flavored milk; to taste)'])
        end
      end

      context "and the selector '//*[contains(text(),'Method')]/following::p[1]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Place all ingredients in blender.\n2. Blend for four minutes in a regular blender, or two minutes in high-speed blender like a Vitamix.\n3. Enjoy!")
        end
      end
    end

    context "when the selector '//*[contains(text(),'adapted')]/following::p[./following::*[contains(text(),'Preheat')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/money_saving_mom_glazed_cinnamon_scones_recipe.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['Scone Ingredients:', '2 cups flour (I used unbleached, but you could use half whole-wheat, half unbleached)', '2 teaspoons baking powder', '1/2 teaspoon baking soda', '1/2 teaspoon salt', '1/2 cup butter', '1 egg, separated', '3 Tablespoons honey', '1/3 cup buttermilk (or 1/3 cup milk mixed with 1/2 teaspoon of lemon juice)', 'Crumb Topping (can be adjusted to your liking):', '1-2 Tablespoons turbinado (or sugar)', '1/2 teaspoon cinnamon', 'Glaze Ingredients (can be adjusted to your liking):', '1 cup powdered sugar (you can', 'make your own homemade unprocessed powered sugar', ')', '1-3 teaspoons milk (enough to make a glaze)', '1/2 teaspoon vanilla'])
      end
    end

    context "when the selector '//*[@class='print-this-content']/ol/li' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/money_saving_mom_sugar_free_baked_oatmeal.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Preheat oven to 350 degrees.\nIn a large bowl, mix eggs, vanilla, applesauce, and Stevia until combined.\nAdd Sucanat, banana, and other fruit of your choice. (If using chocolate chips add them last)\nIn another bowl, mix oats, salt, baking powder, flax seed, and cinnamon.\nDump dry ingredients into wet ingredients; mix well.\nPour in milk and stir until combined.\nSpray one 12 and one 6 cup muffin tin with cooking spray or use cupcake liners.\nScoop mixture evenly into muffin cups.\nBake 35-40 minutes or until the center of each muffin is set.\nCool and enjoy — or freeze them in gallon freezer bags.")
      end
    end

    context "when the selector '//*[contains(text(),'DIRECTIONS:')]/following::p[./following::*[contains(text(),'NOTE')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/money_saving_mom_chocolate_raspberry_cheesecake_pie.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Preheat the oven to 350 degrees.\nArrange raspberries on the bottom of the crust (if using frozen raspberries, do not thaw; if using fresh, make sure they are not wet).\nBeat together cream cheese and sweetened condensed milk until smooth. Beat in egg, lemon juice, and vanilla. Mix well.\nCarefully pour on top of the raspberries.\nBake 350 degrees for 30-35 minutes, or just until set. Cool completely.\nMelt the semi-sweet chocolate and whipping cream together over low heat, stirring often (I do this in the microwave).\nPour over the cheesecake. Chill several hours before serving.")
      end
    end

    context "when the selector '//*[contains(text(),'DIRECTIONS')]/following::ul[1]/li' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/money_saving_mom_chewy_no_bake_granola_bars.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Combine first 4 dry ingredients in a bowl.\nIn a saucepan, mix the honey, brown sugar and salt. Over medium-low heat, stir until the mixture comes to a complete boil for 30-60 seconds.\nRemove from heat and stir in the peanut butter and vanilla until smooth.\nPour over the dry ingredients and mix well.\nLet cool for 5-10 minutes before mixing in the chocolate chips.\nPress into a greased 9×13 pan and let cool before cutting into bars.")
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
