# encoding: UTF-8

describe Hangry do
  context "foodiecrush.com baked cake cup recipe" do
    before(:all) do
      @html = File.read("spec/fixtures/foodiecrush_baked_cake_cup.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:description) { should == 'Fun and creative treat that can be whipped up in just 30 minutes.' }
    its(:image_url) { should == "https://www.foodiecrush.com/wp-content/uploads/2011/09/FoodieCrush-Apple-Cups.jpg" }
    its(:ingredients) { should == ["1½ sheets frozen puff pastry", "2 T melted butter", "2 tsp sugar", "3 apples, cored and thinly sliced", "2 T plus 1 tsp brown sugar", "½ tsp cinnamon", "⅛ tsp nutmeg", "2 tsp lemon juice", "1 tsp zested lemon rind"] }
    its(:instructions) { should == "Preheat oven to 350 degrees. Thaw puff pastry dough and cut into six pieces, discarding remaining two pieces. Brush with melted butter and place into greased 6 X 1 cup muffin tins. Sprinkle pastry with refined sugar. Combine remaining ingredients reserving 1 tsp brown sugar. Divide between tins and sprinkle with remaining brown sugar. Bake for 25-30 minutes or until pastry is golden. Serve with vanilla ice cream." }

    its(:name) { should == "Baked Apple Cups" }
    its(:yield) { should == 'serves 6' }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
