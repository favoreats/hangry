# encoding: UTF-8

describe Hangry do
  context "superhealthykids.com oatmeal christmas cookies" do
    before(:all) do
      @html = File.read("spec/fixtures/super_healthy_kids_oatmeal_christmas_cookies.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == 'Lizzy Early' }
    its(:image_url) { should == "https://shk-images.s3.amazonaws.com/wp-content/uploads/2014/09/Oatmeal-Christmas-Cookies-6-300x300.jpg" }
    its(:ingredients) { should == ["1 1/4 cup – oats, dry",
                                   "1/4 cup – oats, quick-cooking, dry",
                                   "1 teaspoon – cinnamon",
                                   "1/4 teaspoon – salt",
                                   "1 teaspoon – vanilla extract",
                                   "1 large – egg",
                                   "1/4 cup packed – brown sugar",
                                   "1/2 cup – coconut oil",
                                   "1/2 cup – cranberries, dried",
                                   "1/3 cup chips – white chocolate chips",
                                   "2 teaspoon – coconut oil" ] }
    its(:instructions) { should == "Take 3/4 cup of old fashioned oats and turn it into oat flour by zipping it in a blender or food processor. Add to bowl with oats, quick oats, cinnamon, salt, vanilla, egg (at room temperature), brown sugar and melted coconut oil. Stir until combined.\nAdd cranberries and stir again.\nLet chill in the fridge for 30 minutes. This solidifies the coconut oil and makes the dough thick enough to scoop.\nPreheat the oven to 375 degrees and make small dough balls and place on cookie sheets. Slightly flatten your dough balls so they don't bake too tall! Bake for 7-9 minutes.\nLet cool.\nUsing a double boiler or the microwave, combine white chocolate chips and coconut oil. Stir until smooth and then dip cookies and add sprinkles!" }
    its(:name) { should == "Oatmeal Christmas Cookies" }
    its(:prep_time) { should == 35 }
    its(:cook_time) { should == 7 }
    its(:yield) { should == '12' }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
