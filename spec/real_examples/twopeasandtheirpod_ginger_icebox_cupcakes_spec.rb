# encoding: UTF-8

describe Hangry do
  context 'twopeasandtheirpod.com Ginger Icebox Cupcakes' do
    before(:all) do
      @html = File.read('spec/fixtures/twopeasandtheirpod_ginger_icebox_cupcakes.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == 'Ginger Icebox Cupcakes' }
    its(:author) { should == 'Two Peas &amp; Their Pod' }
    its(:yield) { should == '8 cupcakes' }
    its(:description) { should == 'These easy no-bake Ginger Icebox Cupcakes are perfect for any holiday party!' }
    its(:image_url) { should == 'https://www.twopeasandtheirpod.com/wp-content/uploads/2012/12/Ginger-Icebox-Cupcakes2.jpg' }
    its(:ingredients) { should == ["2 cups heavy cream",
                                   "3 tablespoons powdered sugar",
                                   "1/2 teaspoon vanilla extract",
                                   "1/8 teaspoon ground cinnamon",
                                   "1/8 teaspoon ground nutmeg",
                                   "1 (5.25 ounce) box Anna's Ginger Thins",
                                   "Sprinkles, optional"
    ] }
    its(:instructions) { should == "1. In the bowl of a stand mixer, whip the cream, powdered sugar, and vanilla together on high speed until medium peaks form. Gently fold in the cinnamon and nutmeg.\n2. To assemble the cupcakes, spread about 1 tablespoon of the whipped cream on top of a ginger thin. Place another ginger thin on top of the cream to make a sandwich. Spread more whipped cream over the thin and add another thin. Top the final ginger thin with cream. You want to use four wafers per cupcake and end with cream.\n3. Place cupcakes in cupcake liners or on a plate and stick in the refrigerator for at least 2 hours. You can refrigerate them overnight. The whipped cream will soften the ginger thins making them soft like cake.\n4. When ready to serve, remove the cupcakes from the refrigerator and sprinkle with sprinkles, if desired." }

    context 'when brown_butter_buttermilk_syrup recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/two_peas_and_their_pod_brown_butter_buttermilk_syrup.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector 'h1.post-title' is used in the parse_name method" do
        it 'should be able to parse the name' do
          expect(subject.name).to eq('Brown Butter Buttermilk Syrup')
        end
      end

      context "and the selector '[class*='wp-image']' is used in the parse_image_url method" do
        it 'should be able to parse the image_url' do
          expect(subject.image_url).to eq('https://www.twopeasandtheirpod.com/wp-content/uploads/2013/09/Brown-Butter-Buttermilk-Syrup-6.jpg')
        end
      end

      context "and the selector '.instructions' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("In a small saucepan, melt the butter over medium-low heat and continue to cook, swirling occasionally, until butter turns golden brown, Skim foam from top, and remove from heat. Pour into a bowl to stop the cooking, leaving any burned sediment behind.\nCombine browned butter, sugar, and buttermilk in a large saucepan. Heat over medium heat and whisk together until sugar dissolves. When butter mixture starts to boil, carefully whisk in the baking soda and vanilla. The mixture will bubble up, so make sure you use a large saucepan. Serve with waffles, pancakes, or French toast.\nNote-syrup will keep in the refrigerator for up to 2 weeks. Reheat before serving.")
        end
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
