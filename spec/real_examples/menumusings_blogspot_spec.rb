# encoding: UTF-8

describe Hangry do
  context 'menumusings.blogspot.com avocado-lime-vinaigrette' do
    before(:all) do
      @html = File.read('spec/fixtures/menumusings_blogspot_avocado_lime_vinaigrette.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'http://menumusings.blogspot.com/2012/07/avocado-lime-vinaigrette.html' }
    its(:image_url) { should == 'http://1.bp.blogspot.com/-mfSlGbTslCI/URzfUcuyKpI/AAAAAAAAPH8/9erkfEoLPxc/s1600/17.5+-+Copy+skinny+labeled.jpg' }
    its(:ingredients) { should == ['1/4 cup fresh squeezed lime juice', '1 jalapeno pepper', '1 medium/large avocado', '1/3 cup cilantro', '1 Tbsp olive oil', '5 Tbsp water', 'kosher salt and black pepper to taste'] }
    its(:name) { should == 'Avocado Lime Vinaigrette' }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
