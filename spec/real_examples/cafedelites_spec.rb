# encoding: UTF-8

describe Hangry do
  context "cafedelites.com mongolian-glazed-meatballs" do
    before(:all) do
      @html = File.read("spec/fixtures/cafedelites_mongolian_glazed_meatballs.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://cafedelites.com/mongolian-glazed-meatballs/" }
    its(:image_url) { should == "https://i1.wp.com/cafedelites.com/wp-content/uploads/2016/12/Mongolian-Glazed-Meatballs-Appetizers-24-1.jpg?fit=800%2C1200&ssl=1" }
    its(:ingredients) { should == ["2 pounds | 1 kilogram ground beef mince ((or pork, chicken, turkey))", "3/4 cup Panko or breadcrumbs", "2 eggs", "2 tablespoons low sodium soy sauce", "1 tablespoon Shaoxing ((Chinese) wine*)", "1 tablespoon minced garlic", "1 teaspoon minced ginger", "1 cup thinly-sliced or chopped green onions ((4 green onions))", "Kosher salt and pepper (, to taste)", "1 teaspoon sesame oil", "4 cloves (or 1 tablespoon) garlic, minced", "½ tablespoon minced ginger", "½ cup low sodium soy sauce", "⅔ cup water", "1/2 cup brown sugar", "2 tablespoons hoisin sauce", "1 tablespoon oyster sauce", "1 tablespoon ground white pepper ((or ½-1 tablespoon red chilli / powder))", "Sesame seeds", "1 green onion (, finely sliced)"]}
    its(:instructions) { should == "Preheat oven to 200°C | 400°F.\nMix together all of the meatball ingredients in a large bowl until well-combined. Spoon out 1-inch of meat mixture with a cookie scoop (or tablespoon), shape into balls and place onto 2 large baking sheets or trays lined with baking or parchment paper.\nBake for 10-12 minutes, or until golden browned and cooked through.\nWhile the meatballs are baking, make your sauce!\nHeat a non-stick pan or skillet over medium-high heat. Whisk ALL of the sauce ingredients together in the pan until well blended. Bring to a simmer and continue cooking until sauce thickens, while stirring occasionally to prevent burning or sticking on the bottom of the pan (about 8 minutes).\nWhen meatballs have finished cooking, add half of the meatballs into the pan and gently coat each meatball generously and evenly in the sauce. Transfer the glazed meatballs to a serving dish or tray with a slotted spoon and add in the remaining non-coated meatballs to the sauce. Coat and transfer to serving dish.\nSprinkle with sesame seeds and sliced green onions (if desired), and serve warm with toothpicks or mini forks." }
    its(:name) { should == "Mongolian Glazed Meatballs" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end