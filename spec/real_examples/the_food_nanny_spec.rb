# encoding: UTF-8

describe Hangry do
  context "thefoodnanny.com " do
    before(:all) do
      @html = File.read("spec/fixtures/the_food_nanny.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == 'The Food Nanny' }
    its(:image_url) { should == 'http://static1.squarespace.com/static/5a57eabb12abd94e2ac5d9a3/5a715a4af29efeb2c4428cfc/5a715a8cf29efeb2c442979b/1517378188897/Peanut-Butter-Waffle-1024x768.jpg?format=original' }
    its(:canonical_url) { should == "https://www.thefoodnanny.com/recipes/peanut-butter-waffles" }
    its(:ingredients) { should == ["1 cup packaged pancake and waffle mix (how easy is that!)", "2 Tablespoons sugar", "1/3 cup chunky style Peanut Butter (can use creamy if you like)", "1 egg", "3/4 cup milk", "2 Tablespoons vegetable oil"] }
    its(:instructions) { should == "Preheat the waffle iron.\nTo 1 cup prepackaged pancake mix, add the sugar, peanut butter, egg, milk, and oil.\nBeat almost smooth.\nBake in preheated waffle iron. Makes about 4." }
    its(:name) { should == "Peanut Butter Waffles" }
    its(:published_date) { should == Date.new(2016, 05, 02) }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when ArgumentError is raised in the parse_published_date method" do
      before(:each) do
        @html = File.read("spec/fixtures/the_food_nanny_peanut_butter_waffles.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should set published date attribute to nil" do
        expect(subject.published_date).to be_nil
      end
    end
  end
end