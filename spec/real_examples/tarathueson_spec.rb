#encoding: UTF-8

describe Hangry do
  context "tarathueson.com cucumber-and-green-pepper-salad" do
    before(:all) do
      @html = File.read("spec/fixtures/tarathueson_cucumber_and_green_pepper_salad.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:image_url) { should == "https://tarathueson.com/wp-content/uploads/2018/04/AP7A9450-Edit_WEB-400x300.jpg" }
    its(:ingredients) { should == ["1 large cucumber", "1 green bell pepper", "1/2 tsp salt", "1/8 tsp pepper", "2 tablespoons red wine vinegar"] }
    its(:instructions) { should == "Slice cucumber in discs no more than 1/4″ thick. If smaller pieces are desired, cut the cucumber in half lengthwise first. Cut each strip every 1/4 inch to form bite-sized pieces.\nSlice off the top of the pepper. Slice the pepper in half and use your fingers or a knife to slice out the seeds and innards. Lay each half face down on the cutting board and slice the pepper into strips 1/4″ in width.\nPlace the cucumbers and pepper into a bowl. Pour red wine vinegar into the mixture and sprinkle salt and pepper to taste. Make sure you stir in all ingredients/seasonings thoroughly.\nKeep in fridge until ready to eat!" }
    its(:name) { should == "Cucumber and Green Pepper Salad" }

    context "when the selector '.ingredients ul li' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/tarathueon_banana_bread_with_crumb_topping.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["4 TBSP butter, melted", "½ cup sugar or honey", "2 eggs, beaten", "1 cup banana, mashed", "1¾ cups Pamela’s Baking & Pancake Mix", "½ teaspoon salt", "1 teaspoon vanilla", "1/2 cup of Pamelas Baking & Pancake Mix", "1/2 cup Brown Sugar", "4 Tbsp butter softened", "1 tsp cinnamon"])
      end
    end
  end
end
