# encoding: UTF-8

describe Hangry do
  context "earthlydelightsblog.com coconut-lime-ice-cream" do
    before(:all) do
      @html = File.read("spec/fixtures/earthly_delights_blog.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://earthlydelightsblog.com/coconut-lime-ice-cream/" }
    its(:image_url) { should == "http://earthlydelightsblog.com/wp-content/uploads/2012/05/DSC_4429b_coconut-lime-ice-cream_featimg.jpg" }
    its(:ingredients) { should ==["1 large can coconut cream (19 oz, 560 mL)", "1 cup heavy cream", "juice of 1 lime", "zest of 1 lime", "6 egg yolks", "1 cup sugar", "1 tbsp coconut rum"] }
    its(:instructions) { should == "In a pot, combine coconut cream, heavy cream and lime juice. Bring to a boil over medium heat.\nIn a bowl, whisk together egg yolks and sugar until it forms lemon yellow ribbons.\nPour 1/4 of the hot cream mixture slowly into the egg mixture while whisking, to temper the eggs, then pour eggs into the pot of cream and return to the stove on low heat, stirring constantly, until the mixture thickens enough to coat the back of a spoon.\nRemove from heat and stir in lime zest and rum.\nCool thoroughly and chill in fridge overnight, then churn in an ice cream maker according to manufacturer’s suggestions. Spoon into a freezer-safe container and let it firm up in the freezer for a few hours, before serving." }
    its(:name) { should == "Coconut Lime Ice Cream" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end