describe Hangry do
  context "thepioneerwoman.com broccoli-wild-rice-casserole" do
    before(:all) do
      @html = File.read("spec/fixtures/the_pioneer_woman.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://thepioneerwoman.com/cooking/broccoli-wild-rice-casserole/" }
    its(:image_url) { should == "http://tastykitchen.com/recipes/wp-content/uploads/sites/2/2013/11/broccoli-420x279.jpg" }
    its(:ingredients) { should == ["2 cups Uncooked Wild Rice", "10 cups Low-sodium Chicken Broth, More If Needed For Thinning", "3 heads Broccoli, Cut Into Small Florets", "1 pound White Button Or Crimini Mushrooms, Finely Chopped", "1/2 cup (1 Stick) Butter", "1 whole Medium Onion, Finely Diced", "2 whole Carrots, Peeled And Finely Diced", "2 stalks Celery, Finely Diced", "4 Tablespoons All-purpose Flour", "1/2 cup Heavy Cream", "1 teaspoon Salt, More To Taste", "1 teaspoon Black Pepper", "1 cup Panko Breadcrumbs"] }
    its(:instructions) { should == "Add the wild rice into a medium saucepan with 5 cups of the chicken broth. Bring it to a boil over medium high heat, then reduce the heat to low and cover the pan. Cook the rice until it has just started to break open and is slightly tender, about 35 to 40 minutes Set it aside.\nMeanwhile, blanch the broccoli by throwing the florets into boiling water for 1½ to 2 minutes, until bright green and still slightly crisp. Immediately drain the broccoli and plunge it into a bowl of ice water to stop the cooking process. Remove it from the ice water and set it aside.\nHeat a large pot over medium-high heat, then melt 6 tablespoons of the butter. Add the onions and the mushrooms and cook, stirring them occasionally, for 3 to 4 minutes, or until the liquid begins to evaporate. Add the carrots and celery and cook for 3 to 4 minutes, until the vegetables are soft and the mixture begins to turn darker in color.\nSprinkle the flour on the vegetables and stir to incorporate it, then cook for about a minute. Pour in the remaining 5 cups of broth and stir to combine. Bring the mixture to a gentle boil and allow it to thicken, about 3 minutes. Pour in the heavy cream, stirring to combine. Let the mixture cook until it thickens. Season with the salt and pepper, then taste and adjust the seasonings as needed.\nAdd half the cooked rice to the bottom of a 2-quart baking dish, then lay on half the broccoli. (You can do one layer of each or two layers of each. Using a ladle, scoop out the vegetable/broth mixture and spoon it evenly all over the top. Continue with the rest of the sauce, totally covering the surface with vegetables.\nMelt the remaining 2 tablespoons of butter, then pour it into a separate bowl with the panko breadcrumbs. Toss the mixture together to coat the breadcrumbs in butter, then sprinkle the breadcrumbs all over the top.\nCover with foil and bake the casserole for 20 minutes, then remove the foil and continue baking for 15 minutes or until golden brown on top. Sprinkle on the parsley after you remove it from the oven." }
    its(:name) { should == "Broccoli Wild Rice Casserole" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end