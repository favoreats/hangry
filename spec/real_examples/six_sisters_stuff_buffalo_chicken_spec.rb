# encoding: UTF-8

describe Hangry do
  context 'sixsistersstuff.com buffalo chicken bake' do
    before(:all) do
      @html = File.read('spec/fixtures/six_sisters_stuff_baffalo_chicken.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:prep_time) { should == 10 }
    its(:cook_time) { should == 20 }
    its(:image_url) { should == 'https://www.sixsistersstuff.com/wp-content/uploads/2017/10/BuffaloChickenBake1.jpg' }
    its(:ingredients) { should == ['4 cooked and shredded chicken breasts', '8 oz cream cheese', '½ cup buffalo sauce', '¼ cup ranch', '1 cup + ½ cup mozzarella cheese', '16 oz penne pasta', '¾ cup Italian bread crumbs', '1 tablespoon butter'] }
    its(:instructions) { should == "Preheat oven to 400 degrees.\nCook noodles according to package.\nIn a bowl, combine cream cheese, buffalo sauce, ranch, shredded chicken, 1 cup mozzarella and cooked noodles (cooled).\nSpread mixture in 9x13? pan.\nIn a small bowl, combine melted butter and bread crumbs. Sprinkle mixture over casserole and top with remaining ½ cup cheese.\nBake for 20 minutes, or until cheese has melted and begins to golden." }
    its(:name) { should == 'Buffalo Chicken Bake' }
    its(:published_date) { should == Date.parse('2017-10-03') }
    its(:yield) { should == '8' }

    context 'when grilled-flank-steak-salad recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/six_sisters_stuff_peanut_butter_brownie_buckeye_cupcakes.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Recipe adapted')]/following::p[./following::*[contains(text(),'Preheat')]]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['Batter:', '12-16 miniature peanut butter cups', '1 box Duncan Hines® Milk Chocolate Premium Brownie Mix', '3 large eggs', '1/4 c water', '1/2 c oilIcing:', '1/2 c creamy peanut butter', '1/4 c butter, softened', '2 c powdered sugar, sifted', '3 tbsp milk', '1/2 tsp pure vanilla', '1/8 tsp saltTopping:', '12-14 miniature peanut butter cupsGanache:', '4 oz bittersweet chocolate, chopped into very small pieces', '1/2 c heavy whipping cream', '2 tbsp honey', '2 tbsp light corn syrup', '2 tbsp vanilla'])
        end
      end

      context "and the selector '//*[contains(text(),'Recipe adapted')]/following::p[./following::*[contains(text(),'Preheat')]][last()]/following::p[./following::*[contains(text(),'***')]]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Preheat oven to 350°F. Place paper baking cups in each of 12 regularsize muffin cups. Remove foil and cups from candy pieces. Set aside.\nPrepare brownie mix as described on box to make cakey brownies using the eggs, water and oil. Fill each up about 2/3 full with batter. Place 1 unwrapped candy piece, upside down, on top of batter in each cup. Lightly tap piece down into batter.\nBake 23 to 25 minutes or until toothpick inserted in center of cup comes out almost clean. Cool 5 minutes; remove from pan. Place on wire rack; cool completely.\nMake icing by placing peanut butter and butter into medium bowl. Beat with an electric mixer on medium speed until light and creamy; scrape bowl. Add powdered sugar, milk, vanilla and salt. Beat on low speed 1 minute; scrape bowl. Beat on medium speed until smooth and creamy. Spread about 2 tablespoons of icing on each cupcake leaving the edge uniced.\nPrepare ganache: in a small saucepan over medium heat, warm the heavy cream until very hot, but not boiling. Place chocolate pieces in a heat safe bowl. Pour the hot cream over the chocolate and allow it to sit for about 5 minutes. Whisk the cream and chocolate until smooth and thoroughly combined. Whisk in the honey, corn syrup, and vanilla. Allow to cool for about 15 minutes. Do not let the glaze sit for too long or it will harden up before you spoon it over the frosting. Spoon the glaze on the tops of the frosted cupcakes, allowing it to drip down the sides a little bit. Don?t add too much glaze or it will drip all over your liners.\nTop each cupcake with a peanut butter cup. Store cupcakes in the refrigerator. Remove them to room temperature one hour before serving.")
        end
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
