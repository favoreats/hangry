# encoding: UTF-8

describe Hangry do
  context 'popculture.com recipe-skinny-baked-burrito' do
    before(:all) do
      @html = File.read('spec/fixtures/popculture_recipe_skinny_baked_burrito.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://popculture.com/lifestyle/2016/08/29/recipe-skinny-baked-burrito/' }
    its(:image_url) { should == 'http://www.skinnymom.com/wp-content/uploads/2016/07/Skinny-Baked-Burritos_EDIT-4.jpg' }
    its(:ingredients) { should == ['2 teaspoons extra virgin olive oil', '1 pound fat-free ground turkey', '1 onion, diced', '½ teaspoon cumin', '¼ teaspoon chili powder', '1 (8.8-ounce) pouch Uncle Ben’s® Ready Rice Spanish-Style Rice', '1 (15-ounce) can reduced sodium pinto beans, drained and rinsed', '8 low-carb, high-fiber tortillas', '1 (10-ounce) can mild green chile enchilada sauce', '½ cup reduced-fat Mexican shredded cheese'] }
    its(:instructions) { should == "Preheat the oven to 375°F and grease a 13x9-inch casserole dish with nonstick cooking spray and set aside.\nHeat a large skillet over medium heat. Add the oil, turkey, and onions and cook until the turkey is cooked and the onions are soft, 8-10 minutes.\nReduce the heat to low and season the turkey with the cumin and chili powder.\nMicrowave the rice according to package directions and add it to the skillet, along with the beans. Stir together and turn off the heat.\nFill ¾ cup of the burrito filling along one side of each tortilla and fold the ends in and over on themselves. Place the burritos seam-side down in the prepared casserole dish.\nPour the enchilada sauce evenly on top of the burritos and cover in the cheese.\nBake for 10-15 minutes, or until the cheese is melted and they are heated through.\nServe with optional toppings as desired." }
    its(:name) { should == 'Skinny Baked Burrito' }

    context 'when recipe-classic-tuna-salad recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/popculture_recipe_classic_tuna_salad.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '[itemprop='articleBody'] ul li' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['4 (5-ounce) cans chunk light tuna in water', '¼ cup plain, nonfat Greek yogurt', '¼ cup light mayonnaise', '1 cup diced celery (about 3 stalks)', '¼ cup diced red onion', '2 tablespoons chopped fresh parsley', '1 tablespoon no-sugar added sweet relish', '1 tablespoon Dijon mustard', '½ teaspoon salt', 'black pepper, to taste'])
        end
      end

      context "and the selector '[itemprop='articleBody'] ol li' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Drain the tuna very well by placing a colander in the sink. Empty the tuna in it, and gently press the water out with a rubber spatula.\nAdd the rest of the ingredients to a large mixing bowl, and stir to combine.\nAdd the drained tuna and stir to combine; serve chilled.")
        end
      end
    end

    context 'when recipe-sour-cream-enchiladas recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/popculture_recipe_sour_cream_enchiladas.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '.zlrecipe ul li' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(["1 (10-ounce) can green enchilada sauce", "1 pound boneless, skinless chicken breasts", "½ cup light sour cream", "1 (10.5-ounce) can Campbell’s Healthy Request condensed cream of chicken soup", "⅓ cup fat-free milk", "1 teaspoon extra virgin olive oil", "1 small onion, diced", "1 (10-ounce) can Ro*Tel \"Original\" diced tomatoes and green chilies", "1 (4.5-ounce) can chopped green chilies", "1 teaspoon ground cumin", "½ teaspoon salt", "½ teaspoon black pepper", "8 large low-carb, high-fiber tortillas, warmed", "¾ cup shredded reduced-fat four-cheese Mexican blend", "¼ cup chopped fresh cilantro"])
        end
      end

      context "and the selector '.zlrecipe ol li' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Preheat the oven to 350°F. Coat a 13x9-inch baking dish with cooking spray.\nIn a medium skillet, heat the enchilada sauce over medium-high heat. Add the chicken breasts and cook until cooked through, 7 to 8 minutes per side.\nTransfer the chicken breasts to a plate to cool. After the chicken has slightly cooled, use two forks to shred the chicken and set aside.\nMeanwhile, return the skillet of enchilada sauce to low heat. Add the sour cream, chicken soup, and fat-free milk. Stir frequently for 2 to 3 minutes, then remove from the heat.\nIn a separate large skillet, heat the olive oil over medium-high heat. Add the onion and cook until translucent, 2 to 3 minutes. Stir in the diced tomatoes, green chilies, cumin, salt, pepper, and the shredded chicken. Continue to stir frequently until the mixture is heated through, 2 to 3 minutes, then remove from heat.\nTo assemble, fill each tortilla with a heaping ⅓ cup of the chicken mixture. Tightly roll each tortilla and place seam side down in the prepared baking dish.\nPour and evenly spread the sour cream sauce over the enchiladas and sprinkle with the cheese. Bake until the cheese is melted and bubbly, 18 to 20 minutes.\nServe garnished with the cilantro.")
        end
      end
    end

    context "when the selector '.aligncenter img' is used in the parse_image_url method" do
      before(:each) do
        @html = File.read('spec/fixtures/popculture_recipe_4_ingredient_strawberry_banana_ice_cream.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the image_url' do
        expect(subject.image_url).to eq('http://media.popculture.com/2018/02/strawberry-banana-ice-cream-picstitch-20025419.jpeg')
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
