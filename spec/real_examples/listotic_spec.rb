# encoding: UTF-8

describe Hangry do
  context "listotic.com 3-ingredient-healthy-peanut-butter-dip" do
    before(:all) do
      @html = File.read("spec/fixtures/listotic_3_ingredient_healthy_peanut_butter_dip.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://www.listotic.com/3-ingredient-healthy-peanut-butter-dip/" }
    its(:image_url) { should == "http://www.listotic.com/wp-content/uploads/2017/09/easy-3-ingredient-peanut-butter-fluff-recipe...-fruit-dip.-.jpg"}
    its(:ingredients) { should == ["1 cup plain greek yogurt", "1/2 cup creamy peanut butter (I prefer unsweetened, all natural)", "honey to taste (1-3 TBSP)"]}
    its(:instructions) { should == "Cinnamon is a nice addition if you’re into that. You can also substitute the peanut butter for almond butter (or any nut butter, I suppose).\nThe more peanut butter you add, the richer this dip will be. If you would like to lighten it up, add less peanut butter to the ratio." }
    its(:name) { should == "Healthy Peanut Butter Fluff Dip" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end