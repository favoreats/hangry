describe Hangry do
  context "thekitchn.com Tofu and Broccoli Salad" do
    before(:all) do
      @html = File.read("spec/fixtures/the_kitchn.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == 'Hali Bey Ramdene' }
    its(:image_url) { should == 'https://atmedia.imgix.net/c4728f07eadfeb29ef1b133dfbafcad1778f2775?auto=format&q=45&w=640.0&fit=max&cs=strip' }
    its(:canonical_url) { should == "https://www.thekitchn.com/recipe-peanut-butter-broccoli-and-tofu-salad-232445" }
    its(:ingredients) { should == ["1/2 cup smooth peanut butter", "1/4 cup rice vinegar", "3 tablespoons tamari or soy sauce", "2 tablespoons water", "1 to 2 teaspoons Sriracha hot sauce (optional)", "1 teaspoon toasted sesame oil", "12 ounces broccoli slaw (no dressing)", "1 medium red bell pepper, julienned", "1 pound baked tofu (see Recipe Notes), cut into small cubes", "1/4 cup shelled and cooked edamame", "1/4 cup roasted peanuts", "1/4 cup loosely packed fresh cilantro leaves"] }
    its(:instructions) { should == "Place the peanut butter, rice vinegar, tamari or soy sauce, water, Sriracha (if using), and sesame oil in a bowl and whisk until smooth; set aside.\nPlace the broccoli slaw and bell pepper in a large bowl and toss to combine. Cover and refrigerate until ready to serve.\nWhen ready to serve, divide the slaw mixture between 4 plates. Top with the baked tofu, peanuts, and cilantro. Drizzle with the peanut sauce." }
    its(:name) { should == "Tofu and Broccoli Salad with Peanut Butter Dressing" }
    its(:description) { should == "Sometimes I shy away from ordering salads at restaurants because I fear they won't be flavorful enough — I just can't handle the possibility of that kind of disappointment — but when something sounds colorful and crunchy, and includes peanut butter, I'm far more inclined to leap" }
    its(:published_date) { should == "Jun 23, 2016" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when NoMethodError is raised in the parse_description method" do
      before(:each) do
        @html = File.read("spec/fixtures/the_kitchn_tofu_and_broccoli_salad.html")
        @parsed = Hangry.parse(@html)
      end

      subject { @parsed }

      it "should set description attribute to nil" do
        expect(subject.description).to be_nil
      end
    end

    context "when the_kitchn_slow_cooker_chicken recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/the_kitchn_slow_cooker_chicken.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the ingredients are specified in '[data-render-react-id='recipes/Recipe']' selector" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1 cup <p>frozen corn kernels</p>", "1 to 1 1/2 pounds <p>boneless skinless chicken breasts, chicken thighs, or a mix</p>", "1 <p>(14.5-ounce) can diced tomatoes</p>", "1 cup <p>low-sodium chicken broth, divided, plus more if needed</p>", "2 teaspoons <p>chili powder</p>", "2 teaspoons <p>salt</p>", "1 teaspoon <p>ground cumin</p>", "1 cup <p>brown rice</p>", "1 <p>(15-ounce) can black beans, drained and rinsed</p>", "<p><em>Optional toppings: </em>Shredded cheese, chopped cilantro, sour cream, diced avocado, salsa, hot sauce, diced green onions, shredded lettuce</p>"])
        end
      end

      context "and the instructions are specified in '[data-render-react-id='recipes/Recipe']' selector" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("<p>Combine the chicken breasts, diced tomatoes and their juices, 1/2 cup of chicken broth, chili powder, salt, and cumin in the bowl of a 2 1/2- to 3 1/2-quart slow cooker. Make sure the chicken is covered, and add additional broth if needed. Cover with the lid and cook on low for 3 to 4 hours.\n</p><p>Uncover and add the rice, beans, corn, and remaining 1/2 cup chicken broth. Cover and continue cooking on low for another 3 to 4 hours. Check the rice periodically in the last hour of cooking, stirring once or twice to make sure the rice cooks evenly and adding more chicken broth if the mixture seems dry. Cooking is done when the rice is tender — if the rice is done while there is still liquid left in the slow cooker, remove the lid and cook on high to let the liquid evaporate.\n</p><p>Use 2 forks to shred the chicken into bite-sized pieces. You can do this either in the slow cooker itself and then mix it into the rice, or you can transfer the chicken to a cutting board if you prefer to keep it separate. Taste the burrito mix and stir in more salt or other seasonings to taste.\n</p><p>Serve burrito bowls with a selection of toppings.\n</p>")
        end
      end

      context "and the image_url is specified in '[data-render-react-id='images/LazyPicture']' selector" do
        it "should be able to parse the image_url" do
          expect(subject.image_url).to eq("https://atmedia.imgix.net/f33558b0c4da039b6315aa4ada2aa09939f2cad7?auto=format&q=45&w=640.0&fit=max&cs=strip")
        end
      end
    end
  end
end