# encoding: UTF-8

describe Hangry do
  context "ashleysfreshfix.com fresh-fix-bruschetta" do
    before(:all) do
      @html = File.read("spec/fixtures/ashleys_fresh_fix_fresh_fix_bruschetta.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.ashleysfreshfix.com/fresh-fix-bruschetta/" }
    its(:image_url) { should == "https://www.ashleysfreshfix.com/wp-content/uploads/2018/10/recipes-51.jpg" }
    its(:ingredients) { should == ["Cherry Tomatoes 3 cups", "Basil leaves 10", "Minced garlic 1 tsp", "Salt 1/4 tsp", "Olive Oil 1 tsp", "Baguette", "Balsamic Glaze"]}
    its(:instructions) { should == "Chop cherry tomatoes and basil, mix together with garlic, salt, olive oil. Slice baguette and top with tomato mixture, drizzle with balsamic glaze." }
    its(:name) { should == "Fresh Fix Bruschetta" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end