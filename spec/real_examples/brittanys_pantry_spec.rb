# encoding: UTF-8

describe Hangry do
  context 'brittanyspantry.com super-gooey-peanut-butter-brownies' do
    before(:all) do
      @html = File.read('spec/fixtures/brittanys_pantry_super_gooey_peanut_butter_brownies.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'http://brittanyspantry.com/2015/08/super-gooey-peanut-butter-brownies/' }
    its(:image_url) { should == 'https://i0.wp.com/brittanyspantry.com/wp-content/uploads/2015/08/IMG_4876.jpg?fit=1024%2C1024' }
    its(:ingredients) { should == ['3/4 c sweetened condensed milk, divided', '1/4 c canola or grape seed oil', '1/4 c milk', '1 box devils food cake mix', '1 egg', '1 jar (7 oz) marshmallow fluff', '3/4 c peanut butter chips'] }
    its(:instructions) { should == "Preheat the oven to 350 degrees. Lightly spray a 9X13 inch baking pan and set aside.\nIn a large bowl, using a hand mixer or stand mixer, combine 1/4 c of the sweetened condensed milk and the next 4 ingredients. The batter will be VERY sticky! Spread 2/3 of the batter in the bottom of the prepared pan. The layer will be thin, but keep at it! 🙂 Bake for 10 minutes, until just set. Meanwhile, combine the remaining 1/2 c of condensed milk and the marshmallow fluff until smooth. Fold in the peanut butter chips. When the crust comes out of the oven, pour on the marshmallow mixture and carefully smooth and spread to the edges. Drop the remaining chocolate batter evenly over the marshmallow stuff. Gently swirl with a toothpick, if desired. Its all pretty thick, so you just want to make sure everything is even. It will all bake together anyway. Return the pan to the oven and bake for an additional 25-30 minutes, or until dry and set in the middle. Let brownies cool almost completely and cut into SMALL squares. These are UBER rich so resist the urge to make them giant and ere on the smaller side. Besides….\nyou know you are going to eat two anyway…." }
    its(:name) { should == 'Super Gooey Peanut Butter Brownies' }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
