# encoding: UTF-8

describe Hangry do
  context "foodnetwork.com one-pot-caprese-pasta" do
    before(:all) do
      @html = File.read("spec/fixtures/foodnetwork_one_pot_caprese_pasta.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.foodnetwork.com/healthyeats/recipes/2015/08/one-pot-caprese-pasta" }
    its(:image_url) { should == "//food.fnr.sndimg.com/content/dam/images/food/fullset/2015/10/30/0/HE_caprese-pasta-salad-3_s4x3.jpg.rend.hgtvcom.616.462.suffix/1446485890461.jpeg" }
    its(:ingredients) { should == ["3 cups chopped tomatoes (fresh is best, but you can use diced canned tomatoes)", "1 pound whole-wheat spaghetti", "4 cups water", "2 garlic cloves, chopped", "1/4 teaspoon red pepper flakes", "1/2 teaspoon dried oregano", "1 teaspoon olive oil", "1/2 cup packed basil leaves, plus more for garnish, roughly torn", "1 cup shredded mozzarella", "Shaved Parmesan cheese, for topping (optional)"] }
    its(:instructions) { should == "Place the tomatoes, whole-wheat spaghetti, water, garlic, red pepper flakes, oregano and olive oil in a stockpot, and bring to a boil.\nReduce heat to a simmer and cook for 10 to 12 minutes until the pasta is al dente, stirring occasionally.\nOnce the pasta is cooked, remove from heat and stir in the basil leaves, mozzarella, and a pinch of salt and pepper to season." }
    its(:name) { should == "One-Pot Caprese Pasta" }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

