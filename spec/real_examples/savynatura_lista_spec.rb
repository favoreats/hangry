describe Hangry do
  context "savynaturalista.com molasses-pear-butter-bread" do
    before(:all) do
      @html = File.read("spec/fixtures/savynatural_ista_molasses_pear_butter_bread.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == "Molasses Pear Butter Bread" }
    its(:canonical_url) { should == "http://www.savynaturalista.com/2013/10/29/molasses-pear-butter-bread/" }
    its(:image_url) { should == "http://www.savynaturalista.com/wp-content/uploads/019.jpg" }
    its(:ingredients) { should == ["2 cups pear butter", "2 eggs (room temperature)", "1/2 cup oil (I used coconut)", "1/2 cup sugar", "1 tsp. vanilla extract", "1 ¾ cup all-purpose flour", "2 tsp. cinnamon", "1/2 tsp. salt", "1 tsp. baking soda", "1 tbsp. molasses"] }
    its(:instructions) { should == "Preheat oven to 350 degrees, grease two medium loaf pans, and in a small bowl sift dry ingredients and set aside. Mix sugar, eggs, and oil until creamy; add vanilla, pear butter and molasses and mix into the wet ingredients. Pour flour in small batches mixing and adding until all the flour is incorporated in the batter. Place batter into loaf pans and in the oven for 40 minutes. Stick a tooth pick in the center of the loaf if it comes out clean the bread is done." }

    context "when the selector '.entry-content p:nth-of-type(11)' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/savynatural_ista_banana_chia_bread.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["Banana Chia Bread Recipe", "1 ½ cup flour", "2 tsps. baking powder", "¼ tsp. salt", "1 large egg (room temperature)", "2 tbsps. whipped butter, softened", "½ cup sugar (I used muscovado)", "1/3 cup chia seeds", "2 large bananas (smashed)", "½ cup almond milk"])
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end