# encoding: UTF-8

describe Hangry do
  context "howsweeteats.com shredded sprouts salad" do
    before(:all) do
      @html = File.read("spec/fixtures/how_sweet_eats.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == "Jessica" }
    its(:canonical_url) { should == "https://www.howsweeteats.com/2010/10/shredded-sprouts-salad/" }
    its(:image_url) { should == "https://www.howsweeteats.com/wp-content/uploads/2010/10/Easy-Shredded-Brussels-Sprouts-Salad-3.jpg" }
    its(:ingredients) { should == ["2 cups whole brussels sprouts", "1/2 clove of garlic, minced", "1/4 teaspoon salt", "1/4 teaspoon pepper", "1 tablespoon butter", "1 slice bacon, fried and crumbed", "1/2 ounce grated asiago cheese", "1 tablespoon balsamic vinegar"] }
    its(:instructions) { should == "I begin by cutting the stems off of the brussels sprouts – you can do this, or skip this step.\nAdd sprouts to a food processor and pulse until sprouts are tiny shreds. Do it manually so you can control the size of the shreds. You want some larger pieces and you don’t want them to be crumbs!\nMelt butter in a large skillet over medium heat. Add garlic and shredded sprouts, sprinkle with salt and pepper, and saute for about 5-7 minutes, or until sprouts are browning. Add vinegar while stirring and make sure sprouts are all coated. Saute for another 1-2 minutes. Remove and plate.\nTop with crumbled bacon and a small sprinkling of asiago cheese. Devour!"}
    its(:name) { should == "Shredded Sprouts Salad." }
    its(:total_time) { should == 25 }
    its(:published_date) { should == Date.new(2010, 10, 29) }
    its(:yield) { should == 'serves 1-2' }

    context "when white-pizza-dip recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/how_sweet_eats_white_pizza_dip.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'serves')]/following::p[./following::*[contains(text(),'Preheat')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1 pint grape tomatoes, tomatoes cut in half", "1 teaspoon olive oil", "1/4 teaspoon salt", "1/4 teaspoon pepper", "1 1/2 (12 ounces) blocks cream cheese, softened", "8 ounces mozzarella cheese, freshly grated", "8 ounces provolone cheese, freshly grated", "1/4 cup finely grated parmesan cheese + more for garnish", "4 garlic cloves, minced or pressed", "1/4 cup freshly chopped basil leaves", "2 tablespoons freshly chopped thyme leaves", "1/2 tablespoon freshly chopped oregano leaves", "crackers, bread or chips for serving"])
        end
      end

      context "and the selector '//*[contains(text(),'crackers')]/following::p[./following::*[contains(text(),'The end')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Preheat oven to 400 degrees F. Line a baking sheet with aluminum foil then place tomatoes on top. Sprinkle with olive oil and salt, then roast for 20-25 minutes, until bursting. Set aside.\nWhile tomatoes are roasting, mix softened cream cheese with about 7 ounces each of provolone and mozzarella, then and parmesan. Stir in fresh herbs, garlic and roasted tomatoes, mixing well to combine. Transfer mixture to an oven-safe baking dish (mine was 6 x 4 round). Sprinkle with remaining provolone and mozzarella. Bake for 25-30 minutes, or until top is golden and bubbly. Serve immediately with crackers, chip or toasted bread.")
        end
      end
    end

    context "when roasted-strawberry-brie-chocolate-grilled-cheeeeeeeese recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/how_sweet_eats_roasted_strawberry_brie_chocolate_grilled_cheeeeeeeese.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'makes')]/following::p[./following::*[contains(text(),'saucepan') or contains(text(),'Preheat')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["6 strawberries, quartered", "1/2 teaspoon canola oil", "pinch of salt", "2 tablespoons butter", "4 thick-cut slices of multigrain bread", "2-3 ounces of brie cheese, sliced", "1-2 ounces high-quality chocolate"])
        end
      end

      context "and the selector '//*[contains(text(),'makes')]/following::p[./following::*[contains(text(),'Preheat')]][last()]/following::p[./following::*[contains(text(),'And')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Preheat oven to 375 degrees. Toss strawberries with oil and salt, then spread on a baking sheet and roast for 20 minutes.\nHeat a large skillet or grilled over medium-low heat. Butter the sides of two slices of bread, place them on the skillet buttered-side down, then layer on a few slices of cheese, chocolate and strawberries. Top with a few more slices of cheese, then butter one side of the other bread slices, and place on top of the cheese. Cook for 2-3 minutes or until the cheese has started to melt, then gently flip the sandwich and cook until golden. Serve immediately!")
        end
      end
    end

    context "when the selector '//*[contains(text(),'makes')]/following::p[./following::*[contains(text(),'saucepan') or contains(text(),'Preheat')]][last()]/following::p[./following::*[contains(text(),'Note')]]' is used in parse_instruction" do
      before(:each) do
        @html = File.read("spec/fixtures/how_sweet_eats_chocolate_chip_cookie_dough_peanut_butter_cups.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("In a small saucepan, heat butter until melted. Whisk in brown sugar until dissolved, then let it bubble for 1-2 minutes. Remove from heat and whisk in peanut butter. The mixture will be liquidly. Whisk in vanilla. Let sit and cool completely, about 20 minutes.\nLine a mini muffin with with liners. Melt 1 1/4 cups of chocolate chips (I do mine in the microwave, heating on full power for 20 seconds, stir, 30 seconds again, and stir until melted). Drop 1/2-1 teaspoon of chocolate into each liner, then use a pastry brush to brush chocolate up the sides of the liner. Place in the freezer for 20 minutes. By this point the butter + sugar mix should be cool. Whisk in powdered sugar, salt and flour, until combine and few lumps remain. Fold in mini chocolate chips. The dough will be wet, but pop it in the fridge for 15 minutes to harden.\nRemove muffin tin and cookie dough, then place 1 teaspoon of cookie dough (I somewhat rolled mine) into the chocolate cups. Place back in the freezer for 10-15 minutes. Melt the remaining chocolate chips, then cover each cookie dough top with chocolate, smoothing with a spoon. Freeze again for 15-20 minutes. These are fine out in room temperature for a few hours (they may soften a bit) but I like them best stored in the fridge.")
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when ArgumentError is raised in the parse_published_date method" do
      before(:each) do
        @html = File.read("spec/fixtures/how_sweet_eats_sprout_salad.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should set published date attribute to nil" do
        expect(subject.published_date).to be_nil
      end
    end
  end
end

