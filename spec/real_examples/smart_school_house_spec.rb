# encoding: UTF-8

describe Hangry do
  context 'smartschoolhouse.com lavender-soap-jellies' do
    before(:all) do
      @html = File.read('spec/fixtures/smart_school_house_lavender_soap_jellies.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.smartschoolhouse.com/diy-crafts/lavender-soap-jellies' }
    its(:image_url) { should == 'https://www.smartschoolhouse.com/wp-content/uploads/2018/06/IMG_9347.jpg' }
    its(:ingredients) { should == ['1 package of gelatin (found in the baking isle at the grocery store)', '2/3 cup of hot water', '1/2 cup of lavender scented liquid soap. We used this lavender body wash by dial', 'Measuring cups', 'Mixing spoons', 'Small mixing bowl'] }
    its(:instructions) { should == "Add 2/3 cup of boiling hot water to a mixing bowl\nAdd the packet of gelatin and stir until the gelatin powder is completely dissolved\nPour in 1/2 cup of lavender scented liquid soap and stir until the ingredients are completely combined\nPour the mixture into an ice cube tray\nLet it set for 3 hours until firm (or 1.5 hours in the refrigerator for quicker results)\nUse with water on hands or body, depending on the type of liquid soap chosen\nJust like regular Jello, these jellies will melt if they are left out of the refrigerator. So, we recommend keeping them chilled:)" }
    its(:name) { should == 'Lavender Soap Jellies' }

    context "when smart-school-house-watermelon-slushie recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/smart_school_house_watermelon_slushie.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector 'div.entry-content ul li' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['5 cups of crushed ice', '4 cups of cubed watermelon (seedless)', '1 squeeze of lemon juice', 'And the secret ingredient: 1/2 cup of Cool Whip'])
        end
      end

      context "and the selector 'div.entry-content ol li' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("I like to mix the cubed watermelon pieces and the crushed ice together in a bowl first so that they blend together easily in the blender.\nAdd the Cool Whip and the squeeze of lemon juice (optional)\nBlend until full combined.\nServe in a cold glass with a straw!")
        end
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end