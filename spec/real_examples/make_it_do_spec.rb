# encoding: UTF-8

describe Hangry do
  context "make-it-do.com moms-scones" do
    before(:all) do
      @html = File.read("spec/fixtures/make_it_do.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://www.make-it-do.com/cook-it-bake-it/moms-scones/" }
    its(:image_url) { should == "http://www.make-it-do.com/wp-content/uploads/2011/05/IMG_8139.jpg" }
    its(:ingredients) { should == ["1 cup warm water", "1 cup warm milk", "1 Tbsp. dry active yeast", "3 Tbsp. granulated sugar", "1/4 cup melted butter, cooled", "1 Tbsp. salt", "5 – 6 cups bread flour", "Vegetable oil for deep frying"]}
    its(:instructions) { should == "without flour\nCombine warm water and warm milk. Stir in the sugar. Add dry active yeast and stir lightly. Allow yeast to activate, about 10 minutes. In a stand mixer, fitted with a bread hook, add milk mixture and melted cooled butter. Add the salt. Start the mixer and add the flour one cup at a time until the dough pulls away from the sides of the bowl. Dough should be slightly sticky but workable. Cover the dough in the bowl with a towel. Set in a warm place and allow raise until double.\nPour your oil in a large pan (with sides deep enough to fully immerse the scones) or a deep fryer and heat slowly over medium to medium high heat. While the oil is heating, roll the dough out onto the counter, without flour, into a large rectangle about 1/2 inch thick. You may choose to butter the counter lightly. Using a pizza cutter, cut into squares or triangles.\nHeat oil to between 350 – 400 degrees and cook scones until golden brown. Serve immediately." }
    its(:name) { should == "Mom’s Scones" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end