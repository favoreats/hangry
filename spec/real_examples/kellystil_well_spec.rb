# encoding: UTF-8

describe Hangry do
  context 'kellystilwell.com one-pot-italian-pasta-soup-easy-weeknight-supper' do
    before(:all) do
      @html = File.read('spec/fixtures/kellystil_well_one_pot_italian_pasta_soup_easy_weeknight_supper.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://kellystilwell.com/one-pot-italian-pasta-soup-easy-weeknight-supper/' }
    its(:image_url) { should == 'https://kellystilwell.com/wp-content/uploads/2017/02/one-pot-Italian-pasta-soup-pin.png' }
    its(:ingredients) { should == ['8 oz spaghetti', '2 onions, chopped', '6 tomatoes, chopped', '3 garlic cloves, chopped', '3 cups vegetable broth', '1 tsp ground basil', '2 fresh basil leaves', '1 tsp thyme', '1 rosemary sprig', '¼ cup grated parmesan cheese', '2 tbsp fresh parsley leaves, minced', 'Salt and pepper to taste'] }
    its(:instructions) { should == "In a pot bring the vegetable broth to a boil, add the rosemary sprig, ground basil, fresh basil leaves, spaghetti, salt, and pepper.\nCook according to package directions on spaghetti.\nWhile waiting, add the onions, tomatoes, thyme, and garlic to a food processor. Pulse until well combined.\nWhen al dente, discard the basil leaves and rosemary sprig and incorporate the tomato sauce from the food processor. Add in the parmesan cheese and stir well.\nDivide into bowls, top with the fresh parsley. Serve and enjoy. How easy is that?" }
    its(:name) { should == 'One Pot Italian Pasta Soup for an Easy Weeknight Supper' }

    context 'when quick-easy-lazy-lasagna recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/kellystil_well_quick_easy_lazy_lasagna.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients')]/following::p[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['1 jar Bertolli® Tomato & Basil Sauce', '1/2 box dried farfalle', '2 pounds ground sweet sausage', '16 oz. part skim ricotta cheese', '9 small balls mozzarella', '1/2 cup shaved aged parmesan', 'fresh basil'])
        end
      end

      context "and the selector '//*[contains(text(),'Directions')]/following::p[1]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Put water on to boil in large pot and follow directions for cooking the pasta\nWhile pasta is cooking, brown the sausage\nDrain the pasta and put back in same pan\nDrain the fat from the sausage\nAdd sausage to pot with pasta\nAdd jar of Bertolli\n®\nTomato & Basil Sauce\nAdd shaved parmesan\nAdd ricotta cheese\nMix all ingredients until combined well and pour into a 9 x 11 baking dish\nPlace 9 small mozzarella balls over pasta\nBake in 350 degree oven for 20 minutes")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients')]/following::div[./following::*[contains(text(),'Instructions')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/kellystil_well_low_fat_blueberry_muffin_with_toasted_coconut_recipe.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['2 1/4 cups of flour (I used Swan Down Cake Flour)', '2 teaspoons baking powder', '1/2 teaspoon salt', '2/3 cup raw sugar', '3 oz. Weight Watchers Cream Cheese Spread', '1 1/2 Tablespoons Coconut Oil', '1 large egg', '2 egg whites', '1 teaspoon pure vanilla extract', '3/4 cup skim milk', '1 1/2 cups fresh blueberries', '1/8 cup sweetened coconut, toasted'])
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients')]/following::ul[1]/li' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/kellystil_well_delicious_bacon_wrapped_chicken_bites.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['3Tbsp. Grey Poupon Savory Honey Mustard, divided', '2Tbsp. Lea & Perrins Worcestershire Sauce', '1 large boneless skinless chicken breast (6 oz.), cut into 24 pieces', '8 slices Oscar Mayer Bacon, each slice cut into thirds', '4 oz. Cracker Barrel Vermont Sharp-White Cheddar Cheese, cut into 12 slices, each slice cut in half', '24 Ritz Crackers', '2 tsp. chopped fresh parsley'])
      end
    end

    context "when the selector '//*[contains(text(),'Directions:')]/following::p[./following::*[@class='entry-meta']]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/kellystil_well_deliciously_easy_mexican_rice_bowl_recipe_summerfiesta.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Set aside cooked rice and keep warm\nHeat Taco Boats for 57 minutes, a bit longer if you like them slightly crunchy, like we do\nHeat Refried Beans\nStart with Refried Beans, top with rice, cheese, and add your favorites!\nI told you it was easy!\n ")
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
