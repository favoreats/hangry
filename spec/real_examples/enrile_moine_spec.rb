# encoding: UTF-8

describe Hangry do
  context 'enrilemoine.com cachapas-with-queso-de-mano' do
    before(:all) do
      @html = File.read('spec/fixtures/enrile_moine_cachapas_with_queso_de_mano.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://enrilemoine.com/en/2016/07/22/cachapas-with-queso-de-mano/' }
    its(:image_url) { should == 'https://enrilemoine.com/wp-content/uploads/2016/07/Cachapas2Bcon2Bqueso2BSAVOIR2BFAIRE2Bby2Benrilemoine2B1.jpg' }
    its(:ingredients) { should == ['3 1/2 of fresh corn (from 4 corn ears)', '1/4 cup of heavy cream', '1 tablespoon of sugar', '1/2 teaspoon of salt', '1/3 cup of corn (arepa) flour', 'Butter', '4 quesos de mano divided in 2, o queso fresco'] }
    its(:instructions) { should == "1. Put all ingredients except butter and cheese in a blender and blend until it forms a thick batter.\n2. Preheat a thick pan over medium heat.\n3. When the pan is hot grease it with butter.\n4. Do each cachapa pouring 1/4 cup of the batter at a time, and making a circle about 3 inches.\n5. Cook for 2-3 minutes and turn with a spatula. Cook for 2 more minutes.\n6. Serve hot with butter and cheese" }
    its(:name) { should == 'Cachapas with queso de mano' }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
