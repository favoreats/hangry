describe Hangry do
  context "thegardengrazer southwestern-chopped-salad" do
    before(:all) do
      @html = File.read("spec/fixtures/the_garden_grazer_black_bean_tacos_with_avocado_cilantro.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == "Black Bean Tacos with Avocado Cilantro-Lime Sauce" }
    its(:canonical_url) { should == "http://www.thegardengrazer.com/2014/05/black-bean-tacos-with-avocado-cilantro.html"}
    its(:image_url) { should == "https://3.bp.blogspot.com/-mMx8a6p0Fi0/U3-MG_wpvaI/AAAAAAAADds/iGXT2772CHY/s1600/blackbeantacoavocado2.jpg" }
    its(:ingredients) { should == ["Makes about 8 tacos (can easily cut in half if desired)", "Two 15 oz. cans black beans", "1 cup salsa (", "restaurant-style", "or other)", "1 tsp. cumin", "Corn tortillas", "Toppings of your choice:", " lettuce, tomato, onion, roasted red peppers, corn, avocado, cilantro, etc.", "{For the sauce}", "1/2 ripe avocado", "3/4 cup cilantro, stems removed", "Juice from 1 lime", "1 clove garlic", "1 Tbsp. olive oil", "1 tsp. agave/honey", "1/8 tsp. salt"] }
    its(:instructions) { should == "Make the avocado sauce: in a food processor or blender, add all sauce ingredients and blend. Add a touch of water to thin if necessary, and tweak seasonings as desired. Set aside, or refrigerate if making ahead of time.\nIn a pan over medium heat, add black beans (rinsed and drained), salsa, and cumin. Heat for about 5 minutes stirring occasionally, until heated through. (Optional: mash beans after heating for a creamier filling.)\nWhile the beans are heating, chop and prepare your toppings. Warm the tortillas if desired.\nAssemble the tacos: spoon the black bean mixture in the center of the tortillas, drizzle a small amount of avocado sauce over the top, and add your toppings.\n{\nPrinter Friendly Version\n}\nCome join The Garden Grazer on Instagram 🍓 and share a pic if you make these! I love seeing what you guys cook up." }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end