# encoding: UTF-8

describe Hangry do
  context "lmld.org cinnamon-sugar-soft-pretzel-bites" do
    before(:all) do
      @html = File.read("spec/fixtures/lmld_cinnamon_sugar_soft_pretzel_bites.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://lmld.org/cinnamon-sugar-soft-pretzel-bites/" }
    its(:image_url) { should == "https://lmld.org/wp-content/uploads/2016/01/cinnamon-sugar-pretzel-bites-4.jpg" }
    its(:ingredients) { should == ["2 1/4 tsp instant yeast", "1 1/2 cups warm water", "1 tsp salt", "1 TBS brown sugar", "2 TBS melted butter", "4 cups flour", "1 large egg + 1 TBS water", "9 cups water", "1/2 cup baking soda", "1/4 cup melted butter", "3/4 cup sugar", "3 tsp cinnamon", "4 oz cream cheese (softened))", "1/4 cup butter ((softened))", "1/2 tsp vanilla", "1 1/2 cups powdered sugar"] }
    its(:instructions) { should == "Dissolve your yeast in your warm water in a large bowl. Stir together for about 1 minute until mostly combined.\nAdd in your salt, sugar and butter and stir everything together.\nAdd in your flour, 1 cup at a time until dough is thick and no longer sticky. Add more flour 1 TBS at a time if needed to make sure dough isn't sticky. (Poke dough with your finger, and dough should bounce back - then it is ready)\nLightly flour a flat surface.\nKnead your dough for 3 minutes and form into a ball.\nOil a large bowl with nonstick spray and place dough in. Cover bowl with a towel and place in a warm area.\nAllow to rest in warm area for about 30 minutes.\nPreheat your oven to 425 degrees.\nBring 9 cups of water with ½ cup of baking soda to boil.\nCut your dough ball into 6 sections.\nRoll each section into long ropes about 20-22 inches long.\nCut each rope into about 10-12 1.5 inch pieces.\nDrop one rope worth of pretzel bites into your baking soda bath and let sit for 20-30 seconds. Remove with a slotted spoon and place on a baking sheet lined with parchment or a silicone liner.\nBeat together your egg and 1 TBS water in a small bowl.\nBrush egg over each pretzel bite and place on prepared pan.\nPlace in oven and bake for 15 minutes or until pretzel tops are a nice golden brown.\nRemove from oven.\nCombine sugar and cinnamon in a small bowl.\nDip each pretzel bite into melted butter then into cinnamon sugar mixture.\nSet back on tray and repeat with additional pretzel bites.\nMix together your butter and cream cheese until smooth.\nAdd in your powdered sugar and vanilla and mix until completely combined. (Add in milk 1/2 TBS at a time, if needed to make dip softer and smoother) Microwave for 20 seconds to reheat if you're not using immediately." }
    its(:name) { should == "Cinnamon Sugar Soft Pretzel Bites" }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

