# encoding: UTF-8

describe Hangry do
  context 'tasteofhome.com recipe' do
    before(:all) do
      @html = File.read('spec/fixtures/tasteofhome.com.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    it 'should use a non-standard parser' do
      expect(Hangry::ParserSelector.new(@html).parser).to be_an_instance_of(Hangry::Parsers::NonStandard::TasteOfHomeParser)
    end

    its(:author) { should == 'Patricia Kile' }
    its(:canonical_url) { should == 'https://www.tasteofhome.com/recipes/rhubarb-popover-pie' }
    its(:cook_time) { should == 20 }
    its(:description) { should == 'This fabulous spring breakfast "pie" is also delicious when pineapple or even fresh strawberries are mixed in with the rhubarb filling. Yum!—Patricia Kile, Elizabethtown, Pennsylvania.' }
    its(:image_url) { should == 'https://cdn2.tmbi.com/TOH/Images/Photos/37/300x300/Rhubarb-Popover-Pie_exps49051_HCA1864839B02_17_3bC_RMS.jpg' }
    its(:ingredients) {
      should == [
        '1/2 cup all-purpose flour',
        '1/4 teaspoon salt',
        '2 large eggs',
        '1/2 cup 2% milk',
        '2 tablespoons butter',
        'FILLING:',
        '1-1/2 cups sliced fresh or frozen rhubarb, thawed',
        '1/2 cup canned pineapple chunks',
        '1/3 cup butter, cubed',
        '1/2 cup packed brown sugar',
        'Whipped cream or vanilla ice cream, optional'
      ]
    }
    its(:name) { should == 'Rhubarb Popover Pie' }
    its(:nutrition) do
      should == {
        calories: '279 calories: 1 piece',
        cholesterol: '109mg cholesterol',
        fiber: '1g fiber)',
        protein: '4g protein.',
        saturated_fat: nil,
        sodium: '239mg sodium',
        sugar: nil,
        total_carbohydrates: '31g carbohydrate (21g sugars',
        total_fat: '16g fat (10g saturated fat)',
        trans_fat: nil,
        unsaturated_fat: nil
      }
    end
    its(:instructions) { should == "Directions\nIn a large bowl, combine flour and salt. In another bowl, whisk eggs and milk.\nPlace butter in an 9-in. pie plate; heat in a 425° oven for 3-5 minutes or until butter is melted. Meanwhile, stir egg mixture into dry ingredients just until moistened.\nCarefully swirl the butter in the pan to coat the sides and bottom of pan; add batter. Bake at 425° for 16-20 minutes or until puffed and golden brown.\nMeanwhile, in a large skillet, saute rhubarb and pineapple in butter until rhubarb is tender. Stir in brown sugar; bring to a boil over medium heat, stirring constantly. Pour into the center of puffed pancake; cut into six wedges. Serve immediately with whipped cream if desired.\nYield: 6 servings.\nEditor's Note: If using frozen rhubarb, measure rhubarb while still frozen, then thaw completely. Drain in a colander, but do not press liquid out.\nOriginally published as Rhubarb Popover Pie in Taste of Home's Holiday & Celebrations Cookbook\nAnnual 2010, p157\nwindow._taboola = window._taboola || [];\n_taboola.push({\nmode: 'thumbnails-i',\ncontainer: 'taboola-native-stream-thumbnails',\nplacement: 'Native Stream Thumbnails Redesign',\ntarget_type: 'mix'\n});" }

    its(:prep_time) { should == 25 }
    its(:total_time) { should == 45 }
    its(:yield) { should == '6 servings' }

    context 'when homemade-lemon-curd recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/taste_of_home_homemade_lemon_curd.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector 'h1.recipe-title' is used in the parse_name method" do
        it 'should be able to parse the name' do
          expect(subject.name).to eq('Homemade Lemon Curd')
        end
      end

      context "and the selector '.recipe-image-and-meta-sidebar img' is used in the parse_image_url method" do
        it 'should be able to parse the image_url' do
          expect(subject.image_url).to eq('https://www.tasteofhome.com/wp-content/uploads/2018/01/Homemade-Lemon-Curd_EXPS_THAM18_17339_D11_14_4b-696x696.jpg')
        end
      end

      context "and the selector '.recipe-ingredients li' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['3 large eggs', '1 cup sugar', '1/2 cup lemon juice (about 2 lemons)', '1/4 cup butter, cubed', '1 tablespoon grated lemon zest'])
        end
      end

      context "and the selector '.recipe-directions__list li' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq('In a small heavy saucepan over medium heat, whisk eggs, sugar and lemon juice until blended. Add butter and lemon zest; cook, whisking constantly, until mixture is thickened and coats the back of a metal spoon. Transfer to a small bowl; cool 10 minutes. Refrigerate, covered, until cold.')
        end
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
