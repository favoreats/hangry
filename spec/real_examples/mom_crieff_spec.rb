# encoding: UTF-8

describe Hangry do
  context "momcrieff.com shredded-beef-5-recipes" do
    before(:all) do
      @html = File.read("spec/fixtures/mom_crieff_shredded_beef_5_recipes.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://momcrieff.com/shredded-beef-5-recipes-you-can-use-it-in/" }
    its(:image_url) { should == "https://momcrieff.com/wp-content/uploads/2016/01/IMG_2336-e1453160120261.jpg" }
    its(:ingredients) { should == ["3 – 4 lb chuck or other inexpensive beef roast (as in lower quality)", "1 envelope of brown gravy", "1 envelope ranch dressing", "2 cups water"]}
    its(:instructions) { should == "Pour the water and mix the packages of brown gravy and ranch dressing in with the water.  I usually whisk it because you can’t really stir it once you put the roast in.\nTurn your crock pot on high.\nCook at least 6 hours.  More is better than less.\nIf you use a huge roast or two small ones, 6 lbs or so, use two packages of the gravy and dressing.  Add a couple of hours onto the cooking time.\nWhen the meat is fork tender, put it in a bowl and pull it with two forks to shred it.  Bet you can’t do that without taste testing the meat!  Yum!!  And it will make your house smell really good!\nWhile I’m shredding, I pour the gravy into a large cup (or a bowl if you don’t have a cup large enough).  Let it sit for about 10 -15 minutes and skim off as much of the fat layer as you can.\nPlace the gravy and the shredded meat back into the crock pot, change the temperature to low or warm until ready to serve." }
    its(:name) { should == "Shredded Beef – How to make it & 5 recipes you can use it in." }

    context "when belt-sandwich-bacon-lettuce-and-tomato recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/mom_crieff_belt_sandwich_bacon_lettuce_and_tomato_with_a_special_e_ingredient.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients:')]/following::ul[1]/li' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["Two pieces of bread (pick your favorite!)", "Lettuce (pick your favorite kind of lettuce)", "Tomato (I get the biggest tomato I can find, love big slices on my BELT sandwich)", "Bacon (3 slices, if you are indulging)", "1 egg (fried, of course. I like mine sunny side up BUT, over easy is perfectly OK!)", "Mayo (optional)"])
        end
      end

      context "and the selector '//*[contains(text(),'Directions')]/following::ul[1]/li[./following::*[contains(text(),'How')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Make sure you have bacon ready!\nToast your bread.\nAdd mayo to the toast, if you want it.\nLayer the lettuce, tomato and bacon on top.\nAdd 1 fried egg.\nCut in half and serve.")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Here’s the recipe')]/following::p[not(@class='wp-caption-text')][./following::*[contains(text(),'Hope')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/mom_crieff_pretzels_hershey_kisses_super_simple_snack.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Unwrap the Hershey Kisses. If you are making lots, this takes longer than you think : ).\nPreheat oven to 200 degrees Fahrenheit.\nPlace pretzels on a cookie sheet. I prefer the regular small pretzels but other people prefer the waffle ones.\nPlace an unwrapped Hershey Kiss on the center of the pretzels.\nBake for about 4 minutes at 200 degrees fahrenheit. Others I know do higher and faster but I tend to scorch my chocolate that way.\nGently place your topping treat on the Kiss and press it down into the Kiss.\nCool slightly and let the chocolate set before you devour them.")
      end
    end

    context "when the selector '//*[contains(text(),'Directions')]/following::p[./following::*[@class='entry-meta']]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/mom_crieff_three_ingredient_pineapple_cupcakes_cherries.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Preheat oven to 325 F.\nUse cupcake liners. Makes 24 cupcakes\nBake for 18 minutes or until toothpick comes out dry.\nMix cake mix with can of pineapple. Use all the juice and crushed pineapple in the can.\nDo NOT add any other ingredients (like those listed on the box).\nFill each cupcake liner about 3/4 full. Makes 24 cupcakes\nBake 18 minutes at 325F\nAfter the cupcakes have cooled slightly, poke three holes in each cupcake with a straw.\nDrizzle about a teaspoon of the maraschino cherry juice into each hole.\nAdd a cherry on top : ).")
      end
    end

    context "when the selector '//*[contains(text(),'measure')]/following::p[./following::*[contains(text(),'maybe')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/mom_crieff_get_green_smoothie.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["1 c. green grapes", "1-2 c. fresh pineapple and any juice", "2 kiwi (cut off the ends but include the skin! ) major vitamins in there!", "1 c. kale (break the leaves off the stem)", "1 c. spinach", "ice", "Optional: pinch of cayenne pepper and bits of cilantro."])
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end