# encoding: UTF-8

describe Hangry do
  context "thesweetchick.com churro-donuts-with-homemade-dulce-de" do
    before(:all) do
      @html = File.read("spec/fixtures/the_sweet_chick_churro_donuts_with_homemade_dulce_de.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) {should == "http://www.thesweetchick.com/2012/05/churro-donuts-with-homemade-dulce-de.html" }
    its(:image_url) { should == "http://www.recipage.com/images/user1466/1338424306/recipe_image.jpg" }
    its(:ingredients) { should == ["1/3 cup vegetable oil", "1 cup sugar", "2 eggs", "1 cup milk", "1/4 cup vinegar", "1/2 tsp vanilla", "2 cups AP flour", "4 tsp baking powder", "1/2 tsp baking soda", "1/4 tsp salt", "1 1/2 tsp cinnamon", "1 cup powdered sugar", "1 tsp vanilla extract", "4 tbsp milk", "http://www.thesweetchick.com/2012/05/homemade-dulce-de-leche.html"]}
    its(:instructions) { should == "Mix milk and vinegar together and set aside for 5-10 minutes until it forms curds.\nMeanwhile using a hand mixer or stand mixer, beat together oil and sugar. Add eggs and vanilla. Stir in the milk/vinegar and mix well.\nStir together the dry ingredients and slowly add to to the wet ingredients, making sure to have a nice smooth mixture.\nUse a piping bag or a Ziplock bag with tip cut off to fill each donut reservoir with about 2 tbsp of batter.\nBake for about 4-5 minutes or until toothpick inserted in center of donut comes out clean.\nPlace hot donuts on cooling rack with a wax paper underneath and prepare the glaze.\nIn a small bowl mix together powdered sugar, vanilla, and milk. You want the consistency to be runny, not thick.\nDip each donut in the mixture and flip it around by hand or with a fork until the whole donut is covered. Then place back on the cooling rack until the glaze is dry.\nOnce the glaze dries, spread the homemade dulce de leche over each donut. Eat and enjoy!" }
    its(:name) { should == "Churro Donuts" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end