#encoding: UTF-8

describe Hangry do
  context "skinnytaste.com " do
    before(:all) do
      @html = File.read("spec/fixtures/skinny_taste.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == "Gina" }
    its(:image_url) { should == "https://lh5.ggpht.com/_BizpeaUzxq8/TUDLlqO2O8I/AAAAAAAACok/84-XWKEtnkY/s800/skinny-arroz-con-pollo.jpg" }
    its(:ingredients) { should == ["8 skinless chicken thighs", "1 tbsp vinegar", "2 tsp sazon (I use Badia Sazon Tropical)", "about 1/2 tsp adobo powder", "about 1/2 garlic powder", "3 tsp olive oil", "1/2 onion", "1/4 cup cilantro", "3 cloves garlic", "5 scallions", "2 tbsp bell pepper", "1 tomato, diced", "2 1/2 cups enriched long grain rice", "4 cups water", "1 chicken bouillon cube", "salt to taste (about 2 tsp)"] }
    its(:instructions) { should == "Season chicken with vinegar, 1/2 tsp sazon, adobo and garlic powder and let it sit 10 minutes. Heat a large deep heavy skillet on medium, add 2 tsp oil when hot. Add chicken and brown 5 minutes on each side. Remove and set aside.\nPlace onion, cilantro, garlic, scallions and pepper in mini food processor. Add remaining teaspoon of olive oil to the skillet and sauté onion mixture on medium-low until soft, about 3 minutes. Add tomato, cook another minute. Add rice, mix well and cook another minute. Add water, bouillon (be sure it dissolves well) and remaining sazon, scraping up any browned bits from the bottom of the pot. Taste for salt, should taste salty enough to suit your taste, add more as needed.\nAdd chicken and nestle into rice, bring to a boil. Simmer on medium-low until most of the water evaporates and you see the liquid bubbling at the top of the rice line, then reduce heat to low heat and cover. Make sure the lid has a good seal, no steam should escape (You could place a piece of tin foil or paper towel in between the lid and the pot if steam escapes).\nCook 20 minutes without opening the lid. Shut heat off and let it sit with the lid on an additional 10 minutes (don’t peak!!!) Fluff with a fork and eat!" }
    its(:name) { should == "Arroz Con Pollo, Lightened Up" }
    its(:published_date) { should == Date.new(2011, 1, 26) }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when ArgumentError is raised in the parse_published_date method" do
      before(:each) do
        @html = File.read("spec/fixtures/skinny_taste_asian_peanut_noodles_with_chicken.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should set published date attribute to nil" do
        expect(subject.published_date).to be_nil
      end
    end

    context "when NoMethodError is raised in the parse_author method" do
      before(:each) do
        @html = File.read("spec/fixtures/skinny_taste_asian_peanut_noodles_with_chicken.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should set author attribute to nil" do
        expect(subject.author).to be_nil
      end
    end

    context "when the selector '.post .ingredients ul li' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/skinny_taste_spaghetti_squash_boats.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["3 small spaghetti squash (24 oz each)", "olive oil spray", "1 lb 93% ground turkey", "1 tsp garlic powder", "1 tsp cumin", "1 tsp kosher salt", "1/2 tsp chili powder", "1/2 tsp paprika", "1/2 tsp oregano", "1/2 small onion, minced", "2 tbsp bell pepper, minced", "1/2 cup water", "4 oz can tomato sauce", "3/4 cup part-skim shredded Mexican cheese blend (omit for W30)", "1 cup chopped tomato", "1/4 cup chopped scallion", "1/4 cup chopped fresh cilantro", "1/2 jalapeno, minced", "2 tablespoons fresh lime juice", "1/4 teaspoon kosher salt"])
      end
    end

    context "when the selector '//*[contains(text(),'Directions:')]/../following-sibling::p' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/skinny_taste_red_white_and_blueberry_trifle.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Whisk the condensed milk and water in a bowl.\nWhisk in the pudding mix for 2 minutes.\nLet stand for 2 minutes or until soft-set; fold in the whipped topping.\nArrange half of the cake in the bottom of a 14-cup trifle dish.\nSprinkle evenly with a layer of blueberries.\nSpread half of the cream mixture over the blueberries and gently spread (I piped it using a plastic bag and cut the corner off).\nTop with a layer of strawberries.\nLayer the remaining cake cubes on top of the strawberries, then add more blueberries and top with the remaining cream mixture.\nFinish with the remaining strawberries and blueberries, arranging them in a pretty pattern.\nCover and refrigerate at least one 1 hour.")
      end
    end
  end
end