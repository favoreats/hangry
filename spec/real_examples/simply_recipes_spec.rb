describe Hangry do
  context "simplyrecipes.com Mexican Street Corn Nachos" do
    before(:all) do
      @html = File.read("spec/fixtures/simply_recipes.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == "Mexican Street Corn Nachos" }
    its(:canonical_url) { should == "https://www.simplyrecipes.com/recipes/mexican_street_corn_nachos/" }
    its(:image_url) { should == "https://www.simplyrecipes.com/wp-content/uploads/2015/08/mexican-street-corn-nachos-horiz-a-1200.jpg" }
    its(:ingredients) { should == ["2 Tbsp canola oil (or other high smoke point oil)", "1 pound frozen corn (about 3 cups), still frozen", "2 Tbsp mayonnaise", "14 ounce bag low salt tortilla chips", "1 pound monterey jack cheese, grated (do not buy pre-grated, it won't melt properly)", "1 Tbsp cornstarch", "1/2 cup cream", "1/2 cup milk", "1 clove garlic, minced", "1/4 cup crumbled Cotija cheese (can sub crumbled feta)", "1/8 teaspoon chipotle chili powder (can use regular chili powder or smoked paprika)", "2 Tbsps of Mexican crema or 2 Tbsp sour cream that have been slightly diluted with water", "1/2 lime, sliced into wedges", "2 Tbsp chopped cilantro"] }
    its(:instructions) { should == "1 Sear the corn, then mix with mayo: Heat oil on high heat in a large, cast iron pan. When the oil is almost smoking hot, add half of the frozen corn (not defrosted) to the pan. Spread it out in the pan and let sear.\nStir occasionally, until most of the corn kernels have browned a little on at least one side. You'll know the corn is ready when some of the kernels start \"popping\".\nRemove the corn from the pan to a bowl, and repeat with the remaining frozen corn kernels.\nMix the corn with the mayonnaise and set aside.\n2 Toast tortilla chips: Preheat oven to 350°F. Spread the tortilla chips out on a large sheet pan and place in the oven for 10 minutes, until lightly browned.\n3 Make cheese sauce: While the tortilla chips are toasting, make the cheese sauce. Set up a double boiler with an inch of water in the lower pan.\nIf you don't have a double boiler, place an inch of water in a small saucepan and place a metal bowl over it so that steam from the boiling water below heats the bottom of the bowl. Do not let the bowl touch the water.\nPlace the grated monterey jack cheese in the top pan or bowl of the double boiler. Mix with the cornstarch. Pour in the cream and milk (can use half and half if you want). Add the minced garlic.\nHeat the water in the double boiler to a boil. Let the cheese gently melt, stirring occasionally, until the sauce is smooth.\n4 Assemble nachos: Once the tortilla chips are lightly browned, remove them from the oven. Transfer to a large serving platter.\nPour the cheese sauce over the chips. Sprinkle the top with the toasted corn. Sprinkle with cotija cheese crumbles. Sprinkle with chipotle chili powder. Drizzle with the crema or diluted sour cream. Sprinkle with cilantro.\nSprinkle with a little lime juice and serve immediately with lime wedges." }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end