# encoding: UTF-8

describe Hangry do
  context 'realsimple.com food-recipes-spanish-chicken-rice' do
    before(:all) do
      @html = File.read('spec/fixtures/real_simple_food_recipes_spanish_chicken_rice.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.realsimple.com/food-recipes/browse-all-recipes/spanish-chicken-rice' }
    its(:image_url) { should == 'https://imagesvc.timeincapp.com/v3/mm/image?url=https%3A%2F%2Fcdn-image.realsimple.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fportrait_435x518%2Fpublic%2Fshrimp-leek-pasta.jpg%3Fitok%3DMSRiE2hs&w=800&q=85' }
    its(:ingredients) { should == ['1 tablespoon olive oil', '1 pound boneless, skinless chicken breasts, cut into 2½-inch pieces', 'kosher salt and pepper', '1 medium onion, sliced', '1 green bell pepper, sliced', '2 cloves garlic, finely chopped', '1 cup dry white wine (such as Sauvignon Blanc)', '1 28-ounce can diced tomatoes, with juice', '1 cup long-grain white rice', '1 cup frozen peas', '¼ cup fresh flat-leaf parsley, roughly chopped', '¼ cup pimiento-stuffed Spanish olives, chopped (optional)'] }
    its(:instructions) { should == "In a large saucepan, heat the oil over medium heat. Pat the chicken dry with paper towels. Season with ½ teaspoon salt and ¼ teaspoon pepper.\nCook the chicken until golden brown, 2 minutes per side. Add the onion and bell pepper and cook, stirring occasionally, until soft, about 5 minutes. Add the garlic and cook, stirring, for 1 minute.\nAdd the wine, tomatoes and juice, rice, ½ teaspoon salt, and ¼ teaspoon pepper and bring to a boil. Reduce heat and simmer, covered, for 20 minutes.\nStir in peas and cook, covered, until heated through, about 2 minutes. Stir in the parsley.\nSpoon the chicken and rice onto plates. Serve with the olives, if desired." }
    its(:name) { should == 'Spanish Chicken and Rice' }

    context 'when real-simple-food-recipes-cook-salmon recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/real_simple_food_recipes_cook_salmon.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '[itemprop='articleBody'] ul li' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['¾ pound salmon sliced into 2 fillets', 'Extra virgin olive oil', 'Kosher salt', 'Freshly-ground black pepper'])
        end
      end

      context "and the selector '[itemprop='articleBody'] ol li' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Heat oven to 375 F.\nLine a baking sheet with parchment paper.\nPlace salmon on the baking sheet.\nDrizzle with olive oil.\nSeason with salt and pepper.\nRoast in oven for 20-25 minutes, until opaque and flaky.")
        end
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
