# encoding: UTF-8

describe Hangry do
  context "chocolatecoveredkatie.com triple-chocolate-nutella-fudge" do
    before(:all) do
      @html = File.read("spec/fixtures/chocolate_covered_katie_triple_chocolate_nutella_fudge.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://chocolatecoveredkatie.com/2012/05/21/triple-chocolate-nutella-fudge/" }
    its(:image_url) { should == "http://www.chocolatecoveredkatie.com/wp-content/uploads/Choco_AA98/nutella-fudge_thumb_3.jpg" }
    its(:ingredients) { should == ["1/4 cup coconut butter, melted", "1/4 cup chocolate hazelnut spread or Homemade Healthy Nutella", "1-2 tsp cocoa powder or cacao powder", "65g very ripe banana – about 1/4 a large banana", "1/8 tsp salt", "sugar or sweetener of choice, if desired (Some people want it; others don’t)"]}
    its(:instructions) { should == "Healthy Nutella Fudge Recipe: Combine all ingredients in a food processor or blender. (It’s best if the coconut butter is not solid, and if you use a frozen banana be sure to thaw so it doesn’t harden the coconut butter.) After blending, taste to see if you’d like to add any sweetener. Scoop into a container or even a little pie pan, and put in the fridge or freezer so it will firm up. (Alternatively, you can eat it soft, like frosting!) If desired, top with chocolate chips and extra Healthier Nutella (recipe linked above) like I did in the top two photos. This is freezer fudge, so it needs to be kept cold. It makes a small serving, but feel free to double or triple the recipe!"}
    its(:name) { should == "Healthy Nutella Fudge" }

    context "when the selector '//*[@class='entry-content']/ul/ul/li' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/chocolatecovered_katie_chocolate_peanut_butter_cake_in_a_mug.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["1 tablespoon plus 2 tsp cocoa powder (8g)", "3 tablespoons powdered peanut butter (PB2 or Betty Lou’s) (For substitutions, see “nutrition facts” link below) (18g)", "scant 1/16 tsp salt", "1 tsp sugar or xylitol (omit if you like bitter chocolate cake) (4g)", "1/4 tsp baking powder", "pinch stevia OR 1 additional tablespoon sugar", "2 tsp coconut or veg oil, or mashed banana or applesauce (I personally don’t like the fat-free option and recommend the oil, but I’m adding the option because I know many of you don’t mind the taste/texture of the lower-fat version.) (8g)", "3 tablespoons milk of choice (45g)", "1/4 tsp pure vanilla extract"])
      end
    end

    context "when the selector '.entry-content ol li' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/chocolate_covered_katie_homemade_cinnamon_rolls.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Heat the almond milk in a small saucepan over medium heat just until it starts to bubble on top. Remove from heat and whisk in ¼ cup of the buttery spread until melted (it will melt quicker if you cut it into chunks). Set aside and let cool to a lukewarm temperature while you get the dry ingredients together\nIn a large mixing bowl, whisk together whole wheat pastry flour, yeast, brown sugar, salt, and baking powder. Beat in the vanilla, flax egg, and the lukewarm milk/buttery spread mixture.\nAdd the all-purpose flour in two waves, stirring well with a wooden spoon after each addition. Once you have a loose dough, either use a knead hook in your mixer for about 3 minutes, or knead on a lightly-floured surface until smooth (about 5 minutes). This sounds gross, but you want the the dough to feel just a bit softer than your ear lobe when you pinch it (I know, what?).\nCover the dough with a damp towel and let rest for 25-30 minutes, until it holds its shape when poked.\nWhile the dough is resting, heat the raspberry, brown sugar, water and cornstarch in a small saucepan. Bring to a boil, and stir for 6-7 minutes (It will thicken in this time). Set aside to cool*.\nUse a rolling pin to create a 16×12 inch rectangle with the dough. Lightly brush the dough with the remaining melted buttery spread, then spoon the raspberry mixture evenly over the entire dough. Carefully roll up the dough. Some of the raspberry mixture will be squeezed out, but that’s okay. Pinch the seam to seal. Cut off the end pieces, as it seems unfair that someone would get stuck with the runt of the bunch. Cut the remaining dough into 8-9 rolls (depending on how big you want them). I used a cheese knife to cut mine, as the dough is quite soft**. Place cut-side-up in a greased 9-inch cake or pie pan, cover with a damp towel, and let rise for another 30-minutes. Set the oven to preheat to 375.\nAfter the dough has risen, put the rolls in the oven for 20 minutes, until golden brown on top.\nWhile the rolls are baking, mix almond milk and vanilla into the powdered sugar. You can adjust the levels to get the consistency you want.\nDrizzle the icing on top of the warm rolls. Best served warm! If you’ve enjoyed this recipe, check out the other recipes on Katie’s site.")
      end
    end

    context "when the selector '//*[contains(text(),'Instructions')]/following-sibling::div/p[not(a)]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/chocolate_covered_katie_minute_coffee_cake_in_a_mug.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("I prefer the oil or buttery spread, but that’s simply because I’m not a fan of fat-free baked goods. If using an oven, preheat to 330 F. Combine batter dry ingredients and mix well. Add wet and mix until just mixed. In a tiny bowl, combine all streusel ingredients. Fill a greased muffin tin 1/2 way with the batter (or use a ramekin or mug, if using the microwave). Sprinkle on two-thirds of the streusel, then spoon the remaining batter on top. Finally, sprinkle on the rest of the streusel. Cook 12-13 minutes in the oven, or around 1 minute in the microwave. Microwave times will vary, depending on microwave wattage.")
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

