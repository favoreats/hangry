# encoding: UTF-8

describe Hangry do
  context "embellishmints.com caramel-marshmallow-popcorn" do
    before(:all) do
      @html = File.read("spec/fixtures/embellish_mints_caramel_marshmallow_popcorn.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://embellishmints.com/caramel-marshmallow-popcorn/" }
    its(:image_url) { should == "https://embellishmints.com/wp-content/uploads/2013/03/Caramel-marshmallow-popcorn-recipe-600x800.jpg" }
    its(:ingredients) { should == ["1 package microwave popcorn", "1 tsp butter (you can use more if you don’t use butter flavored popcorn)", "1 cup brown sugar", "2 Tbsp. light corn syrup", "1/2 tsp. vanilla", "3 cups marshmallows", "Perfect M&M Pudding Cookies", "Heavenly Oreo Dessert", "Carrot Cake Muddy Buddies", "Cinnamon Roll Cake"] }
    its(:instructions) { should == "Pop popcorn according to package directions. Remove unpopped kernels\nMelt butter in a medium saucepan over medium low heat\nAdd brown sugar, corn syrup, vanilla and marshmallows\nStir until marshmallows are melted and the caramel mixture is smooth, turn up heat if necessary\nPour over popcorn and stir until popcorn is evenly coated\nStore in an airtight container" }
    its(:name) { should == "Caramel Marshmallow Popcorn" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end