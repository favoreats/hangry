# encoding: UTF-8

describe Hangry do
  context "embellishmints.com caramel-marshmallow-popcorn" do
    before(:all) do
      @html = File.read("spec/fixtures/everyday_annie_roasted_red_pepper_and_basil_pesto_penne.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://everydayannie.com/2008/11/26/roasted-red-pepper-and-basil-pesto-penne/" }
    its(:image_url) { should == "http://annies-eats.net/wp-content/uploads/2008/11/red-pepper-penne.jpg" }
    its(:ingredients) { should == ["3 cloves garlic", "1/3 cup pine nuts", "1/3 cup grated Parmesan cheese", "1 cup roasted red bell pepper (fresh or jarred)", "1 cup basil leaves", "½ tsp. salt", "¼ tsp. pepper", "1/3 cup extra virgin olive oil", "1 lb. penne pasta", "¼ cup heavy cream, optional"] }
    its(:instructions) { should == "Place garlic and pine nuts in a food processor. Pulse until finely chopped. Add Parmesan, red pepper, basil leaves, salt and pepper. Pulse until well combined. With the motor running, add olive oil through the feed tube and process until incorporated. Set aside.\nCook pasta according to package directions. Drain and return to the pot. Add the pesto to the pasta and stir to combine. Stir in heavy cream if desired for a creamier sauce. Heat until warmed through. Serve immediately." }
    its(:name) { should == "Roasted Red Pepper and Basil Pesto Penne" }

    context "when chocolate-sugar-cookies-and-how-to-marble-royal-icing recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/everyday_annie_chocolate_sugar_cookies_and_how_to_marble_royal_icing.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Printer')]/following::p[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients"do
          expect(subject.ingredients).to eq(["¾ cup all-purpose flour", "1/3 cup", "Dutch process cocoa powder", "Pinch of salt", "6 tbsp. (3 oz.) unsalted butter, at room temperature", "¾ cup confectioners’ sugar", "1 large egg", "½ tsp. vanilla extract"])
        end
      end

      context "and the selector '//*[contains(text(),'Directions')]/following::p[./following::*[contains(text(),'Note')]] | //*[contains(text(),'Directions')]/..' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("In a small bowl, combine the flour, cocoa powder and salt; whisk to blend and set aside. In the bowl of an electric mixer, combine the butter and sugar. Beat on medium-high speed until light and fluffy, 2-3 minutes. Blend in the egg and vanilla. With the mixer on low speed, add the dry ingredients and mix just until incorporated and no streaks remain. Form the dough into a disc, wrap tightly with plastic wrap and refrigerate until firm, 1-2 hours.\nPreheat the oven to 325˚ F. Line a baking sheet with parchment paper or a silicone baking mat. On a lightly floured work surface, roll the dough out to about ¼-inch thickness. Cut out desired shapes with cookie cutters and place cut outs on the prepared baking sheet. Bake 10-12 minutes, just until set. Let cool on the baking sheet about 5 minutes, then transfer to a wire rack to cool completely. Decorate as desired.")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients')]/following::span[not(contains(@style,'font-size:12pt'))][./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/everyday_annie_tomato_and_mozzarella_pasta_al_forno.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients"do
        expect(subject.ingredients).to eq(["2 tbsp. extra virgin olive oil", "2 garlic cloves, crushed", "2-14 oz. cans diced Italian plum tomatoes", "1 tsp. dried oregano", "S&P", "1 lb. pasta (tubes)", "8 oz. mozzarella cheese, cubed", "2/3 c. parmesan"])
      end
    end

    context "when the selector '.ingredient-text p' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/everyday_annie_baked_oven_fries.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients"do
        expect(subject.ingredients).to eq(["3 russet potatoes (about 24 oz. total), peeled and cut lengthwise into even sized wedges", "5 tbsp. vegetable, canola or peanut oil, divided", "¾ tsp. kosher salt, plus more to taste", "¼ tsp. freshly ground black pepper, plus more to taste"])
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end