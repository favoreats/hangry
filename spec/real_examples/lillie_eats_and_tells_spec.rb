# encoding: UTF-8

describe Hangry do
  context "lillieeatsandtells.com macro friendly egg sandwich with skinny lemon garlic aioli" do
    before(:all) do
      @html = File.read("spec/fixtures/lillie_eats_and_tells.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == 'Lillie' }
    its(:image_url) { should == "https://i0.wp.com/lillieeatsandtells.com/wp-content/uploads/2018/01/IMG_9690_1.jpg?resize=610%2C849&ssl=1" }
    its(:canonical_url) { should == "https://lillieeatsandtells.com/macro-friendly-egg-sandwich-with-skinny-lemon-garlic-aioli/" }
    its(:ingredients) { should == ["Kaiser bun from bakery section", "1 large egg", "50 g egg whites", "20 g avocado", "1 jimmy dean turkey sausage sliced horizontally into three strips (we keep these in stock from Costco)", "reduced fat pepperjack cheese", "arugula", "red onion", "lemon garlic aioli (\"recipe\" below)"] }
    its(:instructions) { should == "Make lemon garlic “aioli” by stirring together a couple of tbs of light mayo* with 1/2 cube frozen garlic, and just 1-2 grams lemon olive oil. It doesn’t take much but it adds so much flavor! If you don’t have it I’m sure the plain garlic mayo will still be great. *\nWhisk together egg and whites (sprinkled with some kosher salt and pepper) and cook over medium heat, JUST until set and then turn it off! They’ll continue to cook and dry eggs are NO BUENO. You want this pillowy and soft and delicious even though it’s short two yolks. I don’t really scramble, more like move it around and on top of itself so you have a nice round disc of soft cooked egg.\nDrape immediately with cheese and cover to let melt.\nSlice your Kaiser so the bottom is as thin as possible and hollow out the top as much as possible. You won’t miss it! But you’ll appreciate the carbs freed up for dessert later. I try and get mine down to about 38-40 grams which is about 20 carbs. Less than the English muffin I considered that’s much smaller overall.  Plus it makes a perfect cozy pocket to hold in all your toppings.\nSpray the insides with cooking spray (olive oil spray is my fave) and set your eggs aside so you can toast bun in the same pan.\nTop toasted bun with 1 tbs lemon-garlic aioli, cheesy egg, sausage strips, avocado, arugula, and red onion." }
    its(:name) { should == "Macro-Friendly Egg Sandwich with Skinny Lemon Garlic Aioli" }
    its(:prep_time) { should == 5 }
    its(:cook_time) { should == 5 }
    its(:total_time) { should == 10 }
    its(:yield) { should == '1' }
    its(:published_date) { should == Date.new(2018, 2, 21) }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when ArgumentError is raised in the parse_published_date method" do
      before(:each) do
        @html = File.read("spec/fixtures/lillie_eats_and_tells_macro_friendly_egg_sandwich.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should set published date attribute to nil" do
        expect(subject.published_date).to be_nil
      end
    end
  end
end
