# encoding: UTF-8

describe Hangry do
  context "nutmegnanny.com pesto-chicken-caprese-pasta" do
    before(:all) do
      @html = File.read("spec/fixtures/nutmeg_nanny.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.nutmegnanny.com/pesto-chicken-caprese-pasta/" }
    its(:image_url) { should == "https://www.nutmegnanny.com/wp-content/uploads/2016/08/pesto-chicken-caprese-pasta-290x290.jpg" }
    its(:ingredients) { should == ["4 boneless skinless chicken cutlets", "1/4 cup store-bought pesto", "Kosher salt and pepper, to taste", "1 pint grape tomatoes", "1 (14.5 ounce) can diced tomatoes", "3 cloves garlic, minced", "1 pound pasta, cooked", "1 pound small mozzarella balls", "1/3 cup pesto", "1/3 cup chiffonade basil", "2 cups reserved pasta water", "Grated parm, for topping", "Crushed red pepper flakes, for topping", "Balsamic glaze, for topping", "1 cup balsamic vinegar"]}
    its(:instructions) { should == "Coat chicken in pesto and desired amount of kosher salt and pepper. Grill until fully cooked, set aside.\nIn a large high-sided skillet add 1 tablespoon olive oil and set over medium-high heat. Once hot add in the tomatoes blistering their skin until they just start to soften. Remove from the skillet and set aside.\nTo the same skillet add diced tomatoes and garlic. Simmer just for a few minutes just to combine flavors. Add in the cooked pasta and toss. Moisten the pasta with reserved pasta water if needed. You can also drizzle on a bit of olive oil or add more pesto if you desire.\nAdd in blistered tomatoes, mozzarella balls and fresh basil. Toss to combine.\nSliced chicken and toss with pasta.\nIf desired, sprinkle top with grated parm, crushed red pepper flakes and balsamic glaze.\nFor balsamic glaze:\nAdd balsamic vinegar to a small pan and set over medium-high heat. Once the vinegar starts to bubble turn the heat down to low and let simmer until thick and syrupy. Make sure to whisk or stir your glaze while you're cooking it down. DO NOT leave this unattended. You can go from perfect glaze to burn smelly mess in seconds. This whole process should take about 10 minutes and leave you with about 1/4 cup glaze.\nNote: if the pasta gets dry just add in more reserved pasta water or moisten with more olive oil or pesto. Also, the small mozzarella balls I used are about the size of marbles." }
    its(:name) { should == "Pesto Chicken Caprese Salad" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end