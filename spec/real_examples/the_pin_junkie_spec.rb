# encoding: UTF-8

describe Hangry do
  context "thepinjunkie.com red-lobster-cheddar-biscuits" do
    before(:all) do
      @html = File.read("spec/fixtures/the_pin_junkie_red_lobster_cheddar_biscuits.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) {should == "http://www.thepinjunkie.com/2013/05/red-lobster-cheddar-biscuits.html" }
    its(:image_url) { should == "http://2.bp.blogspot.com/-lnGfU5eE4Os/UadjbRaIeqI/AAAAAAAACos/2GO1UppFy7I/s640/copy+cat+red+lobster+cheddar+biscuits.jpg" }
    its(:ingredients) { should == ["2 1/2 cups Bisquick", "4 Tablespoons cold butter", "1 cup cheddar cheese, shredded", "3/4 cup milk", "1/4 tsp garlic powder", "3 Tablespoons melted butter", "1/2 teaspoon garlic powder", "3/4 teaspoon dried parsley flakes"] }
    its(:instructions) { should == "  Add cold butter to Bisquick  Cut in with a pastry cutter\n  Add cheese and milk  Mix just until all ingredients are combined  \n  Drop by teaspoonfuls onto parchment lined baking sheets\n  In degree oven, bake - minutes or just until biscuits begin to turn light brown\n  Remove biscuits from baking sheets and place on wire racks to cool" }
    its(:name) { should == "Red Lobster Cheddar Biscuits" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end