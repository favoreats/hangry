# encoding: UTF-8

describe Hangry do
  context 'foxandbriar.com apple-cider-waffles' do
    before(:all) do
      @html = File.read('spec/fixtures/fox_and_briar_apple_cider_waffles.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.foxandbriar.com/apple-cider-waffles/' }
    its(:image_url) { should == 'https://www.foxandbriar.com/wp-content/uploads/2015/11/IMG_7646-Copy-1024x683.jpg' }
    its(:ingredients) { should == ['½ cup all purpose flour', '¼ cup whole wheat flour', '2 Tablespoons cornstarch', '½ teaspoon baking powder', '¼ teaspoon salt', '½ cup nonfat greek yogurt', '½ cup fresh apple cider (or apple juice)', '⅓ cup canola oil or vegetable oil', '2 eggs', '1 tablespoon brown sugar', '1 teaspoon vanilla extract', '¼ teaspoon cinnamon', '¼ teaspoon ground ginger', '⅛ teaspoon allspice', '⅛ teaspoon ground cloves', '⅛ teaspoon nutmeg'] }
    its(:instructions) { should == "In a medium sized bowl, mix the flour, cornstarch, baking powder, salt and spices until combined, set aside.\nIn a separate bowl, mix the yogurt, apple cider, oil, eggs, brown sugar, and vanilla until well combined.\nPour the wet ingredients into the dry ingredients and mix until combined. Allow to sit for 30 minutes.\nHeat the waffle iron and cook according to manufacturer instructions. Serve waffles immediately with butter and maple syrup or arrange in a single layer on a cooling rack and keep in a 200 degree oven until ready to serve." }
    its(:name) { should == 'Apple Cider Waffles' }

    context 'when sausage-stuffed-portobello-mushrooms recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/fox_and_briar_sausage_stuffed_portobello_mushrooms.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '.cookbook-ingredient-item' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['8 Portobello mushrooms, stems and gills removed', '1 pound hot Italian Sausage', '2 Tablespoons olive oil', '1 onion, diced', '1 red bell pepper, diced', '1 green bell pepper, diced', '4 garlic cloves, minced', '1 teaspoon oregano', '28 ounce can crushed tomatoes', '16 ounces mozzarella cheese, grated', 'salt', 'pepper', 'fresh basil for garnish (optional)'])
        end
      end

      context "and the selector '.cookbook-instruction-item' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Preheat oven to 375 degrees.\nRemove stems from mushrooms and use a spoon to scrape out the gills.  Arrange on a baking sheet, brush with olive oil and season with salt and pepper.  Set aside.\nIn a saute pan over medium heat, brown the sausage.  Remove from pan.  Add 1 tablespoon olive oil.  When hot, add the onions and bell peppers, season with salt and pepper.  Saute until they have released their water and water has evaporated, and they are soft and starting to turn golden. \nAdd the garlic and oregano, saute for 30 seconds.  Add the crushed tomatoes and the sausage back into the pan, stir. Simmer for at least 20 minutes to let flavors combine.\nSpoon sausage mixture into the mushrooms, top with shredded cheese.\nBake for 15 minutes, or until cheese is melted and mushrooms are tender.  Finish under the broiler for 1-2 minutes to brown cheese, if desired.\nGarnish with fresh chopped basil if desired.")
        end
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
