# encoding: UTF-8

describe Hangry do
  context "hellofresh.com chicken-schnitzel-for-dinner" do
    before(:all) do
      @html = File.read("spec/fixtures/hello_fresh_chicken_schnitzel_for_dinner.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.hellofresh.com/recipes/chicken-schnitzel-for-dinner-5b6e02acae08b52016280372" }
    its(:image_url) { should == "https://res.cloudinary.com/hellofresh/image/upload/f_auto,fl_lossy,h_300,q_auto,w_450/v1/hellofresh_s3/image/w39-r12-83f99eab.jpg" }
    its(:ingredients) { should == ["2 unitScallions", "12 ounceYukon Gold Potatoes", "8 tablespoonSour Cream(ContainsMilk)", "½ cupPanko Breadcrumbs(ContainsWheat)", "1 tablespoonFry Seasoning", "20 ounceChicken Cutlets", "1 unitLemon", "4 ounceShredded Red Cabbage", "2 unitBrioche Buns(ContainsMilk,Wheat,Eggs)", "2 sliceCheddar Cheese(ContainsMilk)", "2 teaspoonHot Sauce", "2 tablespoonMayonnaise(ContainsEggs)", "1 unitDill Pickle", "1 tablespoonSweet and Smoky BBQ Seasoning", "1 teaspoonVegetable Oil", "2 tablespoonButter(ContainsMilk)", "1 teaspoonSugar", "Salt", "Pepper"] }
    its(:instructions) { should == "Wash and dry all produce. Halve lemon; cut one half into wedges. Trim, then thinly slice scallions, separating greens and whites. Cut potatoes into ½-inch cubes, then place in a large pot with enough salted water to cover by 1 inch. Bring to a boil and cook until tender, about 15 minutes. Meanwhile, stir together panko and fry seasoning on a large plate. Season with plenty of salt and pepper. Pat chicken dry with a paper towel.\nBrush two pieces of chicken with 2 TBSP sour cream (1 packet). Coat chicken in panko mixture, pressing to adhere. Set aside. Take remaining chicken and season all over with barbecue seasoning, salt, and pepper. Heat a drizzle of oil in a large pan over medium-high heat. Add barbecue chicken and cook until browned on surface and no longer pink throughout, 4-6 minutes per side. Remove from pan, store in a reusable container, and refrigerate overnight.\nOnce potatoes are tender, reserve 1/2 cup cooking water, then drain well. Place scallion whites and 1 TBSP butter in empty pot. Cook over low heat until fragrant, about 30 seconds. Add potatoes and mash until smooth. Stir in 2 TBSP sour cream (1 pack) and reserved cooking water as needed to create a creamy consistency. Season with salt and pepper. Keep on low heat, stirring occasionally, until meal is ready.\nHeat ¼-inch layer of oil in pan used for barbecue chicken. Once hot, add breaded chicken to pan in a single layer. Cook until panko is crisp and chicken is no longer pink throughout, 4-6 minutes per side. Transfer to a paper towel-lined plate. Season with salt and pepper. Discard oil and wipe pan with a paper towel.\nMelt 1 TBSP butter in same pan over medium-high heat. Add cabbage and season with plenty of salt and pepper. Cook, tossing, until tender, 3-4 minutes. Stir in juice from lemon half, 1 tsp sugar, and half the scallion greens. Cook until fragrant, about 1 minute. Season with salt and pepper. Divide potatoes and cabbage between plates. Arrange breaded chicken alongside, garnish with remaining scallion greens, and serve with lemon wedges on the side.\nIn the morning, split buns (toast if desired). Slice pickle into thin rounds. Mix mayonnaise, ranch seasoning, and remaining sour cream in a small bowl. Season with salt and pepper. Spread mixture onto buns, then fill each with barbecue chicken, cheddar, and pickle slices. Wrap in plastic wrap or foil and pack each with hot sauce. Drizzle with hot sauce before enjoying. TIP: If desired, remove chicken and microwave separately until hot." }
    its(:name) { should == "Chicken Schnitzel for Dinner with a BBQ Chicken Sandwich for Lunch" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

