# encoding: UTF-8

describe Hangry do
  context 'somethingswanky.com peanut-butter-pretzel-muddy-buddies' do
    before(:all) do
      @html = File.read('spec/fixtures/something_swanky.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.somethingswanky.com/peanut-butter-pretzel-muddy-buddies/' }
    its(:image_url) { should == 'https://www.somethingswanky.com/wp-content/uploads/2015/06/DSC_0280-edited-620x926.jpg' }
    its(:ingredients) { should ==  ['1/2 cup creamy peanut butter', '1 cup semi-sweet chocolate chips', '1/4 cup unsalted butter', '8 cups peanut butter filled pretzel nuggets', '1 1/2 cups confectioners’ sugar'] }
    its(:instructions) { should == "Place the peanut butter, chocolate chips, and butter in a large microwave-safe bowl. Microwave on medium power for 1 minute or until the chocolate chips are melted. If needed, add additional heating time in 30-second increments, stirring after each time, until the chocolate, peanut butter, and butter are melted and the mixture is smooth.\nAdd the pretzel pieces to the bowl. Using a large rubber spatula, gently stir to coat all of the pieces well.\nPlace the confectioners’ sugar in a gallon-size zip-top bag.\nSpoon the coated pretzel pieces into prepared zip-top bag. Seal the bag and shake to coat the pretzels in the sugar.\nOnce the pretzel pieces are coated, transfer to a bowl to serve immediately. The pretzels may be stored in an airtight container for up to 3 weeks." }
    its(:name) { should == 'Peanut Butter Pretzel Muddy Buddies' }

    context "when the selector '.ingredients' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/something_swanky_smores_cookie_bars.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['1/2 cup butter, softened', '2/3 cup light brown sugar', '1 egg', '1 teaspoons vanilla extract', '1/2 teaspoon baking soda', '1/4 teaspoon salt', '1 cups All-Purpose Flour', '3/4 cup fine graham cracker crumbs', '1/2 cup marshmallow cream', '1/2 cup chocolate semi-sweet chocolate chips'])
      end
    end

    context 'when easy-and-fluffy-belgian-waffles recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/something_swanky_easy_and_fluffy_belgian_waffles.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '.ingredient' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['1 3/4 c. AP flour', '1/4 cup corn starch', '2 tbsp. sugar', '1 tbsp. baking powder', '1/4 tsp. salt', 'a pinch of nutmeg', '1/2 tsp cinnamon', '2 eggs', '1 3/4 c. milk', '1/2 c. cooking oil', '1 1/2 tsp. vanilla'])
        end
      end

      context "and the selector '.instructions' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Mix together the dry ingredients.\nAdd the eggs, milk, oil, and vanilla. Mix until batter forms.\nPour 1/2 cup batter onto hot waffle griddle and cook according to waffle maker instructions (will vary depending on appliance).\nServe warm!")
        end
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
