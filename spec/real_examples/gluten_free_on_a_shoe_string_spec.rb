# encoding: UTF-8

describe Hangry do
  context 'glutenfreeonashoestring.com cake-pops-go-gluten-free' do
    before(:all) do
      @html = File.read('spec/fixtures/gluten_free_on_a_shoe_string_cake_pops_go_gluten_free.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://glutenfreeonashoestring.com/cake-pops-go-gluten-free/' }
    its(:image_url) { should == 'https://glutenfreeonashoestring.com/wp-content/uploads/2011/11/DSC_0028-1024x682.jpg' }
    its(:ingredients) { should == ['1 box gluten-free cake mix, baked according to package directions', '1 (16 ounce) can gluten-free frosting', '10 to 12 ounces gluten-free semi-sweet baking chocolate, roughly chopped', 'Lollipop sticks (optional)', 'Sprinkles or nonpareils for decorating (optional)'] }
    its(:instructions) { should == "In a large bowl, crumble the cake between your fingers until it’s all … crumbs.\nAdd about half the can of frosting. Press the cake together with the frosting with the back of a spoon until the mixture is smooth (see the photo). Add more frosting as necessary.\nRoll the mixture into balls about 1 inch in diameter. Place the balls on a rimmed baking sheet lined with parchment paper, and then place in the freezer for at least 30 minutes, and up to a few hours.\nMelt and temper the first 10 ounces of chocolate by placing it in a deep microwave-safe bowl, and microwaving it on high for 30 seconds at a time, stirring in between until smooth and shiny. Place the remaining 2 ounces of chocolate (this is the “seed”) into the melted chocolate and stir constantly until most of the “seed” is melted and the chocolate is no longer hot but is still smooth.\nStick the tip of each of the lollipop sticks into the tempered chocolate, place on a baking sheet, and freeze for a minute until the chocolate is set. Insert the chocolate-dipped end of each stick halfway into each cake ball. Holding the other end of the stick, dip it straight down into the tempered chocolate and up and out about two inches forward, in a motion the shape of a “U.” Tap off the excess chocolate, very very gently, on the side of the bowl.\nDip the still-wet chocolate in sprinkles if using, and allow to dry standing up in a glass (or stuck in a block of styrofoam) until the chocolate is set.\nStore at room temperature. The chocolate forms quite the protective layer, and will remain solid at room temperature." }
    its(:name) { should == 'Cake Pops Go Gluten-Free!' }

    context 'when gf-lemon-bars-for-dad-plain-lemons-for-me recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/gluten_free_on_a_shoe_string_gf_lemon_bars_for_dad_plain_lemons_for_me.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(@class,'section-title ingredients')]/following::p[./following::*[contains(@class,'section-title post-steps')]][not(@class='p4')]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['1 2/3 cups (233 g) basic gum-free gluten free flour blend (superfine white rice flour + potato starch + tapioca starch/flour)', '1/2 cup (58g) confectioners’ sugar, plus more for dusting', '1/2 teaspoon kosher salt', 'Zest of 1 large lemon', '9 tablespoons (126g) unsalted butter, melted and cooled', '4 eggs (200 g, weighed out of shell)', '1 cup (200 g) granulated sugar', '3/4 teaspoon baking powder', '2/3 cup freshly squeezed lemon juice (juice of about 4 lemons)'])
        end
      end

      context "and the selector '.directions-list li' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Preheat your oven to 325°F. Grease an 8-inch square baking pan, line with criss-crossed pieces of parchment paper that overhang the sides, and grease the parchment paper. Set the pan aside.\nMake the crust. In a medium-sized bowl, combine 1 cup (140 g) of the flour, confectioners’ sugar, salt and lemon zest and whisk to combine, breaking up any clumps of lemon zest. Add the butter and mix with a fork until well-combined. Press the mixture into the bottom of the prepared baking dish in an even layer. Place the baking dish in the center of the preheated oven and bake for about 15 minutes or until firm. Remove from the oven and allow to cool briefly.\nMake the custard layer. In a medium-sized bowl, place the eggs, granulated sugar, baking powder, lemon juice and remaining 2/3 cup (93 g) flour, whisking vigorously to combine after each addition. Pour the custard mixture into the baked crust and return the pan to the center of the oven. Bake until just set (20 to 25 minutes). The custard is set when it does not jiggle more than a tiny bit in the center when the pan is shaken gently back and forth. Remove the pan from the oven and allow to cool in the pan for about 20 minutes.\nPlace in the refrigerator to chill until firm, about 2 hours and up to overnight. Remove the bars from the pan by running a butter knife or thin spatula around the perimeter of the baking dish, and then lifting the bars out of the pan by the overhung pieces of parchment paper. Dust lightly with confectioners sugar, and slice into 9 or 12 squares with a large knife. Serve chilled.")
        end
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
