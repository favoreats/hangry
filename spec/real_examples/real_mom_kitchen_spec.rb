# encoding: UTF-8

describe Hangry do
  context 'realmomkitchen.com baked-cheesy-chicken-penne' do
    before(:all) do
      @html = File.read('spec/fixtures/real_mom_kitchen_baked_cheesy_chicken_penne.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) {should == 'https://realmomkitchen.com/1505/baked-cheesy-chicken-penne/' }
    its(:image_url) { should == 'https://realmomkitchen.com/wp-content/uploads/2010/03/Chicken-Pasta-450.jpg' }
    its(:ingredients) { should == ["6 tablespoons butter, plus more for baking dishes", "kosher salt and black pepper", "1 pound penne rigate (", "I used a 14.5 oz box of Ronzoni Smart Taste Penne Rigate", ")", "1 teaspoon olive oil", "2 boneless, skinless chicken breast halves (about 8 ounces each), halved horizontally (", "I used some leftover cooked chicken", ")", "1/2 cup plus 2 tablespoons flour", "6 garlic cloves, finely minced", "6 cups whole milk", "10 ounces white or cremini mushrooms, trimmed and thinly sliced (", "I used 8 oz of button mushrooms", ")", "1 cup sliced oil-packed sun-dried tomatoes, drained", "1 1/2 cups shredded provolone (6 ounces)", "(I used an italian blend that had provolone in it)", "1 1/2 cup freshly grated Parmesan (about 6 ounces)"] }
    its(:instructions) { should == "Preheat oven to 400. Butter two shallow 2-quart baking dishes. If you’re going to freeze one, use a disposable foil pan. In a large pot of boiling salted water, cook pasta 3 minutes short of al dente; drain pasta and set aside.\nWhile pasta is boiling, heat oil over medium-high heat in a large skillet. Season chicken with salt and pepper; cook until opaque throughout, 3 to 5 minutes per side. Halve each piece lengthwise, then thinly slice crosswise. While chicken pan is still hot, toss the mushrooms in and saute until golden brown (3-4 minutes). Use a little extra olive oil if needed.(\nSince I used left over chicken, I just cooked the mushrooms in the olive oil)\nIn a 5-quart Dutch oven or heavy pot , melt butter over medium. Add flour and garlic; cook, whisking, 1 minute. While whisking constantly, gradually add milk; bring to a simmer. Keep whisking frequently as sauce thickens, about 1 minute more. Add mushrooms and tomatoes; cook 1 minute. Take the pan off the heat and and gradually stir in provolone and 1/2 cup Parmesan.\nAdd chicken and pasta to pot; season with salt and pepper to taste. Divide pasta mixture between baking dishes. If freezing, place remaining parmesan cheese in a zip-top baggie. If cooking, sprinkle on top.\nBake, uncovered, until top is golden and bubbling, about 25 minutes. Let stand 5 minutes before serving." }
    its(:name) { should == 'Baked Cheesy Chicken Penne' }

    context "when the selector '//*[@class='row']//strong/following::p[1]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/real_mom_kitchen_baked_zucchini_with_mozzarella.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Slice your zucchini up into 1/2 inch rings.\nLay them out flat on a large cookie sheet. Sprinkle with Johnny’s Garlic Seasoning and a bit of salt.\nBake in the oven at 350 for about 10-15 minutes or until crisp tender.\nTake the hot pan out of the oven and sprinkle the zucchini with cheese. Turn the oven to broil, throw that cookie sheet back into the oven and let the cheese get all bubbly and a bit browned. This should only take about 3-5 minutes. Be careful here. Broiling things in the oven can get out of control in a fast hurry. (my trick for broiling is to not move the rack up, I just keep it in the middle of the oven and things are less likely to burn. It may take a minute longer, but no burnt food)")
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
