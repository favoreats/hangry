# encoding: UTF-8

describe Hangry do
  context 'athriftymom.com Restaurant_style_breakfast_made_easy_lemon_blueberry_stuffed_french_toast_recipe' do
    before(:all) do
      @html = File.read('spec/fixtures/athrifty_mom_restaurant_style_breakfast_made_easy_lemon_blueberry_stuffed_french_toast_recipe.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://athriftymom.com/restaurant-style-breakfast-made-easy-lemon-blueberry-stuffed-french-toast-recipe/' }
    its(:image_url) { should == 'http://1pa6q42ounl23kigam363hm3-wpengine.netdna-ssl.com/wp-content/uploads//2015/05/French-Toast-Stuffed-French-Toast-breakfast-ideas-restaurant-style-Easy-recipes-that-look-super-fancy-breakfast-menu-Restaurant-Style-Breakfast-Made-Easy-Lemon-Blueberry-Stuffed-French-Toast-Recipe.jpg' }
    its(:ingredients) { should == ['1 ½ C blueberries', '¼ C coconut sugar (or regular sugar)', '1 lemon-its zest and juice', '1 loaf cinnamon raisin bread', '2 C milk', '3 eggs', '1 tsp vanilla', '1 tsp cinnamon-optional', '1 tub cool whip', '1 block cream cheese'] }
    its(:instructions) { should == "Start by whisking together your eggs, milk, vanilla, and cinnamon in an 8X8 dish. Once combined soak thee cinnamon raisin bread into the milk mixture. I tend to just dunk them in and then flip to the other side and pull it out. Take them out and put them on a hot pan or a hot skillet. Cook both sides until they are a golden brown.\nTo make the cream filling add soft cream cheese to a bowl and mix with electric mixture until smooth and fluffy. Add in the tub of cool whip and mix on medium speed for 1 minute. You will use this mixture to spread between 2 slices of French toast. Or you can add a little on top.\nIn a sauce pan add in the blueberries, sugar, and lemon zest and lemon juice.\nBring to a boil and stir constantly until blueberries have softened and mixture has thickened up. Serve over stuffed French toast." }
    its(:name) { should == 'Restaurant Style Breakfast Made Easy ~ Lemon Blueberry Stuffed French Toast Recipe' }

    context 'when Cupcakes-in-a-cone-diy-kid-friendly-perfect-for-parties-and-picnics recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/athrifty_mom_cupcakes_in_a_cone_diy_kid_friendly_perfect_for_parties_and_picnics.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients')]/following::span[./following::*[contains(text(),'Instructions')]]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['1 Cake Mix', '1 tub of frosting', '24 ice cream cones (normal size)', 'muffin tin', 'Small Jelly beans ( we used Gimbal’s Allergen-Free Fine Candies Gourmet Jelly Beans)'])
        end
      end

      context "and the selector '//*[contains(text(),'Instructions')]/following::span[./following::*[contains(text(),'cupcakes')]]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Put the cones in the muffin tins.\n2. Follow the cake mix instructions on the box.\n3. Pour the cake mix batter in the cones up to the line before the top of the cone. (look at photo)\n4. Bake-Use the chart on the back of the box for cupcakes. Check with toothpick.\nCool completely.\n6. Frost and decorate.")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients') or contains(text(),'Yield') or contains(text(),'need')]/following::ul[1]/li' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/athrifty_mom_easy_homemade_healthy_fried_rice_recipe_sidedish.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['3 cups cooked white rice', '3 Tablespoons sesame oil (or vegetable oil) – I find Sesame Oil gives it more of the taste of a Chinese Restaurant', '1 cup frozen peas and carrots (thawed)', '1 small onion, chopped', '2 teaspoons minced garlic', '2 eggs, slightly beaten', '3-4 Tablespoons soy sauce'])
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
