# encoding: UTF-8

describe Hangry do
  context "dessertnowdinnerlater.com garlic-parmesan-orzo" do
    before(:all) do
      @html = File.read("spec/fixtures/dessert_now_dinner_later_garlic_parmesan_orzo.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.dessertnowdinnerlater.com/garlic-parmesan-orzo/" }
    its(:image_url) { should == "https://www.dessertnowdinnerlater.com/wp-content/uploads/2013/03/Garlic-Parmesan-Orzo1.jpg" }
    its(:ingredients) { should == ["1 cup orzo pasta", "3 Tbsp butter", "2 tsp garlic, minced", "1/3 cup shredded parmesan", "1/4 cup milk", "1/2 tsp salt", "1/2 tsp pepper", "1 tsp dried parsley"] }
    its(:instructions) { should == "Boil orzo according to directions on the package (usually 9-10 minutes.) Drain &amp; set aside.\nIn a saucepan, melt butter &amp; saute garlic until fragrant about 1-2 minutes.\nAdd cooked orzo, parmesan, milk, salt, pepper, &amp; parsley. Stir until cheese melts &amp; everything is combined. Taste &amp; adjust seasonings if needed." }
    its(:name) { should == "Garlic Parmesan Orzo" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end