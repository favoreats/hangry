# encoding: UTF-8

describe Hangry do
  context "goodcheapeats.com Whole Grain Cinnamon Rolls" do
    before(:all) do
      @html = File.read("spec/fixtures/good_cheap_eats.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == "Jessica Fisher" }
    its(:image_url) { should == "https://goodcheapeats.com/wp-content/uploads/2009/10/Whole-Grain-Cinnamon-Rolls.jpg" }
    its(:ingredients) { should == ["2 cups milk", "4 1/2 tablespoons butter", "1/2 tablespoon vanilla extract", "1/2 cup All-Bran cereal or quick rolled oats", "2 cups whole wheat flour", "2 to 2 1/2 cups unbleached, all-purpose flour", "1/4 cup granulated sugar", "1 3/4 teaspoon salt", "3 3/4 teaspoon yeast", "3 1/2 tablespoons butter, softened", "3/4 cup dark brown sugar", "1 tablespoon cinnamon", "3/4 cup powdered sugar", "1 – 2 Tablespoons milk"] }
    its(:instructions) { should == "Combine dough ingredients in the bread machine according to manufacturer’s instructions. Use 4 cups of flour to start. Add more flour in case the dough is too wet. Program for Dough and allow the machine to do its magic.\nIf you don’t have a bread machine, you can still make this recipe. Warm the milk slightly and melt the 4 1/2 tablespoons butter in a medium saucepan. As long as the temperature is not above 115 degrees, add the granulated sugar, vanilla, and yeast. Let that rest for 5 minutes. Transfer to a large mixing bowl, then add the flours and salt. Stir until you have a sticky dough but all the flour is incorporated. Turn it onto a floured surface and knead until the dough becomes elastic. Set into a greased bowl and allow to rise until doubled in bulk, about an hour.\nWhen the dough is ready, roll out on a lightly floured surface until you have a 12 x 15-inch rectangle. Spread the 3 1/2 tablespoons softened butter over surface. In small bowl, combine the brown sugar and cinnamon. Sprinkle over butter. Roll up dough, jelly-roll fashion, starting from the long edge and pinching the seam to seal.\nSlice into 12 equal portions and arrange evenly in a greased, 9 x 13-inch pan. You can use unflavored dental floss to neatly cut the rolls.\nAllow the rolls to rise another 30 minutes before baking. Or you can cover the with plastic wrap and refrigerate overnight or wrap well with foil and freeze. Check this post for more tips on freezing cinnamon rolls. In the morning, remove from refrigerator and allow to rest 20 minutes.\nPreheat oven to 350°. Bake rolls until lightly browned, about 20 to 25 minutes. Remove from oven and cool on a wire rack.\nCombine the powdered sugar and 1 to 2 tablespoons milk in a small mixing bowl, stirring with a wooden spoon until combined. Glaze baked rolls with icing."}
    its(:name) { should == "Whole Grain Cinnamon Rolls" }
    its(:prep_time) { should == 100 }
    its(:cook_time) { should == 25 }
    its(:total_time) { should == 125 }
    its(:yield) { should == "12 rolls" }
    its(:published_date) { should == "October 28, 2009" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
