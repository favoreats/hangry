# encoding: UTF-8

describe Hangry do
  context "raininghotcoupons.com copycat-sonic-cherry-limeade" do
    before(:all) do
      @html = File.read("spec/fixtures/raining_hot_coupons_copycat_sonic_cherry_limeade.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) {should == "http://www.raininghotcoupons.com/copycat-sonic-cherry-limeade/"}
    its(:image_url) { should == "http://www.raininghotcoupons.com/wp-content/uploads/2014/03/cherry-limeade.png.png?x93331" }
    its(:ingredients) { should == ["12 oz Sprite", "2 Marischino Cherries", "1/8 Lime (cut in 2 wedges)", "3 tbsp liquid from Marischino Cherry jar", "1 cup Crushed Ice"] }
    its(:instructions) { should == "Place cherries and 1 lime wedge in bottom of a glass.\nFill with crushed ice.\nAdd Sprite.\nAdd cherry liquid and squeeze juice from second lime wedge on top." }
    its(:name) { should == "Copycat Sonic Cherry Limeade" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end