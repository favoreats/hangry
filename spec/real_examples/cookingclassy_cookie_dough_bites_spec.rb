# encoding: UTF-8

describe Hangry do
  context "cookingclassy.com chocolate chip cookie dough bites" do
    before(:all) do
      @html = File.read("spec/fixtures/cookingclassy_cookie_dough_bites.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == "Chocolate Chip Cookie Dough Bites" }
    its(:author) { should == 'Jaclyn' }
    its(:yield) { should == 'About 2 dozen' }
    its(:prep_time) { should == 20 }
    its(:cook_time) { should == 5 }
    its(:description) { should == 'Yield: About 2 dozen' }
    its(:image_url) { should == "https://www.cookingclassy.com/wp-content/uploads/2017/07/chocolate-chip-cookie-dough-bites-13.jpg" }
    its(:ingredients) { should == ["1 1/4 cups (178g) all-purpose flour",
                                   "1/2 cup (113g) unsalted butter, (softened)",
                                   "1/2 cup (110g) packed light-brown sugar",
                                   "1/3 cup (70g) granulated sugar",
                                   "1/4 tsp salt",
                                   "1 1/2 Tbsp half and half, (then more as needed)",
                                   "1 tsp vanilla extract",
                                   "3/4 cup (130g) chocolate chips ((I like a blend of semi-sweet and milk chocolate))"] }
    its(:instructions) { should == "Preheat oven to 350 degrees. Sprinkle flour evenly onto a rimmed 18 by 13-inch baking sheet. Bake in preheated oven 5 minutes, then remove and let cool completely.\nIn the bowl of an electric mixer fitted with the paddle attachment cream together butter, brown sugar, granulated sugar and salt until pale and fluffy about 3 minutes. &nbsp;\nStir in half and half and vanilla extract.&nbsp;Add in flour and mix just until combined.\nDrizzle in more half and half, 1/2 Tbsp at a time, as needed to bring dough together. Mix in chopped chocolate chips (set some aside if you want to press some into top after shaping for looks).&nbsp;\nScoop dough out about 1 tablespoon at a time and roll into balls. Place on parchment paper.&nbsp;\nServe immediately or store cookie dough bites in an airtight container in refrigerator.\nRecipe originally shared May 2012. Updated August 2017.&nbsp;\nRecipe source: Cooking Classy" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when NoMethodError is raised in the parse_yield method" do
      before(:each) do
        @html = File.read("spec/fixtures/cookingclassy.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should set yield attribute to nil" do
        expect(subject.yield).to be_nil
      end
    end
  end
end
