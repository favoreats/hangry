#encoding: UTF-8

describe Hangry do
  context "sweettwistofblogging.com apricot-cranberry-and-almond-spelt-oat" do
    before(:all) do
      @html = File.read("spec/fixtures/sweet_twist_of_blogging_apricot_cranberry_and_almond_spelt_oat.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://www.sweettwistofblogging.com/2013/05/apricot-cranberry-and-almond-spelt-oat.html" }
    its(:image_url) { should == "http://4.bp.blogspot.com/-0tk39kGnIjY/UZwitHWUbSI/AAAAAAAAHt4/H4mOQmXbRd0/s640/Spelt+Oat+Bars-20130513-3254+text.jpg" }
    its(:ingredients) { should == ["3/4 cup rolled oats", "3/4 cup spelt oats", "1/2 cup toasted almonds, chopped", "1/2 cup dried cranberries", "1/2 cup chopped apricots", "1 tsp ground cinnamon", "1/3 cup honey", "2/3 cup unsweetened almond milk", "1 pear, grated", "1 tsp pure vanilla extract"] }
    its(:instructions) { should == "Preheat oven to F.  Spray a inch square pan wit non stick spray. \nIn a large bowl combine oats, almond, dried fruit and cinnamon. \nIn a large measuring cup , whisk together honey, almond milk, apple and vanilla\nPour the wet mixture over the dry mixture and mix until well combined. \nBake for - minutes, until golden brown.   Let cool and refrigerate until firm.  Slice into squares. " }
    its(:name) { should == "Apricot, Cranberry and Almond Spelt Oat Bars." }

    context "when the selector '.ingredients ul li' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/tarathueon_banana_bread_with_crumb_topping.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["4 TBSP butter, melted", "½ cup sugar or honey", "2 eggs, beaten", "1 cup banana, mashed", "1¾ cups Pamela’s Baking & Pancake Mix", "½ teaspoon salt", "1 teaspoon vanilla", "1/2 cup of Pamelas Baking & Pancake Mix", "1/2 cup Brown Sugar", "4 Tbsp butter softened", "1 tsp cinnamon"])
      end
    end
  end
end
