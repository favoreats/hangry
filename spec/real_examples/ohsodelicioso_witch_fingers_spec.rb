# encoding: UTF-8

describe Hangry do
  context "ohsodelicioso.com witch fingers breadstick" do
    before(:all) do
      @html = File.read("spec/fixtures/ohsodelicioso_witch_fingers.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == "Desarae" }
    its(:image_url) { should == "https://ohsodelicioso.com/wp-content/uploads/2017/10/fingrebreadsticks-4.jpg" }
    its(:ingredients) { should == ["1 1/2 cups of warm water", "3 cups flour (about)", "1 T yeast", "1/2 tsp salt", "2 T sugar", "almonds"] }
    its(:instructions) { should == "In bowl mix warm water and yeast. Make sure yeast is good by letting it sit for a couple minutes and rise.\nAdd sugar and salt. Start mixing and add flour till it is smooth and elastic.\nLet rise for 10 minutes in the bowl.\nMelt 1 cube of butter and pour in edged cookie sheet.\nRoll dough out in rectangle about the same size as the cookie sheet. Cut in 1\" wide x 4\" long strips using a pizza cutter. Roll in the butter, and then place on cookie sheet.\nPress almonds in one end of breadstick.\nSprinkle with your favorite toppings such as: garlic salt, parmesan, parsley flakes, italian seasonings, seasoning salts.\nLet rise for 15-20 minutes more on the pan and then bake at 350 deg for about 10 minutes. Until the edges are just starting to turn a beautiful golden brown." }
    its(:name) { should == "Witch Fingers Breadsticks" }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
