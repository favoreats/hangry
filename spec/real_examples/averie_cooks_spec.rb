# encoding: UTF-8

describe Hangry do
  context "averiecooks.com berry greens vanilla smoothie" do
    before(:all) do
      @html = File.read("spec/fixtures/averie_cooks.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == "Averie Cooks" }
    its(:canonical_url) { should == "https://www.averiecooks.com/berry-greens-vanilla-smoothie/" }
    its(:image_url) { should == "https://www.averiecooks.com/wp-content/uploads/2015/01/berrygreenssmoothie-16-360x360.jpg"}
    its(:ingredients) { should == ["about 1 1/4 cups mixed berries (I use Trader Joe’s Berry Medley which includes strawberries, blueberries, blackberries, and raspberries)", "about 1 1/4 cups fresh spinach, loosely laid in measuring cup (frozen may be substituted)", "1 small ripe banana, peeled (previously frozen in chunks is ideal)", "1 scoop Vega One All-in-One Nutritional Shake (use this link for 20% off until 1/31/15)", "about 1/2 cup unsweetened cashew milk (almond milk or your favorite milk may be substituted)"]}
    its(:instructions) { should == "Add all ingredients to the canister of a blender or food processor in the order listed. Blend on high power until texture is as smooth as desired. Serve immediately. Smoothie is best when freshly made, however sometimes I made a double batch and place half the batch in a freezer-safe (plastic) glass for later. Smoothie will keep in the freezer for up to 1 month; allow to thaw at room temp for about 45 minutes before serving." }
    its(:name) { should == "Berry Greens Vanilla Smoothie" }
    its(:total_time) { should == 2 }
    its(:yield) { should == "about 10 to 12 ounces, serves one very generously" }
    its(:published_date) { should == "January 9, 2015" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end