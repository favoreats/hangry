# encoding: UTF-8

describe Hangry do
  context "picky-palate.com chocolate-peanut-butter-sandwiches-from-disneyland" do
    before(:all) do
      @html = File.read("spec/fixtures/picky_palate_chocolate_peanut_butter_sandwiches_from_disneyland.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://picky-palate.com/chocolate-peanut-butter-sandwiches-from-disneyland/" }
    its(:image_url) { should == "https://picky-palate.com/wp-content/uploads/2011/06/Chocolate-Peanut-Butter-Sandwich-2.jpg" }
    its(:ingredients) { should == ["3 cups", "good quality chopped milk chocolate", "4 tablespoons", "shortening", "12", "halved graham crackers", "2 cups", "creamy peanut butter"] }
    its(:instructions) { should == "Melt chocolate and shortening over a double boiler, stirring to melt. I use a small saucepan, boil water, reduce heat to low and place a heat proof bowl over the saucepan then add my chocolate, stirring until melted.\nDip each graham cracker into chocolate and let set up on wax or parchment paper. Stick in the freezer for a few minutes to speed this up. Spread 2 tablespoons (or a cookie scoopful) of peanut butter over the top of each dipped graham cracker then place in the freezer for 10 more minutes. This step is important. While the peanut butter is freezing, remove chocolate from heat and stir, so it cools slightly and thickens up a touch. Slowly and carefully spoon melted chocolate over tops and sides of peanut butter. This is where the patience and work comes in. Use a small spoon working the chocolate around the edges and sides. You’ll then use a little plastic knife to run the chocolate around the edges and clean it up a little. Let set up, (stick back in the freezer for another 10 minutes) then with a spoon, drizzle chocolate over top to get the “lines” you see in the pictures above. (If your chocolate is too thick at this point, set it back over the double boiler so it melts a little bit).\nMy reviews…Definitely keep these chilled just like Disneyland does, they melt rather quickly once your fingers get on them. It was tricky to get mine to look quite as pretty as the Disneyland ones, but with a little patience you can do it! Have fun and keep them chilled!"}
    its(:name) { should == "Chocolate Peanut Butter Sandwiches from Disneyland" }

    context "when homemade-hawaiian-haystacks recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/picky_palate_homemade_hawaiian_haystacks.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@class='paper-text']/p[./following-sibling::*[contains(text(),'1.')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["8 tablespoons (1 stick) unsalted butter", "1/2 cup all purpose flour", "1 teaspoon kosher salt", "1/2 cup sour cream", "1/4 teaspoon freshly ground black pepper", "32 ounces reduced sodium chicken broth", "2 teaspoons of dry Ranch Dressing seasoning packet", "1/4 teaspoon garlic salt", "2 cups cooked shredded chicken breast", "Steamed rice, enough for your family", "Toppings", "Chow Mein Crispy Noodles", "sliced green onions", "black olives", "pineapple", "cranberries", "almonds", "tomatoes", "shredded cheese"])
        end
      end

      context "and the selector '//*[@class='paper-text']/p[./following-sibling::*[contains(text(),'1.')]][last()]/following::p[./following-sibling::*[contains(text(),'Makes') or contains(text(),'servings') or contains(text(),'full') or contains(text(),'bars') or contains(text(),'about')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Place butter into a large saucepan over medium heat, stirring to melt. Slowly whisk in flour, salt and pepper until smooth and thick, for 1-2 minutes. Slowly whisk in chicken broth until smooth. Increase heat to high whisking until thick, about 2 minutes. Add sour cream, Ranch seasoning, garlic salt, and cooked chicken breast. Stir and season accordingly. You may prefer additional, salt, pepper and garlic salt. I transferred this to my crock pot and kept it on low all day until dinner. You can either serve it immediately or crock pot it until you are ready.\nPlace rice on serving plates and top with chicken gravy. Have toppings at the table and top with whatever you like! Have fun!")
        end
      end
    end

    context "when caramel-apple-cream-cheese-cookie-bars recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/picky_palate_caramel_apple_cream_cheese_cookie_bars.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@id='content']//p/strong/following::p[./following-sibling::*[contains(text(),'1.')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["3 Tablespoons melted butter", "3 Cups Golden Graham Cereal, ground in food processor", "1 Roll Pillsbury Sugar Cookie Dough or 2 pkgs Betty Crocker Sugar Cookie Dough Mix prepared and divided into 2 equal parts", "8 oz softened cream cheese", "1/4 Cup sugar", "1 teaspoon vanilla", "21 oz can apple pie filling", "1.5 oz package Nature Valley Oats N’ Honey Crunchy Granola Bars (2 count), crushed (I use a rolling pin to crush them right in the bag before opening)", "9.5 oz Kraft Caramels", "1/2 Cup half and half"])
        end
      end

      context "and the selector '//*[@id='content']//p/strong/following::p[./following-sibling::*[contains(text(),'1.')]][last()]/following::p[./following-sibling::*[contains(text(),'squares') or contains(text(),'mini')]]' is used in the parse_instructions method" do
        it "should be able to parse the intructions" do
          expect(subject.instructions).to eq("Preheat oven to 350 degrees F. Combine melted butter and ground cereal into a large bowl; press into an 8×8 inch baking dish lined with foil that’s been sprayed with cooking spray. Bake for 10 minutes then remove from oven. Crumble half of the cookie dough over partially baked crust.\nPlace cream cheese, sugar and vanilla into a mixer; beat until smooth. Pour over crumbled cookie dough layer. Bake for 25 minutes then remove from over. Top with 3/4 of the apple pie filling (I chose to just add part, but you can add the whole can if desired). Next top with remaining cookie dough, breaking off into little pieces evenly over top. Sprinkle with crushed granola bars and bake for 30-35 minutes or until toothpick comes clean from center. Let cool completely then cut into squares. Melt caramels and half and half according to package directions. Drizzle over each bar. Refrigerate leftovers.")
        end
      end
    end

    context "when easy-cheesy-bacon-biscuit-pull-aparts recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/picky_palate_easy_cheesy_bacon_biscuit_pull_aparts.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@class='paper-text']/p[1]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["2 tablespoons melted butter", "One roll refrigerated biscuit dough, 8 count", "6 thin slices fresh mozzarella, 1/4-inch thick", "1 cup Smithfield cooked, crumbled bacon", "1 cup shredded cheddar cheese"])
        end
      end

      context "and the selector '//*[@class='paper-text']/p[2]' is used in the parse_instructions method" do
        it "should be able to parse the intructions" do
          expect(subject.instructions).to eq("Preheat oven to 350 degrees F. and brush a 9-inch cast iron skillet with melted butter.\nUnroll biscuits from can and place into bottom of skillet. Top with slices of mozzarella cheese, bacon crumbles then shredded cheddar. Bake for 15 to 20 minutes, until cheese is melted and biscuits are cooked through. Serve warm.")
        end
      end
    end

    context "when if-you-like-starbucks-coffee-cake recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/picky_palate_if_you_like_starbucks_coffee_cake.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@id='content']/div/p[5] | //*[@id='content']/div/p[6]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["(half recipe)", "1 Box Yellow Cake Mix (plus ingredients on back of box)", "2 Sticks cold salted butter", "2 1/4 Cups flour", "1 1/2 Tablespoons cinnamon", "1 ¾ Cups brown sugar", "1 1/2 Tablespoons vanilla", "Powdered sugar (for dusting)"])
        end
      end

      context "and the selector '//*[@id='content']/div/p[6]/following::p[./following::*[contains(text(),'doubles')]]' is used in the parse_instructions method" do
        it "should be able to parse the intructions" do
          expect(subject.instructions).to eq("PREHEAT oven to 350.\nPREPARE cake mixe in large bowl according to directions on box. Spray 9X13 pan with non-stick spray. Pour batter into pan. Bake at 350 for 15-20 minutes or until center is just barely set.\nWHILE cakes are baking, prepare crumb topping. In large bowl of electric mixer combine, butter, flour, cinnamon, sugar and vanilla until all crumbly.\nIMMEDIATELY after cake is removed from oven, break crumb topping into marble size pieces with fingers, sprinkling over top. Put back in oven and bake an additional 10-15 minutes (topping will begin to look a little less wet/raw). Let cool fully then dust with powdered sugar, using a sieve.\nCUT into squares and serve.")
        end
      end
    end

    context "when buttery-cinnabun-cake recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/picky_palate_buttery_cinnabun_cake.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@class='MsoNormal'][./following-sibling::*[contains(text(),'1.')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["3 Cups flour", "¼ teaspoon salt", "1 Cup sugar", "4 teaspoons baking powder", "1 ½ Cups milk", "2 eggs", "2 teaspoons vanilla", "1 Stick real butter, melted", "2 sticks real butter, softened", "1 Cup brown sugar", "2 Tablespoons flour", "1 Tablespoon cinnamon", "2/3 Cups nuts, optional", "2 Cups powdered sugar", "5 Tablespoons milk", "1 teaspoon vanilla"])
        end
      end

      context "and the selector '//*[@class='MsoNormal'][./following-sibling::*[contains(text(),'1.')]][last()]/following::p[./following::*[(@class='instagram')]]' is used in the parse_instructions method" do
        it "should be able to parse the intructions" do
          expect(subject.instructions).to eq("In an electric or stand mixer mix the flour, salt, sugar, baking powder, milk, eggs and vanilla. Once combined well, slowly stir in melted butter. Pour batter into a greased 9 x 13 inch baking pan.\nIn a large bowl, mix the 2 sticks of softened butter, brown sugar, flour, cinnamon and nuts until well combined. Drop evenly over cake batter by the tablespoonfuls and use a knife to marble/swirl through the cake.\nBake for 25-30 minutes or until toothpick comes out nearly clean from center.\nPlace powdered sugar, milk and vanilla in a large bowl. Whisk until smooth. Drizzle over warm cake. Serve warm or at room temperature. Enjoy!")
        end
      end
    end

    context "when ice-cream-sundae-brownies-2 recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/picky_palate_ice_cream_sundae_brownies_2.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@class='storycontent']/h2/following::p[./following-sibling::*[contains(text(),'1.')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1 box Brownie Mix", "2 eggs", "1/2 Cup canola or vegetable oil", "Heaping 1/2 Cup of your favorite Ice Cream (I used Ben and Jerry’s Phish Food)", "1 1/2 Cups chocolate chips", "1/4 Cup hot fudge topping (chilled)"])
        end
      end

      context "and the selector '//*[@class='storycontent']/h2/following::p[./following-sibling::*[contains(text(),'1.')]][last()]/following::p[./following-sibling::*[contains(text(),'brownies')]]' is used in the parse_instructions method" do
        it "should be able to parse the intructions" do
          expect(subject.instructions).to eq("Preheat oven to 350 degrees F. In a large bowl mix the brownie mix, 2 eggs and oil until combined…and thick. I did NOT add the water. Scoop in ice cream, chocolate chips and hot fudge; mix until combined. Pour into a foil lined 9×13 inch baking dish that’s been GENEROUSLY sprayed with cooking spray. More is better here 🙂 Bake for 40-50 minutes or until toothpick comes clean from center.\nLet cool completely. Remove foil from baking dish, cut into squares and serve.")
        end
      end
    end

    context "when the selector '//*[@class='paper-text']/p[./following-sibling::*[contains(text(),'1.')]][last()]/following::p[./following::*[(@class='instagram')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/picky_palate_walking_tacos.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Heat oil in a large dutch oven over medium heat. Saute onion and peppers for 5 minutes or until softened. Add garlic and cook for 1 minute. Add ground beef, salt and pepper. Cook until browned. Drain if needed. I usually don’t need to for the lean beef. Pour in tomatoes, salsa, tomato paste then stir to combine. Add 1 Cup warm water to thin out slightly then add in beans, olives, chili powder, cumin, hot sauce, lime juice, salt, pepper and garlic salt. I start with just a pinch of salt then keep tasting until it’s to my liking. Add chopped cilantro and reduce heat to low to simmer until ready to serve.\nOpen bags of fritos. Spoon in chili, cheese, sour cream and jalapenos. Stick a fork in it and eat!!")
      end
    end

    context "when the selector '//*[@id='content']//p/strong/following::p[./following-sibling::*[contains(text(),'1.')]][last()]/following::p[./following-sibling::*[contains(text(),'squares') or contains(text(),'mini')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/picky_palate_chocolate_peanut_butter_cup_layered_krispie_treats.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Place cereal into a large mixing bowl, set aside. Place marshmallows and butter into a large bowl and microwave until puffed up, about 3 minutes. Remove and pour into cereal, mix to combine then pour into a greased 9×13 inch baking dish and press to make even. Melt peanut butter until nice and runny, about 45 seconds. Pour in quartered peanut butter cups, stir gently then pour over treats. Drizzle melted chocolate over top and refrigerate to harden. When set, cut into squares and serve!!\nsquares")
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end