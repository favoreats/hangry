# encoding: UTF-8

describe Hangry do
  context "justfoodrecipes.com yellow-cake-recipe/" do
    before(:all) do
      @html = File.read("spec/fixtures/just_food_recipes_yellow_cake_recipe.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://justfoodrecipes.com/uncategorized/yellow-cake-recipe/" }
    its(:image_url) { should == "http://justfoodrecipes.com/wp-content/uploads/2014/04/yellow-cake-238x300.jpg" }
    its(:ingredients) { should == ["1 stick unsalted butter, softened, plus more for cake pans and parchment", "2 cups all-purpose flour", "1 tablespoon baking powder", "1/2 teaspoon fine salt", "1 1/2 cups granulated sugar", "3 large eggs, room temperature", "1 cup whole milk", "1 teaspoon pure vanilla extract", "1 stick unsalted butter, softened", "4 ounces cream cheese, room temperature", "5 cups confectioner’s sugar", "1/4 teaspoon fine salt", "1/4 cup whole milk", "1/2 teaspoon pure vanilla extract"] }
    its(:instructions) { should == "Cake: Preheat oven to degrees Butter two -inch round cake pans and line bottoms with parchment; butter parchment as well Whisk together flour, baking powder, and salt Beat together butter and granulated sugar with a mixer on medium speed until combined, to minutes Add eggs and beat well, scraping down sides of bowl as necessary Reduce speed to low and gradually add flour mixture, beating until combined Add milk and vanilla and beat until just combined\nDivide batter between pans; smooth tops with an offset spatula Bake until golden and a toothpick inserted into centers comes out clean, to minutes Let cakes cool in pans on wire racks minutes Turn out cakes onto racks to cool completely\nFrosting: Beat together butter and cream cheese with a mixer on medium-high speed until pale and creamy, about minute Reduce speed to medium Add confectioner’s sugar, cup at a time, beating well after each addition Add salt, milk, and vanilla and beat until fluffy, about minutes If not using immediately, cover surface of frosting with plastic wrap Frosting can be refrigerated in an airtight container up to week Before using, bring to room temperature, then beat on low speed until smooth\nPlace cake layer on a cake plate and spread cup frosting on top Place remaining cake layer on top Spread top and sides of cake with remaining frosting, swirling to coat in a decorative fashion (If frosting becomes too soft, refrigerate to firm up) Cake can be covered with a cake dome and refrigerated overnight Bring cake to room temperature before serving" }
    its(:name) { should == "Yellow Cake Recipe" }

    context "when secret-chocolate-chip-cookie recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/just_food_recipes_secret_chocolate_chip_cookie_recipe.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'INGREDIENTS')]/following::p[1]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1 cup butter softened- ROOM TEMPERATURE", "1 1/2 Cup Sugar", "2 large eggs – ROOM TEMPERATURE", "2 teaspoons vanilla", "2 1/4 cups unsifted flour", "1 teaspoon baking soda", "1/2 teaspoon salt", "2 Cups of Chocolate Chips- Ones you would eat by themselves"])
        end
      end

      context "and the selector '//*[contains(text(),'Directions')]/following::p[./following::*[contains(text(),'Tips')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Stir Flour, Baking Soda, and Salt together and set aside\nBeat butter and sugar together on medium speed till mixed together Make sure not to over mix\nAdd vanilla and egg to butter mixture and beat slowly\nGradually Stir in Flour Mixture Don’t over mix, just mix until flour is mixed into it\nStir in Chocolate Chips in by hand\nGrease Cookie Sheet and place cookies on at even sizes\nPut the cookies in the pre heated oven for - minutes Cooking times vary\nWatch cookies in the oven Make sure not to over cook the cookies or else you will have crunchy cookies\nTake cookies out of the oven, let them rest on cookie sheet for minutes and then place them on a cooling rack")
        end
      end
    end

    context "when asian-chicken-salad-recipe recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/just_food_recipes_asian_chicken_salad_recipe.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '[itemprop = 'ingredients']' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1/4 cup rice wine vinegar", "1 teaspoon chopped fresh ginger or 1/2 teaspoon powder ginger", "1 to 2 tablespoons soy sauce", "1 tablespoon honey", "3 tablespoons toasted sesame oil", "3 tablespoons vegetable oil", "2 tablespoons sesame seeds, toasted", "6 cups baby spinach leaves, washed, and dried", "1 large carrot, shredded", "1 red bell pepper, thinly sliced", "1/4 of a whole red onion, thinly sliced", "1 tablespoon sesame seeds toasted", "2 cups shredded rotisserie chicken or grilled chicken, sliced"])
        end
      end

      context "and the selector '[itemprop = 'recipeInstructions']' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Combine all salad dressing ingredients, except for sesame seeds, and using only tablespoon of soy sauce Whisk until emulsified Taste your dressing and add another tablespoon of soy sauce, if desired, to make it saltier Add tablespoons sesame seeds to the dressing and mix them in\nCombine all salad ingredients in a large bowl, except for tablespoon of sesame seeds and chicken Add the salad dressing to the salad and toss – do not add all salad dressing at once: you might not need all of it Add just enough salad dressing to coat the salad ingredients\nServe the salad in the individual serving bowls and top with the remaining tablespoon of sesame seeds and sliced chicken")
        end
      end
    end

    context "when ried_chicken_recipe_easy recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/just_food_recipes_fried_chicken_recipe_easy.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '.ingredient' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["5 lb chicken, cut up", "BRINE", "2 cups buttermilk", "2 Tablespoon kosher salt", "1/4 cup hot sauce", "SPICE BLEND (you’ll have plenty left over, it’s great on chicken)", "1 tablespoon sugar", "1 tablespoon seasoning salt", "1 tablespoon paprika", "1 tablespoon black pepper", "1 tablespoon garlic powder", "FLOUR COATING", "1 1/5 cups self-rising flour", "salt", "black pepper", "celery salt"])
        end
      end

      context "and the selector '.instructions' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Combine brine ingredients in a bowl\nPlace chicken in a small bowl and pour brine over chicken Chicken should be completely submerged in the brine\nRefrigerate for hour\nAfter an hour pour off the brine and rinse chicken off thoroughly, shaking off excess water\nMix together the spice blend ingredients\nPlace chicken on a plate or tray and season generously You will have plenty of seasonings left over\nSeason the flour with salt, pepper and celery salt Taste the flour, it should taste seasoned\nCoat the chicken pieces in the flour\nLet sit for minutes until the flour absorbs a little\nRecoat the chicken in flour\nHeat oil in deep fryer to\nPlace chicken in hot oil and fry until golden brown Fry dark meats together and white meats together Cooking time will vary based on the size of the chicken and method used\nJuices will run clear when done\nDrain on paper towels on a wire rack (if needed keep cooked chicken in a degree preheated oven to keep warm while the other chicken cooks\nServe hot!!\nEnjoy!")
        end
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

