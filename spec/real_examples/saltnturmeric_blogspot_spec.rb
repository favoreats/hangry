# encoding: UTF-8

describe Hangry do
  context "saltnturmeric.blogspot.com cheesy-garlic-biscuits" do
    before(:all) do
      @html = File.read("spec/fixtures/saltnturmeric_blogspot.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://saltnturmeric.blogspot.com/2008/11/cheesy-garlic-biscuits.html" }
    its(:image_url) { should == "http://1.bp.blogspot.com/_-jeCf-few9o/STLft3aV5GI/AAAAAAAABF8/ffsGUp4mpQ0/s400/thanksgiving3.jpg" }
    its(:ingredients) { should == ["1 pack buttermilk biscuit mix (I used Bisquick, about 1 1/2cups)", "1 1/2cups shredded cheddar cheese", "1/2 cup milk", "2 tbsp butter", "1 tbsp oregano (you can omit or use less)", "3/4 tsp garlic salt"]}
    its(:instructions) { should == "Preheat oven to 400F. Spray cooking sheet with non-stick spray.\nPut biscuit mix, cheese and milk in a bowl and mix well to form a sticky dough.\nUsing spoon, drop lumps of dough onto cookie sheets 1.5in apart. Bake for 10min.\nIn a bowl, melt butter and mix with oregano and garlic salt.\nWhen the timer goes off, take them out and brush with butter mixture and bake 5min at 400F and another 5-6min at 350F.\nTransfer into a plate and serves immediately." }
    its(:name) { should == "Cheesy Garlic Biscuits Recipe" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end