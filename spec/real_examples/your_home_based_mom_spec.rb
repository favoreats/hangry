# encoding: UTF-8

describe Hangry do
  context 'yourhomebasedmom.com slow-cooker-cranberry-sauce' do
    before(:all) do
      @html = File.read('spec/fixtures/your_home_based_mom_slow_cooker_cranberry_sauce.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == 'Slow Cooker Cranberry Sauce' }
    its(:canonical_url) { should == 'https://www.yourhomebasedmom.com/slow-cooker-cranberry-sauce/' }
    its(:image_url) { should == 'https://www.yourhomebasedmom.com/wp-content/uploads/2018/10/Cranberry-Sauce-Recipe.jpg' }
    its(:ingredients) { should == ['1 lb whole fresh cranberries', '1/2 cup freshly squeezed orange juice', '1/2 cup water', '1/2 cup brown sugar', '1/2 cup granulated sugar', '1/4 tsp salt', '1 orange (zested)', '1 20 oz. can crushed pineapple (with juice)'] }
    its(:instructions) { should == "Place all the ingredients into a 6 qt. slow cooker and stir to combine.\nCook on high for 2-3 hours and then on low for 4-5 hours.\nRefrigerate until ready to use." }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
