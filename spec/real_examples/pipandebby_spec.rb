# encoding: UTF-8

describe Hangry do
  context "pipandebby.com hot-cocoa-cookies" do
    before(:all) do
      @html = File.read("spec/fixtures/pipandebby.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.pipandebby.com/pip-ebby/2011/11/14/hot-cocoa-cookies.html" }
    its(:image_url) { should == "https://static1.squarespace.com/static/55370317e4b047f1053ee431/t/56197027e4b0769b468e4056/1329189041763/1000w/IMG_1005post.jpg" }
    its(:ingredients) { should == ["1 stick (4 oz.) unsalted butter", "7 bars (3.5 oz. each) semisweet chocolate – 12 oz. chopped, 7.5 oz. cut into 1-inch squares and the rest for garnish", "1 1/2 cups flour", "1/4 cup unsweetened cocoa powder", "1 1/2 teaspoons baking powder", "1/4 teaspoon salt", "1 1/4 cups light brown sugar", "3 eggs, at room temperature", "1 1/2 teaspoons pure vanilla extract", "30 marshmallows"]}
    its(:instructions) { should == "In a medium saucepan, melt the butter and chopped chocolate, stirring frequently, over medium heat. Let cool for 15 minutes. In a medium bowl, whisk together the flour, cocoa powder, baking powder and salt.\nUsing an electric mixer, beat the sugar, eggs and vanilla at low speed until smooth, 2 minutes. Mix in the cooled chocolate mixture just until blended. Add the flour mixture in 2 batches, mixing on low speed until just combined. Refrigerate the dough for at least 1 hour.\nPreheat oven to 325 degrees F. Line 2 large baking sheets with parchment paper. Using a tablespoon, scoop the dough and roll between your palms to form 1-inch balls. Arrange about 16 balls 2 inches apart on each cookie sheet, flattening slightly. Bake until the tops of the cookies crack, about 12 minutes.\nMeanwhile, snip 8 marshmallows in half crosswise and stick 1 square of chocolate onto each of the cut sides.\nRemove the cookie sheets from the oven; gently press a marshmallow half, chocolate side down, into each cookie. Bake until the marshmallows are just softened, about 4 minutes.\nTransfer the pans to racks to cool for 5 minutes; grate the remaining chocolate over the hot cookies. Using a spatula, transfer the cookies to the racks; let cool. Repeat the process with the remaining dough, marshmallows and chocolate. Bake each batch on a clean sheet of parchment." }
    its(:name) { should == "Hot Cocoa Cookies" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end