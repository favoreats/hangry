# encoding: UTF-8

describe Hangry do
  context "fitnessmagazine.com kale-and-butternut-squash-saute" do
    before(:all) do
      @html = File.read("spec/fixtures/fitness_magazine_kale_and_butternut_squash_saute.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.fitnessmagazine.com/recipe/kale-and-butternut-squash-saute/"}
    its(:image_url) { should == "https://images.meredith.com/fitness/images/recipe/ss_RU195106.jpg" }
    its(:ingredients) { should ==["2 tablespoons extra-virgin olive oil", "3 pounds butternut squash, peeled and cut into 1/2-inch cubes (about 3 1/2 cups)", "1/2 cup diced yellow onion", "2 garlic cloves, minced", "4 1/2 cups chopped fresh kale", "1 teaspoon grated lemon zest", "2 tablespoons freshly squeezed lemon juice", "1/2 teaspoon kosher salt", "1/2 teaspoon black pepper", "2 tablespoons dried cranberries", "4 tablespoons chopped walnuts or pecans, toasted", "4 tablespoons crumbled goat cheese"] }
    its(:instructions) { should == "In a large skillet, heat oil over medium-high heat. Add squash, onion and garlic and cook, stirring constantly, until squash is lightly browned and slightly tender, about 7 minutes.\nAdd kale, lemon zest, lemon juice, salt and pepper and cook until kale is wilted and squash is tender, 5 to 7 minutes. Remove from heat.\nAdd cranberries and nuts; toss to combine. Sprinkle with goat cheese." }
    its(:name) { should == "Kale and Butternut Squash Saute" }

    context "when green-tea-smoothie-recipe-detox recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/fitness_magazine_green_tea_smoothie_recipe_detox.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@property='content:encoded']/ul/li' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1 cup pear, chopped", "1/3 avocado, chopped", "1 1/2 cups steeped green tea, frozen", "2/3 cup kale, packed", "juice of 1/2 lemon", "1 teaspoon maple syrup", "1/4 teaspoon cayenne pepper"])
        end
      end

      context "and the selector '//*[@property='content:encoded']/ul/following::p[./following::*[contains(text(),'Make')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Add green tea ice cubes and kale to the blender first, followed by the rest of the ingredients.\nBlend until smooth and garnish with additional cayenne pepper.")
        end
      end
    end

    context "when healthy-mac-n-cheese recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/fitness_magazine_healthy_mac_n_cheese.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@property='content:encoded']/ul/following::p[./following::*[contains(text(),'Make')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["Nonstick cooking spray", "Salt", "4 ounces whole wheat macaroni", "1/2 cup onion-garlic puree (see \"Rocco's Secret Weapon,\" below)", "1/2 teaspoon dry mustard", "Pinch cayenne pepper", "1 cup shredded 50 percent reduced-fat cheddar", "1/3 cup nonfat Greek yogurt", "1/4 cup whole wheat panko bread crumbs", "1/4 cup grated Parmesan"])
        end
      end

      context "and the selector '//*[@property='content:encoded']/ol/li' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Preheat the oven to 425 degrees. Mist an 8-by-8-inch baking dish with cooking spray; set it aside.\nBring a large pot of salted water to a boil. Add macaroni and cook according to package directions, drain.\nMeanwhile, bring onion-garlic puree, mustard, and cayenne to a simmer in a small saucepan over medium heat, stirring often. Whisk in cheddar until melted. Remove from heat and whisk in yogurt.\nIn a medium bowl, toss the macaroni with the cheese sauce. Season with salt to taste. Pour the mixture into the prepared baking dish and sprinkle panko over the top. Top with Parmesan.\nBake until Parmesan is melted and macaroni is hot throughout, about 10 minutes.")
        end
      end

      context "and the selector '.wg_img_disp_left img' is used in the parse_image_url method" do
        it "should be able to parse the image_url" do
          expect(subject.image_url).to eq("https://images.fitnessmagazine.mdpcdn.com/sites/fitnessmagazine.com/files/styles/story_detail/public/story/101587097_w.jpg?itok=JGlrSX-Y")
        end
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end