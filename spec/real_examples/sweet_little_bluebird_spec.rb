# encoding: UTF-8

describe Hangry do
  context "sweetlittlebluebird.com tried-true-tuesday-crazy-cake-no-eggs" do
    before(:all) do
      @html = File.read("spec/fixtures/sweet_little_bluebird_tried_true_tuesday_crazy_cake_no_eggs.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://sweetlittlebluebird.com/tried-true-tuesday-crazy-cake-no-eggs/" }
    its(:image_url) { should == "https://sweetlittlebluebird.com/wp-content/uploads/Chocolate-Crazy-Cake-no-eggs-milk-butter-bowls-900x900.jpg" }
    its(:ingredients) { should == ["1 1/2 Cups flour (all-purpose)", "3 Tbsp. cocoa (unsweetened)", "1 Cup sugar (All purpose sugar – Granulated Pure Cane Sugar)", "1 tsp. baking soda", "1/2 tsp. salt", "1 tsp. white vinegar", "1 tsp. pure vanilla extract", "5 Tbsp. vegetable oil", "1 Cup water"]}
    its(:instructions) { should == "Preheat oven to 350 degrees F.\nMix first 5 dry ingredients in a greased 8″ square baking pan. Make 3 depressions in dry ingredients – two small, one larger (see #3 in photo below). Pour vinegar in one depression, vanilla in the other and the vegetable oil in third larger depression. Pour water over all. Mix well until smooth.\nBake on middle rack of oven for 35 minutes. Check with toothpick to make sure it comes out clean. Cool. Top with your favorite frosting. Enjoy!" }
    its(:name) { should == "Chocolate Crazy Cake (No Eggs, Milk, Butter or Bowls)" }

    context "when the selector '//*[contains(text(), 'Directions')]/../following::p[./following::p[strong]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/sweet_little_bluebird_sausage_gravy_biscuits_quick_easy.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Make your biscuits first – the sausage gravy comes together quickly. Have your biscuits ready to pop in the oven once you start the gravy. The gravy is best served over warm biscuits fresh out of the oven.\nIn a large skillet over medium-high heat, cook sausage until thoroughly cooked (approximately 7-9 minutes), continuously stirring.\nNext, reduce heat to Medium, add flour, mixing into sausage. Cook the flour and sausage mixture, stirring constantly for 3-5 minutes, until all the flour is absorbed by the fat.\nLast, slowly add milk, stirring continuously, bringing to a boil. Allow to boil until gravy thickens. Once thickened, salt and pepper to taste and serve over freshly baked split biscuits. *If the gravy is too thick, add a small amount of milk and stir.")
      end
    end

    context "when the selector '//*[contains(text(), 'DIRECTIONS')]/../following::span[1]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/sweet_little_bluebird_tried_true_whole_foods_market_sonoma.html")
        @parsed = Hangry.parse(@html)

      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("For the full recipe, head to WHOLE FOODS MARKET, you won’t be disappointed! Be sure to print extra copies, you’ll need them to pass out to family and friends.")
      end
    end

    context "when the selector '//*[contains(text(), 'Directions')]/../following-sibling::div[1]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/sweet_little_bluebird_slow_cooker_shredded_chicken_tacos.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Lightly spray slow-cooker with non-stick cooking spray. Place chicken in single layer. In a medium size bowl, mix broth, taco seasoning and a can of Rotel. Pour mixture over chicken and cook on low for 6 to 8 hours, or on high for 4 to 6 hours. Cooking times may vary depending on your slow cooker, adjust accordingly. When ready, shred chicken using two forks and serve in shells or tortillas with your favorite toppings.")
      end
    end

    context "when the selector '//*[contains(text(), 'Directions')]/../following::span[1]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/sweet_little_bluebird_hot_and_spicy_sausage_dip.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("In a large skillet, cook sausage, breaking up into small pieces (like taco meat) until fully cooked. Drain all excess fat. Add Rotel and cream cheese, mix until well blended and heated thoroughly. Place in a serving dish or small crock pot, serve warm with you favorite tortilla chips or Fritos Scoops.")
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients')]/../following::ul[./following::*[contains(text(),'DIRECTIONS')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/sweet_little_bluebird_creamy_ranch_chicken_crock_pot_or_oven.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["4 boneless, skinless chicken breasts", "2 tablespoons butter, melted (I used 4 tablespoons)", "1 can cream of chicken soup (10 3/4 ounce)", "8 ounces of cream cheese, cut into cubes", "1/2 cup chicken broth (I used low sodium)", "1 packet (1 ounce)  Hidden Valley Ranch Dressing Mix", "paprika – few dashes", "1/4 teaspoon minced garlic (I used 1/2 teaspoon)", "1/4 teaspoon dried parsley flakes (I used 1/2 teaspoon)", "1/8 teaspoon dried oregano (I omitted)"])
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients')]/../following::div[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/sweet_little_bluebird_no_bake_chocolate_peanut_butter_cookies.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["1/2 cup butter (1 stick, salted – real butter, do not substitute)", "1/2 cup milk (skim, 1%, 2% or whole)", "2 cups sugar", "1/2 cup cocoa (I use Hershey’s Natural Unsweetened Cocoa)", "1 teaspoon pure vanilla extract", "1/2 cup creamy peanut butter", "3 cups Quick Oats (quick-cooking oatmeal) or Old-Fashioned Oats"])
      end
    end

    context "when the selector '//*[contains(text(),'INGREDIENTS')]/../following::ul[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/sweet_little_bluebird_magic_stuffed_cinnamon_crescent_rolls.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["1/2 cup granulated sugar", "1 Tablespoon ground cinnamon", "1 can Pillsbury refrigerated crescent dinner rolls (8 count)", "8 large marshmallows", "1/4 cup butter, melted", "1/2 cup powdered sugar", "1/2 teaspoon vanilla", "2-3 teaspoons of milk", "1/4 cup chopped pecans"])
      end
    end

    context "when the selector '//*[contains(text(),'INGREDIENTS')]/../following::ul[./following::*[contains(text(),'DIRECTIONS')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/sweet_little_bluebird_tried_true_whole_foods_market_sonoma.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["2 pounds, chicken breasts – boneless, skinless (pre-baked)", "2 cups red seedless grapes, cut in half", "3/4 cup pecan pieces (my sister uses honey roasted pecans)", "3 stalks celery, sliced thin", "Spring salad mix to serve chicken salad on top", "2 teaspoons poppy seeds", "5 teaspoons honey", "4 teaspoons apple cider vinegar", "1 cup mayonnaise", "salt and fresh ground pepper to taste"])
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end