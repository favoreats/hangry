# encoding: UTF-8

describe Hangry do
  context "gimmesomeoven.com apple-cider-baked-chicken" do
    before(:all) do
      @html = File.read("spec/fixtures/gimme_some_oven_apple_cider_baked_chicken.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.gimmesomeoven.com/apple-cider-baked-chicken/" }
    its(:image_url) { should == "https://www.gimmesomeoven.com/wp-content/uploads/2011/10/apple-cider-baked-chicken1.jpg" }
    its(:ingredients) { should == ["1 large onion, peeled and cut into eighths", "1 large lemon, sliced into thin rounds", "2 cups apple cider, homemade or storebought", "2 tablespoons olive oil", "4 sprigs fresh thyme, plus more for garnish", "2 Tbsp. apple cider vinegar", "2 tsp. Dijon mustard", "3 garlic cloves, minced", "2 bay leaves", "1/2 tsp. salt", "1/2 tsp. freshly ground black pepper", "6-8 chicken legs or thighs", "2 large apples, each sliced into eighths", "1 lb. small, red or Yukon Gold potatoes, halved"]}
    its(:instructions) { should == "Place a gallon size heavy-duty zip-top bag into a large bowl. Place the onion and next 10 ingredients into the zip-top bag, combining well. Add the chicken to the marinade. Close the bag, place in the refrigerator to marinate for at least 4 hours or up to 24 hours.\nPreheat oven to 350 degrees. Arrange the chicken pieces in a large roasting pan skin-side up. Pour all of the marinade, including onions and lemons over and around the pieces. Tuck the apples and potatoes around the chicken. Cook for 1 hour and 15 minutes, stirring once halfway through to re-coat everything with the marinade.\nArrange chicken and potatoes on a platter and sprinkle with fresh thyme leaves." }
    its(:name) { should == "Apple Cider Baked Chicken" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end