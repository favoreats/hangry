# encoding: UTF-8

describe Hangry do
  context "ohsweetbasil.com Award Winning Instant Pot Chili Recipe" do
    before(:all) do
      @html = File.read("spec/fixtures/oh_sweet_basil.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == 'Carrian Cheney' }
    its(:image_url) { should == 'https://ohsweetbasil.com/wp-content/uploads/quick-and-easy-instant-pot-chili-or-slow-cooker-chili-best-chili-recipe-ohsweetbasil.com-8-350x350.jpg' }
    its(:canonical_url) { should == "https://ohsweetbasil.com/instant-pot-award-winning-chili-recipe/" }
    its(:ingredients) { should == ["1 1/2 Pounds Ground Beef", "6 Strips of Bacon, chopped", "1 Can (15 ounces) Kidney Beans, drained", "1 Can (15 ounces) Pinto beans, drained", "1 Can (15 ounces) Black beans, drained", "1 Can (15 ounces) Fire Roasted diced tomatoes with juice", "1 Can (6 oounce) Tomato paste", "1 large Red onion, chopped", "1 Red bell pepper, seeded and chopped", "1 Jalapeño, seeded and minced *optional", "2 Cups Beef stock", "1 Tablespoon Dried oregano", "2 Teaspoons ground cumin", "2 Teaspoons Kosher salt", "1 Teaspoon ground black pepper", "1 Teaspoon Smoked Paprika", "2 Tablespoons chili powder", "1 Tablespoon Worcestershire sauce", "1 Tablespoon Minced garlic", "For Garnish", "Sour Cream", "Cilantro", "Cheese, shredded"] }
    its(:instructions) { should == "Turn your instant pot to sauté and add the bacon. Cook until crisp, stirring often to cook evenly. Remove the bacon to a paper towel lined plate.\nAdd the onions and peppers and cook until tender. Add the meat and cook until browned. Drain off any excess grease, we just tilt the pot and use a large spoon. Add all of the remaining ingredients and 3/4 of the bacon and stir to combine.\nTurn the instant pot to chili and cook for 18-20 minutes*. Allow pressure to release for 10-15 minutes or quick release with the vent.\nServe with limes, sour cream, cheese, and a little bacon!" }
    its(:name) { should == "Award Winning Instant Pot Chili Recipe" }
    its(:prep_time) { should == "10" }
    its(:cook_time) { should == "40" }
    its(:total_time) { should == "50" }
    its(:yield) { should == '8 servings' }
    its(:published_date) { should == Date.new(2018, 1, 8) }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end