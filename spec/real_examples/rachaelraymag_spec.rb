# encoding: UTF-8

describe Hangry do
  context "rachaelraymag.com eggs-in-clouds" do
    before(:all) do
      @html = File.read("spec/fixtures/rachael_raymag.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.rachaelraymag.com/recipe/eggs-in-clouds" }
    its(:image_url) { should == "https://www.rachaelraymag.com/.image/t_share/MTQ3MjAwMDAxMDkxNTc3Mjk0/egg-in-clouds-0112-rr-ym03-01.jpg" }
    its(:ingredients) { should == ["4 eggs", "1/4 cup grated pecorino-romano", "1/4 cup chopped chives", "1/4 cup crumbled bacon", "Pepper"]}
    its(:instructions) { should == "Separate eggs, putting whites in 1 large bowl and yolks in 4 separate small bowls. Whip whites until stiff peaks form. Fold in cheese, chives and bacon. Spoon into 4 mounds on parchment-lined baking sheet; make a deep well in center of each. Bake at 450 degrees for 3 minutes, then and 1 yolk to each well; season with pepper. Bake until yolks are just set, 2 to 3 minutes." }
    its(:name) { should == "Eggs in Clouds" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end