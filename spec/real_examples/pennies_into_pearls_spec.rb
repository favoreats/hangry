#encoding: UTF-8

describe Hangry do
  context "penniesintopearls.com Cent Savvy Strawberry Chocolate Kabobs" do
    before(:all) do
      @html = File.read("spec/fixtures/pennies_into_pearls.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == "Brittany Cooper" }
    its(:canonical_url) { should == "http://www.penniesintopearls.com/cent-savvy-strawberry-chocolate-kabobs/" }
    its(:image_url) { should == "https://i0.wp.com/www.penniesintopearls.com/wp-content/uploads/2015/01/Strawberry-Kabobs-Featured-crop.jpg?resize=1024%2C461" }
    its(:ingredients) { should == ["Strawberries", "Marshmallows", "Cake bites / donuts", "Skewers", "Melting Chocolate"] }
    its(:instructions) { should == "Decide on which assembly process you would like to offer your valentine and proceed as needed. I think this recipe is pretty self-explanatory but all I did to make the kabobs was alternate on which ingredients I placed on the skewers. Then once they were all put together I melted just a little chocolate to drizzle over the top.\nI did make my cake bites from scratch using my cake pop maker. But if you don’t have one of those you could easily cut pieces of angel food cake or pound cake to use. Another option would be to use donut holes. All are tasty alternatives! Enjoy!\nSharing ideas over at:" }
    its(:name) { should == "Cent Savvy Strawberry Chocolate Kabobs" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end