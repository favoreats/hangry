#encoding: UTF-8

describe Hangry do
  context '' do
    before(:all) do
      @html = File.read('spec/fixtures/the_frugal_girls_chocolate_cookie_mix_in_a_jar.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'http://thefrugalgirls.com/2013/10/chocolate-cookie-mix-in-a-jar.html' }
    its(:image_url) { should == 'http://thefrugalgirls.com/wp-content/uploads/2013/10/Triple-Chocolate-Fudge-Cookie-Mix-in-a-Jar-at-TheFrugalGirls.com_.jpg' }
    its(:ingredients) { should == ['1 box Betty Crocker Triple Chocolate Fudge Cake Mix {15.25 oz.}', '1 cup Nestle Toll House Semi-Sweet Chocolate Chips', '1 quart size Wide Mouth Mason Jar', '1 Red Bandana', 'Twine'] }
    its(:instructions) { should == "Carefully empty the contents of the cake mix package into your quart-sized wide mouth Mason Jar, and gently pat down the top.\nPour in the Chocolate Chips\nPut the silver jar topper on top, place a round scrap cut from a Red Bandana on top, then screw on the jar’s lid. {or you could use a doilie like I did here}\nTie some twine around the jar, and you’re done!\nOptional: If you’ve got a Silhouette, you could even print the words ‘Cookie Mix’ like I did here." }
    its(:name) { should == 'Chocolate Cookie Mix in a Jar!' }

    context "when the selector '//*[@class='entry-content']/ol/li' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/the_frugal_girls_coconut_oil_sugar_scrub.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Place 1/2 cup Coconut Oil in microwave safe dish, and microwave for 45 seconds, or until completely melted.\nTransfer melted Coconut Oil to small mixing bowl, and stir in 1 cup of White Sugar. Mix well.\nAdd 10 drops of Vanilla Essential Oil {or more for a stronger scent}, and mix again until well combined.\nTransfer to Mason Jar, and you’re done! So Easy! {For scrubs, I love using the Wide Mouth Half Pint Jars}")
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
