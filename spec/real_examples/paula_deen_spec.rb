# encoding: UTF-8

describe Hangry do
  context 'pauladeen.com peel-and-eat-shrimp' do
    before(:all) do
      @html = File.read('spec/fixtures/pauladeen_peel_and_eat_shrimp.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.pauladeen.com/recipe/peel-and-eat-shrimp/' }
    its(:image_url) { should == 'https://cdn-pauladeen-com.s3.amazonaws.com/wp-content/uploads/p/e/peel-and-eat-shrimp_1.jpg' }
    its(:ingredients) { should == ['2 lbs large shells on shrimp', '3 tablespoons crab boil seasoning', '1 sliced into wedges lemon', 'cocktail sauce'] }
    its(:instructions) { should == 'Add 8 cups water and crab boil seasoning to a large pot. Bring to a boil and stir in shrimp. Remove from heat, cover, and let stand for 15 minutes, until the shrimp turn pink. Drain and serve with lemon wedges and cocktail sauce.' }
    its(:name) { should == 'Peel and Eat Shrimp' }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
