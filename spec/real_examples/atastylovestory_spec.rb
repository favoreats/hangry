# encoding: UTF-8

describe Hangry do
  context "atastylovestory.com winter-salad-pomegranate-kale-balsamic-dressing" do
    before(:all) do
      @html = File.read("spec/fixtures/atastylovestory_winter_salad_pomegranate.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }
    its(:name) { should == "Winter Salad with Kale & Pomegranate dressed with a Balsamic Dressing" }
    its(:image_url) { should == "http://atastylovestory.com/wp-content/uploads/2012/12/DSC_0164-1-1170x770.jpg" }
    its(:ingredients) { should == ["Winter salad w. kale & pomegranate", "10-12 big leaves of kale (250-300 g)", "1 pomegranate", "2 apples", "3/4 cup almonds (100 g)", "2 Tbsp red wine balsamic", "1 Tbsp honey"] }
    its(:instructions) { should == "Heat a pan and dry roast the almonds for - minutes Add the balsamic and as soon as it is gone add the honey and stir for minute Transfer almonds to a baking shed and let them cool Afterwards chop them coarsely\nWash and dry the kale Remove the stems and use a food processor or mini chopper to chop the kale into a very fine texture You can use a sharp knife\nPrepare the pomegranate and cut the apple into thin boats and mix all of it it with the kale\nMake the dressing by stirring everything thoroughly together and season to taste (The balsamics can vary a bit in sweetness and flavour so make sure you adjust the amount you use according to your own balsamic)\nStir everything together and serve This salad is great the next day and even the day after that" }
  end
end
