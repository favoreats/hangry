# encoding: UTF-8

describe Hangry do
  context "laurenslatest.com Crockpot Chicken Nacho Dip" do
    before(:all) do
      @html = File.read("spec/fixtures/laurens_latest.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == 'Lauren Brennan' }
    its(:image_url) { should == 'https://laurenslatest.com/wp-content/uploads/2017/10/Chicken-Nacho-Dip-Pressure-Cooker-6.jpg' }
    its(:canonical_url) { should == "https://laurenslatest.com/crockpot-chicken-nacho-dip/" }
    its(:ingredients) { should == ["1 1/2 cups chicken broth, divided", "3 boneless, skinless chicken breasts (about 1.25 lbs)", "1/4 cup taco seasoning", "pinch of cayenne pepper, optional", "1-14.5 oz. can petite diced tomatoes, drained", "8 oz. cream cheese, cut into pieces", "6 oz. shredded mexican blend cheese (I used 365 Everyday Value)", "1 1/2 tablespoons cornstarch", "1-15 oz. can white beans**, rinsed and drained", "salt &amp; pepper, to taste", "2 roma tomatoes, finely diced", "chopped cilantro", "sliced jalapeno pepper"]}
    its(:instructions) { should == "Place 1 1/4 cups chicken broth into a pressure cooker. Whisk in taco seasoning and cayenne. Add in petite diced tomatoes and chicken breasts and stir to coat in liquid. Cover with lid, ensuring pressure valve is closed. Press the \"Chicken\" cook function button, adjust cook time to 15 minutes and press \"high pressure\" button. Press \"Start\" button. It will take about 7-10 minutes to preheat before cooking time starts.\nOnce cooking time has completed, open the pressure valve to release pressure quickly, using a long kitchen tool. Once all pressure has been released, remove lid. Remove chicken from the liquid and shred. Set aside.\nSwitch to \"Brown/Saute\" function and whisk in cream cheese blocks until melted. Sprinkle in shredded cheese bit by bit, whisking to melt. Whisk remaining 1/4 cup chicken broth with corn starch and pour in. Boil for 30 seconds to 1 minute or until dip starts to thicken. Add shredded chicken and rinsed beans. Stir. Press \"Stop\" button. Taste and adjust seasoning with salt, pepper, cayenne or even more taco seasoning if you'd like.\n*Feel free to omit cayenne pepper if you don't like spicy things. Use as much as your little heart desires.\n**I used white beans, but black beans are perfectly fine to use too! Just make sure you drain and rinse really well." }
    its(:name) { should == "Crockpot Chicken Nacho Dip" }
    its(:prep_time) { should == 15 }
    its(:cook_time) { should == 25 }
    its(:total_time) { should == 80 }
    its(:yield) { should == '8 servings' }
    its(:published_date) { should == "Nov 1, 2017" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end