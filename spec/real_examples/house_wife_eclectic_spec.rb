# encoding: UTF-8

describe Hangry do
  context "housewifeeclectic.com grown-up-chicken-nuggets" do
    before(:all) do
      @html = File.read("spec/fixtures/house_wife_eclectic_grown_up_chicken_nuggets.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) {should == "https://www.housewifeeclectic.com/2015/01/grown-up-chicken-nuggets.html" }
    its(:image_url) { should == "https://www.housewifeeclectic.com/wp-content/uploads/2015/01/GrownUpChickenNuggets-1.jpg" }
    its(:ingredients) { should == ["2-3 chicken breasts, cut into bite-size chunks", "1 cup milk", "1 Tbsp vinegar", "1 Tbsp Louisiana hot sauce", "1 egg", "3/4 cup flour", "1/2 cup cornstarch", "1/2 tsp salt", "1/4 tsp black pepper", "Panko bread crumbs", "Vegetable oil (for frying)"]}
    its(:instructions) { should == "Whisk the milk, vinegar, hot sauce, egg, flour, cornstarch, salt and pepper together in a large bowl\nAdd chicken to the mixture Remove the chicken in small handfuls and coat in Panko bread crumbs\nUsing a large electric skillet, warm oil to medium-high heat\nCarefully add the chicken to the oil, being careful not to let the chicken clump together If it does, let it fry for a minute, then break apart with a metal spatula\nFry the chicken on each side for - minutes Remove from the oil and allow to rest on a paper towel lined plate" }
    its(:name) { should == "Grown-up Chicken Nuggets" }

    context "when the selector '//*[contains(text(),'INGREDIENTS')]/../following::div[./following::*[contains(text(),'PREPARATION') or contains(text(),'DIRECTIONS')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/house_wife_eclectic_forget_ikea_makeyour_own_swedish.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["Sauce", "2 Tbsp butter", "2 Tbsp + 2 tsp flour", "6 Tbsp sour cream", "2 cups beef broth", "1 Tbsp parsley", "1 tsp oregano", "Salt and pepper to taste", "Meatballs, cooked"])
      end
    end

    context "when the selector '//*[contains(text(),'INGREDIENTS')]/following::p[./following::*[contains(text(),'PREPARATION')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/house_wife_eclectic_ranch_chicken_crockpot_tacos.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["4 frozen chicken breasts", "1 packet taco seasoning", "1 packet ranch", "1/2 tsp cumin", "1 can chicken broth", "8-10 tortillas", "Shredded cheese", "Tomatoes, diced", "Lettuce, shredded", "Cilantro, chopped", "Sour cream"])
      end
    end

    context "when the selector '//*[contains(text(),'Steps')]/following::div[./following::*[contains(text(),'Feel')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/house_wife_eclectic_chinese_cashew_chicken_with_tayler_from.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Cook the brown rice\nChop up the chicken, red pepper, and broccoli\nIn small bowl, mix chopped chicken, cornstarch, cumin, garlic powder, and ginger\nIn separate bowl, mix soy sauce, brown sugar, and vinegar\nPour the oil in a pan and cook the chicken in it Add in the red peppers, the broccoli, cashews, and the sauce mixture\nCook on medium heat for about minutes–stir occasionally Put the lid on and put heat on low for minutes The cashews shouldn’t be dry anymore\nPut this mixture on top of rice and sprinkle Chow Mein noodles on top\nEnjoy!")
      end
    end

    context "when the selector '//*[@class='entry-content']/ul/following::p[./following::*[@class='centered']] | //*[@class='entry-content']/ul/following::div[./following::*[@class='centered']]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/house_wife_eclectic_russian_tea_cookies.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Mix together the butter, powdered sugar and vanilla until well combined Add the flour and salt, mixing again Roll the dough into -inch balls and then roll them in the sugar sprinkles Bake the cookies on a GREASED cookie sheet for minutes at degrees Cookie should be stiff but not brown\nHave you ever tried these kind of cookies before?")
      end
    end

    context "when the selector '//*[contains(text(),'PREPARATION')]/following::p[./following::*[@class='centered']]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/house_wife_eclectic_the_best_new_york_street_food_you_can.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Chicken Prep: Place the lemon juice, olive oil, oregano, coriander and garlic in a blender and blend until smooth Put half the marinade and chicken in a gallon bag and refrigerate Spread the marinade well to make sure the chicken is coated Refrigerate for to hours\nRemove the chicken and pat dry Sprinkle with salt and pepper (don’t skimp on the pepper! Heat oil in an electric skillet at medium-high heat Cook the chicken for minutes Flip the chicken, and reduce the heat to medium and cook until the chicken is cooked through (about minutes Remove chicken from heat and allow to cool for minutes for juices to redistribute\nChop the chicken into /-inch chunks and place in a large bowl Add the other half of the marinade that you didn’t use to marinate the chicken originally\nRice Prep: Melt the butter in a large pan Add the cumin and turmeric and cook about minute Add the rice and stir until rice is coated a nice yellow color Lightly toasted the rice, stirring occasionally Add the chicken broth and salt and pepper to taste Bring to a boil Cover the rice and reduce to a simmer Cook for about minutes without disturbing Remove from heat and let the rice absorb the rest of the liquid until tender, which takes about minutes\nSauce Prep: Combine the sour cream, mayo, sugar, vinegar, lemon juice, parsley, and teaspoons black pepper Whisk together well Season to taste with salt\nServe: Place the chicken and the marinade back in the heated electric skillet and cook over medium-high heat until heated through\nServe by placing a bed of rice on a plate and top with the chicken Serve with chopped lettuce, diced tomato, and toasted pita bread The sauce is on the side (or on top if you love that much sauce, but be careful because it’s kind of tangy Hot sauce is also good with it")
      end
    end

    context "when the selector '//*[contains(text(),'DIRECTIONS')]/following::p[./following::*[@class='centered']]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/house_wife_eclectic_eggtastic_egg_rolls.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Place bacon strips on an aluminum foil covered baking sheet Bake at for minutes Flip bacon over and continue baking for another minutes, until bacon becomes crispy Remove and chop the bacon\nLightly saute the diced jalapeño\nIn a mixing bowl, crack the six eggs and whip Add garlic salt, onion powder, black pepper, cayenne pepper, paprika and oregano Mix in well Add jalapeños, cilantro and bacon and mix\nLightly butter a pan and cook the egg mixture Scramble and remove from heat\nAssemble each egg roll by laying out an egg roll wrapper in a diamond formation Lay the eggs across the middle Add some grated cheese Take the bottom point of the wrapper, fold up, and tuck under the filling Fold in each side Add water to the bottom of the edges to help it stick Roll the rest of the egg roll up and lightly water the edge to help it seal\nOnce all egg rolls have been created, place each egg roll, fold-side down, in a pan of heated vegetable oil, about -inch full Fry until each side is a nice golden brown and remove from the oil Place on a plate lined with a paper towel and allow to rest for a couple minutes before serving\nDip the egg rolls in maple syrup It might sound weird, but it’s insanely good")
      end
    end

    context "when completely-tasty-bbq-taquitos recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/house_wife_eclectic_completely_tasty_bbq_taquitos.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'INGREDIENTS')]/following::p[./following::*[contains(text(),'DIRECTIONS') or contains(text(),'INSTRUCTIONS')]] | //*[contains(text(),'INGREDIENTS')]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients"do
          expect(subject.ingredients).to eq(["6 corn tortillas", "6 fajita-size flour tortillas", "3-4 chicken breasts", "1 cup BBQ sauce", "1/2 tsp. chili powder", "Pico de gallo", "3 roma tomatoes, diced", "1/4 onion, diced", "cilantro, chopped", "1/2 jalapeno, diced", "Toppings", "Sour cream", "Shredded cheese", "Shredded lettuce"])
        end
      end

      context "and the selector '//*[contains(text(),'DIRECTIONS')]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Boil chicken breasts (best to split them into tender-sized pieces first for faster boiling\nDrain water and place warm chicken breasts in mixer Mix until chicken is shredded\nPlace shredded chicken in medium bowl and top with the BBQ sauce and chili powder Stir well until chicken is evenly coated\nWarm the tortilla shells in the microwave\nPlace a strip of chicken down the edge of the tortilla You don’t want to do it in the middle or too close to the edge About / of the way away from the edge of the tortilla is a good place to place the chicken\nWrap the tortilla into a tight taquito, with the open end down against the plate to hold it closed\nFry the taquito in vegetable oil in either a fryer or in a pan on the stove Be sure to turn halfway through so each side gets a nice light, golden crisp\nNOTE: If you use a fryer, you may run into your flour tortilla taquitos popping open during frying To avoid this, you can either hold the taquitos down with metal tongs or you can pin the flour tortillas shut with toothpicks and remove the toothpics before eating If you fry on the stove, you shouldn’t have as much of a problem\nServe the taquitos with the pico de gallo, shredded lettuce, shredded cheese and sour cream Taquitos are best eaten when used almost like a strange spoon that scoops up all the toppings on the way to your mouth")
        end
      end
    end

    context "when simplified-challah-bread recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/house_wife_eclectic_simplified_challah_bread.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'You need') or contains(text(),'You will need')]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients"do
          expect(subject.ingredients).to eq(["1 and 1/2 tablespoons yeast", "1/2 cup and 1 tablespoon sugar", "1/2 cup oil", "5 eggs", "1 tablespoon salt", "8 cups of flour", "poppy seeds to sprinkle on top", "water", "(You can add raisin to the bread, but as a general rule, I hate raisins in bread, so I never do.)"])
        end
      end

      context "and the selector '//*[contains(text(),'You need') or contains(text(),'You will need')]/following::p[./following::*[contains(text(),'Filed')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Mix tablespoon of sugar and the yeast in and / cups warm water Add the oil while mixing, then add  four of the eggs, one at a time Add the rest of the sugar and the salt, mixing while you go Add the flour one cup at a time until the dough sticks together Knead the bread until smooth Place in a covered, greased bowl to rise Let the dough rise for an hour or until double in size\nWhen double in size, knead the dough again and then place to rise for another minutes Roll the dough out into strands about a foot long and braid Beat the last egg and brush on the bread, let the bread rise for about another hour\nSprinkle the bread with seeds Bake in a degree oven for about minutes or until golden")
        end
      end
    end

    context "when curried-couscous recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/house_wife_eclectic_curried_couscous.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@class='entry-content']/p[2]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients"do
          expect(subject.ingredients).to eq(["1 and 1/2 cups couscous", "3 cups chicken broth", "1 tablespoon curry powder", "2 teaspoons salt", "1 teaspoon pepper", "2 tablespoons olive oil", "handful of cilantro, chopped"])
        end
      end

      context "and the selector '//*[@class='entry-content']/p[2]/following::p[./following::*[contains(text(),'Filed')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Mix all of the ingredients, except the couscous and cilantro, in a sauce pan and bring to a boil, stirring frequently Pour the boiling liquid over the couscous in a mixing bowl and immediately cover with plastic wrap Allow the mixture to sit for about ten minutes Fluff up the couscous with a fork Top with the cilantro\nSome of my tips:\nI know it sounds odd to top it with cilantro but it really makes the dish It is good without the cilantro and amazing with it It adds the perfect accent flavor\nThis makes quite a bit of couscous Couscous really fluffs up when you add the liquid")
        end
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

