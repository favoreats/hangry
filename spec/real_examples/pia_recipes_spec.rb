# encoding: UTF-8

describe Hangry do
  context 'piarecipes.com baked-parmesan-paprika-chicken' do
    before(:all) do
      @html = File.read('spec/fixtures/pia_recipes_baked_parmesan_paprika_chicken.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.piarecipes.com/2012/11/baked-parmesan-paprika-chicken.html' }
    its(:image_url) { should == 'http://3.bp.blogspot.com/-VmVDGlVEYGc/UK605eJ-0VI/AAAAAAAACzg/wubbsPshAd0/s1600/Baked+Parmesan+Paprika+Chicken.jpg' }
    its(:ingredients) { should == ['1/4 cup all-purpose flour', '1/2 cup grated Parmesan cheese', '2 teaspoons paprika', '1/2 teaspoon salt', '1/2 teaspoon black pepper', '1 egg, beaten', '2 tablespoons milk', '4 skinless, boneless chicken breast halves', '1/4 cup butter, melted'] }
    its(:instructions) { should == "Preheat oven to 350 degrees F (175 degrees C). Coat a shallow baking dish with nonstick cooking spray.\nCombine flour, parmesan, paprika, salt, and pepper in a bowl. In a separate bowl, whisk together the egg and milk. Dip the chicken in the egg, then dredge in the flour mixture. Place in the baking dish, and pour the melted butter evenly over the chicken.\nBake for about 1 hour and 15 minutes in the preheated oven, until the cheese has browned, and the chicken has cooked.\nConnect together with your instructors once conceivable.\nEven in case you are doing smartly within the category, its extraordinarily vital to increase a rapport together with your trainer. When you do that early on, you temporarily understand what the priorities are within the route, what expectancies the professor has of you and youre going to be motivated to prevail, surer of whats forward and higher ready to do smartly with the impending subject material." }
    its(:name) { should == 'Baked Parmesan Paprika Chicken' }

    context "when the selector '//*[contains(text(),'Ingredients')]/following::div[./following::*[contains(text(),'Instructions')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/pia_recipes_shrimp_and_pesto_pizza_with_goat_cheese.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['4 (6 inch) round pizza crusts or flat breads', '2 heaping tablespoons pesto spread', '20 small-medium shrimp, peeled and deveined', '2 garlic cloves, finely chopped', 'salt and pepper', '1 1/2 tablespoons olive oil', '1 cup shredded mozzarella cheese', '2 ounces crumbled goat cheese'])
      end
    end

    context "when the selector '//*[contains(text(),'Instructions:')]/following::div[./following::*[(@class='googlepublisherads')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/pia_recipes_john-legends_macaroni_and_cheese.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Preheat oven to 375 degrees. Generously butter a 13-by-9-inch glass baking dish; set aside. Bring a large pot of water to a boil; add salt and macaroni. Cook until al dente according to package directions. Drain, and return to pot. Add butter, and toss until pasta is coated and butter has melted; set aside.\nIn a medium bowl, whisk together evaporated milk, skim milk, and eggs. Add seasoned salt, garlic powder, 1 teaspoon salt, and 1/2 teaspoon pepper; set aside. In another medium bowl, combine cheeses; set aside.\nPlace 1/3 macaroni in an even layer in the bottom of prepared baking dish; cover evenly with 1/3 cheese. Repeat with remaining macaroni and cheese mixture. Pour milk mixture evenly over contents of baking dish. Sprinkle with paprika. Bake until top layer is lightly browned, 35 to 45 minutes. Let stand 10 to 15 minutes before serving.\noriginal recipe: http://www.\nmarthastewart.\ncom/348566/john-legends-macaroni-and-cheese?czone")
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
