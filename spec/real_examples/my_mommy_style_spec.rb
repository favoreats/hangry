# encoding: UTF-8

describe Hangry do
  context "mymommystyle.com the-best-homemade-pizza-sauce" do
    before(:all) do
      @html = File.read("spec/fixtures/my_mommy_style_the_best_homemade_pizza_sauce.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.mymommystyle.com/2014/01/05/the-best-homemade-pizza-sauce/" }
    its(:image_url) { should == "https://www.MyMommyStyle.com/wp-content/uploads/2014/01/the-best-and-easiest-homemade-pizza-sauce-744x1024.jpg" }
    its(:ingredients) { should == ["2 (15-ounces each) cans tomato sauce", "1 (12 ounce) can tomato paste", "1 tablespoon Italian seasoning", "1 tablespoon dried oregano", "1 teaspoon fennel seed, crushed*", "1 teaspoon onion powder", "1 teaspoon garlic powder", "1/2 teaspoon salt"]}
    its(:instructions) { should == "In a large saucepan over medium heat, whisk together tomato sauce and paste. Add remaining ingredients and mix well. Bring to a boil, stirring constantly.\nReduce heat to low; cover and simmer for 1 hour, stirring occasionally. Cool." }
    its(:name) { should == "The best homemade pizza sauce" }

    context "when pumpkin-nutella-muffins recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_pumpkin_nutella_muffins.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(@class,'ingredient')]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["3 1/3 C. flour", "2 tsp. baking soda", "3 C. sugar", "1 ½ tsp. salt", "1 tsp. cinnamon", "1 tsp. nutmeg", "1 C. oil", "4 eggs", "2/3 C. milk", "2 C. pumpkin puree", "Nutella"])
        end
      end

      context "and the selector '//*[contains(@class,'instruction')]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Preheat oven to 400 degrees and line pans with cupcake liners.\nCombine all dry ingredients and then all the remaining ingredients.\nStir just until combined and then fill cupcake liners 3/4 full.\nDrop about 1 teaspoon of Nutella into each muffin tin over the pumpkin batter (you don’t want to use too much Nutella or the muffins won’t rise correctly). Use a toothpick to swirl the Nutella in.\nBake for 15-17 minutes or until an inserted toothpick comes out clean (well, mostly clean, there may be some Nutella that sticks to the toothpick)")
        end
      end
    end

    context "when the selector '//*[contains(@class,'rd_ingredient')]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_slow_cooker_enchilada_casserole.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("In a large skillet, cook beef, peppers and onion over medium heat until meat is no longer pink; drain. Stir in the beans, salsa, 1/2 the can of enchilada sauce, and taco seasoning.\nCoat the bottom of a 6 or 7 quart slow cooker with cooking spray. Place three tortillas in slow cooker, overlapping if necessary and tearing one of the tortillas in half to fit. Layer with a third of the beef mixture and cheese. Repeat layers twice.\nPour the remaining enchilada sauce over the top of the tortillas and top with the remaining cheese.\nCover and cook on low for 2 to 2-1/2 hours or until heated through.\nTop with your favorite enchilada toppings.")
      end
    end

    context "when chicken-burrito-bowls recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_chicken_burrito_bowls.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'serves')]/following::p[1]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["2 cups cooked chicken, shredded (I used this super easy recipe for", "5 ingredient salsa chicken", ")", "2 cups rice, cooked", "1 (15 oz) can black beans, rinsed and drained", "1 (15 oz) can corn, drained", "2 roma tomatoes, diced", "1 small red onion, diced", "1 avocado, diced", "4 cups Romaine lettuce, shredded", "(You can also top with additional toppings of your choice: cheese, bell peppers, green onions, cilantro, olives, jalapenos, sour cream, guacamole, salsa, etc)", "Salad dressing of your choice (I used ranch)"])
        end
      end

      context "and the selector '//*[contains(text(),'Directions')]/following::p[1]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("To make individual servings, layer 1/2 cup rice, 1/2 cup chicken, lettuce, black beans, corn, tomatoes, onions, avocados and any other toppings you choose. Drizzle with your favorite salad dressing. That’s it!\nCheck out these other chicken recipes below:\nPosts may contain affiliate links. If you purchase a product through an affiliate link, your cost will be the same but My Mommy Style will receive a small commission. Your support is greatly appreciated!")
        end
      end
    end

    context "when crock-pot-brown-sugar-dijon-ham recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_crock_pot_brown_sugar_dijon_ham.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(@class,'recipePartIngredient')]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1 fully cooked boneless ham (5 to 6 lb)", "1/2 cup honey Dijon mustard", "1/2 cup real maple syrup", "1/2 cup packed brown sugar"])
        end
      end

      context "and the selector '//*[contains(@class,'liststyle')]/li' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Spray 6- to 7-quart oval slow cooker with cooking spray. Or use a very convenient Crock Pot liner, they make clean up a breeze! Make cuts about 1 inch apart and 1/4 inch deep in diamond pattern in top of ham, mine was a spiral ham, so I didn’t need to make cuts in it. Place ham in slow cooker.\nIn small bowl, stir together mustard, syrup and brown sugar with whisk until well blended. Pour mixture over ham.\nCover; cook on Low heat setting 3 to 4 hours.\n4 Remove ham from slow cooker. Cover loosely with foil; let stand 10 to 15 minutes.")
        end
      end
    end

    context "when steak-tortas recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_steak_tortas.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'What you need')]/following::p[./following::*[contains(text(),'How to make')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1 lb flank or skirt steak", "1 tsp cumin", "1/8 tsp cayenne pepper", "Salt and Pepper to taste", "1 medium red onion, sliced into 1/4″ thick rings", "4 crusty rolls, lightly toasted", "1/2 cup low-fat re fried beans (we omitted this)", "1 avocado peeled and sliced", "1 cup shredded jack cheese", "pico de gallo", "hot sauce (optional)"])
        end
      end

      context "and the selector '//*[contains(text(),'How to make')]/following::p[./following::*[contains(@class,'adjacent-posts-links')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Preheat a grill or grill pan over high heat. Season the steak all over with the cumin, cayenne, salt and pepper (or use your pre-seasoned meat). Grill the steak on each side for approximately 10 minutes until a nice “char” has developed on the outside and the meat is firm yet yielding to the touch. (Think of the firmness of a tennis ball.) Thickest center should reach an internal temperature of 135 degrees F. While the steak cooks grill your onions for about 10 minutes until carmelized and soft, this could also be done in a skillet with extra virgin olive oil. Allow the steak to rest for 5 minutes, this allows the moisture to stay locked in the meat while it cools. Slice meat into think pieces against the natural grain of the meat.\nTop your rolls with your re fried beans, avocado, onions, cheese, and pico de gallo. Serve with hot sauce if you’d like. ENJOY!\nThis was such an easy meal and was a fun excuse to pull out the grill in the middle of winter!")
        end
      end
    end

    context "when easy-crockpot-lasagna recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_easy_crockpot_lasagna.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients:')]/following::p[./following-sibling::*[contains(text(),'1.')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1 lb. ground beef/turkey (we prefer turkey)", "1 medium onion chopped", "2 cloves garlic minced or 1 tsp of prepared garlic", "29 oz. can tomato sauce", "1 tsp salt", "1/2 C water (this can be optional depending on the consistency and how long you cook it)", "1 tsp oregano", "8 oz package of lasagna noodles uncooked", "4 cups (16 oz) shredded mozzarella cheese", "1- 1/2 cups (12 oz) small curd cottage cheese (I use fat free)", "1/2 C grated parmesan cheese"])
        end
      end

      context "and the selector '//*[contains(text(),'Ingredients:')]/following::p[./following-sibling::*[contains(text(),'1.')]][last()]/following::p[./following-sibling::*[contains(text(),'Note')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Cook meat, onion, and garlic together in saucepan until browned. Drain.\nStir in tomato sauce, water, salt, and oregano. Mix well.\nSpray slow cooker with non-stick cooking spray. Spread one fourth of meat sauce. Arrange one third of noodles over sauce.\nCombine the cheeses spoon one third of mixture over noodles. Repeat layers twice. Top with remaining meat sauce.\nCover. Cook on low 4-5 hours.")
        end
      end
    end

    context "when roasted-pineapple-and-habanero-shish-kabobs recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_roasted_pineapple_and_habanero_shish_kabobs.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients:')]/following::p[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["2 zucchini; sliced lengthwise and chopped", "1 bell pepper; chopped", "8 oz baby bella mushrooms; cut in half", "1 cup fresh pineapple; cut into chunks", "12 cherry tomatoes", "2 boneless, skinless chicken breasts, cut into chunks", "2 cups Robert Rothschild Farm Roasted Pineapple and Habanero sauce"])
        end
      end

      context "and the selector '//*[contains(text(),'Directions')]/following::p[./following::*[contains(@class,'adjacent-posts-links')]][not(a)]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Marinate chicken and vegetables in sauce overnight. (Do not marinate the pineapples.)\nSoak your skewers in water for 30 minutes.\nAdd marinated chicken and vegetables and chunks of pineapple to the skewers.\nGrill until chicken is cooked through and vegetables are soft.\nMakes about 15 shish kabobs\nFor those of you that aren’t close to Costco (I feel your pain) you can purchase this sauce on Amazon or Walmart.\ncom.")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients')]/following::div[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/mymommystyle_chicken_fajita_marinade.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["1/2 cup extra virgin olive oil", "2 tablespoons chili powder", "2 tablespoons fresh lime juice", "2 tablespoons honey", "2 tablespoons garlic powder", "1/2 teaspoon paprika", "1/2 teaspoon cumin", "1/2 teaspoon ground black pepper", "3 pounds skinless, boneless chicken", "breasts, cut into strips", "1 large red onion; sliced", "1 green bell pepper; sliced", "1 red bell pepper; sliced", "Any other fajita toppings you enjoy. Some suggestions: sour cream, shredded cheese, avocado, salsa, chopped tomatoes"])
      end
    end

    context "when the selector '//*[contains(@class,'article-content')]/ul[last()]/following::p[./following::*[contains(text(),'If')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_roasted_spaghetti_squash_with_meatballs.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Preheat oven to 400 degrees. Lightly drizzle spaghetti squash with olive oil and rub in to cover the entire squash. Place squash halves, cut sides down, on a baking dish. Bake for 50-60 minutes until tender.\n* Let cool slightly. Use a fork to separate the strands of squash.\nMeanwhile, in a large bowl combine the egg, water, bread crumbs, Italian seasoning, garlic, salt and pepper. Add ground beef, broken into chunks, and mush with your hands to combine. Shape into 1-inch meatballs.\nLightly coat a very large skillet with cooking spray; heat skillet over medium-high heat. Add meatballs to hot skillet; cook until browned, turning occasionally to brown evenly. Add tomatoes, sugar, crushed red pepper, salt, and 2 teaspoons Italian seasoning. Bring to a boil; reduce heat. Simmer, uncovered for 10 minutes.\nServe meatballs and sauce over squash. Top with grated Parmesan cheese and fresh basil (if desired).")
      end
    end

    context "when the selector '//*[contains(text(),'DIRECTIONS')]/following::p[./following::*[contains(text(),'Enjoy')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_easy_peasy_blueberry_buckle_the_best_breakfast_cake_or_anytime_cake.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Preheat oven to 350 degrees. Melt the butter in a microwave safe dish. Mix together the sugar (leaving out the last 3 TBS of sugar), flour, baking powder, and salt in a large mixing bowl. Whisk in the milk and melted butter, stirring until just mixed (don’t over blend, it should be slightly lumpy). Butter a 9×13 pan or similar sized baking dish. Pour the batter into the buttered dish and sprinkle the blackberries evenly.\n**To Make the Crumb Topping: Mix together cup sugar, cup flour, cup butter, and mix with fork, and sprinkle over buckle before baking. Bake uncovered for 40-45 minutes or until a toothpick inserted near the center comes out clean. Refrigerate leftovers.")
      end
    end

    context "when the selector '.Directions ul li' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_crock_pot_beef_kale_stew.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Layer the potatoes, carrots, onion and celery in a 6-qt. slow cooker.\nPlace flour and 1/2 teaspoon salt in a large resealable plastic bag. Add stew meat, seal and toss to coat evenly. In a large skillet, brown meat in oil. Place over vegetables.\nIn a large bowl, combine the tomatoes, broth, wine, mustard, 1 teaspoon salt, pepper, and thyme. Pour over beef. Cover and cook on high for\n1/2 hours. Reduce heat to low; cook 7-8 hours longer or until the meat and vegetables are tender.\nAdd chopped Kale to the Crock Pot and cook for 20 additional minutes. Yields: 8 servings")
      end
    end

    context "when the selector '//*[contains(text(),'You will need')]/following::p[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/my_mommy_style_banana_nut_protein_green_smoothie.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["Handful of ice 1", "tblspoon PB2", "or peanut butter", "10 oz.", "unsweetened almond milk/coconut milk/regular milk", "handful of spinach or", "muffin", "size frozen spinach", "banana (I like to freeze mine)", "1 scoop", "Renew Life Skinny Gut Vanilla Shake", "(Vanilla or Chocolate)", "Renew Life Skinny Gut Chocolate Shake"])
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

