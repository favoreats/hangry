# encoding: UTF-8

describe Hangry do
  context 'weelicious.com crock-pot-spaghetti-recipe' do
    before(:all) do
      @html = File.read('spec/fixtures/weelicious_crock_pot_spaghetti_recipe.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == 'Crock Pot Spaghetti' }
    its(:canonical_url) { should == 'https://weelicious.com/2015/05/29/crock-pot-spaghetti-recipe/' }
    its(:image_url) { should == 'https://weelicious.com/wp-content/uploads/2015/05/Crock-Pot-Spaghetti-1-1.jpg' }
    its(:ingredients) { should == ['1 pound lean ground beef, turkey, or chicken', '1/2 Onion, finely diced', '1 tablespoon olive oil', '1/2 teaspoon garlic powder', '1/2 teaspoon kosher salt', '2 26-ounce-jars pasta sauce', '1 cup water', '1 pound spaghetti noodles', '1/2 cup grated parmesan cheese'] }
    its(:instructions) { should == "In a skillet over medium heat, cook the oil, meat, onion, garlic powder and salt for about 5-7 minutes, until cooked through. Drain excess fat.\nPour one jar of pasta sauce and 1 cup of water in the bottom of the slow cooker.\nBreak the spaghetti noodles in half and place them on top of the sauce.\nAdd the cooked meat the crock pot, sprinkle with the cheese, and cover with the second jar of pasta sauce.\nCover the crock pot and cook on high for 2- 2 1/2 hours or low for 6 hours\nRemove the lid, stir to combine and mix everything together." }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
