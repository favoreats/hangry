# encoding: UTF-8

describe Hangry do
  context "kayotic.nl honey-glazed-chicken-bacon-bites" do
    before(:all) do
      @html = File.read("spec/fixtures/kayotic.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://www.kayotic.nl/blog/honey-glazed-chicken-bacon-bites" }
    its(:image_url) { should == "http://farm5.static.flickr.com/4024/4394321601_5c14a8bee8.jpg" }
    its(:ingredients) { should == ["1 pound boneless chicken breasts", "20 thin bacon slices", "3 tbsp honey", "2 tsp coarse mustard", "fresh lemon juice"] }
    its(:instructions) { should == "Cut the chicken breasts—or fillets, as we call them here—in thin strips. Not too thin, the bacon has to brown and if the chicken is too thin it’ll get dry before the bacon gets crispy. That would be a sin.\nGrab a bowl and combine 3 tbsp honey with 2 tsp coarse mustard.\nSqueeze in some lemon juice. Not too much, about a tbsp or so. I think orange juice will be great, too.\nBacon rights all the wrongs in this world. It brings justice to all the injustices. Or something like that.\nI asked my favorite pair of extra hands to wrap the bacon around the chicken strips.\nPlace them in a roasting tray or on a cookie sheet. If you’re afraid of cleaning up, line the tray or sheet with some tin-foil first. I’m hard-core (And own a dish washing machine. It helps).\nBrush the chicken bites with half the marinade.\nAs soon as one side is done, take the tray out of the oven. Does this look good or what? I could never be a vegetarian simply because I cannot forsake bacon. I absolutely love it, in an artery clogging kind of way!\nFlip them over and brush them with the remaining marinade.\nDon’t these just scream: love me, bite me, chew me, eat me, devour me? If I can give you a tip, a really useful one at it, it would be to fix twice the amount you think you’ll need. And then some."}
    its(:name) { should == "Honey Glazed Chicken and Bacon Bites" }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

