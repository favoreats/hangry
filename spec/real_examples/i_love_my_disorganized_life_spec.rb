# encoding: UTF-8

describe Hangry do
  context "ilovemydisorganizedlife.com fruit-punch-vanilla-cupcakes" do
    before(:all) do
      @html = File.read("spec/fixtures/i_love_my_disorganized_life_fruit_punch_vanilla_cupcakes.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.ilovemydisorganizedlife.com/fruit-punch-vanilla-cupcakes/" }
    its(:image_url) { should == "https://www.ilovemydisorganizedlife.com/wp-content/uploads/2013/03/Fruit+Punch+Vanilla+Cupcakes+2.png" }
    its(:ingredients) { should == ["1 box Duncan Hines Vanilla cake mix", "2 packages Duncan Hines Frosting Creations Flavor Mix {Fruit Punch}", "1 can Duncan Hines Frosting Creations Frosting Starter"] }
    its(:instructions) { should == "Preheat oven to 350*. Spray muffin tins with non-stick baking spray or use liners\nPrepare cake mix as directed on package\nDivide batter equally into 2 bowls\nAdd one package Fruit Punch flavoring to one of the bowls and mix well\nAlternating batters, fill tins 2/3 full {vanilla, fruit punch, vanilla, fruit punch}, being careful not to overfill \nBake for 15-20 min, or until a toothpick inserted in the middle comes out clean\nCool completely on baking rack\nMix 2nd package Fruit Punch flavor with frosting starter\nFrost cupcakes as desired and enjoy!" }
    its(:name) { should == "Fruit Punch Vanilla Cupcakes" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

