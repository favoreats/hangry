# encoding: UTF-8

describe Hangry do
  context "smittenkitchen.com zucchini grilled cheese" do
    before(:all) do
      @html = File.read("spec/fixtures/smitten_kitchen_zucchini_grilled_cheese.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == 'deb' }
    its(:image_url) { should == "https://smittenkitchendotcom.files.wordpress.com/2017/06/zucchini-grilled-cheese.jpg?w=750" }
    its(:ingredients) { should == ["1 pound (about 2 large) zucchini or other summer squash, trimmed",
    "1 1/4 teaspoons fine sea or table salt, plus more if needed",
    "1 cup (3 ounces or 85 grams) coarsely grated gruyere cheese",
    "3/4 cup (2 1/2 ounces or 70 grams) coarsely grated fontina or provolone cheese",
    "1/4 cup (20 grams) finely grated parmesan or pecorino cheese",
    "Freshly ground black pepper",
    "8 thin slices bread of your choice, I used a country-style white bread",
    "A couple tablespoons softened butter or olive oil for brushing bread"] }
    its(:instructions) { should == "Prepare zucchini: Use a food processor with a grater attachment or the large holes of a box grater to grate the zucchini. In a large colander, toss together the zucchini and salt. Let stand for 20 to 30 minutes, until the zucchini has wilted and begun to release liquid. Drain the zucchini in a colander and then use your hands to squeeze out as much water as possible, a fistful at a time. Place wrung-out zucchini on paper towels to drain further.\nMake filling and assemble sandwiches: Mix zucchini with grated cheese, a lot of freshly ground black pepper, and more salt if needed.\nBrush or spread the bread sides that will form the outsides of the sandwiches with olive oil or softened butter. Spread zucchini-cheese on insides and close the sandwiches.\nCook the sandwiches: Place sandwiches on a large griddle or frying pan over low-medium heat. I like to cook grilled cheese slowly to give the centers a chance to really melt before the outsides get too brown. When the undersides are a deep golden brown, flip the sandwiches and cook until the color underneath matches the lid. Cut sandwiches in half and dig in. Perhaps some pickled vegetable sandwich slaw on the side?" }

    its(:name) { should == "Zucchini Grilled Cheese" }
    its(:total_time) { should == 45 }
    its(:yield) { should == 'Servings: Makes 4 sandwiches' }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
