# encoding: UTF-8

describe Hangry do
  context 'abountifulkitchen.com Chocolate Chip Oatmeal Cookie' do
    before(:all) do
      @html = File.read('spec/fixtures/abountifulkitchen_oatmeal_cookie.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == 'Si Foster' }
    its(:name) { should == 'The Best Chocolate Chip Oatmeal Cookie' }
    its(:image_url) { should == 'http://abountifulkitchen.com/wp-content/uploads/2018/02/fullsizeoutput_a5f5-667x1000.jpeg' }
    its(:ingredients) { should == ['1 cup butter ( unsalted), softened for 30 minutes on counter or microwave for 10 seconds',
                                    '1 cup brown sugar (light or dark)',
                                    '1/2 cup granulated sugar',
                                    '2 eggs',
                                    '1 tablespoon vanilla',
                                    '2 cups all-purpose flour',
                                    '1 cup old-fashioned oats',
                                    '1 teaspoon baking powder',
                                    '1 teaspoon baking soda',
                                    '1 teaspoon sea salt or kosher salt',
                                    '3 cups semisweet chocolate chips or milk chocolate chips'
                                   ] }
    its(:instructions) { should == "Preheat the oven to 375 degrees.\nCut butter into tablespoons and place into mixing bowl.\nMix butter and sugars until smooth, using medium speed on mixer.\nAdd eggs, one at a time, and vanilla. Mix until smooth.\nAdd all dry ingredients, including chocolate chips to the bowl.\nPulse the dry ingredients into the wet ingredients in mixing bowl. Mix just until wet and dry ingredients are incorporated. I scrape down the sides of bowl at least once during this process to insure the chocolate chips are evenly distributed.\nScoop the cookies onto a sheet of parchment or lightly greased cookie sheet, 6-8 cookies per pan. With the palm of your hand, give each cookie a light press to flatten cookie a bit.\nBake at 375 degrees for 8 minutes or until cookies are lightly golden on top. When the cookies are removed from oven, use a small spatula to push edges in to form the cookie into a round shape if the cookie spreads while baking. This must be done while cookie is still hot out of the oven!" }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when the selector '.cookbook-notes' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/butter_lettuce_grapefruit_and_avocado.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("-If making ahead, prepare the dressing and refrigerate up to three days in advance.\nWash the lettuce and store in a Ziploc bag with paper towels to absorb moisture.\nCut up the grapefruit, store in a bowl in refrigerator.\nDrain the grapefruit juice before placing the fruit in lettuce leaves.\nSlice the avocado and assemble salad just before serving.\n-I only used about half of the dressing total.")
      end
    end

    context 'when overnight-refrigerator-rolls recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/abountiful_kitchen_overnight_refrigerator_rolls.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '.wprm-recipe-prep_time' is used in the parse_prep_time method" do
        it 'should be able to parse the prep_time' do
          expect(subject.prep_time).to eq('30')
        end
      end

      context "and the selector '.wprm-recipe-servings' is used in the parse_yield method" do
        it 'should be able to parse the serving_size' do
          expect(subject.yield).to eq('16')
        end
      end
    end

    context 'when sweet-fresh-cranberry-salsa recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/sweet_fresh_cranberry_salsa.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'printable recipe')]/../following::p[1]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['1 bag fresh cranberries, washed', '1 handful cilantro', '1-3 serrano or *jalapeno pepper', '1/2 chopped red onion', '1 clove garlic', '1 tablespoon grated lime peel', '1/2 teaspoon salt', '1/2 cup sugar', '1 tablespoons lime juice'])
        end
      end

      context "and the selector '//*[contains(text(),'printable recipe')]/../following::p[2]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Wash and remove seeds from jalapeno, using food handlers gloves, or plastic sack. In a food processor, place cranberries. cilantro, pepper, garlic and red onion.\nStir in sugar, salt and lime juice. Cover and refrigerate for 2-4 hour to allow flavors to mix. If serving over cream cheese, drain excess juice before using. Serve with tortilla chips.")
        end
      end
    end

    context 'when perfect_au_gratin_potatoes recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/perfect_au_gratin_potatoes.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//i[contains(text(),'A Bountiful Kitchen')]/../following::p[1]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['2 lbs Yukon Gold potatoes', 'salt', '1 1/4 cups grated Parmesan cheese', '1/4 cup melted butter, unsalted', '1 cup cream, I prefer heavy cream'])
        end
      end

      context "and the selector '//i[contains(text(),'A Bountiful Kitchen')]/../following::p[2]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Grease a 9 inch pie plate or other baking dish of similar size. Set aside.\nWash the potatoes, and place in a large pot. Fill with water until just barely covering potatoes.\nPlace pan on stove and cook over high heat until boiling, then reduce heat to medium heat for about 30 minutes, or until a fork inserted easily goes through to the middle of the potato.\nDrain the water and place the potatoes on a cutting board.\nUsing a paring knife and a dishcloth, peel the potatoes, and slice thin. Place about half of the potatoes in the prepared dish for baking. Generously salt the potatoes. Sprinkle about 1/2 of the cheese on the potatoes, and drizzle about half of the butter on as well.\nContinue to slice the potatoes and place on top of the potatoes in the pan. Sprinkle with additional salt, cheese and drizzle with remaining butter.\nFinally, drizzle the cream on top and around the edges of the potatoes in the dish.\nBake in a 375 degree oven on top third of oven for about 20 minutes or until bubbly and golden.")
        end
      end
    end

    context 'when abountiful_kitchen_our_favorite_fried_rice recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/abountiful_kitchen_our_favorite_fried_rice.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector 'img.alignleft' is used in the parse_image_url method" do
        it 'should be able to parse the image_url' do
          expect(subject.image_url).to eq('http://abtflkitchen.wpengine.com/wp-content/uploads/2014/04/IMG_9589-1024x682.jpg')
        end
      end

      context "and the selector '//*[contains(text(),'print recipe') or (contains(text(),'Print recipe'))]/../following::p[1]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['7 cups cooked sticky Japanese (pearl) rice, cooled completely (see tips below)', '1/2 cup chopped onion (yellow or white)', '1/2 lb chopped ham (about 1 1/2 cups or two thick deli slices)', '2 cups cabbage, sliced thin', '4 tablespoons butter, separated', '2 tablespoons vegetable oil', 'salt', 'pepper', 'soy sauce', '1/2 bunch green onions, chopped white and green parts'])
        end
      end

      context "and the selector '//*[contains(text(),'print recipe') or (contains(text(),'Print recipe'))]/../following::p[2] | //*[contains(text(),'print recipe')]/../following::p[2]/following-sibling::div' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Using a large non stick pan, cook each of the vegetables, separately*, in a small amount of oil and butter. I use about 1 teaspoon for each vegetable/meat. Salt and pepper each vegetable.\nTransfer to a bowl or plate. It’s OK to let the vegetables and meat inter-mingle at this point 🙂\nAfter cooking all of the vegetables and meat, add about 2 tablespoons of butter to the Teflon pan. Over medium heat, add the cooked and cooled rice a little at a time, breaking apart clumps with two wooden spoons until the rice is evenly distributed in the pan, and the butter is mixed in well.\nAdd all of the vegetables and meat to the rice. Toss lightly in pan. Season again with fresh ground pepper.\nAdd the soy sauce, a little at a time. We don’t like to drown the rice in soy sauce, so I only use about 2-3 tablespoons to 7 cups of rice. Taste, add salt and more pepper, if desired. After heated through, add fresh chopped green onions. Turn the heat off. If you continue to leave the heat on, your beautiful fried rice will end up as gummy rice.")
        end
      end
    end

    context 'when challah-bread recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/abountiful_kitchen_challah_bread.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '.wprm-recipe-ingredients li' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['2 tablespoons dry yeast', '3/4 cup honey', '1 3/4 cups warm water', '2 cups flour', '4 teaspoons sea salt', '1 cup vegetable or canola oil', '3 eggs', '8-10 cups flour', '1 egg', 'poppy seeds or sesame seeds'])
        end
      end

      context "and the selector '.wprm-recipe-instructions li' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("You may use a stand mixer for this recipe, if you do not have one, use a large bowl and mix with a large spoon.\nPlace warm water, honey and yeast in a bowl and stir. Let sit until yeast starts to foam and rise up.\nMix in 2 cups of flour. Stir well. Add 4 teaspoons sea salt and stir again.\nPour in 1 cup vegetable or canola oil and 3 eggs. Mix until all ingredients are incorporated.\nAdd the flour one cup at a time, until the dough loses its stickiness. This may take about 10 minutes. Read note about flour and how much to add.\nRemove the dough and place in a large oiled bowl and cover. Let rise for about 2 hours, punching down if needed.\nAfter the dough has doubled in size, punch down and knead. If the dough is sticky, knead in a little more flour to make handling easier.\nDivide the dough into two even pieces. Divide each portion of dough into three even pieces. Roll the dough into log rope-like strands and braid the pieces of dough to form a loaf about 15 inches long. Tuck the ends of the loaf under.\nPlace the dough onto a greased pan, one loaf per pan. Brush with additional beaten egg and sprinkle with poppy seeds or sesame seeds if desired. Cover loosely and let rise for about 30-45 minutes in a warm place.\nTurn oven to 325 degrees. Place rack on bottom third of oven.\nWhen the dough has risen for the second time, place the bread in the preheated oven. Lower the temperature to 300 and bake for 50-60 minutes. The bread should be golden on the top and bottom of the loaf and be cooked through and not doughy in the middle. If the bread is not completely cooked, continue cooking for 5-10 minutes.")
        end
      end

      context "and the selector 'h2.wprm-recipe-name' is used in the parse_name method" do
        it 'should be able to parse the name' do
          expect(subject.name).to eq('Challah Bread')
        end
      end
    end

    context 'when ArgumentError is raised in the parse_published_date method' do
      before(:each) do
        @html = File.read('spec/fixtures/abountifulkitchen_oatmeal_cookie_chocolate_chip.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should set published date attribute to nil' do
        expect(subject.published_date).to be_nil
      end
    end
  end
end
