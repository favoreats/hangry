# encoding: UTF-8

describe Hangry do
  context "asliceofstyle.com roasted-cauliflower/" do
    before(:all) do
      @html = File.read("spec/fixtures/a_slice_of_style.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://asliceofstyle.com/roasted-cauliflower/" }
    its(:image_url) { should == "http://asliceofstyle.com/wp-content/uploads/2014/04/Roasted-Cauliflower-7.jpg" }
    its(:ingredients) { should == ["Cauliflower pieces", "Seasoning of choice (I like poultry seasoning)", "Olive oil"] }
    its(:instructions) { should == "1. Heat olive oil in nonstick pan over medium heat on stove for about 5 minutes\n2. Cut head of cauliflower into bite sized pieces \n3. When pan is heated, add desired amount of cauliflower pieces and sprinkle with desired amount of seasoning \n4. Cover the pan for 2 minute increments, then stir and recover until pieces of cauliflower are browned\n5. Remove from heat and serve immediately " }
    its(:name) { should == "Roasted Cauliflower" }

    context "when post-signature recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/a_slice_of_style_post_signature.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '.ingredient' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["2/3 cup melted coconut oil", "1 cup honey", "4 eggs", "2 cups mashed bananas (I used 4 large bananas)", "2 teaspoons vanilla extract", "1 teaspoon salt", "1 teaspoon cinnamon, plus more to swirl on top", "31/2 cups whole wheat flour (I used normal flour but I’m sure whole wheat pastry flour would be great)", "2 teaspoon baking soda", "1/2 cup hot water"])
        end
      end

      context "and the selector '.instruction' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Preheat oven to 325 degrees Fahrenheit (165 degrees Celsius) and grease two 9×5 inch loaf pans. I grease my bread pans by painting on Crisco with a pastry brush then coating with flour. I make sure the sides are fully covered then I tap off excess flour in the sink. \nIn a large bowl or mixer, beat oil and honey together. Add eggs and beat well.\nStir in bananas and vanilla, then stir in the salt and cinnamon. Lastly, stir in the flour, just until combined.\nAdd baking soda to hot water, stir to mix, and then mix briefly into batter until it is evenly distributed. Spread batter into the greased loaf pan.\nSprinkle with cinnamon\nBake for 60 to 65 minutes. Be sure to check that the bread is done baking by inserting a toothpick in the top. It should come out clean. Let the bread cool in the loaf pan for 5 minutes, then transfer it to a wire rack to cool for 30 minutes before slicing.")
        end
      end
    end

    context "when romaine-chicken-tacos recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/a_slice_of_style_romaine_chicken_tacos.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients')]/following::span[./following::*[(text()='Directions') or (contains(text(),'Instructions'))]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["Rotisserie chicken", "Green, red and yellow peppers", "Roasted corn", "Guacamole", "Romaine lettuce"])
        end
      end

      context "and the selector '//*[(text()='Directions') or (contains(text(),'Instructions'))]/following::span[./following::*[contains(text(),'Posted')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Heat chicken, peppers, and corn on a pan on the stove.\nPrepare romaine leaves by washing and drying them, then top with chicken mixture and guacamole.\nEnjoy! ")
        end
      end
    end

    context "when grandmas-fried-chicken recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/a_slice_of_style_grandmas_fried_chicken.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients')]/../following::div[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1 tsp paprika", "1 tsp garlic powder", "1 tsp onion powder", "1 tsp dried oregano", "1/2 tsp dried rubbed sage", "1/2 tsp dried powdered rosemary", "1/2 tsp dried powdered thyme", "1 tsp dried powdered parsley", "1/2 tsp salt", "1 tsp ground pepper", "1/2 tsp marjoram", "1/2 tsp powdered ginger", "1 cube bouillon crushed ", "These ingredients depend on how much you are making:", "Chicken", "Eggs", "Flour of choice (my grandma used brown rice flour here)", "Half sleeve of saltine crackers"])
        end
      end

      context "and the selector '//*[contains(text(),'Directions')]/../following::div[./following::*[contains(text(),'Posted')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("1. Combine all spices into a bowl. Get out 3 bowls, combine 1 and 1/2 Tbsp of spice mixture with flour and put into one, put eggs that have been whisked into another, and crackers in the last bowl. \n2. Heat oil in pan. \n3. Cut chicken breasts in half, and dip each piece first into flour, then into eggs, then into crackers.\n4. Cook pieces of chicken in pan until golden on one side then flip until thoroughly cooked.\n5. When chicken is done, take out of pan. Add 1 tsp of seasoning mixture into pan, and add a bit more oil as well as flour to form a roux until browned. Add milk and stir until thickened for the gravy. Add more seasoning mix to taste. \nEnjoy!")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Directions')]/following::span[./following::*[contains(text(),'Note')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/a_slice_of_style_chewy_chocolate_chip_cookies.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Mix together the flour, baking soda, corn starch and salt in a large bowl. Set aside.\nIn a medium size bowl, whisk the melted butter, brown sugar, and granulated sugar together until no brown sugar lumps remain. Whisk in the egg, then the egg yolk. Finally, whisk in the vanilla. Pour the wet ingredients into the dry ingredients and mix together with a large spoon or rubber spatula. The dough will be very soft, yet thick. Fold in the chocolate chips. Cover the dough and chill for 2 hours, or up to 3 days. Chilling is mandatory.\nTake the dough out of the refrigerator and allow to slightly soften at room temperature for 10 minutes.\nPreheat the oven to 325 F degrees. Line large baking sheets with parchment paper or silicone baking mats. Set aside. To get the perfect cookie bottom, double-stack two cookie sheets for each batch. \nFor large cookies, roll the dough into balls, about 3 Tablespoons of dough each. The dough will be crumbly, but the warmth of your hands will allow the balls to stay intact. For smaller cookies, scoop using the Oneida cookie scoop. Roll the cookie dough balls to be taller rather than wide, to ensure the cookies will bake up to be thick. Press a few more chocolate chips/chunks on top of the dough balls for looks, if desired. Bake the cookies for 11-12 minutes. The cookies will look very soft and under baked. They will continue to bake on the cookie sheet. Allow to cool on the cookie sheet for 10 minutes before moving to a wire rack to cool completely.\nCookies stay soft and fresh for 7 whole days at room temperature. Cookies may be frozen up to 3 months. Rolled cookie dough may be frozen up to three months and baked in their frozen state for 12 minutes.\nEnjoy with a cold glass of milk")
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients')]/following::span//center[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/a_slice_of_style_cinnamon_pumpkin_waffles.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["2 and 1/2 cups flour (scoop and level)", "4 teaspoons baking powder", "2-3 teaspoons cinnamon", "1 and 1/2 teaspoon allspice", "1/4 teaspoon nutmeg", "1/4 teaspoon ground cloves", "1/2 teaspoon salt", "1/3 cup brown sugar, lightly packed", "4 medium eggs", "1 cup buttermilk, or regular milk", "1 and 1/2 teaspoon vanilla extract", "1 and 1/4 cup canned pumpkin (not pumpkin pie filling)", "4 tablespoons butter, melted"])
      end
    end

    context "when the selector '//*[contains(text(),'Directions')]/following::span//span[./following::*[contains(text(),'Note')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/aslice_of_style_paleo_banana_nut_muffins.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Preheat oven to 350 degrees Fahrenheit. Line a muffin tin with cups. In a large bowl, add bananas, eggs, almond butter, coconut oil and vanilla. Using a hand blender, blend to combine.\nCombine coconut flour, cinnamon, nutmeg, baking powder, baking soda, and salt in a small bowl then blend into the wet mixture, scraping down the sides with a spatula. Distribute the batter evenly into the lined muffin tins. Top with crushed almonds if desired. \nBake for 20-25 minutes, until a toothpick comes out clean. Serve warm or store in the refrigerator in a resealable bag. Enjoy!")
      end
    end

    context "when rolls-garlic-parmesan-homemade-rolls recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/a_slice_of_style_rolls_garlic_parmesan_homemade_rolls.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '[class*='wp-image']' is used in the parse_image_url method" do
        it "should be able to parse the image_url" do
          expect(subject.image_url).to eq("http://asliceofstyle.com/wp-content/uploads/2018/01/garlic-parmesan-easy-homemade-rolls-rhoades-rolls_60-700x1050.jpg")
        end
      end

      context "and the selector '.entry-content ul:nth-of-type(1) li' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["1/2 cup butter", "12 Rhoades frozen dinner rolls (They are the best price as Costco, but you can buy them at any grocery store in the freezer section)", "1 Tbsp garlic (I LOVE this Lighthouse freeze dried garlic! I discovered it at Costco and I will never touch another clove of garlic again. This stuff is AMAZING!)", "1 cup parmesan cheese"])
        end
      end

      context "and the selector '.entry-content ul:nth-of-type(2) li' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Melt 1/2 cup butter and 1 Tbsp garlic in the microwave, then stir in 1 cup parmesan cheese.\nTake frozen dough balls and roll them in the butter mixture. Since they are frozen and the mixture is thick, you may need to use both hands to get the roll covered!\nSpray a glass baking dish with oil and place the rolls onto the dish.\nSpoon any remaining mixture onto the top of the rolls.\nCover the pan with plastic wrap sprayed with oil and let them raise until doubled in size.\nFollow instructions on the Rhoades bag for baking instructions!")
        end
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end