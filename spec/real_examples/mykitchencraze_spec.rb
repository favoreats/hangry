# encoding: UTF-8

describe Hangry do
  context 'mykitchencraze.com glazed-meatloaf' do
    before(:all) do
      @html = File.read('spec/fixtures/my_kitchen_craze_glazed_meatloaf.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://mykitchencraze.com/glazed-meatloaf/' }
    its(:image_url) { should == 'https://mykitchencraze.com/wp-content/uploads/2015/03/IMG_6459-300x300.jpg' }
    its(:ingredients) { should == ["1 cup ketchup", "1/4 cup brown sugar, packed", "2 1/2 tablespoons apple cider or white vinegar", "1/2 teaspoon Tabasco sauce", "2 teaspoons canola oil", "1 medium onion, finely chopped", "3 garlic cloves, minced", "2/3 cup saltine crackers, about 17 crackers", "1/3 cup 2% milk", "1 pound 90% lean ground beef", "1 pound ground turkey", "2 large eggs", "1 egg yolk", "2 teaspoons dijon mustard", "2 teaspoons Worcestershire sauce", "1/2 teaspoon dried thyme", "1/3 cup fresh parsley, finely chopped", "1 teaspoon salt", "1 teaspoon pepper"] }
    its(:instructions) { should == "Preheat oven to 350 degrees F. Line a baking sheet with foil and lightly spray with non-stick cooking spray; set aside.\nIn a small sauce pan add all the glaze ingredients and whisk to combine. Remove a 1/4 cup and place in a small bowl; set aside for later. Simmer remaining glaze over medium heat until slightly thickened, about 5 minutes. Remove from heat and cover to keep warm.\nHeat oil in a skillet over medium-high heat. Add onions and cook until golden, about 7-9 minutes. Add garlic and cook for one more minute, making sure to stir constantly so you don't burn the garlic. Remove from heat and transfer to a large bowl. Set aside and let cool.\nIn a food processor add crackers and milk. Process until smooth. Add the beef and turkey. Pulse until well combined. If you can't get it all combined, don't worry. You can do some by hand later on.\nTransfer meat mixture to the cooled onion bowl. Add the eggs, egg yolk, mustard, Worcestershire sauce, thyme, parsley, salt and pepper. Mix throughly with your hands. Hands work best for this method.\nTurn your oven to the high broil setting.\nPlace meat mixture onto prepared baking sheet. Spread and pat meatloaf into a 9x5 inch loaf. Place meatloaf in the oven on the top rack and broil until well browned, about 5 minutes.\nBrush 2 tablespoon of reserved glaze onto the meatloaf and boil for another 2 minutes.\nTransfer meatloaf to the middle of the rack. Brush with remaining reserved glazed. Return oven temperature to Bake at 350 degrees F.\nBake meatloaf for 40-45 minutes, until cooked through.\nRemove from oven, cover lightly with foil tent and let rest 20 minutes.\nSlice into pieces and smother with cooked warm glaze.\nServe & Enjoy!!" }
    its(:name) { should == 'Glazed Meatloaf' }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
