# encoding: UTF-8

describe Hangry do
  context "greatist.com creamy-avocado-pasta " do
    before(:all) do
      @html = File.read("spec/fixtures/greatist_creamy_avocado_pasta.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://greatist.com/team/my-creamy-avocado-pasta" }
    its(:ingredients) { should == ["1 medium sized ripe Avocado, pitted", "1/2 lemon, juiced + lemon zest to garnish", "2-3 garlic cloves, to taste", "1/2 tsp kosher salt, or to taste", "1/4 cup Fresh Basil", "2 tbsp extra virgin olive oil", "2 servings/6 oz of your choice of pasta", "Freshly ground black pepper, to taste"] }
    its(:instructions) { should == "Bring several cups of water to a boil in a medium sized pot. Add in your pasta, reduce heat to medium, and cook until Al Dente, about 8-10 minutes.\nMeanwhile, make the sauce by placing the garlic cloves, lemon juice, and olive oil into a food processor. Process until smooth. Now add in the pitted avocado, basil, and salt. Process until smooth and creamy.\nWhen pasta is done cooking, drain and rinse in a strainer and place pasta into a large bowl. Pour on sauce and toss until fully combined. Garnish with lemon zest and black pepper. Serve immediately." }
    its(:name) { should == "Creamy Avocado Pasta" }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

