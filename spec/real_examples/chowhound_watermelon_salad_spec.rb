# encoding: UTF-8

describe Hangry do
  context "chowhound.com Grilled Watermelon, Feta, and Mint Salad" do
    before(:all) do
      @html = File.read("spec/fixtures/chowhound_watermelon_salad.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == "Grilled Watermelon, Feta, and Mint Salad" }
    its(:author) { should == 'Kim Laidlaw' }
    its(:total_time) { should == 40 }
    its(:yield) { should == '6 servings' }
    its(:description) { should == 'For this fresh, healthy summer salad recipe that balances sweet, tart, and salty, grilled watermelon, combines with feta, and mint.' }
    its(:image_url) { should == "https://www.chowstatic.com/assets/2015/04/31397_grilled_watermellon_mint_salad_2.jpg" }
    its(:ingredients) { should == ["1 small seedless watermelon (about 4 pounds)",
                                   "Olive oil",
                                   "Juice of 1 lime",
                                   "Juice of 1/2 orange",
                                   "Kosher salt",
                                   "1/4 cup chopped fresh mint",
                                   "1/2 cup crumbled feta cheese"] }
    its(:instructions) { should == "Quarter the watermelon lengthwise. Cut each quarter into 2-inch-thick slices. Place on a baking sheet, then brush lightly with olive oil. Set aside.\nPreheat a charcoal or gas grill to medium heat. Or preheat a stovetop grill over medium heat. Clean and oil the cooking grate. Add the watermelon slices and grill just until marked, about 2 minutes per side. Set aside to cool on the baking sheet.\nIn a bowl, whisk together the citrus juices, a pinch of salt, and 2 to 3 tablespoons olive oil.\nRemove and discard the rinds, then cut the watermelon into chunks and add to a serving bowl. Pour the dressing over the top and toss gently. Top with the feta and mint and toss to coat. Serve right away." }



    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
