# encoding: UTF-8

describe Hangry do
  context "bakersroyale.com german-chocolate-cakes" do
    before(:all) do
      @html = File.read("spec/fixtures/bakers_royale_german_chocolate_cakes.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://www.bakersroyale.com/german-chocolate-cakes/" }
    its(:image_url) { should == "http://www.bakersroyale.com/wp-content/uploads/2010/10/German-Chocolate-Cake-Bakers-Royale-copy1.jpg" }
    its(:ingredients) { should == ["2 ounces bittersweet or semisweet chocolate chopped", "2 ounces unsweetened chocolate, chopped", "6 tablespoons water", "8 ounces (2 sticks) unsalted butter, at room temperature", "1 ¼ cup + ¼ cup sugar", "4 large eggs, separated", "2 cups all-purpose flour", "1 teaspoon baking powder", "1 teaspoon baking soda", "½ teaspoon salt", "1 cup buttermilk, at room temperature", "1 teaspoon vanilla extract", "11/4 cup heavy cream", "1/2 cup of cream of coconut", "1 cup sugar", "3 large egg yolks", "3 ounces butter, cut into small pieces", "½ teaspoon salt", "1 1/2 cup pecans, toasted and finely chopped", "2 cups unsweetened coconut, toasted", "1 cup water", "1 cup sugar", "3 tablespoons dark rum", "4 oz. of semi-sweet chocolate, chopped", "4 tablespoon of unsalted butter", "1 tablespoon light corn syrup"]}
    its(:instructions) { should == "Melt both chocolates together with the 6 tablespoons of water. Use either a double-boiler or a microwave. Stir until smooth, then set aside until room temperature.\nIn the bowl of an electric mixer, or by hand, beat the butter and 1 ¼ cup of the sugar until light and fluffy, about 5 minutes. Beat in the melted chocolate, then the egg yolks, one at a time.\nSift together the flour, baking powder, baking soda, and salt.\nMix in half of the dry ingredients into the creamed butter mixture, then the buttermilk and the vanilla extract, then the rest of the dry ingredients.\nIn a separate metal or glass bowl, beat the egg whites until they hold soft, droopy peaks. Beat in the ¼ cup of sugar until stiff.\nFold about one-third of the egg whites into the cake batter to lighten it, then fold in the remaining egg whites just until there’s no trace of egg white visible.\nDivide the batter into the 2 prepared cake pans, smooth the tops, and bake for about 45 minutes, until a toothpick inserted into the center comes out clean.\nCool cake layers completely.\nAdd the cream, cream of coconut, sugar, and egg yolks in a medium saucepan.\nAdd the 3 ounces butter, salt, toasted coconut, and pecan pieces in a large bowl. Set aside.\nHeat the cream mixture and cook, stirring constantly making sure to scrape the bottom as you stir. Cook until the mixture begins to thicken and coats the spoon (an instant-read thermometer will read 170°.)\nPour the hot mixture immediately into the pecan-coconut mixture bowl and stir until the butter is melted. Cool completely to room temperature. Filling will thicken will upon standing.\nIn a small saucepan, heat the sugar and water until the sugar has melted. Remove from heat, let cool for 3 minutes and stir in the dark rum.\nCut mini cakes into 4 layers with each being an 1/8 inch thick. Brush each layer with syrup.\nSpead one teaspoon of filling on the top of bottom layer, place another layer and repeat filling layer until each layer is filled except the top.\nPour chocolate on top and place a few chopped pecan pieces to finish." }
    its(:name) { should == "German Chocolate Cake" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end