# encoding: UTF-8

describe Hangry do
  context 'damndelicious.net creamy-beef-and-shells' do
    before(:all) do
      @html = File.read('spec/fixtures/damn_delicious_creamy_beef_and_shells.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) {should == 'https://damndelicious.net/2018/09/16/creamy-beef-and-shells/' }
    its(:image_url) { should == 'https://s23209.pcdn.co/wp-content/uploads/2018/09/Creamy-Beef-and-ShellsIMG_6408.jpg' }
    its(:ingredients) { should == ['8 ounces medium pasta shells', '1 tablespoon olive oil', '1 pound ground beef', '1/2 medium sweet onion, diced', '2 cloves garlic, minced', '1 1/2 teaspoons Italian seasoning', '2 tablespoons all-purpose flour', '2 cups beef stock', '1 (15-ounce) can tomato sauce', '3/4 cup heavy cream', 'Kosher salt and freshly ground black pepper, to taste', '6 ounces shredded extra-sharp cheddar cheese, about 1 1/2 cups'] }
    its(:instructions) { should == "In a large pot of boiling salted water, cook pasta according to package instructions; drain well.\nHeat olive oil in a large skillet over medium high heat. Add ground beef and cook until browned, about 3-5 minutes, making sure to crumble the beef as it cooks; drain excess fat. Set aside.\nAdd onion, and cook, stirring frequently, until translucent, about 2-3 minutes. Stir in garlic and Italian seasoning until fragrant, about 1 minute.\nWhisk in flour until lightly browned, about 1 minute.\nGradually whisk in beef stock and tomato sauce. Bring to a boil; reduce heat and simmer, stirring occasionally, until reduced and slightly thickened, about 6-8 minutes.\nStir in pasta, beef and heavy cream until heated through, about 1-2 minutes; season with salt and pepper, to taste. Stir in cheese until melted, about 2 minutes.\nServe immediately." }
    its(:name) { should == 'Creamy Beef and Shells' }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
