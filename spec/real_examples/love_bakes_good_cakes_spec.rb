# encoding: UTF-8

describe Hangry do
  context "lovebakesgoodcakes.com homemade-alfredo-sauce" do
    before(:all) do
      @html = File.read("spec/fixtures/love_bakes_good_cakes_homemade_alfredo_sauce.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.lovebakesgoodcakes.com/homemade-alfredo-sauce/" }
    its(:image_url) { should == "https://www.lovebakesgoodcakes.com/wp-content/uploads/2016/11/HomemadeAlfredoSauce4a.png" }
    its(:ingredients) { should == ["¼ cup (4 tbsp. or ½ stick) butter", "1 cup heavy cream", "2 cloves garlic, minced", "1½ cup freshly grated Parmesan cheese"] }
    its(:instructions) { should == "Melt the butter in a saucepan over medium-low heat Add the heavy cream and allow to simmer for - minutes Add the garlic and Parmesan and whisk continuously until the cheese in completely melted\nUse in your favorite recipe or serve with cooked pasta of your choice"}
    its(:name) { should == "Homemade Alfredo Sauce" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

