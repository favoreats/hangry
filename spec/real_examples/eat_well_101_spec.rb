# encoding: UTF-8

describe Hangry do
  context 'eatwell101.com garlic-butter-steak-and-potatoes-recipe' do
    before(:all) do
      @html = File.read('spec/fixtures/eat_well_101_keto_berry_cobbler_recipe.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.eatwell101.com/keto-berry-cobbler-recipe' }
    its(:image_url) { should == 'https://www.eatwell101.com/wp-content/uploads/2018/05/gluten-free-cobbler-recipe.jpg' }
    its(:ingredients) { should == ['4 cups of fresh berries (we used strawberries and blueberries)', '1 egg, slightly beaten', '1 teaspoon vanilla extract', '1 1/2 stick butter, softened (or 1/2 cup coconut oil, melted and cooled)', '2 tablespoons keto-friendly sweetener of your choice (we used Swerve)', '1 cup coconut flour', '3/4 cup almond flour', '4 cups of fresh berries (we used strawberries and blueberries)', '1 egg, slightly beaten', '1 teaspoon vanilla extract', '1 1/2 stick butter, softened (or 1/2 cup coconut oil, melted and cooled)', '2 tablespoons keto-friendly sweetener of your choice (we used Swerve)', '1 cup coconut flour', '3/4 cup almond flour'] }
    its(:instructions) { should == "Preheat your oven to 375°F (190°C).\nIn a cast iron skillet, toss together strawberries and blueberries, 1 tablespoon sweetener, and a pinch of salt.\nIn a large bowl, combine beaten egg, softened butter, 1 tablespoon sweetener, and vanilla extract. Add in almond flour and coconut flour, mixing well to combine and form a dough.\nSpread the topping evenly on top of the berry mixture with your hand and press it down lightly.\nBake the cobbler for 20 – 25 minutes, until the edges are bubbling and the top of the crust is golden brown. Serve warm with whipped cream, enjoy!\nNotes:\nThis green beans and wax beans recipe is perfect for a Holiday dinner.\nContinue Reading →\nChoose from these fall casserole recipes to warm up your dinner.\nContinue Reading →\nThe perfect chicken soup for a busy day - and the Instant Pot makes it even simpler!\nContinue Reading →\nA decadent garlic parmesan mushroom cream sauce that will have you licking your plate clean!\nContinue Reading →\nTreat your guests with these incredibly easy and delicious sweet potato recipes!\nContinue Reading →\nHere is a selection of our favorite slow cooker meals, perfect for all skill levels!\nContinue Reading →\n© 2018 Eatwell101 - Home - About - FAQ - Contact - Press - Advertise - Legal - Privacy Policy\nKeto Berry Cobbler – The perfect summer dessert, with a Keto twist. This Keto cobbler is super easy to make and packed full of juicy, sweet strawberries and blueberries, topped with a crunchy cake-like gluten-free topping. Absolutely delicious – Enjoy!" }
    its(:name) { should == 'Berry Cobbler {Low-Carb, Keto}' }

    context "when the selector '//*[contains(text(),'Directions')]/following::p[./following::*[@class='brandon black bold']]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/eat_well_101_meal_prep_roasted_chicken_and_sweet_potato.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Preheat your oven to 425°F (210°C). In a salad bowl, toss chicken pieces with the spices and a quick dash of olive oil. Stir to combine; store in the fridge for about 15 to 30 minutes while you prepare the other ingredients.\nArrange diced sweet potatoes on a sheet pan. Drizzle with olive oil and sprinkle generously with salt and pepper. Arrange chicken pieces in a single layer on the same sheet pan, beside sweet potato.\nBake in the oven for 12-15 minutes. Remove chicken from the sheet pan and set aside. Stir sweet potatoes and roast them another 5 minutes or so, until cooked through. In the meantime finish cutting vegetables for the salsa.\nDivide roasted chicken and sweet potato into meal prep containers and top with the veggies salsa. Sprinkle with lemon juice and olive oil before serving.")
      end
    end

    context "when the selector '//*[contains(text(),'Cooking instructions') or contains(text(),'Instruction')  or contains(text(),'Method')]/following::p[1]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/eat_well_101_grilled_salmon_fillet_recipe.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Boil a pot of water and add the mint leaves and basil to infuse for 15 seconds. Remove and drain.\n2. Remove excess water on the leaves with a dish towel. Put the mint leaves and basil with olive oil in a blender and mix well. Marinate for 15 minutes.\n3. In a bowl, put the egg yolk, mustard and necessary seasoning. Whisk until you get a smooth sauce.\n4. Stir the mint and basil-flavored olive oil into this mixture gradually until the preparation gets thick and creamy, like a mayonnaise. Beat well with the lemon zest, lemon juice and cream. Place this sauce in the refrigerator.\n5. Brush salmon fillets with oil. Cook on the barbecue at medium heat for 4 minutes on each side.\n6. Serve warm with cream, basil and mint.\nThese Keto Crock-Pot recipes are packed with nutritive ingredients.\nContinue Reading →")
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
