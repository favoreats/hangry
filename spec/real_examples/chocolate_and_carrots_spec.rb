# encoding: UTF-8

describe Hangry do
  context "chocolateandcarrots.com cinnamon-ice-cream" do
    before(:all) do
      @html = File.read("spec/fixtures/chocolate_and_carrots.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://chocolateandcarrots.com/2011/09/cinnamon-ice-cream" }
    its(:image_url) { should == "http://chocolateandcarrots.com/wp-content/uploads/2011/09/Cinnamon-Ice-Cream-6677.jpg" }
    its(:ingredients) { should == ["1 cup heavy cream", "2 cups unsweetened almond milk", "1/2 cup honey", "1 tablespoon ground cinnamon", "1 teaspoon vanilla extract", "5 large egg yolks"] }
    its(:instructions) { should == "Prepare an ice bath and set aside.\nWhisk the egg yolks up in a small bowl and set aside.\nIn a medium sauce pan, whisk the heavy cream, almond milk, honey, cinnamon, and vanilla.\nWith the sauce pan attached to the candy thermometer over medium heat, bring the mixture up to 170F.\nWhile quickly whisking the egg yolks, slowly pour in 3/4-1 cup of the hot milk mixture.\nOnce combined, whisk the egg yolk mixture back into the hot milk mixture.\nBring the temperature up to 175F – 180F.\nPour the mixture into the ice bath and let it rest for 10 minutes.\nTake the bowl out of the ice bath and cover with plastic wrap.\nRefrigerate for about 3 hours, or until chilled.\nUsing your ice cream maker and the ice cream maker’s instructions, freeze the ice cream until it reaches a soft serve consistency.\nPlace the ice cream in a freezable container and place it in the freezer to harden.\nOnce hardened, scoop the ice cream out and enjoy!"}
    its(:name) { should == "Cinnamon Ice Cream" }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

