# encoding: UTF-8

describe Hangry do
  context 'eatliverun.com crock_pot_tex-mex-chicken-lettuce-wraps' do
    before(:all) do
      @html = File.read('spec/fixtures/eat_live_run_crock_pot_tex_mex_chicken_lettuce_wraps.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'http://www.eatliverun.com/crock-pot-tex-mex-chicken-lettuce-wraps/' }
    its(:image_url) { should == 'http://s6655.pcdn.co/wp-content/uploads/2012/08/lettuce-wraps-0397.jpg' }
    its(:ingredients) { should == ['1 1/2 lbs boneless, skinless chicken breast', '1 1/2 tbsp taco seasoning', '1 jalapeno, seeded and sliced', '1 16 oz jar of salsa', '1 yellow onion, diced', 'salt, as needed (I used 1 tsp)', 'iceberg lettuce leaves for wrapping'] }
    its(:instructions) { should == "Sprinkle seasoning all over chicken breasts, then place breasts at the bottom of the slow cooker.\nScatter onion and jalapeno over chicken breasts. Pour salsa over top. Place lid on slow cooker and cook on LOW for 4-5 hours.\nAfter 4-5 hours, remove chicken breasts from slow cooker and chop up. Place chopped chicken back in slow cooker and stir to combine. Season with salt to taste.\nTo serve lettuce wraps, spread one lettuce leaf out and scoop about 1/4th cup chicken mixture on top. Roll up lettuce leaf and enjoy!" }
    its(:name) { should == 'Crock Pot Tex-Mex Chicken Lettuce Wraps' }

    context 'when instant-pot-barbecue-chicken is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/eat_live_run_instant_pot_barbecue_chicken.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '.wprm-recipe-ingredients li' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['3.5 lbs bone-in chicken thighs and/or drumsticks', '18 oz barbecue sauce', '1 whole onion small diced', '1/2 cup water', '2 tbsp honey', '1 tsp salt', '1/4 tsp pepper'])
        end
      end

      context "and the selector '.wprm-recipe-instructions li' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Lay all your chicken pieces in your Instant Pot.\nCombine the barbecue sauce, onion, water, honey, salt and pepper in a bowl and whisk well.\nPour sauce over chicken.\nPlace Instant Pot lid on and set to either the poultry button or manual pressure for 15 minutes. Preheat broiler.\nWhen time is up, do a quick release. Remove chicken and place on a tin foil lined baking sheet. Pour sauce into a saucepot and place over medium high heat.\nPlace chicken under the broiler for 3 minutes until skin has crisped up a bit. Boil sauce for 5 minutes to thicken slightly. Pour sauce over the chicken and serve.")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Print this recipe!')]/following::p[./following::*[contains(text(),'Rub')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/eat_live_run_saucy_crock_pot_pulled_pork.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients" do
        expect(subject.ingredients).to eq(["2 lbs pork butt shoulder (ask your butcher if you don’t see it on the shelf)", "1/2 bottle Lawry’s Baja Chipotle Marinade", "1/2 bottle of your favorite barbecue sauce", "1/2 onion, chopped", "1 tsp salt", "1 tsp ground mustard", "1/4 tsp cayenne pepper"])
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
