# encoding: UTF-8

describe Hangry do
  context "recipesfilledwithlove.blogspot.com toffee-chocolate-bars-ingredients-base" do
    before(:all) do
      @html = File.read("spec/fixtures/recipes_filled_with_love.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://recipesfilledwithlove.blogspot.com/2014/03/toffee-chocolate-bars-ingredients-base.html" }
    its(:image_url) { should == "http://2.bp.blogspot.com/-X0Xxo3Qf_8E/UxSQKOIw7cI/AAAAAAAAAH0/I__94f5pbzg/s1600/Toffee+Chocolate+Bars.jpg" }
    its(:ingredients) { should == ["3/4 c margarine,softened", "3/4 c packed brown sugar", "1 1/2 c flour", "10 oz tin sweetened condensed milk", "2 tbsp margarine", "1 3/4 cups milk chocolate chips", "1 1/3 c toffee bits"] }
    its(:instructions) { should == "Base..\ncream together margarine,brown sugar and flour until well blended and mixture sticks together. Press into 9x13 pan. Bake at 350* for 20-25 min or until light golden. Cool while preparing filling.\nfilling...\nHeat sweetened condensed milk and margarine in heavy pan,stirring constantly over med heat for 5-10 min or until thickened. Spread over cooled base. Bake at 350* for 12-15 min or until golden. Sprinkle chocolate chips evenly over the top. Bake for 2 min longer or until chocolate is shiny and soft. Remove from oven. Spread chocolate evenly. Sprinkle toffee bits on top,pressing lightly into chocolate. Cool completely and cut into bars." }
    its(:name) { should == "Toffee Chocolate Bars" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end