#encoding: UTF-8

describe Hangry do
  context "thefirstyearblog.com homemade-nilla-wafers/" do
    before(:all) do
      @html = File.read("spec/fixtures/the_first_year_blog.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://thefirstyearblog.com/homemade-nilla-wafers/" }
    its(:image_url) { should == "https://thefirstyearblog.com/wp-content/uploads/2013/04/correct-nilla-wafers-768x10241-500x666.jpg" }
    its(:ingredients) { should == ["1/2 cup butter, softened", "1/2 cup sugar", "1/4 cup brown sugar", "1 large egg", "1 tsp vanilla extract", "1 1/3 cup all purpose flour", "1/2 tsp salt", "1/2 tsp baking powder"] }
    its(:instructions) { should == "Preheat the oven to 325F and line a baking sheet with parchment paper.\nIn a large bowl, cream together butter and sugars until fluffy. Beat in the egg and vanilla extract.\nNext, add in the flour, salt and baking powder and stir to combine.\nThe original recipe called for putting the dough in a pastry bag to pipe the dough. But I don’t have a pastry bag.. and the dough was too thick for a homemade ziploc pastry bag. So I ended up rolling small balls of dough, which worked great! You want the cookie to be pretty small, about the size of a dime.\nThe cookies will spread out, but only a little space is needed between them.\nBake for 7-10 minutes, or until cookies are a light golden brown. But the baking time will vary depending on the exact size of your cookies. Remember you want cracker like cookies!\nThis recipe makes a lot of little wafers, be forewarned! Also, if you are expecting these to taste like the boxed nilla wafers.. you might be well buy the pre-packaged ones (unless, you’re like me and got “the gluten” haha, as some people say). They are still delicious, but next time I will cut down the amount of sugar." }
    its(:name) { should == "Homemade Nilla Wafers" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
