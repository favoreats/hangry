# encoding: UTF-8

describe Hangry do
  context "the-girl-who-ate-everything.com.com lasagna-cupcakes" do
    before(:all) do
      @html = File.read("spec/fixtures/the_girl_who_ate_everything.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.the-girl-who-ate-everything.com/lasagna-cupcakes/" }
    its(:name) { should == "Lasagna Cupcakes" }
    its(:image_url) { should == "https://www.the-girl-who-ate-everything.com/wp-content/uploads/2012/10/lasagna-row.jpg" }
    its(:ingredients) { should == ["1/3 pound ground beef", "Salt and pepper", "24 wonton wrappers", "1 3/4 cups Parmesan cheese (grated)", "1 3/4 cups mozzarella cheese (shredded)", "1 cup pasta sauce", "Basil for garnish (optional)"]}
    its(:instructions) { should == "Preheat oven to 375ºF. Spray muffin tin with cooking spray.\nBrown beef in a skillet and season with salt and pepper. Drain.\nCut wonton wrappers into circle shapes, about 2 1/4-inches wide, using a biscuit cutter or the top of a drinking glass. You can cut several at a time. Tip: For a more rustic look, leave wonton wrappers uncut.\nReserve 3/4 cup Parmesan cheese and 3/4 cup mozzarella cheese for the top of your cupcakes. Start layering your lasagna cupcakes. Begin with a wonton wrapper and press it into the bottom of each muffin tin cup. Sprinkle a little Parmesan cheese, ricotta cheese, and mozzarella cheese in each. top with a little meat and pasta sauce.\nRepeat layers (wonton, Parmesan, ricotta, mozzarella, and pasta sauce). Top each cupcake with some of the reserved Parmesan and mozzarella cheeses.\nBake for 18-20 minutes or until edges are brown. Remove from oven and let cool for 5 minutes. To remove, use a knife to loosen the edges, then pop each lasagna cupcake out. Garnish with basil and serve." }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
