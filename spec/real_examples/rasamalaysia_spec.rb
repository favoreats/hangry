#encoding: UTF-8

describe Hangry do
  context "https://rasamalaysia.com chili-lime-chicken" do
    before(:all) do
      @html = File.read("spec/fixtures/rasamalaysia_chili_lime_chicken.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:image_url) { should == "https://rasamalaysia.com/wp-content/uploads/2015/05/chili-lime-chicken-thumb-253x253.jpg" }
    its(:ingredients) { should == ["2 – 2.5 lbs skin on chicken thighs", "1/2 cup fresh lime juice", "3 teaspoons fresh lime zest", "1/4 cup olive oil", "4 tablespoons fresh cilantro, chopped finely", "2 jalapeño, chopped finely", "4 garlic cloves, chopped finely", "1 tablespoon honey", "2 teaspoons salt", "1 teaspoon chili powder or to taste"] }
    its(:instructions) { should == "Rinse the chicken thighs, remove the bones, leave the skin on, and pat dry with paper towels. Set aside.\nGet a big bowl, mixing all the ingredients of the Marinade together using a whisk. Make sure the Marinade is well combined together.\nAdd chicken to the Marinade, make sure to stir and coat the chicken evenly. Marinate for 2 hours.\nFire up the grill, brush a little bit of oil on the surface. Add a little bit of the garlic, cilantro, and jalapeno from the Marinade on top of the chicken and grill them until they turn golden brown and charred on both sides. Serve immediately." }
    its(:name) { should == "Chili Lime Chicken" }

    context "when the selector '#Recipe p:nth-of-type(7)' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read("spec/fixtures/rasamalaysia_chicken_tikka_masala_recipe.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the ingredients"do
        expect(subject.ingredients).to eq(["l lb", "Tandoori Chicken", "4 tablespoons butter", "1 teaspoon finely minced ginger", "2 teaspoons finely minced garlic", "1/2 cup (120 ml) canned tomato paste", "2 tablespoons chili powder", "1/2 teaspoon", "Garam Masala", "2 tablespoons ketchup", "1/2 teaspoon sugar", "Salt, to taste", "1/2 cup (120 ml) water", "4 tablespoons heavy whipping cream/yogurt", "2 sprigs cilantro, leaves only, roughly chopped"])
      end
    end

    context "when the selector '//*[contains(text(),'Method')]/following-sibling::p[./following::*[contains(text(),'miss')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/rasamalaysia_parmesan_roasted_cauliflower.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions"do
        expect(subject.instructions).to eq("Preheat oven to 400°.\nToss the sliced cauliflower with the butter and olive oil. Season with salt and black pepper. Transfer the cauliflower to a baking sheet, in single layer, roast until almost tender about 20-30 minutes. Remove from the oven and sprinkle the grated Parmesan and chopped parsley leaves on top of the cauliflower, roast again until the cheese melts and slightly crusty, about 5 minutes. Remove from oven and serve immediately.")
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end