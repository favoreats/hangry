#encoding: UTF-8

describe Hangry do
  context "recipegirl.com amish pumpkin cinnamon rolls with caramel icing" do
    before(:all) do
      @html = File.read("spec/fixtures/recipe_girl.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == "Lori Lange" }
    its(:image_url) { should == "https://www.recipegirl.com/wp-content/uploads/2009/01/Amish-Pumpkin-Cinnamon-Rolls-200x150.jpg" }
    its(:ingredients) { should == ["ROLLS:", "1/3 cup milk", "2 Tablespoons butter", "1/2 cup unsweetened pumpkin puree", "2 Tablespoons granulated white sugar", "1/2 teaspoon salt", "1 large egg, beaten", "1 envelope active dry yeast", "1 cup unbleached all-purpose flour", "1 cup bread flour", "FILLING:", "1/3 cup brown sugar, packed", "1 teaspoon ground cinnamon", "2 Tablespoons butter, melted", "FROSTING:", "4 Tablespoons butter", "1/2 cup brown sugar, packed", "2 Tablespoons milk", "1/4 teaspoon vanilla extract", "1/8 teaspoon salt", "1/2 to 3/4 cup powdered sugar, sifted"] }
    its(:instructions) { should == "1. Prepare rolls: In a small saucepan, heat milk and 2 Tablespoons butter just until warm (120 - 130°) and butter is almost melted, stirring constantly.\n2. In large mixer bowl, combine pumpkin, sugar and salt. Add milk mixture and beat with an electric mixer until well mixed. Beat in egg and yeast. In a separate mixing bowl, combine flours. Add half of flour mixture to pumpkin mixture. Beat mixture on low speed for 5 minutes, scraping sides of bowl frequently. Add remaining flour and mix thoroughly (dough will be very soft). Turn into lightly greased bowl, then grease surface of dough lightly. Cover and let rise in warm place until doubled, about 1 hour.\n3. Punch dough down. Turn onto floured surface. Knead a few turns to form a smooth dough, sprinkling with enough additional flour to make dough easy to handle. On lightly floured surface, roll dough into 12x10-inch rectangle.\n4. In a small bowl, combine brown sugar and cinnamon. Brush surface of dough with melted butter. Sprinkle with brown sugar mixture. Beginning with long side of dough, roll up jelly-roll style. Pinch seam to seal. With a sharp serrated knife, gently cut roll into twelve 1-inch slices. Place rolls cut-side-up in greased 9-inch-square baking pan.\n5. Cover and let rise until nearly doubled, 30 to 45 minutes.\n6. Preheat oven to 350°F. Bake rolls about 20 minutes, or until golden. Remove from pan to waxed paper-lined wire rack. Cool 10 to 15 minutes.\n7. While rolls are cooling, prepare icing: In small saucepan, heat butter until melted. Stir in brown sugar and milk. Stir in brown sugar and milk. Cool over medium-low heat for 1 minute. Transfer to a small mixing bowl and cool mixture slightly. Stir in vanilla, salt and powdered sugar. Beat with an electric mixer until well blended. If necessary, add more powdered sugar for desired consistency.\n8. Drizzle icing over warm rolls." }
    its(:name) { should == "Amish Pumpkin Cinnamon Rolls with Caramel Icing" }
    its(:prep_time) { should == 120 }
    its(:cook_time) { should == 20 }
    its(:yield) { should == '12 rolls' }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when NoMethodError is raised in the parse_yield method" do
      before(:each) do
        @html = File.read("spec/fixtures/recipe_girl_amish_pumpkin_cinnamon_rolls.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed  }

      it "should set yields attribute to nil" do
        expect(subject.yield).to be_nil
      end
    end
  end
end