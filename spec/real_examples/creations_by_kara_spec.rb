# encoding: UTF-8

describe Hangry do
  context "creationsbykara.com cookies-cream-ice-cream" do
    before(:all) do
      @html = File.read("spec/fixtures/creations_by_kara_cookies_cream_ice_cream.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.creationsbykara.com/cookies-cream-ice-cream/" }
    its(:image_url) { should == "https://www.creationsbykara.com/wp-content/uploads/2009/02/Baking-supplies.jpg" }
    its(:ingredients) { should == ["1 cup milk", "4 egg yolks, well beaten", "1 cup sugar", "1/4 tsp salt", "4 tsp vanilla", "2 cups cream", "1 1/2 cups broken Oreos"] }
    its(:instructions) { should == "Combine milk, egg yolks, sugar, and salt in a saucepan Whisk together till blended well Heat over medium heat, stirring constantly, till mixture barely comes to a boil Remove from heat Cover and chill till cold (I usually put the pan in a sinkful of ice water to speed the process) Stir in vanilla and cream Freeze in ice cream freezer according to manufacture instructions Stir in Oreo pieces and place in an airtight container in the freezer to harden for - hours" }
    its(:name) { should == "Cookies and Cream Ice Cream" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end