# encoding: UTF-8

describe Hangry do
  context "cookinginstilettos.com pineapple-slaw" do
    before(:all) do
      @html = File.read("spec/fixtures/cooking_in_stilettos.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://cookinginstilettos.com/pineapple-slaw/" }
    its(:image_url) { should =="https://cookinginstilettos.com/wp-content/uploads/2016/06/Pineapple-Slaw-Ingredients.jpg" }
    its(:ingredients) { should == ["1 cup of pineapple chunks, drained and diced (reserve the pineapple juice)", "1 bag of coleslaw mix", "4 green onions, diced, reserving some for garnish.", "2/3 cup of Greek yogurt", "1 1 1/2″ piece of fresh ginger, grated – about 2 teaspoons or so", "3 tablespoons of apple cider vinegar", "1 tablespoon of honey", "2 tablespoons of reserved pineapple juice", "1/8 teaspoon of crushed red pepper flakes", "1/2 teaspoon of salt", "1/4 teaspoon of freshly cracked black pepper"]}
    its(:instructions) { should == "In a large bowl, toss together the diced pineapple, coleslaw mix and green onions.\nIn a small bowl, whisk together the Greek yogurt, grated fresh ginger, apple cider vinegar, honey, pineapple juice, red pepper flakes, salt and pepper. If the dressing still seems too thick, add another tablespoon of the reserved pineapple juice.\nAdd some of the dressing to the coleslaw mix and toss together. Add more dressing as needed.\nChill for at least 30 minutes before serving so that the flavors blend.\nEnjoy!" }
    its(:name) { should == "Pineapple Slaw" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end