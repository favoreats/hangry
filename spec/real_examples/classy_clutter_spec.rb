# encoding: UTF-8

describe Hangry do
  context 'classyclutter.net pina-colada-poke-cake' do
    before(:all) do
      @html = File.read('spec/fixtures/classy_clutter.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.classyclutter.net/pina-colada-poke-cake/' }
    its(:image_url) { should == 'https://bakedinaz.com/wp-content/uploads/2015/04/coconut-poke-cake-683x1024.jpg' }
    its(:ingredients) { should == ['1 (16.5 ounce) pineapple cake mix, made according to package directions', '1 (14 ounce) can sweetened condensed milk', '1 (15 ounce) can cream of coconut*', '1 (8 ounce) can crushed pineapple, optional', '1 (8 ounce) tub cool whip', '1 (7 ounce) bag sweetened coconut flakes'] }
    its(:instructions) { should == "Make the cake according to the package directions & bake in a 9×13″ cake pan. While the cake is baking, combine the sweetened condensed milk & cream of coconut in a bowl till combined.\nPoke holes all over the warm cake using the handle of a wooden spoon or a wood skewer. Then pour the condensed milk/cream of coconut mixture over the cake. Cover and place in the refrigerator. Allow the cake to cool completely (overnight is best).\nIf you are going to add the crushed pineapple, drain the can and spread the pineapple over the cake. Top with cool whip and then sprinkle with the coconut flakes when ready to serve.\nThis cake needs to be kept covered in the fridge.\nNOTES: *The can of cream of coconut is usually found in the liquor aisle of the store (there is no alcohol in it however). Do not use coconut milk or it won’t work. There are lots of variations you can do with this recipe. You can use a white or yellow cake mix instead of the pineapple flavored mix for just a coconut version. You can also make homemade sweetened whipped cream if you prefer that over cool whip." }
    its(:name) { should == 'Pina Colada Poke Cake' }
    its(:published_date) { should == 'April 15, 2015' }

    context 'when the-best-lemon-crinkle-cookies recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/classy_clutter_the_best_lemon_crinkle_cookies.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients')]/../following::div[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['1/2 cups butter, softened', '1 cup granulated sugar', '1/2 teaspoon vanilla extract', '1 egg', '1 teaspoon lemon zest', '2 Tablespoons fresh lemon juice', '1/4 teaspoon salt', '1/4 teaspoon baking powder', '1/8 teaspoon baking soda', '1 1/2 cups all-purpose flour', '1/2 cup powdered sugar (for rolling cookie dough balls)'])
        end
      end

      context "and the selector '//*[contains(text(), 'Directions')]/../following::div[./following::*[div//a//img]]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Preheat oven to 350 degrees. Grease light colored baking sheets with non stick spray and set aside OR line baking sheets with parchment paper.\nIn a large bowl, cream butter and sugar together until light and fluffy. Whip in vanilla, egg, lemon zest, and juice. Scrape sides and mix again. Stir in all dry ingredients slowly until just combined, excluding the powdered sugar. Scrape sides of bowl and mix again briefly. Pour powdered sugar onto a large plate. Roll a heaping teaspoon of dough into a ball and roll in powdered sugar. Place on baking sheet and repeat with remaining dough.\nBake for 9-11 minutes or until bottoms begin to barely brown and cookies look matte. Remove from oven and cool cookies about 3 minutes before transferring to cooling rack.\nThese are super quick and easy and DELICIOUS!")
        end
      end
    end

    context 'when sonic-ocean-water-copycat recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/classy_clutter_sonic_ocean_water_copycat_recipe.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients') or contains(text(),'INGREDIENTS')]/following::p[./following::*[contains(text(),'Instructions') or contains(text(),'Directions') or contains(text(),'DIRECTIONS')]]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['4 Tablespoons of regular white sugar', '4 Tablespoons of water', '24 oz of lemon-lime soda (2 – 12 oz cans works great!)', '1 1/2 teaspoons of coconut extract', '3 drops of blue food coloring'])
        end
      end

      context "and the selector '//*[contains(text(),'Directions')]/following::p[1]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("In a microwave safe bowl, mix white sugar and water and heat for 30 seconds in the microwave. This should dissolve most of the sugar. Add the sugar mixture to the lemon-lime soda, coconut extract and food coloring to a pitcher and mix gently.")
        end
      end
    end

    context "when the selector '//*[contains(text(),'Instructions:')]/following-sibling::p' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/classy_clutter_green_chili_chicken_pockets.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Preheat oven to 350 degrees F.\nMix chicken, green chilis and softened cream cheese thoroughly until it has a chicken salad like texture. I used my stand mixer but you can easily do this by hand in a large bowl.\nSpoon a 2-3 heaping tablespoons into an unrolled crescent roll. Roll chicken mixture into crescent roll.\nBake at 350*F for 10-12 minutes until the crescent roll is golden brown.\nYummy!! 15 minutes people!\nBe sure to check out even more recipes on the Cans get you Cooking website! One of my cooking idols, Kelsey Nixon (I’m even friends with her sister!) has created several amazing recipes on this site so you know they’ll be good!")
      end
    end

    context "when the selector '//*[contains(text(),'Instructions')]/following-sibling::ul' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/classy_clutter_sonic_cherry_limeade_copycat_recipe.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("In a large pitcher, combine Sprite, frozen limeade and the entire jar of maraschino cherries (including the syrup).\nMix slowly so your Sprite doesn’t go flat.\nPour over ice and add a wedge of lime (optional but awesome)\nEnjoy!")
      end
    end

    context "when the selector '//*[contains(text(),'DIRECTIONS')]/following::p[./following::*[contains(text(),'that!')]]' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/classy_clutter_easy_chicken_recipe_the_best_parmesan_c.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Mix mayonnaise, cheese and seasonings.\nSpread mixture over the chicken breast and place in a baking dish.\nBake at 375 degrees for 45 minutes.")
      end
    end

    context "when the selector '//*[contains(text(),'Directions')]/following::ol/li' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read('spec/fixtures/classy_clutter_pina_colada_poke_cake.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the instructions' do
        expect(subject.instructions).to eq("Make the cake according to the package directions & bake in a 9×13″ cake pan. While the cake is baking, combine the sweetened condensed milk & cream of coconut in a bowl till combined.\nPoke holes all over the warm cake using the handle of a wooden spoon or a wood skewer. Then pour the condensed milk/cream of coconut mixture over the cake. Cover and place in the refrigerator. Allow the cake to cool completely (overnight is best).\nIf you are going to add the crushed pineapple, drain the can and spread the pineapple over the cake. Top with cool whip and then sprinkle with the coconut flakes when ready to serve.\nThis cake needs to be kept covered in the fridge.\nNOTES: *The can of cream of coconut is usually found in the liquor aisle of the store (there is no alcohol in it however). Do not use coconut milk or it won’t work. There are lots of variations you can do with this recipe. You can use a white or yellow cake mix instead of the pineapple flavored mix for just a coconut version. You can also make homemade sweetened whipped cream if you prefer that over cool whip.")
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
