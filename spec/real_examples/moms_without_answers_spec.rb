# encoding: UTF-8

describe Hangry do
  context 'momswithoutanswers.com worlds-best-italian-wedding-soup' do
    before(:all) do
      @html = File.read('spec/fixtures/moms_without_answers_worlds_best_italian_wedding_soup.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.momswithoutanswers.com/worlds-best-italian-wedding-soup/' }
    its(:image_url) { should == 'https://www.momswithoutanswers.com/wp-content/uploads/2016/03/The-Worlds-Best-Italian-Wedding-Soup.jpg' }
    its(:ingredients) { should == ['1 small onion, grated', '⅓ cup chopped fresh Italian parsley', '1 large egg', '1 teaspoon minced garlic', '1 teaspoon salt', '1/4 cup Italian Bread Crumbs', '½ cup grated Parmesan cheese', '1/4 teaspoon red chili pepper flakes', '8 oz ground beef', '8 oz ground pork', '12 cups organic low sodium chicken broth', '1 box frozen chopped spinach', 'approx 6-8 oz of Acini de Pepe pasta', '2 tbsp shaved Parmesan cheese'] }
    its(:instructions) { should == "Meatballs: Stir the onion, parsley, egg, breadcrumbs, salt, garlic and chili pepper flakes in a bowl . Once mixed add the cheese, beef, and pork. Roll into meatballs and set aside. You can make the meatballs any size you would like but we prefer to make them about 2in each.\nSoup: In a large pot bring the chicken broth and frozen spinach to a boil . Add the uncooked meatballs and let simmer for about 10 minutes minute. Be sure to  stir occasionally to make sure the meatballs don’t stick. Next, add the pasta and continue and let come to a low boil. Let cook for approximately 15-20 minutes until the pasta and meatballs are cooked thoroughly. Season with salt and pepper if needed. Sprinkle with parmesan cheese shaving before serving. Enjoy!" }
    its(:name) { should == 'World’s Best Italian Wedding Soup' }

    context 'when best-summer-chopped-salad recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/moms_without_answers_best_summer_chopped_salad.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Here')]/following::p[1]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['2 cups chopped cucumber', '2 cups chopped roma tomatoes', '1/4 cup chopped sweet onion', '6 slices chopped bacon', '1/2 cup diced avocado', '1 can sweet corn drained', '2 tablespoons cilantro diced', 'Favorite Salad Dressing'])
        end
      end

      context "and the selector '//*[contains(text(),'Here')]/following::p[3]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq('In a large pot over medium/high heat cook your chopped bacon until crispy. Set aside and let cool. Chop and mix cucumbers, tomatoes, onion and cilantro a large mixing bowl. Rinse and drain the corn and add as well. Once your bacon is cool add it to the vegetable mixture along with the avocado.')
        end
      end
    end

    context 'when no-churn-funfetti-cake-batter-ice-cream recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/moms_without_answers_no_churn_funfetti_cake_batter_ice_cream.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients')]/following::p[./following::*[contains(text(),'Directions') or contains(text(),'Instructions')]]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['2 1/2 Cups Heavy Whipping Cream', '3/4 Cup of Sweetened Condensed Milk', '1 Tablespoon Vanilla Extract', '1 Cup Yellow Cake Mix (optional)', 'Rainbow Sprinkles'])
        end
      end

      context "and the selector '//*[contains(text(),'Instructions') or contains(text(),'Directions')]/following::p[1]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq('Using a mixer, whip the heavy cream until stiff peaks form. Fold in the sweetened condensed milk, cake mix and vanilla until well combined. Now add the sprinkles! We love sprinkles so we added about 1/4 cup. Add as much or as little as you would like. Transfer ice cream mixture to a freezer safe dish. Add more sprinkles on top! Cover with freezer wrap then a lid or foil. Place in the back of the freezer and leave for at least 6 hours. Try not to peek!')
        end
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients')]/following::div[./following::*[contains(text(),'Directions') or contains(text(),'Instructions')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/moms_without_answers_paleo_game_day_chili.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['2 lbs. of grass fed ground beef', '1 large zucchini chopped', '1 medium sized sweet onion chopped', '1 tablespoon of coconut oil', '1 large can of crushed tomatoes', '1 small can of tomato paste', '(when buying your canned tomatoes check the ingredients and make sure you are ONLY getting tomatoes and that there are not any “added” ingredients) 1 small can of water', '3-4 tablespoons of chili powder', '1 tablespoon of granulated garlic', '1-2 tablespoons of paprika', '1 1/2 tablespoons of cumin', 'Salt to Taste'])
      end
    end

    context "when the selector '//*[contains(text(),'Ingredients')]/following::p[./following-sibling::*[contains(text(),'Preheat')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/moms_without_answers_chicken_enchilada_dip.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['1 Rotisserie Chicken- deboned and shredded', '1 1/2 Bags Mexican Shredded Cheese', '1 small can diced green chilis', '1 8oz block of Cream Cheese', '1 8oz jar of Mayonnaise', '1 Tablespoon of diced pickled jalapeno (for added heat add 2 Tablespoons)'])
      end
    end

    context "when the selector '//*[contains(text(),'You will need')]/following::p[./following::*[contains(text(),'greased')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/moms_without_answers_crockpot_pumpkin_bread_pudding.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['1/8 tsp ground cloves', '1/4 tsp ground ginger', '1/2 tsp nutmeg', '1/2 tsp cinnamon', '1 tsp vanilla', '1 stick butter (melted)', '1/2 cup Imperial Brown Sugar (packed)', '1 cup half and half', '1 cup canned pumpkin', '4 eggs', '1/2 cup cinnamon chips', '1/2 cup pecans (optional)', '8 cups day old french bread cubes'])
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
