# encoding: UTF-8

describe Hangry do
  context "theseasonedmom.com moms-easy-marinated-flank-steak" do
    before(:all) do
      @html = File.read("spec/fixtures/the_seasoned_mom_moms_easy_marinated_flank_steak.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) {should == "https://www.theseasonedmom.com/moms-easy-marinated-flank-steak/" }
    its(:image_url) { should == "https://www.theseasonedmom.com/wp-content/uploads/2016/01/Moms-Easy-Marinated-Flank-Steak-1.jpg" }
    its(:ingredients) { should == ["1 cup soy sauce", "4 tablespoons seasoned rice vinegar (can substitute with balsamic vinegar)", "2 teaspoons dried ginger powder", "6 tablespoons honey", "2 teaspoons garlic powder", "1 cup canola oil", "Approximately 2 lbs. flank steak", "4 chopped green onions"] }
    its(:instructions) { should == "In a small bowl, whisk together all of the marinade ingredients until completely combined.\nPlace steak in a deep dish or in a large Ziploc bag. Pour marinade over steaks, add green onions, and cover (or seal bag) and refrigerate for at least 2 hours and up to overnight.\nWhen ready to cook, prepare your grill for high.\nRemove the steak from the marinade and place steak on the hot grill. Dispose of marinade. Grill for about 4-6 minutes per side, or until done to your liking (a meat thermometer should read 125 to 130°F for rare, 140°F for medium rare, and 150°F for medium).\nWhen the steak has cooked to your preferred level of doneness, remove from the grill and place on a cutting board. Cover with aluminum foil to hold in the heat while the steak rests for 10 to 15 minutes.\nCut the steak across the grain of the meat, at a steep diagonal, so that the slices are wide. A serrated knife works best for this.\nPreheat broiler. Remove steak from marinade and place on broiler pan. Broil 4-5 minutes per side. Remove to a cutting board to rest for about 10 minutes until slicing and serving." }
    its(:name) { should == "Mom's Easy Marinated Flank Steak" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end