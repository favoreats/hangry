# encoding: UTF-8

describe Hangry do
  context "chowhound.com chicken tortilla soup" do
    before(:all) do
      @html = File.read("spec/fixtures/chowhound_chicken_tortilla_soup.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == "Chicken Tortilla Soup" }
    its(:author) { should == 'Kevin Garvin and John Harrisson' }
    its(:yield) { should == '4 to 6 servings' }
    its(:total_time) { should == 120 }
    its(:canonical_url) { should == "https://www.chowhound.com/recipes/chicken-tortilla-soup-30472" }
    its(:description) { should == 'There are a lot of ingredients in this soup, but each one plays a part in its big flavor. To make it, first soften vegetables including onion, carrot, and bell pepper in a pot, then add dry spices to bring out their flavors. Add chicken broth, tomatoes, and tortillas, then let the mixture simmer until everything melds and the tortillas disintegrate to make a thick and hearty soup. Finally, throw in shredded chicken and garnish with fried tortilla strips, or if you want to skip frying the strips, just use regular crushed-up tortilla chips. Game plan: For a thicker soup, use 5 corn tortillas rather than 4. You can easily double this recipe to make a big pot of soup for a crowd. Looking to use up leftover roast chicken? Try out our rotisserie chicken noodle soup using specifically leftover. This dish was featured as part of our Cozy Chicken recipe gallery. We also recommend our easy ham soup.' }
    its(:image_url) { should == "https://www.chowstatic.com/assets/2014/09/30472_chicken_tortilla_soup_3000.jpg" }
    its(:ingredients) { should == ["2 tablespoons vegetable oil",
                                   "1/2 medium onion, medium dice (about 3/4 cup)",
                                   "1/2 small carrot, peeled and medium dice (about 1/4 cup)",
                                   "1/2 medium celery stalk, medium dice (about 1/4 cup)",
                                   "1/2 medium red bell pepper, medium dice (about 1/4 cup)",
                                   "1 medium garlic clove, minced",
                                   "1/2 teaspoon chili powder",
                                   "1 teaspoon ground coriander",
                                   "1 teaspoon ground cumin",
                                   "1/2 teaspoon dried oregano",
                                   "1/2 teaspoon paprika",
                                   "1/4 teaspoon cayenne pepper",
                                   "4 cups (1 quart) stock or low-sodium chicken broth",
                                   "1 cup water",
                                   "1/2 cup canned crushed tomatoes",
                                   "1 1/2 teaspoons kosher salt, plus more as needed",
                                   "1/2 teaspoon freshly ground black pepper, plus more as needed",
                                   "4 to 5 (6-inch) corn tortillas, cut into 1/2-inch pieces",
                                   "1 1/2 cups vegetable oil",
                                   "4 (6-inch) corn tortillas",
                                   "Kosher salt",
                                   "Freshly ground black pepper",
                                   "1 1/2 cups shredded, cooked chicken (about 8 ounces)",
                                   "1/2 cup heavy cream",
                                   "Kosher salt",
                                   "Freshly ground black pepper",
                                   "Shredded Monterey Jack cheese",
                                   "Thinly sliced scallions",
                                   "Sour cream"] }
    its(:instructions) { should == "Heat the oil in a large saucepan or Dutch oven over medium heat until shimmering. Add the onion, carrot, celery, and bell pepper and season with salt and pepper. Cook, stirring occasionally, until the vegetables have softened, about 6 minutes.\nAdd the garlic, chili powder, coriander, cumin, oregano, paprika, and cayenne and cook, stirring occasionally, until fragrant, about 2 minutes. Add the broth, water, tomatoes, and measured salt and pepper, stir to combine, and bring to a boil.\nReduce the heat to low, add the tortillas, and stir to combine. Simmer, stirring occasionally, until the tortillas have disintegrated and the soup has thickened, about 1 hour. Meanwhile, make the tortilla strips.\nHeat the oil in a large frying pan over medium-high heat until shimmering but not smoking (about 350°F on a deep-frying/candy thermometer), about 6 minutes.\nMeanwhile, stack the tortillas on a cutting board. Cut the stack in half, then cut crosswise into 1/4-inch strips; set aside. Line a baking sheet with paper towels; set aside.\nWhen the oil is ready, add half of the tortilla strips and fry, stirring occasionally, until golden brown and crisp, about 2 to 2 1/2 minutes. Remove with a slotted spoon to the prepared baking sheet and season with salt and pepper. Repeat with the remaining tortilla strips; set aside.\nWhen the soup is ready, add the chicken and cream and stir to combine. Simmer until the flavors meld, about 15 minutes. Taste and season with salt and pepper as needed.\nServe the soup topped with the tortilla strips, passing any desired garnishes on the side." }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
