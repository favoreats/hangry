# encoding: UTF-8

describe Hangry do
  context "ourbestbites.com Beef & Green Bean Stir-Fry" do
    before(:all) do
      @html = File.read("spec/fixtures/our_best_bites.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == "kate jones" }
    its(:image_url) { should == "https://www.ourbestbites.com/wp-content/uploads/2013/08/beefstirfrysquare1.jpg" }
    its(:ingredients) { should == ["1 1/2 pounds flank steak", "3 tablespoons cornstarch, divided", "1 pound green beans", "5-7 green onions (about 1 small bunch), chopped (reserve the green ends for a flavorful garnish)", "1 red, orange, or yellow bell pepper, sliced", "3 cloves garlic, minced", "1 teaspoon minced fresh ginger"] }
    its(:instructions) { should == "Whisk together the sauce ingredients and set aside. Set aside 2 tablespoons of cornstarch and 2 tablespoons of pear juice as well.\nSlice the steak into 1/4″ slices (or slightly thinner if you prefer). Sprinkle them lightly with 1 tablespoon of cornstarch and toss to coat the steak.\nIn a large skillet, heat 1-2 tablespoons oil (one with a high smoke point like grapeseed, peanut, or canola oil) over medium-high heat. Add the steak in a single layer that covers the whole pan and cook 3-5 minutes or until the steak is cooked through. Move the steak off to the side of the pan (if it’s very large) or transfer it to a plate while you cook the veggies so the meat doesn’t “boil.”\nAdd the green beans and cook for 2-3 minutes or until they start to become tender-crisp. Add the chopped white ends of the green onions and the bell pepper and cook 1-2 minutes more. Add the garlic and ginger and cook until fragrant. Add the steak back to the pan and cook until the steak is heated through.\nTurn the heat to high and add the sauce ingredients and bring to a boil. While the sauce is heating, whisk together the reserved cornstarch and pear juice. When the sauce is boiling, add the cornstarch mixture and cook about 1 minute or until the sauce is clear and thickened. Remove from heat and serve immediately over hot rice. Makes 6 servings."}
    its(:name) { should == "Beef & Green Bean Stir-Fry" }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when the selector '.recipe-instructions ol li' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/our_best_bites_cheesy_breakfast_enchiladas.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Over medium heat in a large skillet, crumble the chorizo and cook, stirring frequently, until cooked through. Using a slotted spoon, remove from the pan and drain on a paper towel, reserving the drippings. When drained, add to a medium mixing bowl.\nIn the same pan (and the same drippings), add the hash browns in a thin layer. Sprinkle with salt and pepper and cook for about 7 minutes or until brown and crispy, then flip and cook until the other side is brown and crispy. Remove from pan and add to the cooked sausage.\nReturn the pan to heat and melt the tablespoon of butter. Add the whisked eggs and scramble. When done, add to the eggs and potatoes and stir to combine.\nIn a small-ish mixing bowl, whisk together the cream of mushroom soup, sour cream, enchilada sauce, chilies, and green onions. Measure out 1 cup of the sauce and mix it with the egg mixture.\nPreheat your oven to 325.\nFill each tortilla with about 1 cup of the egg mixture and roll it securely. Place in a 9x13\" pan. Pour the remaining sauce over the enchiladas and sprinkle with cheese. Bake for about 30-35 minutes or until the cheese is melted and the sauce is bubbly. Remove from oven and allow to stand for about 5-10 minutes before serving. Makes 8 servings.")
      end
    end

    context "when the selector '//*[text()='Instructions:']/following-sibling::p' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/our_best_bites_guiltless_alfredo_sauce.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Place milk, cream cheese, flour, and salt in a blender and blend until smooth.  In a non-stick sauce pan, melt butter on med-high heat and add garlic. Let the garlic saute for about 30 seconds, you don’t want to burn it.\nThen add milk mixture to the pan. Stir constantly for about 3 or 4 minutes or until it just comes to a simmer. Keep stirring and let it cook for a few minutes more. It should be much thicker now. When it’s nice and thickened remove the pan from the heat. Add the cheese, stir it up and then cover immediately. Let stand for at least 10 minutes before using. It will continue to thicken upon standing. Season with additional salt if needed.  Also, if you have leftovers in the fridge, the sauce will thicken almost into a solid. Just re-heat and add a little milk and it will be back to normal again.")
      end
    end

    context "when the selector '//*[text()='Instructions:']/../following-sibling::p' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/our_best_bites_memphis_style_coleslaw.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Combine mayo, mustard, vinegar, sugar, salt, onion, and celery seed. In a large bowl, combine shredded cabbage, carrots, and green pepper.\nToss with dressing and refrigerate for at least 2-3 hours and up to about 3 days (or when it’s unjustifiably soggy).")
      end
    end

    context "when the selector '//*[text()='Instructions']/../following-sibling::p' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/our_best_bites_honey_butter_raspberries.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Dissolve the yeast and 1 tablespoon sugar in warm water. Let stand about 10 minutes or until very bubbly and frothy.\nIn the bowl of a stand or heavy-duty mixture, mix the milk, sugar, melted butter, vegetable oil and salt until completely dissolved. Add baking powder and 3 cups of flour to the milk mixture and beat on low for 30 seconds, scraping sides of bowl constantly. Add yeast mixture and beat on high for 3 minutes.\nAdd the eggs and mix until completely combined. then stir in as much remaining flour as needed to make a soft dough. This dough should be very soft–it will be coming away from the sides of the bowl, but it will still stick to your finger when you touch it. Place the bowl in a warm place and cover with a clean towel; allow to rise 1 hour or is doubled in bulk.\nWhen the dough has risen, lightly sprinkle a large, clean work surface with flour. Punch down the dough and then roll it to about 1/4″ thick. Using a sharp knife or a pizza wheel, cut the dough into equal rectangles. Separate the dough pieces so they have enough room to rise. Cover with a clean cloth.\nIn a large, heavy pan (I use a 7.5 quart Le Creuset), heat about 2-3 inches of peanut or other high smoke point oil over medium heat until it reaches 350-360 degrees (use a candy thermometer). When hot, add a few dough pieces and cook until golden brown on one side, then flip and cook the other side. Repeat with remaining dough pieces. Serve immediately smeared with honey butter with a handful of raspberries pressed into the honey butter. Makes about 24 scones.")
      end
    end

    context "when the selector '//*[text()='Instructions']' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/our_best_bites_coconut_soup.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Heat medium sized pot to medium-high heat on stove top.  Add olive oil, jalapeno, and ginger (if using fresh.)  Cook for 1-2 minutes, stirring frequently, until jalapeno and ginger are softened and fragrant.  If using ground ginger, add now and stir.  Add chicken broth, coconut milk, and salt and increase heat to bring mixture to a boil.  Reduce to simmer and add noodles (note you will discard seasoning packet that comes with noodles.)  Simmer 3-5 minutes, until noodles are softened.  If using mushrooms, add them in the final 2-3 minutes of cooking the noodles.  Add chicken and simmer for about 30 seconds to heat through.  Remove pot from heat and stir in lime juice, chicken, and cilantro.   Ladle into serving bowls and garnish with additional chopped cilantro, green onions, and lime wedges if desired.  Serve immediately.  Serves 4-6.")
      end
    end

    context "when the selector '//*[text()='Instructions:']/../following-sibling::div' is used in the parse_instructions method" do
      before(:each) do
        @html = File.read("spec/fixtures/our_best_bites_coconut_curry_sauce.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the instructions" do
        expect(subject.instructions).to eq("Cook pasta according to the package directions. In a small mixing bowl, whisk together the coconut milk, soy sauce, sweet chili sauce, curry paste, and lime juice. While the pasta is cooking, microwave the frozen vegetables according to the package directions, but only cook them for 1/2 the time–you still want them to be quite crunchy. If you’re using fresh veggies, blanch them in boiling water for 1-2 minutes or until they’re tender-crisp.\nIn a large non-stick skillet or wok, heat the oil over medium-high heat. Add the green onions, mushrooms, ginger, garlic and red peppers and cook until the mushrooms are tender and the garlic is fragrant, about 3 minutes. Be sure to continue stirring constantly to prevent the garlic from burning.\nAdd the the half-cooked (formerly frozen) vegetables. Stir fry for about 1 minute and then add the coconut milk mixture and cook until it begins to boil. Remove from heat and add the cooked noodles. Toss to combine and then allow the mixture to stand for 5 minutes. Toss again, season with salt and pepper to taste and serve with a sprinkling of sesame seeds and 2 lime wedges per person (squeeze the lime juice over the noodles before eating). Serves 8 very generously, so if you can find an extra use for the other half of that can of coconut milk, I say go for it…")
      end
    end
  end
end
