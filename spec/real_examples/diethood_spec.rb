# encoding: UTF-8

describe Hangry do
  context "diethood.com garlic-shrimp-ramen" do
    before(:all) do
      @html = File.read("spec/fixtures/diethood_garlic_shrimp_ramen.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://diethood.com/garlic-shrimp-ramen/" }
    its(:image_url) { should == "https://diethood.com/wp-content/uploads/2017/03/Garlic-Shrimp-Ramen-Recipe-1-e1525523021294.jpg" }
    its(:ingredients) { should == ["2 tablespoons extra virgin olive oil", "4 garlic cloves (, minced)", "1 pound shrimp (, peeled and deveined)", "salt and fresh ground pepper (, to taste)", "1 tablespoon butter", "2 tablespoons extra virgin olive oil", "1 large yellow onion (, sliced)", "2 cups broccoli florets", "salt and fresh ground pepper (, to taste)", "2 packages ((3 ounces each) ramen noodles, broken (discard flavor packet))", "2 cups low sodium vegetable broth", "chopped fresh parsley (, for garnish)"] }
    its(:instructions) { should == "Add olive oil and garlic in a large nonstick skillet over medium heat; cook for 1 minute or until fragrant.\nAdd shrimp and season with salt and pepper; cook for 2 minutes on each side.\nIncrease heat to medium-high and add butter; cook until butter is melted and shrimp is pink with browned edges.\nTransfer to a platter and set aside. DO NOT clean the skillet.\nReturn skillet to stove and add 2 tablespoons olive oil; cook over medium-high heat until hot.\nAdd onions and broccoli; season with salt and pepper and cook for 5 minutes, or until tender.\nAdd broken noodles and vegetable broth to the skillet; mix in previously prepared shrimp and bring mixture to a boil.\nCook and stir for about 5 minutes, or until noodles are tender and most of the liquid is absorbed.\nRemove from heat and let stand couple minutes.\nGarnish with parsley.\nServe." }
    its(:name) { should == "Garlic Shrimp Ramen" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end