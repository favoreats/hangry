# encoding: UTF-8

describe Hangry do
  context "ezrapoundcake.com Chicken Thighs with Mushroom Sauce" do
    before(:all) do
      @html = File.read("spec/fixtures/ezra_pound_cake.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://www.ezrapoundcake.com/archives/24684" }
    its(:image_url) { should == "http://www.ezrapoundcake.com/wp-content/uploads/2012/10/crusty-chicken-thighs-1-570x384.jpg" }
    its(:ingredients) { should == ["4 large chicken thighs (about 1 3/4 pounds total), skin on", "3/4 teaspoon salt", "3/4 teaspoon black pepper", "1 cup onion, diced", "1 1/2 tablespoons chopped garlic", "3 cups white mushrooms, washed and diced", "1/3 cup dry white wine or chicken broth", "1 tablespoon chives, chopped"] }
    its(:instructions) { should == "Set the chicken thighs skin-side down on a cutting board. Cut a 1/2-inch thick slash on either side of the thigh bone. Season the thighs on both sides with 1/2 teaspoon salt and 1/2 teaspoon pepper.\nPlace the chicken skin-side down in a cold non-stick skillet with a tight-fitting lid. (Cast iron works beautifully).\nTurn the heat to high. When the chicken starts to sizzle, turn heat to medium and move the thighs around to make sure they aren’t sticking to the skillet. Cover the skillet, and cook for about 20 minutes, until the internal temperature registers 165 degrees F. (Check occasionally to make sure the skin isn’t browning too much. If it’s cooking too fast, turn the heat to low.)\nMeanwhile, preheat the oven to 150 degrees F.\nWhen the chicken is done, transfer the thighs (skin-side up) to an ovenproof platter, and keep them warm in the oven.\nTo Make the Sauce: Pour off all but 2 tablespoons of chicken fat from the skillet. Add the onion, garlic, and mushrooms, and sauté them over high heat for about 3 minutes. Sprinkle the remaining salt and pepper over the mushroom. Then pour in the wine. Cook for 1 minute over high heat to reduce the sauce.\nTo serve, spoon some of the mushroom sauce onto four plates. Place a thigh in the middle of the sauce, spoon some sauce over, and sprinkle on the chives." }
    its(:name) { should == "Crusty Chicken Thighs with Mushroom Sauce" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end