# encoding: UTF-8

describe Hangry do
  context 'inspiredbycharm.com slow-cooker-thanksgiving-sangria' do
    before(:all) do
      @html = File.read('spec/fixtures/inspired_by_charm_slow_cooker_thanks_giving_sangria.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://inspiredbycharm.com/slow-cooker-thanksgiving-sangria/' }
    its(:image_url) { should == 'https://28wd582ik70pn4qof1ukh902-wpengine.netdna-ssl.com/wp-content/uploads/2017/11/Crock-Pot-Thanksgiving-Sangria-recipe.jpg' }
    its(:ingredients) { should == ["2 bottles Spanish Rioja (or another dry light-bodied red wine)", "1 cup brandy (I used apple brandy.)", "1 cup cranberry juice", "1 cup simple syrup*", "1 orange sliced", "1 cup fresh cranberries", "4-5 cinnamon sticks (plus more for garnish)", "1 teaspoon whole peppercorns (in a sachet)"] }
    its(:instructions) { should == "Add the wine, brandy, cranberry juice, simple syrup, orange slices, cranberries, cinnamon sticks, and whole peppercorns to your Crock-Pot® 4-Qt Digital Slow Cooker. Stir to combine.\nSet the temperature to high and cook the sangria for one hour. Then, set the temperature on low to keep it warm for up to 5 hours.\nServe in insulated mugs and garnish with a cinnamon stick." }
    its(:name) { should == 'Slow Cooker Thanksgiving Sangria' }

    context 'when buckeye-brownie-cookies recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/inspired_by_charm_buckeye_brownie_cookies.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(@class,'recipe-title')]/following::p[2]' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(["1 cup powdered sugar", "1 cup creamy peanut butter", "1 teaspoon vanilla", "1 box brownie mix", "1/4 cup butter, melted", "4 ounces cream cheese, softened", "1 egg", "4 ounces of chocolate (I prefer Ghiradelli's melting wafers.)"])
        end
      end

      context "and the selector '//*[contains(@class,'recipe-title')]/following::p[2]/following::p[./following::*[contains(@class,'print-link')]]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Begin by preheating your oven to 350 degrees F.\nThen, in a medium-sized bowl, mix together the peanut butter, powdered sugar, and vanilla until completely combined. Roll into roughly 24 1-inch-diameter balls. Set aside.\nIn a large bowl mix together the brownie mix, butter, cream cheese, and egg.\nUsing a cookie scoop, scoop the batter onto a cookie sheet lined with parchment paper. Smooth the edges if necessary to form a round cookie.\nBake the cookies for 15 - 20 minutes.\nAfter baking, immediately press one of the prepared peanut butter balls into the center of each cookie. Allow the cookies to cool on the pan for about 5 minutes. Then transfer them to a wire rack to finish cooling.\nOnce the cookies are cool, melt your chocolate in a double boiler (or in the microwave in a microwave safe bowl). Spoon melted chocolate over the top of each cookie. Allow the chocolate to set before serving.")
        end
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
