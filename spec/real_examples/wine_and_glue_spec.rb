# encoding: UTF-8

describe Hangry do
  context "wineandglue.com homemade-cookie-butter" do
    before(:all) do
      @html = File.read("spec/fixtures/wine_and_glue_homemade_cookie_butter.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) {should == "https://www.wineandglue.com/homemade-cookie-butter/" }
    its(:image_url) { should == "https://www.wineandglue.com/wp-content/uploads/2013/10/homemade_cookie_butter5.jpg" }
    its(:ingredients) { should == ["16 oz box ginger snaps (extra dry store bought variety)", "1/2 cup vegetable oil", "2 TBSPS sugar"] }
    its(:instructions) { should == "Grind the ginger snaps in a food processor until they are very fine\nAdd the vegetable oil slowly and let it fully combine with the crumbs, scraping down the sides of the food processor as needed\nAdd the sugar\nPut in a mason jar or other container and let sit for several hours (or over night) before using in a recipe" }
    its(:name) { should == "Homemade Cookie Butter" }

    context "when the selector '.entry-content img' is used in the parse_image_url method" do
      before(:each) do
        @html = File.read("spec/fixtures/wine_and_glue_banana_monkey_bread_with_nutella_ganache.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should be able to parse the image_url" do
        expect(subject.image_url).to eq("https://www.wineandglue.com/wp-content/uploads/blogger/-Ranr3IWYklc/UVOO9KZIkqI/AAAAAAAAI1g/N1gE1tyfK2Y/s640/banana_monkey_bread_nutella_topping.jpg")
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end