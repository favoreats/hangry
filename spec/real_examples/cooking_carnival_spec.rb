# encoding: UTF-8

describe Hangry do
  context 'cookingcarnival.com oven-roasted-veggie-enchilada' do
    before(:all) do
      @html = File.read('spec/fixtures/cooking_carnival_oven_roasted_veggie_enchilada.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.cookingcarnival.com/oven-roasted-veggie-enchilada/' }
    its(:image_url) { should == 'https://www.cookingcarnival.com/wp-content/uploads/2016/03/OvenRoastedVeggieEnchilada_main1.jpg' }
    its(:ingredients) { should == ['Chopped Onion into cubes 1/2 cup', 'Broccoli Florets 1/2 cup', 'Cauliflower Florets 1/2 cup', 'Chopped carrots into cubes 1/2 cup', 'Chopped zucchini into cubes 1/2 cup', 'Colorful bell peppers 1 1/2 cup (I love bell peppers)', 'Corn 1/2 cup (used frozen)', 'Asparagus chopped 1 cup', 'Extra virgin Olive oil 1/4 cup', 'Garlic salt 1 tsp (You can substitute salt and grated garlic as per your taste)', 'Black Pepper powder 1/2 tsp', 'Tortilla 5 to 6', 'Mexican Cheese or any cheese', 'Enchilada Sauce 8oz can or 2 cups'] }
    its(:instructions) { should == "Preheat oven @500 degree F.\nTake all the vegetables in a big mixing bowl. Add Olive oil, garlic salt and pepper powder. Mix them well.\nTake this mixture into a large baking sheet covered with parchment paper. Spread all the veggies evenly. Place in oven and roast for 20 minutes. Remove and set aside.\nReduce the temperature of oven from 500 degree F to 375 Degree F.\nTake tortilla on a working surface. Stuff it with spoon full of roasted veggie and some cheese. You can add chopped jalapenos too. Roll it tightly.\nIn a baking dish ladle a little sauce in the bottom.\nArrange the prepared rolls on a baking dish. Pour the enchilada sauce on top of it. Sprinkle Some cheese and red chili flakes.\nBake it in preheated oven at 375 degree F. for about 25 minutes.\nOven Roasted Veggie Enchilada Is ready. Enjoy it with Guacamole, Salsa, Sour cream and salad." }
    its(:name) { should == 'Oven Roasted Veggie Enchilada' }

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
