# encoding: UTF-8

describe Hangry do
  context "mytableforeight.blogspot.com soft-snickerdoodle-cookies" do
    before(:all) do
      @html = File.read("spec/fixtures/my_table_for_eight_soft_snickerdoodle_cookies.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://mytableforeight.blogspot.com/2013/05/soft-snickerdoodle-cookies.html" }
    its(:image_url) { should == "http://3.bp.blogspot.com/-Zh_PSY7RQcM/UY1a-K4kilI/AAAAAAAALgY/ZLxfKyQhjnM/s640/snickerdoodles.jpg" }
    its(:ingredients) { should == ["1/2 cup butter, softened", "1 cup sugar", "1/4 tsp baking soda", "1/4 tsp cream of tartar", "1 egg", "1/2 tsp vanilla extract", "1 1/2 cups flour", "2 tblsp sugar", "1 tsp ground cinnamon"] }
    its(:instructions) { should == "Beat butter on medium speed for 30 seconds. Add sugar, baking soda, and cream of tartar; beat together. Then beat in egg and vanilla extract. Add flour 1/2 cup at a time, beating in between. Dough is thick. My Kitchen Aid can handle it, but if you're using an electric mixer, beat as much flour as you can with mixer. Stir in remaining flour with a wooden spoon. Cover cookie dough and refrigerate for 1 hour. Combine 2 tblsp sugar and cinnamon in a small bowl; mix well. Shape dough into 1-inch balls; roll in cinnamon mixture to coat. Bake on ungreased cookie sheets at 375° for 8-10 minutes. Let cool for 10 minutes. Enjoy or store while warm in airtight container to keep softness." }
    its(:name) { should == "Soft Snickerdoodle Cookies" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end