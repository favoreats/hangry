# encoding: UTF-8

describe Hangry do
  context "overthebigmoon.com Mac & Cheese bites" do
    before(:all) do
      @html = File.read("spec/fixtures/over_the_big_moon.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) {should == "https://overthebigmoon.com/mac-cheese-bites/"}
    its(:author) { should == "Pam Dana"}
    its(:image_url) { should == "https://overthebigmoon.com/wp-content/uploads/2014/11/Mac-and-cheese-bites.jpg" }
    its(:ingredients) { should == ["1 and 1/2 cups Ritz Crackers, crushed", "2 cups grated Cheddar Jack", "4 tablespoons unsalted Butter, melted", "4 and 1/2 cups cooked Elbow Macaroni (about 8 ounces uncooked)", "Approx 5.5oz Garlic and Herb cheese", "2 tablespoons Unsalted Butter, cold", "2 Large Eggs", "1/2 cup Milk", "1/4 cup Sour Cream", "1/2 teaspoon Cayenne Pepper", "1/4 teaspoon Salt", "3 strips of Bacon, chopped (optional)", "Parsley, for garnish (optional)"] }
    its(:instructions) { should == "Start by preheating the oven to 350 degrees and prepare your mini cupcake pan by spraying it with cooking spray!\nEither by hand or using a food processor crush 1 1/2 cups (approx 1 1/2 – 2 sleeves) of Ritz Crackers. In a large bowl, combine the crushed Ritz crackers, 1 cup of the shredded Cheddar Jack Cheese, and the melted butter. Mix well until everything is combined!\nDivide the mixture through the mini cupcake pan, pressing the mixture firmly down into every spot to create the base of each bite!\nIn a new bowl, combine the freshly cooked (still hot) macaroni noodles with 1/2 cup of the shredded Cheddar Jack, the garlic and herb cheese, and the butter. Mix everything together!\nIn a third bowl smaller bowl, combine the eggs, milk, sour cream, cayenne pepper and salt. Mix with a whisk! Add the egg mixture to the cooked macaroni mixture, mixing everything until all the ingredients are melted!\nScoop enough of the macaroni mixture to have each muffin cup heaping full! Sprinkle each muffin cup with you extra Cheddar Jack and if you want the chopped Bacon!\nBake the Mac and Cheese Bites until lightly golden on top, about 15 minutes for the Mini Cups and more like 20 minutes for the regular Cups! Remove from oven and allow to cool for a few minutes, before removing from the pan! Serve immediately!"}
    its(:name) { should == "Mac & Cheese Bites" }
    its(:published_date) { should == "November 4, 2014" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end