# encoding: UTF-8

describe Hangry do
  context 'cookinglight.com cherry-gingerbread-muffins' do
    before(:all) do
      @html = File.read('spec/fixtures/cooking_light_cherry_gingerbread_muffins.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.cookinglight.com/recipes/cherry-gingerbread-muffins' }
    its(:image_url) { should == 'https://img1.cookinglight.timeinc.net/sites/default/files/styles/medium_2x/public/image/2017/01/main/cherry-gingerbread-muffins.jpg?itok=wpzGOkhg' }
    its(:ingredients) { should == ['6 ounces whole-wheat flour (about 1 1/2 cups)', '1/4 cup wheat bran', '1 teaspoon baking powder', '1 teaspoon baking soda', '1/2 teaspoon kosher salt', '2 tablespoons unsalted butter', '1 cup plain low-fat yogurt', '1/2 cup packed light brown sugar', '1 teaspoon vanilla extract', '1 medium ripe banana, mashed (about 1/2 cup)', '1 large egg', '1/2 cup chopped dried cherries', '1/3 cup molasses', '1 teaspoon ground cinnamon', '1 teaspoon ground ginger', '1/4 teaspoon ground allspice', 'Cooking spray'] }
    its(:instructions) { should == "Preheat oven to 375 degrees F\nWeigh or lightly spoon flour into dry measuring cups; level with a knife. Combine flour, wheat bran, and next 3 ingredients (through salt) in a large bowl, stirring with a whisk.\nMelt butter in a small skillet over medium; cook 90 seconds or until browned and fragrant, swirling pan frequently. Combine butter, yogurt, sugar, vanilla, banana, and egg in a bowl. Add yogurt mixture to flour mixture, stirring just until combined. Add cherries, molasses, cinnamon, ginger, and allspice to muffin batter. Divide batter evenly among 12 muffin cups coated with cooking spray. Bake at 375 degrees F for 22 minutes or until a wooden pick inserted in center comes out clean. Cool in pan 5 minutes. Remove from pan; cool completely on a wire rack." }
    its(:name) { should == 'Cherry-Gingerbread Muffins' }

    context 'when light-grilled-cheese recipe is parsed' do
      before(:each) do
        @html = File.read('spec/fixtures/cooking_light_light_grilled_cheese.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Ingredients')]/following::ul[1]/li' is used in the parse_ingredients method" do
        it 'should be able to parse the ingredients' do
          expect(subject.ingredients).to eq(['½ ounce 1/3-less-fat cream cheese', '1 teaspoon canola mayo', '1 ounce 2% reduced-fat shredded cheddar cheese', '2 (1-ounce) slices whole-grain bread', '¼ teaspoon olive oil'])
        end
      end

      context "and the selector '//*[contains(text(),'How to Make It')]/following::p[./following::*[contains(text(),'Serving')]]' is used in the parse_instructions method" do
        it 'should be able to parse the instructions' do
          expect(subject.instructions).to eq("Combine cream cheese and canola mayo in a small bowl. Add cheddar cheese.\nSpread cheese mixture between whole-grain bread slices.\nHeat a small skillet over medium heat; pour in olive oil and sear each side until bread is browned and crisp.")
        end
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
