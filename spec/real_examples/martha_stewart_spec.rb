# encoding: UTF-8

describe Hangry do
  context "marthastewart.com egg-sandwich" do
    before(:all) do
      @html = File.read("spec/fixtures/martha_stewart_egg_sandwich.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.marthastewart.com/1090477/egg-sandwich" }
    its(:image_url) { should == "https://assets.marthastewart.com/styles/wmax-750/d24/egg-sandwich-194-d111289/egg-sandwich-194-d111289_horiz.jpg?itok=3OdpC6ch" }
    its(:ingredients) { should == ["1 ounce fontina fontal cheese", "2 slices extra-smoky, extra-thick cut bacon", "1 English muffin, split", "1 large egg", "Coarse salt and freshly ground pepper"]}
    its(:instructions) { should == "Place cheese in freezer until well chilled, about 10 minutes. Meanwhile, place bacon in a large skillet over medium-high heat; cook until crisp. Transfer to a plate lined with paper towels to drain.\nRemove cheese from freezer, cut two slices, and grate remaining cheese on the largest holes of a box grater; set aside.\nPlace English muffin split-side down in skillet; cook until toasted. Turn and top both sides with sliced cheese. Reduce heat and cook until cheese is melted. Remove from heat and set aside. Top one half with with bacon.\nCrack egg into bacon fat and season with salt and pepper. Fry egg until the white is set; turn and top egg with grated cheese. Continue cooking until yolk has reached desired degree of doneness.\nCarefully place egg on top of bacon. Sandwich with remaining muffin half; serve immediately." }
    its(:name) { should == "Egg Sandwich" }

    context "when marshmallow-snowflakes recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/martha_stewart_marshmallow_snowflakes_how_to.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '.component-name p' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["Rimmed baking sheet", "Offset spatula", "Snowflake cookie cutters"])
        end
      end

      context "and the selector '.directions-item-text p' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Make a batch of Marshmallow Snowflakes.\nSpread the marshmallow mixture on a rimmed baking sheet with an offset spatula.\nOnce the mixture is firm, use cookie cutters coated with nonstick cooking spray to punch out the flakes.")
        end
      end
    end

    context "when doily_snowflake_decorations recipe is parsed " do
      before(:each) do
        @html = File.read("spec/fixtures/martha_stewart_doily_snowflake_decorations.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '.component-name' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["Doilies", "Foambrush or roller", "Fabric stiffener", "Iron", "Thread or monofilament"])
        end
      end

      context "and the selector '.top-intro-image-play img' is used in the parse_image_url method" do
        it "should be able to parse the image_url" do
          expect(subject.image_url).to eq("https://www.marthastewart.com/sites/all/modules/features/mslo_image/images/lazy-default.gif")
        end
      end
    end

    context "when perfect-macaroni-and-cheese recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/martha_stewart_perfect_macaroni_and_cheese.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[contains(text(),'Serves')]/following::p[1]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["6 slices good-quality white bread, crusts removed, torn into 1/4- to 1/2-inch pieces", "8 tablespoons (1 stick) unsalted butter, plus more for dish", "5 1/2 cups milk", "1/2 cup all-purpose flour", "2 teaspoons kosher salt", "1/4 teaspoon freshly grated nutmeg", "1/4 teaspoon freshly ground black pepper", "1/4 teaspoon cayenne pepper", "4 1/2 cups (about 18 ounces) grated sharp white cheddar", "2 cups (about 8 ounces) grated Gruyere or 1 1/4 cups (about 5 ounces) grated pecorino Romano", "1 pound elbow macaroni"])
        end
      end

      context "and the selector '//*[contains(text(),'Serves')]/following::p[1]/following::p[./following-sibling::*[contains(text(),'Little')]]' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Heat the oven to 375 degrees. Butter a 3-quart casserole dish; set aside. Place bread pieces in a medium bowl. In a small saucepan over medium heat, melt 2 tablespoons butter. Pour butter into the bowl with bread, and toss. Set the breadcrumbs aside. In a medium saucepan set over medium heat, heat milk. Melt remaining 6 tablespoons butter in a high-sided skillet over medium heat. When butter bubbles, add flour. Cook, stirring, 1 minute.\nSlowly pour hot milk into flour-butter mixture while whisking. Continue cooking, whisking constantly, until the mixture bubbles and becomes thick.\nRemove the pan from the heat. Stir in salt, nutmeg, black pepper, cayenne pepper, 3 cups cheddar, and 1 1/2 cups Gruyere or 1 cup pecorino Romano. Set cheese sauce aside.\nFill a large saucepan with water. Bring to a boil. Add macaroni; cook 2 to 3 fewer minutes than manufacturer's directions, until outside of pasta is cooked and inside is underdone. (Different brands of macaroni cook at different rates; be sure to read the instructions.) Transfer the macaroni to a colander, rinse under cold running water, and drain well. Stir macaroni into the reserved cheese sauce.\nPour the mixture into the prepared casserole dish. Sprinkle remaining 1 1/2 cups cheddar and 1/2 cup Gruyere or 1/4 cup pecorino Romano; scatter breadcrumbs over the top. Bake until browned on top, about 30 minutes. Transfer dish to a wire rack to cool for 5 minutes; serve.")
        end
      end

      context "and the selector '.teaser-image img' is used in the parse_image_url method" do
        it "should be able to parse the image_url" do
          expect(subject.image_url).to eq("https://assets.marthastewart.com/styles/img_240x300/d16/ft066_maccheese4/ft066_maccheese4_xl.jpg?itok=0UUdhjTd")
        end
      end
    end

    context "when funny-face-pumpkins recipe is parsed" do
      before(:each) do
        @html = File.read("spec/fixtures/martha_stewart_funny_face_pumpkins.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      context "and the selector '//*[@class='article-content']/h3/following::p[1]' is used in the parse_ingredients method" do
        it "should be able to parse the ingredients" do
          expect(subject.ingredients).to eq(["Pumpkin", "Keyhole saw", "Plaster scraper or fleshing tool", "Masking tape", "Sharp awl, needle tool, or T pin", "Miniature saw", "Linoleum cutter", "Petroleum jelly"])
        end
      end

      context "and the selector '//*[@class='article-content']/ol/li' is used in the parse_instructions method" do
        it "should be able to parse the instructions" do
          expect(subject.instructions).to eq("Hollow out pumpkin: Using keyhole saw, cut a hole in pumpkin. If using a candle for illumination, cut hole in the top (always put candle in a high-sided glass, and never leave it unattended). If using electric light, make hole in bottom or side so you can hide the cord. Using scraper or fleshing tool, remove flesh, pulp, and seeds.\nPrint templates on 8 1/2-by-11-inch white paper. Cut out templates, and tape to pumpkin.\nTrace design onto pumpkin by poking holes with awl, needle tool, or T pin.\nRemove templates, and carve along pattern. Use miniature saw to cut all the way through and linoleum cutter to carve details into just the surface of pumpkin (we generally use #2 and #3 blades for finer details and #5 blades to remove larger sections of skin). Apply petroleum jelly to exposed areas of pumpkin's flesh to prevent them from turning brown.")
        end
      end
    end

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end