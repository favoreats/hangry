# encoding: UTF-8

describe Hangry do
  context "highheelsandgrills.com breakfast-corn-dogs" do
    before(:all) do
      @html = File.read("spec/fixtures/high_heels_and_grills_breakfast_corn_dogs.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) {should == "https://www.highheelsandgrills.com/breakfast-corn-dogs/" }
    its(:image_url) { should == "https://www.highheelsandgrills.com/wp-content/uploads/blogger/-gL36NUpBC54/UVmekKkr5GI/AAAAAAAADBM/QJd4TMTGP2U/s640/Breakfast%2BCorn%2BDogs.png" }
    its(:ingredients) { should == ["1/2 cup corn meal", "1 1/2 cups flour", "1 teaspoon sugar", "1/2 teaspoon baking powder", "1/4 teaspoon baking soda", "1 cup buttermilk", "1/4 cup corn starch", "8 cooked sausage links, cut in half (makes 16 pieces)", "1 container cooking oil (I used 1.5 qts of vegetable oil)", "Syrup", "2-3 shish kabob skewers"]}
    its(:instructions) { should == "Combine dry ingredients (minus corn starch), and whisk in your buttermilk. Mix well.\nHeat up your oil in a deep pan (you want a good 3-4 inches of oil, so I used a deeper, smaller-diameter pot)\nIf your sausage links are dry, wet them a little with water. Dredge them in the corn starch and remove excess.\nStab 2-3 sausageletts with your skewers (as many as you can hold in the hot oil at one time – I did 2). Then, dip them in the batter, fully coating each one.\nQuickly submerge each pancake-battered-dog in the hot oil for about 1-2 minutes or until golden brown. Repeat until each piglet-in-a-blanket is cooked. Eat with syrup (that’s NOT optional…). That’s it!" }
    its(:name) { should == "Breakfast Corn Dogs" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

