# encoding: UTF-8

describe Hangry do
  context "grumpyshoneybunch.com Honey Sesame Chicken" do
    before(:all) do
      @html = File.read("spec/fixtures/grumpys_honey_bunch.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.grumpyshoneybunch.com/2010/04/are-you-seeing-pattern-here-lately.html" }
    its(:image_url) { should == "https://www.grumpyshoneybunch.com/wp-content/uploads/2010/04/Sesame001-11.jpg" }
    its(:ingredients) { should == ["12 oz. chicken breast, cut into bite sized pieces", "1 tablespoons light soy sauce", "1 tablespoon cooking wine", "1/2 tablespoon sesame oil", "1 teaspoon sugar", "1 tablespoons flour", "1 tablespoon cornstarch", "1/2 teaspoon salt", "1/4 teaspoon pepper", "Sauce:", "t tablespoon sweet chili sauce", "2 tablespoons tomato ketchup", "1-2 tablespoons honey", "1 tablespoon oyster sauce", "2 tablespoon light soy sauce", "1/4 cup water", "1 tablespoon toasted sesame seeds"] }
    its(:instructions) { should == "Marinate chicken in soy sauce, wine, sesame oil and sugar for about an hour. Mix flour and cornstarch and place in a large ziploc bag. Drain chicken and add to flour mixture. Shake gently to coat. Fry in hot oil (350 degrees) until golden brown. Drain on paper towels.\nTo make sauce, mix chili sauce, ketchup, honey, oyster sauce, soy, sauce and water in a medium sauce pan. Bring to a boil then simmer until sauce begins to thicken. Toss sauce with chicken pieces, sprinkle with sesame seeds and serve."}
    its(:name) { should == "Honey Sesame Chicken" }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

