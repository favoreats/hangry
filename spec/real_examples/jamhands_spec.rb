# encoding: UTF-8

describe Hangry do
  context "jamhands.net chicken-cordon-bleu-crescents" do
    before(:all) do
      @html = File.read("spec/fixtures/jamhands.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "https://www.jamhands.net/2013/07/chicken-cordon-bleu-crescents.html" }
    its(:image_url) { should == "//4.bp.blogspot.com/-R1sMK-fTgQw/UuvDFUpx-4I/AAAAAAAAGgc/57cVs7gPHrk/s1600/chicken+cordon+bleu+1+final.JPG" }
    its(:ingredients) { should == ["1 can crescent rolls", "4 tsp honey mustard dressing", "4 slices baby swiss cheese", "8 slices deli ham", "8 fully cooked, frozen breaded chicken fingers "] }
    its(:instructions) { should == "If not already done, bake the chicken fingers/strips and set aside. Once that is done, Preheat oven to 375-f.\nSeparate crescent rolls into 8 triangles. Spread 1/2 tsp of honey mustard on each crescent. Place one half slice of baby swiss cheese on top of crescent roll. Place 1 slice of ham on top of cheese and top with chicken finger. Roll up crescent and place on cookie sheet. Repeat with remaining chicken fingers and crescents.\nBake for 18-20 minutes or until golden brown."}
    its(:name) { should == "Chicken Cordon Bleu Crescents" }


    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end

