# encoding: UTF-8

describe Hangry do
  context "cleaneatsandtreats.com instant pot tasty turkey chili" do
    before(:all) do
      @html = File.read("spec/fixtures/clean_eats_and_treats_turkey_chili.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == "Instant Pot Tasty Turkey Chili" }
    its(:yield) { should == '6-8 servings' }
    its(:image_url) { should == "https://www.cleaneatsandtreats.com/wp-content/uploads/2017/09/Mobile-Image.png" }
    its(:ingredients) { should == ["2 tbsp olive oil",
                                   "1 lb Simple Truth Organics natural ground turkey",
                                   '1 package clean taco seasoning (or 3 tbsp <a href="https://cleaneatsandtreats.com/yummythings/clean-taco-seasoning/">homemade</a>)',
                                   "1 tsp garlic powder",
                                   "1 tsp coriander",
                                   "1 tsp smoked paprika",
                                   "1 tsp dried oregano",
                                   "1 tsp salt",
                                   "1/4-1 tsp red pepper flakes (1/4 tsp is mild)",
                                   "2 tbsp tomato paste",
                                   "1 can (14.5 ounces) beef broth",
                                   "1 jar (16 ounces) salsa",
                                   "1 (14.5 ounces) crushed fire roasted tomatoes",
                                   "1 can (7 ounces) mild green chile peppers, chopped",
                                   "1 medium onion, finely diced",
                                   "1 green bell pepper, diced",
                                   "1 can simple truth organic kidney beans, drained",
                                   "1 can simple truth organics black beans, drained",
                                   "2 medium zucchini, shredded",
                                   "1 cup sour cream",
                                   "1 cup cheddar cheese, shredded",
                                   "1 avocado, sliced",
                                   "fresh limes, sliced",
                                   "cilantro (optional)"] }
    its(:instructions) { should == "Select s<em>auté</em> on your Instant Pot. Once the display shows <em>HOT</em> add the oil, onions and peppers. Sauté until onions are translucent.\nAdd ground turkey and seasonings. Brown turkey by stirring often to break up chunks.\nAdd beef broth, green chiles, salsa, tomato paste and crushed tomatoes, in that order and give it a good stir. DO NOT add the beans or zucchini.\nClose the lid and turn valve to sealing. Cook on m<em>anual</em> for 10 minutes, allow for a natural release.\nOnce ready remove the lid and add the beans and zucchini. Stir.\nClose the lid and turn valve to sealing. Cook on m<em>anual</em> for an additional 5 minutes, manually release pressure and allow to simmer on warm for 10-15 minutes. I like to use my glass lid for this.\nAdjust seasonings if needed and serve with cheese, sour cream, avocado and cilantro. Enjoy\nSauté onions and peppers until onions are translucent, add turkey. Brown until no longer pink..\nAdd all remaining ingredients, stir and cook on low for 4-6 hours." }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when NoMethodError is raised in the parse_name method" do
      before(:each) do
        @html = File.read("spec/fixtures/clean_eats_and_treats.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should set name attribute to nil" do
        expect(subject.name).to be_nil
      end
    end
  end
end
