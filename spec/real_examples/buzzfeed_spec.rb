# encoding: UTF-8

describe Hangry do
  context "Buzzfeed.com Roasted Brussels Sprout" do
    before(:all) do
      @html = File.read("spec/fixtures/buzzfeed_roasted_brussels_sprout.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == "Garlic Roasted Brussel Sprouts" }
    its(:image_url) { should == "https://img.buzzfeed.com/buzzfeed-static/static/2016-10/3/17/asset/buzzfeed-prod-fastlane02/sub-buzz-9118-1475529227-4.jpg" }
    its(:ingredients) { should == ["1 pound brussels sprouts, halved",
                                   "4 tablespoons olive oil",
                                   "½ teaspoon salt",
                                   "1 teaspoon pepper",
                                   "1 teaspoon garlic powder",
                                   "¼ cup bread crumbs",
                                   "¼ cup parmesan, grated"] }
    its(:instructions) { should == "Preheat oven to 400˚F/200˚C.\nPlace brussels sprouts in a large bowl. Drizzle olive oil and toss to coat. Sprinkle the rest of the ingredients and toss again.\nSpread the sprouts on a baking sheet. Bake for 20 minutes, then flip the sprouts, then bake for an additional 20 minutes or until the sprouts are fork-tender and golden.\nEnjoy!" }
    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
