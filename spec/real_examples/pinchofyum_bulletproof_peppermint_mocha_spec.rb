# encoding: UTF-8

describe Hangry do
  context 'pinchofyum.com bulletproof peppermint recipe' do
    before(:all) do
      @html = File.read('spec/fixtures/pinchofyum_bulletproof_peppermint_mocha.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:author) { should == 'Lindsay' }
    its(:canonical_url) { should == 'https://pinchofyum.com/bulletproof-peppermint-mocha' }
    its(:description) { should == 'Power Peppermint Mocha! the unicorn of all holiday drinks. with 22 grams of collagen protein, which is good for joints and muscles, but also skin, nails, and hair. SO yummy!' }
    its(:image_url) { should == 'https://pinchofyum.com/wp-content/uploads/Bulletproof-Mocha-1.jpg' }
    its(:ingredients) {
      should == [
          'about a cup of strongly brewed coffee',
          '1/2 cup milk of choice (I recommend light coconut milk)',
          '2 scoops (or 24g) protein powder (I used Bulletproof collagen protein)',
          '2 tablespoons cocoa powder',
          '1 tablespoon agave',
          '1/4 teaspoon peppermint extract',
          'pinch of salt'
      ]
    }
    its(:name) { should == 'Bulletproof Peppermint Mocha' }
    its(:nutrition) do
      should == {
          calories: '205',
          cholesterol: '0 mg',
          fiber: '3.3 g',
          protein: '25 g',
          saturated_fat: '0.8 g',
          sodium: '180.5 mg',
          sugar: '14.4 g',
          total_carbohydrates: '23.8 g',
          total_fat: '2.9 g',
          trans_fat: '0 g',
          unsaturated_fat: nil
      }
    end
    its(:instructions) { should == 'Blend everything together until smooth and foamy. Pour into a mug and serve! Yummy!' }
    its(:prep_time) { should == 0 }
    its(:cook_time) { should == 5 }
    its(:total_time) { should == 5 }
    its(:published_date) { should == Date.parse('2017-11-27T04:00:00+00:00') }
    its(:yield) { should == '1' }
  end
end
