#encoding: UTF-8

describe Hangry do
  context "aneverydayblessing.com ranch-chicken-with-potatoes-green-beans" do
    before(:all) do
      @html = File.read("spec/fixtures/an_everyday_blessing_ranch_chicken_with_potatoes_green_beans.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == "http://aneverydayblessing.com/2014/03/30/ranch-chicken-with-potatoes-green-beans/" }
    its(:image_url) { should == "http://aneverydayblessing.com/wp-content/uploads/2014/03/Ranch-Chicken-576x1024.jpg" }
    its(:ingredients) { should == ["2 lbs boneless skinless chicken breasts", "3 tblsp Ranch Seasoning mix (you may buy a seasoning packet or see below for recipe)", "1/2 cup butter, melted", "6-8 red potatoes", "12 oz green beans (fresh or frozen)"]}
    its(:instructions) { should == "Cut potatoes into bite size pieces and place into 1/3 of a 9×13 baking dish.\nDepending on size of chicken, you may want to slice them into smaller pieces. I had 2 large chicken breasts that were rather thick. So I sliced them in half down the middle to make them thinner (so much easier than pounding them out), then I cut them again in half to make two smaller pieces from each half. So each large breast makes four smaller pieces. Place the chicken in the middle third of your baking dish.\nFill up the remaining third of your baking dish with the green beans.\nSprinkle entire dish evenly with Ranch Seasoning. Pour melted butter over all the food.\nCover with foil and bake at 350 degrees for 1 hour." }
    its(:name) { should == "Ranch Chicken with Potatoes & Green Beans" }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end