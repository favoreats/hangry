#encoding: UTF-8

describe Hangry do
  context 'amydgorin.com wild-blueberry-mini-cakes-with-vanilla-icing' do
    before(:all) do
      @html = File.read('spec/fixtures/amy_d_gorin_wild_blueberry_mini_cakes_with_vanilla_icing.html')
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:canonical_url) { should == 'https://www.amydgorin.com/wild-blueberry-mini-cakes-with-vanilla-icing/' }
    its(:image_url) { should == 'https://image.jimcdn.com/app/cms/image/transf/dimension=560x10000:format=jpg/path/sa9d22091d37ea0e7/image/iaf9beb4d8d0af8e9/version/1509033349/need-recipe-ideas-for-mini-cakes-learn-how-to-make-these-easy-wild-blueberry-mini-bundt-cakes.jpg' }
    its(:ingredients) { should == ['1 ½ cups cake flour (or all-purpose flour)', '1 teaspoon baking powder', '½ teaspoon kosher salt', '½ cup butter, melted', '½ cup + 1 Tablespoon + 2 ¼ teaspoons low-fat milk, divided', '1 ½ cups maple syrup', '2 eggs, beaten', '2 ½ teaspoons vanilla extract, divided', '1 cup plain low-fat Greek yogurt', '1 ¼ cups frozen wild blueberries', 'Spray oil', '½ cup powdered sugar'] }
    its(:instructions) { should == "Preheat oven to 350° F. In a medium mixing bowl, mix flour, baking powder, and salt.\nIn another medium mixing bowl, combine butter, maple syrup, ½ cup milk, eggs, 2 teaspoons vanilla extract, and yogurt. Add flour mixture to wet mixture, stirring gently. Fold in wild blueberries.\nCoat mini Bundt cake tray with spray oil; then spoon batter into tray, filling about three fourths of the way full. Bake for about 15 to 18 minutes, until cooked through. Meanwhile, in a small\nbowl, combine powdered sugar with remaining milk and vanilla extract. Once mini cakes have cooled in the pan, transfer to a wire rack to cool completely. Spoon icing over mini cakes once they’re\nmostly cooled. Enjoy! Makes 20 mini cakes." }
    its(:name) { should == 'Wild Blueberry Mini Cakes with Vanilla Icing' }

    context "when the selector '//*[contains(text(),'Ingredients')]/following::li[./following::*[contains(text(),'Directions')]]' is used in the parse_ingredients method" do
      before(:each) do
        @html = File.read('spec/fixtures/amy_d_gorin_tomato_and_white_bean_naan_pizza.html')
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it 'should be able to parse the ingredients' do
        expect(subject.ingredients).to eq(['1 cup canned no-salt-added white kidney beans, rinsed', '2 tsp extra-virgin olive oil', '2 tsp minced garlic', '1 piece naan bread', '2 oz fresh mozzarella, sliced', '1 plum tomato, thinly sliced', '2 tsp grated Parmesan', '1/2 tsp red pepper flakes', '1 Tbsp torn fresh sage'])
      end
    end

    context 'with no input' do
      subject { Hangry.parse('') }

      it 'should set all attributes to nil' do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end
