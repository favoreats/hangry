# encoding: UTF-8

describe Hangry do
  context "dealstomealsblog.com Perfect Soft Rainbow Bread" do
    before(:all) do
      @html = File.read("spec/fixtures/deals_to_meals.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }
    its(:author) { should == 'Shandra' }
    its(:name) { should == "Perfect Soft Rainbow Bread" }
    its(:image_url) { should == "http://dealstomealsblog.com/wp-content/uploads/2016/03/rainbow-bread2.jpg" }
    its(:ingredients) { should == ["7 cup white flour",
                                   "2/3 cup vital wheat gluten",
                                   "2 1/2 Tablespoon instant yeast",
                                   "5 cups hot water (120-130 F)",
                                   "2 Tablespoon salt",
                                   "2/3 cup oil",
                                   "2/3 cup honey or 1 c. sugar (I like honey the best!)",
                                   "2 1/2 Tablespoon bottled lemon juice",
                                   "5-5 1/2 cups more white flour",
                                   "Food coloring (can use up to 5 or 6 different colors)"
                                 ] }
    its(:instructions) { should == "Mix together the first three ingredients in your mixer with a dough hook. Add water all at once and mix for 1 minute; cover and let rest for 10 minutes (this is called sponging). Add salt, oil, honey or sugar, and lemon juice and beat for 1 minute. Add last bit of flour, 1 cup at a time, beating between each cup. Beat for about 6-10 minutes until dough pulls away from the sides of the bowl. This makes very soft dough.\nPre-heat oven for 1 minute to lukewarm and turn off. Turn dough onto oiled counter top; divide the dough into 4-6 pieces (or however many colors you would like for your bread. Add one of the balls back into the mixer with the dough hook. Place a cloth over the other balls of dough so they don't dry out. Add several drops of food coloring to the ball of dough (use one color at a time). Mix the dough until the food coloring dyes the dough to the vibrancy of the color you are looking for. Once the dough has been colored, remove the dough ball and put into a bowl. Place the second dough ball into the mixer. Add another color of food coloring to the ball of dough and continue to mix until incorporated. Continue to do this with each ball of dough until they are all colored the different colors you are looking for.\nTIP: Start with the lightest color of food coloring that you are using and then go to the darkest. We started with yellow, went to blue, then green, then purple, then red.\nOnce all of the dough is colored, you can start forming your loaves. Spray each of the bread pans. If you are wanting your loaves to look like a rainbow, you take a portion of each color and spread it out into a rectangle shape with your hands. Add a second color on top of the bottom layer and continue until you have all of the colors you want for your 'rainbow' bread. The top layer will need to be a little larger as it will need to roll up around the other colors. Roll up the dough into a a rectangle shape, the length of your bread pan. Smooth down the edges and place the dough into the greased baking pan. Continue to do this for the remaining bread loaves. Once the dough has all been rolled into the pans, let the bread rise in warm oven for 10-15 minutes, or the until dough reaches the top of the pan. Do not remove bread from the oven; turn oven to 350 F and bake for 30 minutes. Remove from pans and cool on racks. Slice the bread and be surprised at your different designs. Enjoy!!" }
    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end

    context "when NoMethodError is raised in the parse_author method" do
      before(:each) do
        @html = File.read("spec/fixtures/deals_to_meals_perfect_soft_rainbow_bread.html")
        @parsed = Hangry.parse(@html)
      end
      subject { @parsed }

      it "should set author attribute to nil" do
        expect(subject.author).to be_nil
      end
    end
  end
end
