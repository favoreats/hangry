describe Hangry do
  context "specialfoodpoint.blogspot.com cinnamon-coffee-cake-bread-ingredients" do
    before(:all) do
      @html = File.read("spec/fixtures/special_food_point_cinnamon_coffee_cake_bread_ingredients.html")
      @parsed = Hangry.parse(@html)
    end
    subject { @parsed }

    its(:name) { should == "Cinnamon Coffee Cake Bread" }
    its(:canonical_url) { should == "http://specialfoodpoint.blogspot.com/2014/03/cinnamon-coffee-cake-bread-ingredients.html"}
    its(:image_url) { should == "http://2.bp.blogspot.com/-ngEMBwHrrAQ/UxMO_87qjgI/AAAAAAAABKQ/nJUtkBVHCGc/s1600/Cinnamon+Coffee+Cake+Bread.JPG" }
    its(:ingredients) { should == ["1 teaspoon baking powder", "1 cup brown sugar", "1 teaspoon baking soda", "1/2 teaspoon salt", "1/4 cup butter at room temperature", "2 cups flour", "2 large eggs", "1 teaspoon vanilla extract", "1 cup buttermilk", "2 heaping tablespoons ground cinnamon", "1 cup white sugar"] }
    its(:instructions) { should == "Preheat oven to 350 degrees and spray a 9x5 inch loaf pan with non stick spray. Mix the flour, baking soda, baking powder, and salt together in a bowl and set aside. Mix the buttermilk and vanilla in a measuring cup and set aside. Mix the brown sugar and cinnamon together in a bowl and set aside. In a bowl cream the butter and white sugar together and then add in the eggs one at a time. Add 1/3 of the flour mixture and mix to combine, then add 1/2 of the buttermilk mixture. Repeat this and end with the flour. Pour 1/3 of the batter into the prepared pan and top with 1/3 of the cinnamon mixture, repeat so that the final layer of cinnamon mixture is the topping on the bread. Bake for 50-60 minutes or until a cake tester comes out clean." }

    context "with no input" do
      subject { Hangry.parse("") }

      it "should set all attributes to nil" do
        Hangry::RECIPE_ATTRIBUTES.each do |attribute|
          expect(subject.send(attribute)).to be_nil
        end
      end
    end
  end
end